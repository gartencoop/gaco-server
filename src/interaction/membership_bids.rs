use super::membership::ScreenMembership;
use crate::GacoSession;
use serde_json::json;
use wallaby::{Output, RenderedOutput};

pub struct MembershipBids;

impl<SCREEN: wallaby::Screen<GacoSession> + ScreenMembership> Output<SCREEN, GacoSession>
    for MembershipBids
{
    fn render(
        &self,
        _interaction_name: String,
        screen: &SCREEN,
        session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            screen.membership().map(|membership| {
                membership
                    .bidding_history()
                    .iter()
                    .enumerate()
                    .map(|(index, bid)| {
                        json!({
                            "index": index,
                            "change_at": bid.change_at().to_rfc3339(),
                            "start_at": crate::common::date_string(&bid.start_at()),
                            "euros": session.format_cents_as_euros(bid.cents(), 2),
                        })
                    })
                    .collect::<Vec<_>>()
            }),
            (),
        )
    }
}

pub struct BidYears;

impl<SCREEN: wallaby::Screen<GacoSession> + ScreenMembership> Output<SCREEN, GacoSession>
    for BidYears
{
    fn render(
        &self,
        _interaction_name: String,
        screen: &SCREEN,
        session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            screen.membership().map(|membership| {
                let bidding_years = membership.bidding_years();
                bidding_years
                    .into_iter()
                    .map(|(year, year_entry)| {
                        json!({
                            "year": year,
                            "months": year_entry.months,
                            "euros": session.format_cents_as_euros(year_entry.cents, 2),
                        })
                    })
                    .collect::<Vec<_>>()
            }),
            (),
        )
    }
}
