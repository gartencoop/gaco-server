use crate::{model::vp::VP, GacoSession};
use serde_json::json;
use wallaby::*;

pub struct Vps;

impl<SCREEN: wallaby::Screen<GacoSession>> Output<SCREEN, GacoSession> for Vps {
    fn render(
        &self,
        _interaction_name: String,
        _screen: &SCREEN,
        _session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            VP::find_all()
                .into_iter()
                .map(|vp| {
                    json!({
                        "id": vp.nid(),
                        "code": vp.code(),
                    })
                })
                .collect::<Vec<_>>(),
            (),
        )
    }
}
