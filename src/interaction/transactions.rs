use crate::{common::date_string, GacoSession};
use serde_json::json;
use wallaby::*;

pub trait ScreenTransactions {
    fn transactions(&self) -> Vec<&crate::model::transaction::Transaction>;
}

pub struct Transactions;

impl<SCREEN: wallaby::Screen<GacoSession> + ScreenTransactions> Output<SCREEN, GacoSession>
    for Transactions
{
    fn render(
        &self,
        _interaction_name: String,
        screen: &SCREEN,
        _session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            screen
                .transactions()
                .iter()
                .map(|transaction| {
                    json!({
                        "id": transaction.id(),
                        "date": date_string(&transaction.date()),
                        "description": transaction.description(),
                        "cents": transaction.cents(),
                        "year": transaction.year(),
                        "membership": transaction.membership_id(),
                    })
                })
                .collect::<Vec<_>>(),
            (),
        )
    }
}

pub struct TransactionsSum;

impl<SCREEN: wallaby::Screen<GacoSession> + ScreenTransactions> Output<SCREEN, GacoSession>
    for TransactionsSum
{
    fn render(
        &self,
        _interaction_name: String,
        screen: &SCREEN,
        _session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            screen
                .transactions()
                .iter()
                .map(|transaction| transaction.cents())
                .sum::<u32>(),
            (),
        )
    }
}
