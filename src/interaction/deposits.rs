use crate::{common::date_string, GacoSession};
use serde_json::json;
use wallaby::*;

pub trait ScreenDeposits {
    fn deposits(&self) -> Vec<&crate::model::deposit::Deposit>;
}

pub struct Deposits;

impl<SCREEN: wallaby::Screen<GacoSession> + ScreenDeposits> Output<SCREEN, GacoSession>
    for Deposits
{
    fn render(
        &self,
        _interaction_name: String,
        screen: &SCREEN,
        _session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            screen
                .deposits()
                .iter()
                .map(|deposit| {
                    json!({
                        "id": deposit.id(),
                        "date": date_string(&deposit.date()),
                        "description": deposit.description(),
                        "cents": deposit.cents(),
                        "membership": deposit.membership_id(),
                        "type": deposit.deposit_type(),
                    })
                })
                .collect::<Vec<_>>(),
            (),
        )
    }
}

pub struct DepositsSum;

impl<SCREEN: wallaby::Screen<GacoSession> + ScreenDeposits> Output<SCREEN, GacoSession>
    for DepositsSum
{
    fn render(
        &self,
        _interaction_name: String,
        screen: &SCREEN,
        _session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            screen
                .deposits()
                .iter()
                .map(|deposit| deposit.cents())
                .sum::<i32>(),
            (),
        )
    }
}
