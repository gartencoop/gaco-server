use crate::{
    model::{membership::Membership, transaction::Transaction},
    GacoSession,
};
use serde::Deserialize;
use wallaby::*;

pub struct TransactionSetMembership;

#[async_trait::async_trait]
impl<SCREEN: Screen<GacoSession>> Input<SCREEN, GacoSession> for TransactionSetMembership {
    async fn action(&self, session: &mut GacoSession, payload: &ActionPayload) -> Vec<Event> {
        if !session.access_level().await.admin() {
            return vec![Event::Message(Message::error(
                session.tr("admin_bidding_error"),
            ))];
        }

        #[derive(Deserialize)]
        struct Payload {
            #[serde(default)]
            transaction_ids: Vec<String>,
            membership_id: u32,
        }

        match payload.extra_fields::<Payload>() {
            Some(payload) => {
                let mut transactions = vec![];
                for transaction_id in payload.transaction_ids.into_iter() {
                    let Some(transaction) = Transaction::find_by_id(&transaction_id).await else {
                        return vec![Event::Message(Message::error(format!(
                            "Could not find transaction \"{}\"",
                            transaction_id
                        )))];
                    };
                    transactions.push(transaction);
                }

                if transactions.is_empty() {
                    return vec![Event::Message(Message::error(
                        "Could not find any transactions".to_string(),
                    ))];
                }

                let Some(membership) = Membership::find_for_id(payload.membership_id) else {
                    return vec![Event::Message(Message::error(format!(
                        "Could not find membership \"{}\"",
                        payload.membership_id
                    )))];
                };

                let date = transactions
                    .first()
                    .map(|transaction| transaction.date().format("%d.%m.%Y").to_string())
                    .unwrap_or_default();
                let membership_id = payload.membership_id;
                tokio::spawn(async move {
                    membership.send_email("Zuordnung Überweisungen", format!("Liebes Gaco-Mitglied,\n\ndu bekommst diese Email, da die Beitrags-Erfassungs-Seite deine Überweisung vom {date} nicht automatisch zuordnen konnte.\n\nBitte ändere den Verwendungszweck im Dauerauftrag oder in der Überweisung auf “GaCo{membership_id}”.\nDas spart uns jeden Monat immens viel Arbeit.\n\nDanke und solidarische Grüße, dein Supportteam")).await.ok();
                });

                for transaction in transactions.iter_mut() {
                    transaction.set_membership_id(payload.membership_id);
                }

                match crate::couchdb::save_batch_owned(transactions).await {
                    Ok(_) => vec![Event::Message(Message::success(
                        "Die Beiträge wurden erfolgreich zugewiesen".to_string(),
                    ))],
                    Err(err) => vec![Event::Message(Message::error(format!(
                        "Could not save transactions: {err:?}"
                    )))],
                }
            }
            _ => vec![Event::Message(Message::error(
                "Data is not valid".to_string(),
            ))],
        }
    }
}
