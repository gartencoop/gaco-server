use super::{deposits::ScreenDeposits, transactions::ScreenTransactions};
use crate::GacoSession;
use serde_json::json;
use wallaby::*;

pub trait ScreenMemberships {
    fn memberships(&self) -> Vec<&crate::model::membership::Membership>;
}

pub struct Memberships;

impl<
        SCREEN: wallaby::Screen<GacoSession> + ScreenMemberships + ScreenTransactions + ScreenDeposits,
    > Output<SCREEN, GacoSession> for Memberships
{
    fn render(
        &self,
        _interaction_name: String,
        screen: &SCREEN,
        _session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(screen.memberships().iter().map(|membership| {
            let vp = membership.vp();
            json!({
                "start_at": crate::common::date_string(&membership.start_at()),
                "end_at": membership.end_at().as_ref().map(crate::common::date_string).unwrap_or_default(),
                "active_months": membership.active_months(),

                "surname": membership.surname().unwrap_or_default(),
                "name": membership.name().unwrap_or_default(),
                "street": membership.street().unwrap_or_default(),
                "zip": membership.zip().unwrap_or_default(),
                "city": membership.city().unwrap_or_default(),

                "has_current_bid": membership.bid_for_current_bidding().is_some(),
                "bid_cents_for_current_year": membership.bid_cents_for_current_year(),
                "subscription": membership.subscription(),
                "vp_code": vp.as_ref().map(|vp|vp.code().to_owned()),
                "vp_title": vp.as_ref().map(|vp|vp.title().to_owned()),
                "vp_path": vp.map(|vp|vp.path().to_owned()),
                "shares": membership.shares(),

                "main_user_name": membership.main_user().map(|user|user.name().to_owned()),
                "user_names": membership.users().into_iter().map(|user|user.name().to_owned()).collect::<Vec<String>>(),
                "extra_user_names": membership.users().into_iter().filter(|user|Some(user) != membership.main_user().as_ref()).map(|user|user.name().to_owned()).collect::<Vec<String>>(),

                "notes": membership.notes(),
                "finance": membership.finance(screen.transactions()),
                "deposit": screen.deposits()
                    .into_iter()
                    .filter(|deposit| deposit.membership_id() == Some(membership.id()))
                    .map(|deposit|deposit.cents())
                    .sum::<i32>(),
                "id": membership.id(),
                "created_at": membership.created_at().to_rfc3339(),
            })
        }).collect::<Vec<_>>(), ())
    }
}
