use super::membership::ScreenMembership;
use crate::GacoSession;
use serde_json::json;
use wallaby::*;

pub struct MembershipVps;

impl<SCREEN: wallaby::Screen<GacoSession> + ScreenMembership> Output<SCREEN, GacoSession>
    for MembershipVps
{
    fn render(
        &self,
        _interaction_name: String,
        screen: &SCREEN,
        _session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            screen.membership().map(|membership| {
                membership
                    .vp_membership_history()
                    .iter()
                    .enumerate()
                    .map(|(index, vp_membership)| {
                        json!({
                            "index": index,
                            "change_at": vp_membership.change_at().to_rfc3339(),
                            "state": vp_membership.state(),
                        })
                    })
                    .collect::<Vec<_>>()
            }),
            (),
        )
    }
}
