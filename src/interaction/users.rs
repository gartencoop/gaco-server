use crate::GacoSession;
use serde_json::json;
use wallaby::*;

pub trait ScreenUsers {
    fn users(&self, interaction_name: &str) -> Vec<&crate::model::user::User>;
}

pub struct Users;

impl<SCREEN> Output<SCREEN, GacoSession> for Users
where
    SCREEN: wallaby::Screen<GacoSession> + ScreenUsers,
{
    fn render(
        &self,
        interaction_name: String,
        screen: &SCREEN,
        session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(screen.users(&interaction_name).iter().map(|user| {
            let membership = user.membership();
            json!({
                "id": user.id(),
                "created_at": user.created_at().to_rfc3339(),
                "username": user.username(),
                "email": user.email(),
                "name": user.name(),
                "phone": user.phone(),
                "language": user.language(),
                "access_level": user.access_level().to_string(session),
                "membership_id": membership.as_ref().map(|membership|membership.id()),
                "membership_active": membership.as_ref().map(|membership|membership.is_active()),
                "is_main_user": membership.map(|membership|membership.main_user_id() == Some(user.id())).unwrap_or_default()
            })
        }).collect::<Vec<_>>(), ())
    }
}
