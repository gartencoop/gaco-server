use crate::GacoSession;
use serde_json::json;
use wallaby::*;

use super::transactions::ScreenTransactions;

pub trait ScreenMembership {
    fn user(&self) -> Option<&crate::model::user::User>;
    fn membership(&self) -> Option<&crate::model::membership::Membership>;
    fn membership_invitation_url(&self) -> Option<&str>;
    fn membership_invitation_url_valid_until(&self) -> Option<&str>;
}

pub struct Membership;

impl<SCREEN: wallaby::Screen<GacoSession> + ScreenMembership + ScreenTransactions>
    Output<SCREEN, GacoSession> for Membership
{
    fn render(
        &self,
        _interaction_name: String,
        screen: &SCREEN,
        _session: &GacoSession,
    ) -> RenderedOutput {
        let value = if let Some(membership) = screen.membership() {
            let manager = screen
                .user()
                .map(|user| user.access_level().manager())
                .unwrap_or_default();
            let vp = membership.vp();
            let main_user = membership.main_user();

            json!({
                "start_at": crate::common::date_string(&membership.start_at()),
                "end_at": membership.end_at().as_ref().map(crate::common::date_string),
                "active_months": membership.active_months(),

                "surname": membership.surname().unwrap_or_default(),
                "name": membership.name().unwrap_or_default(),
                "street": membership.street().unwrap_or_default(),
                "zip": membership.zip().unwrap_or_default(),
                "city": membership.city().unwrap_or_default(),

                "has_current_bid": membership.bid_for_current_bidding().is_some(),
                "bid_cents_for_current_year": membership.bid_cents_for_current_year(),
                "subscription": membership.subscription(),
                "vp_code": vp.as_ref().map(|vp|vp.code().to_owned()),
                "vp_title": vp.as_ref().map(|vp|vp.title().to_owned()),
                "vp_path": vp.map(|vp|vp.path().to_owned()),
                "shares": membership.shares(),

                "main_user_name": main_user.as_ref().map(|user|user.name()),
                "user_names": membership.users().into_iter().map(|user|user.name().to_owned()).collect::<Vec<String>>(),
                "extra_user_names": membership.users().into_iter().filter(|user|Some(user) != membership.main_user().as_ref()).map(|user|user.name().to_owned()).collect::<Vec<String>>(),
                "invitation_url": screen.membership_invitation_url(),
                "invitation_url_valid_until": screen.membership_invitation_url_valid_until(),

                // Only visible to managers:
                "deposit": 0,
                "finance": Some(membership.finance(screen.transactions())).filter(|_|manager),
                "id": Some(membership.id()).filter(|_|manager),
                "created_at": Some(membership.created_at().to_rfc3339()).filter(|_|manager).unwrap_or_default(),
                "notes": membership.notes().filter(|_|manager),
                "main_user_phone": main_user.as_ref().filter(|_|manager).and_then(|user|user.phone()),
                "main_user_email": main_user.as_ref().filter(|_|manager).map(|user|user.email()),
            })
        } else {
            json!({})
        };

        RenderedOutput::new(value, ())
    }
}
