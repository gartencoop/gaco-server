use super::common::Common;
use crate::GacoSession;
use serde_json::{json, Value};
use wallaby::*;

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession", hooks(load_data))]
pub struct VPs {
    #[screen]
    common: Common,

    #[interaction(ty = "output")]
    vps: VPList,

    #[interaction(ty = "output")]
    texts: Texts,

    data: Option<Vec<crate::model::vp::VP>>,
}

#[async_trait::async_trait]
impl LoadData<GacoSession> for VPs {
    async fn load_data(&mut self, _session: &GacoSession) -> Result<(), Error> {
        self.data = Some(crate::model::vp::VP::find_all());
        Ok(())
    }
}

struct VPList;
impl Output<VPs, GacoSession> for VPList {
    fn render(
        &self,
        _interaction_name: String,
        screen: &VPs,
        _session: &GacoSession,
    ) -> RenderedOutput {
        let vps = screen.data.as_ref().map(|vps| {
            vps.iter()
                .map(|vp| {
                    json!({
                        "path": vp.path(),
                        "title": vp.title(),
                        "code": vp.code(),
                        "members_count": vp.members().len(),
                        "waiting_members_count": vp.waiting_members().len(),
                    })
                })
                .collect::<Vec<Value>>()
        });

        RenderedOutput::new(
            json!({
                "vps": vps,
            }),
            (),
        )
    }
}

struct Texts;

impl Output<VPs, GacoSession> for Texts {
    fn render(
        &self,
        _interaction_name: String,
        _screen: &VPs,
        session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            json!({
                "title": session.tr("vps_title"),
                "name": session.tr("vp_name"),
                "code": session.tr("vp_code"),
                "members": session.tr("vp_members"),
                "waiting_members": session.tr("vp_waiting_members"),
            }),
            (),
        )
    }
}
