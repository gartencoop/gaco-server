use super::common::Common;
use crate::GacoSession;
use wallaby::*;

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession", hooks(load_data))]
pub struct EditEvent {
    #[screen]
    common: Common,
}

#[async_trait::async_trait]
impl LoadData<GacoSession> for EditEvent {
    async fn load_data(&mut self, _session: &GacoSession) -> Result<(), Error> {
        //TODO

        Ok(())
    }
}
