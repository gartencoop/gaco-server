use super::common::Common;
use crate::GacoSession;
use wallaby::*;

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession", ui = "HtmlText")]
pub struct Privacy {
    #[screen]
    common: Common,

    #[interaction(ty = "output")]
    title: Title,

    #[interaction(ty = "output")]
    text: Text,
}

struct Title;

impl Output<Privacy, GacoSession> for Title {
    fn render(
        &self,
        _interaction_name: String,
        _screen: &Privacy,
        session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(session.tr("privacy_title"), ())
    }
}

struct Text;

impl Output<Privacy, GacoSession> for Text {
    fn render(
        &self,
        _interaction_name: String,
        _screen: &Privacy,
        session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(session.tr("privacy_text"), ())
    }
}
