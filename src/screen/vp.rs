use super::common::Common;
use crate::{model::user::User, routing::Routing, GacoSession};
use serde_json::{json, Value};
use wallaby::*;

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession", hooks(load_data))]
pub struct VP {
    #[screen]
    common: Common,

    #[interaction(ty = "output")]
    vp: VPOutput,

    #[interaction(ty = "output")]
    texts: Texts,

    user: Option<User>,
    data: Option<crate::model::vp::VP>,
}

#[async_trait::async_trait]
impl LoadData<GacoSession> for VP {
    async fn load_data(&mut self, session: &GacoSession) -> Result<(), Error> {
        self.user = session.user().await;
        self.data = match session.routing() {
            Routing::VP(id) => crate::model::vp::VP::find_for_nid(*id),
            _ => None,
        };

        Ok(())
    }
}

struct Texts;

impl Output<VP, GacoSession> for Texts {
    fn render(
        &self,
        _interaction_name: String,
        screen: &VP,
        session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            json!({
                "title": session.tr_args("vp_title", &std::iter::once(("title".into(), screen.data.as_ref().map(|vp|vp.title().to_owned()).unwrap_or_default().into())).collect()),
                "opening_hours": session.tr("vp_opening_hours"),
                "code": session.tr("vp_code"),
                "members": session.tr("vp_members"),
                "waiting_members": session.tr("vp_waiting_members"),
                "contact": session.tr("vp_contact"),
                "case_storage": session.tr("vp_case_storage"),
                "created_at": session.tr("vp_created_at"),
                "status": session.tr("vp_status"),
                "space": session.tr("vp_space"),
                "infos": session.tr("vp_infos"),
            }),
            (),
        )
    }
}

struct VPOutput;

impl Output<VP, GacoSession> for VPOutput {
    fn render(
        &self,
        _interaction_name: String,
        screen: &VP,
        session: &GacoSession,
    ) -> RenderedOutput {
        let manager = screen
            .user
            .as_ref()
            .map(|user| user.access_level().manager())
            .unwrap_or_default();

        let value = if let Some(vp) = screen.data.as_ref() {
            let members = vp.members();

            json!({
                "code": vp.code().to_owned(),
                "title": vp.title().to_owned(),
                "created_at": vp.created_at().to_rfc3339(),
                "body": vp.body(),
                "status": vp.status().tr(session),
                "contact": vp.contact().to_owned(),
                "case_storage": vp.case_storage().to_owned(),
                "opening": vp.opening().to_owned(),
                "location": vp.location().to_owned(),
                "members_count": members.len(),
                "waiting_members_count": vp.waiting_members().len(),
                "members": members.iter().filter(|_|manager).map(|membership|membership.full_name()).collect::<Vec<String>>(),
                "images": vp.image_paths(),
                "files": vp.files(),
            })
        } else {
            Value::Null
        };

        RenderedOutput::new(value, ())
    }
}
