use super::AdminMembership;
use crate::{
    model::membership::{Membership, VpMembershipState},
    routing::{Page, Routing},
    GacoSession,
};
use serde::Deserialize;
use wallaby::{ActionPayload, Event, Input, Message};

pub struct AddVpMembership;
#[async_trait::async_trait]
impl Input<AdminMembership, GacoSession> for AddVpMembership {
    async fn action(&self, session: &mut GacoSession, payload: &ActionPayload) -> Vec<Event> {
        if !session.access_level().await.admin() {
            return vec![Event::Message(Message::error(
                "You are not allowed to add a vp membership entry".to_string(),
            ))];
        }

        #[derive(Deserialize)]
        struct Payload {
            value: VpMembershipState,
        }

        let Some(state): Option<Payload> = payload.extra_fields() else {
            return vec![Event::Message(Message::error(
                "Data error, expected VpMembershipState".to_string(),
            ))];
        };

        let membership = match session.routing() {
            Routing::Page(Page::AdminMembership { member_id }) => {
                Membership::find_for_id(*member_id)
            }
            _ => None,
        };

        let Some(mut membership) = membership else {
            return vec![Event::Message(Message::error(
                "Could not find membership".to_string(),
            ))];
        };

        membership.add_vp_membership(state.value.into());
        if let Err(err) = membership.save().await {
            return vec![wallaby::Event::Message(wallaby::Message::error(format!(
                "Could not add vp membership: {err:?}"
            )))];
        }
        vec![Event::Message(Message::success(
            "Der Eintrag wurde hinzugefügt".to_string(),
        ))]
    }
}

pub struct RemoveVpMembership;
#[async_trait::async_trait]
impl Input<AdminMembership, GacoSession> for RemoveVpMembership {
    async fn action(&self, session: &mut GacoSession, payload: &ActionPayload) -> Vec<Event> {
        if !session.access_level().await.admin() {
            return vec![Event::Message(Message::error(
                "You are not allowed to remove a vp membership entry".to_string(),
            ))];
        }

        #[derive(Deserialize)]
        struct Payload {
            index: usize,
        }

        let Some(payload) = payload.extra_fields::<Payload>() else {
            return vec![Event::Message(Message::error("Data error".to_string()))];
        };

        let membership = match session.routing() {
            Routing::Page(Page::AdminMembership { member_id }) => {
                Membership::find_for_id(*member_id)
            }
            _ => None,
        };

        let Some(mut membership) = membership else {
            return vec![Event::Message(Message::error(
                "Could not find membership".to_string(),
            ))];
        };

        if !membership.remove_vp_membership(payload.index) {
            return vec![Event::Message(Message::error(
                "Could not remove vp membership".to_string(),
            ))];
        }
        if let Err(err) = membership.save().await {
            return vec![wallaby::Event::Message(wallaby::Message::error(format!(
                "Could not save membership: {err:?}"
            )))];
        }
        vec![Event::Message(Message::success(
            "Der Eintrag wurde entfernt".to_string(),
        ))]
    }
}
