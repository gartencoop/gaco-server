use super::{
    AdminMembership, Deserialize, GacoSession, Membership, NaiveDate, Page, Routing, Shares,
    Subscription,
};
use wallaby::{ActionPayload, Event, Input, Message};

pub struct SaveMembership;

#[async_trait::async_trait]
impl Input<AdminMembership, GacoSession> for SaveMembership {
    async fn action(&self, session: &mut GacoSession, payload: &ActionPayload) -> Vec<Event> {
        if !session.access_level().await.admin() {
            return vec![Event::Message(Message::error(
                session.tr("admin_bidding_error"),
            ))];
        }

        let membership_id = match session.routing() {
            Routing::Page(Page::AdminMembership { member_id }) => Some(*member_id),
            _ => None,
        };
        let membership = membership_id.and_then(Membership::find_for_id);
        let Some(mut membership) = membership else {
            return vec![Event::Message(Message::error(format!(
                "Could not find membership: {membership_id:?}"
            )))];
        };

        #[derive(Deserialize, Debug)]
        struct Payload {
            #[serde(default)]
            name: Option<String>,
            #[serde(default)]
            surname: Option<String>,
            #[serde(default)]
            city: Option<String>,
            #[serde(default)]
            street: Option<String>,
            #[serde(default, deserialize_with = "crate::common::deserialize_optional_date")]
            start_at: Option<NaiveDate>,
            #[serde(default, deserialize_with = "crate::common::deserialize_optional_date")]
            end_at: Option<NaiveDate>,
            #[serde(default)]
            shares: Option<Shares>,
            #[serde(default)]
            subscription: Option<Subscription>,
            #[serde(default)]
            notes: Option<String>,
        }

        match payload.extra_fields::<Payload>() {
            Some(payload) => {
                membership.set_name(payload.name);
                membership.set_surname(payload.surname);
                membership.set_city(payload.city);
                membership.set_street(payload.street);
                membership.set_end_at(payload.end_at);
                membership.set_notes(payload.notes);

                if let Some(start_at) = payload.start_at {
                    membership.set_start_at(start_at);
                }
                if let Some(shares) = payload.shares {
                    membership.set_shares(shares);
                }
                if let Some(subscription) = payload.subscription {
                    membership.set_subscription(subscription);
                }

                match membership.save().await {
                    Ok(_) => vec![Event::Message(Message::success(
                        "Saved membership".to_string(),
                    ))],
                    Err(err) => vec![Event::Message(Message::error(format!(
                        "Could not save membership: {err:?}"
                    )))],
                }
            }
            _ => vec![Event::Message(Message::error(
                "Data is not valid".to_string(),
            ))],
        }
    }
}
