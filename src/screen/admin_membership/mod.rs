mod bidding;
mod deposit;
mod remove;
mod save;
mod subscription;
mod user;
mod vp_membership;

use self::deposit::{CreateDepositDonation, DeleteDeposit, SetDepositDescription};

use super::common::Common;
use crate::{
    interaction::{
        deposits::{Deposits, ScreenDeposits},
        membership::ScreenMembership,
        membership_bids::{BidYears, MembershipBids},
        membership_vps::MembershipVps,
        memberships::{Memberships, ScreenMemberships},
        transaction_set_membership::TransactionSetMembership,
        transactions::{ScreenTransactions, Transactions},
        users::{ScreenUsers, Users},
        vps::Vps,
    },
    model::{
        deposit::Deposit,
        membership::{Membership, Shares, Subscription},
        transaction::Transaction,
        user::User,
    },
    routing::{Page, Routing},
    GacoSession,
};
use bidding::{AddBidding, RemoveBidding};
use chrono::NaiveDate;
use remove::RemoveMembership;
use save::SaveMembership;
use serde::Deserialize;
use subscription::SetSubscription;
use user::{AddUser, RemoveUser, SetMainUser};
use vp_membership::{AddVpMembership, RemoveVpMembership};
use wallaby::{Error, LoadData};

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession", hooks(load_data))]
pub struct AdminMembership {
    #[screen]
    common: Common,

    #[interaction(ty = "output")]
    memberships: Memberships,

    #[interaction(ty = "output")]
    membership: crate::interaction::membership::Membership,

    #[interaction(ty = "output")]
    transactions: Transactions,

    #[interaction(ty = "output")]
    deposits: Deposits,

    #[interaction(ty = "output")]
    membership_users: Users,

    #[interaction(ty = "output")]
    bids: MembershipBids,

    #[interaction(ty = "output")]
    membership_vps: MembershipVps,

    #[interaction(ty = "output")]
    bid_years: BidYears,

    #[interaction(ty = "output")]
    all_users: Users,

    #[interaction(ty = "output")]
    all_vps: Vps,

    #[interaction(ty = "input")]
    save_membership: SaveMembership,

    #[interaction(ty = "input")]
    remove_membership: RemoveMembership,

    #[interaction(ty = "input")]
    transaction_set_membership: TransactionSetMembership,

    #[interaction(ty = "input")]
    add_user: AddUser,

    #[interaction(ty = "input")]
    remove_user: RemoveUser,

    #[interaction(ty = "input")]
    set_main_user: SetMainUser,

    #[interaction(ty = "input")]
    add_bidding: AddBidding,

    #[interaction(ty = "input")]
    remove_bidding: RemoveBidding,

    #[interaction(ty = "input")]
    add_vp_membership: AddVpMembership,

    #[interaction(ty = "input")]
    remove_vp_membership: RemoveVpMembership,

    #[interaction(ty = "input")]
    set_subscription: SetSubscription,

    #[interaction(ty = "input")]
    create_deposit_donation: CreateDepositDonation,

    #[interaction(ty = "input")]
    set_deposit_description: SetDepositDescription,

    #[interaction(ty = "input")]
    delete_deposit: DeleteDeposit,

    user: Option<User>,
    membership_id: Option<u32>,
    memberships_data: Option<Vec<Membership>>,
    membership_data: Option<crate::model::membership::Membership>,
    membership_invitation_url: Option<String>,
    membership_invitation_url_valid_until: Option<String>,
    transactions_data: Option<Vec<Transaction>>,
    deposits_data: Option<Vec<Deposit>>,
    membership_users_data: Option<Vec<User>>,
    all_users_data: Option<Vec<User>>,
}

#[async_trait::async_trait]
impl LoadData<GacoSession> for AdminMembership {
    async fn load_data(&mut self, session: &GacoSession) -> Result<(), Error> {
        self.user = session.user().await;

        if let Some(user) = self.user.as_ref() {
            if !user.access_level().admin() {
                return Err(wallaby::Error::ScreenNotFound(
                    "AdminMembership".to_string(),
                ));
            }
        }

        if let Routing::Page(Page::AdminMembership { member_id }) = session.routing() {
            self.membership_id = Some(*member_id);
            self.memberships_data = Some(Membership::find_all());
            self.membership_data = crate::model::membership::Membership::find_for_id(*member_id);
            self.transactions_data = Some(Transaction::find_for_membership_id(*member_id).await);
            self.deposits_data = Some(Deposit::find_for_membership_id(*member_id).await);
            self.membership_users_data = self
                .membership_data
                .as_ref()
                .map(|membership| membership.users());
            if let Some(membership) = self.membership_data.as_ref() {
                self.membership_invitation_url = membership.invitation_url().await.ok();
                self.membership_invitation_url_valid_until =
                    membership.invitation_valid_until().await.ok().map(|date| {
                        session.tr_args(
                            "invitation_validity",
                            &std::iter::once(("date".into(), date.into())).collect(),
                        )
                    });
            }
        };
        self.all_users_data = Some(User::find_all());

        Ok(())
    }
}

impl ScreenTransactions for AdminMembership {
    fn transactions(&self) -> Vec<&Transaction> {
        self.transactions_data
            .as_ref()
            .map(|transactions| transactions.iter().collect())
            .unwrap_or_default()
    }
}

impl ScreenDeposits for AdminMembership {
    fn deposits(&self) -> Vec<&crate::model::deposit::Deposit> {
        self.deposits_data
            .as_ref()
            .map(|deposits| deposits.iter().collect())
            .unwrap_or_default()
    }
}

impl ScreenUsers for AdminMembership {
    fn users(&self, interaction_name: &str) -> Vec<&User> {
        match interaction_name {
            "membership_users" => self.membership_users_data.as_ref(),
            "all_users" => self.all_users_data.as_ref(),
            _ => None,
        }
        .map(|users| users.iter().collect())
        .unwrap_or_default()
    }
}

impl ScreenMembership for AdminMembership {
    fn membership(&self) -> Option<&crate::model::membership::Membership> {
        self.membership_data.as_ref()
    }

    fn user(&self) -> Option<&crate::model::user::User> {
        self.user.as_ref()
    }

    fn membership_invitation_url(&self) -> Option<&str> {
        self.membership_invitation_url.as_deref()
    }

    fn membership_invitation_url_valid_until(&self) -> Option<&str> {
        self.membership_invitation_url_valid_until.as_deref()
    }
}

impl ScreenMemberships for AdminMembership {
    fn memberships(&self) -> Vec<&Membership> {
        self.memberships_data
            .as_ref()
            .map(|memberships| memberships.iter().collect())
            .unwrap_or_default()
    }
}
