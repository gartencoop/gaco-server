use super::{AdminMembership, GacoSession, Membership, Page, Routing};
use crate::change_path::change_path_event;
use wallaby::{ActionPayload, Event, Input, Message};

pub struct RemoveMembership;

#[async_trait::async_trait]
impl Input<AdminMembership, GacoSession> for RemoveMembership {
    async fn action(&self, session: &mut GacoSession, _payload: &ActionPayload) -> Vec<Event> {
        if !session.access_level().await.admin() {
            return vec![Event::Message(Message::error(
                session.tr("admin_bidding_error"),
            ))];
        }

        let membership_id = match session.routing() {
            Routing::Page(Page::AdminMembership { member_id }) => Some(*member_id),
            _ => None,
        };
        let membership = membership_id.and_then(Membership::find_for_id);
        let Some(membership) = membership else {
            return vec![Event::Message(Message::error(format!(
                "Could not find membership: {membership_id:?}"
            )))];
        };

        match membership.remove().await {
            Ok(_) => {
                session.set_routing(Routing::Page(Page::AdminMemberships));
                vec![
                    change_path_event("/admin_memberships"),
                    Event::Message(Message::success("Removed membership".to_string())),
                ]
            }
            Err(err) => vec![Event::Message(Message::error(format!(
                "Could not remove membership: {err:?}"
            )))],
        }
    }
}
