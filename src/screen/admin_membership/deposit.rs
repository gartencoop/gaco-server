use super::AdminMembership;
use crate::{model::deposit::Deposit, routing::Page, GacoSession};
use serde::Deserialize;
use wallaby::{ActionPayload, Event, Input, Message};

pub struct CreateDepositDonation;
#[async_trait::async_trait]
impl Input<AdminMembership, GacoSession> for CreateDepositDonation {
    async fn action(&self, session: &mut GacoSession, payload: &ActionPayload) -> Vec<Event> {
        if !session.access_level().await.admin() {
            return vec![Event::Message(Message::error(
                session.tr("admin_bidding_error"),
            ))];
        }
        let membership_id = match session.routing() {
            crate::routing::Routing::Page(Page::AdminMembership { member_id }) => member_id,
            _ => {
                return vec![Event::Message(Message::error(
                    "Trying to create deposit on wrong screen".to_string(),
                ))]
            }
        };

        #[derive(Deserialize)]
        struct Payload {
            cents: u32,
            description: String,
        }

        let Some(payload): Option<Payload> = payload.extra_fields() else {
            return vec![Event::Message(Message::error("Data error!".to_string()))];
        };

        let mut deposit =
            Deposit::create_donation(payload.cents, *membership_id, payload.description);

        if let Err(err) = deposit.save().await {
            return vec![wallaby::Event::Message(wallaby::Message::error(format!(
                "Could not save deposit: {err:?}"
            )))];
        }
        vec![Event::Message(Message::success(
            "Die Spende wurde erfolgreich angelegt.".to_string(),
        ))]
    }
}

pub struct SetDepositDescription;
#[async_trait::async_trait]
impl Input<AdminMembership, GacoSession> for SetDepositDescription {
    async fn action(&self, session: &mut GacoSession, payload: &ActionPayload) -> Vec<Event> {
        if !session.access_level().await.admin() {
            return vec![Event::Message(Message::error(
                session.tr("admin_bidding_error"),
            ))];
        }

        #[derive(Deserialize)]
        struct Payload {
            id: String,
            description: String,
        }

        let Some(payload): Option<Payload> = payload.extra_fields() else {
            return vec![Event::Message(Message::error("Data error!".to_string()))];
        };

        let Some(mut deposit) = Deposit::find_by_id(&payload.id).await else {
            return vec![wallaby::Event::Message(wallaby::Message::error(format!(
                "Could not find deposit: {}",
                payload.id
            )))];
        };
        deposit.set_description(payload.description);

        if let Err(err) = deposit.save().await {
            return vec![wallaby::Event::Message(wallaby::Message::error(format!(
                "Could not save deposit: {err:?}"
            )))];
        }
        vec![Event::Message(Message::success(
            "Die Änderung der Einlage wurde erfolgreich abgespeichert.".to_string(),
        ))]
    }
}

pub struct DeleteDeposit;
#[async_trait::async_trait]
impl Input<AdminMembership, GacoSession> for DeleteDeposit {
    async fn action(&self, session: &mut GacoSession, payload: &ActionPayload) -> Vec<Event> {
        if !session.access_level().await.admin() {
            return vec![Event::Message(Message::error(
                session.tr("admin_bidding_error"),
            ))];
        }

        #[derive(Deserialize)]
        struct Payload {
            id: String,
        }

        let Some(payload): Option<Payload> = payload.extra_fields() else {
            return vec![Event::Message(Message::error("Data error!".to_string()))];
        };

        let Some(deposit) = Deposit::find_by_id(&payload.id).await else {
            return vec![wallaby::Event::Message(wallaby::Message::error(format!(
                "Could not find deposit: {}",
                payload.id
            )))];
        };

        if let Err(err) = deposit.remove().await {
            return vec![wallaby::Event::Message(wallaby::Message::error(format!(
                "Could not delete deposit: {err:?}"
            )))];
        }
        vec![Event::Message(Message::success(
            "Der Eintrag wurde erfolgreich gelöscht.".to_string(),
        ))]
    }
}
