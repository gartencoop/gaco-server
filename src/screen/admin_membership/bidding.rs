use super::AdminMembership;
use crate::{
    model::membership::{Bid, Membership},
    routing::{Page, Routing},
    GacoSession,
};
use serde::Deserialize;
use wallaby::{ActionPayload, Event, Input, Message};

pub struct AddBidding;
#[async_trait::async_trait]
impl Input<AdminMembership, GacoSession> for AddBidding {
    async fn action(&self, session: &mut GacoSession, payload: &ActionPayload) -> Vec<Event> {
        if !session.access_level().await.admin() {
            return vec![Event::Message(Message::error(
                session.tr("admin_bidding_error"),
            ))];
        }

        #[derive(Deserialize)]
        struct Payload {
            cents: u32,
            start_at: String,
        }

        let Some(payload): Option<Payload> = payload.extra_fields() else {
            return vec![Event::Message(Message::error(
                "Es müssen das Gebotfeld und das Startfeld ausgefüllt sein!".to_string(),
            ))];
        };

        let membership = match session.routing() {
            Routing::Page(Page::AdminMembership { member_id }) => {
                Membership::find_for_id(*member_id)
            }
            _ => None,
        };

        let Some(mut membership) = membership else {
            return vec![Event::Message(Message::error(
                session.tr("admin_bidding_error"),
            ))];
        };

        let Some(start_at) = crate::common::parse_date(&payload.start_at) else {
            return vec![Event::Message(Message::error(
                "Could not parse date".to_string(),
            ))];
        };

        membership.add_bid(Bid::new(payload.cents, start_at));
        if let Err(err) = membership.save().await {
            return vec![wallaby::Event::Message(wallaby::Message::error(format!(
                "Could not save bidding: {err:?}"
            )))];
        }
        vec![Event::Message(Message::success(
            session.tr("admin_bidding_success"),
        ))]
    }
}

pub struct RemoveBidding;
#[async_trait::async_trait]
impl Input<AdminMembership, GacoSession> for RemoveBidding {
    async fn action(&self, session: &mut GacoSession, payload: &ActionPayload) -> Vec<Event> {
        if !session.access_level().await.admin() {
            return vec![Event::Message(Message::error(
                session.tr("admin_bidding_error"),
            ))];
        }

        #[derive(Deserialize)]
        struct Payload {
            index: usize,
        }

        let Some(payload) = payload.extra_fields::<Payload>() else {
            return vec![Event::Message(Message::error("Data error".to_string()))];
        };

        let membership = match session.routing() {
            Routing::Page(Page::AdminMembership { member_id }) => Some(*member_id),
            _ => None,
        }
        .and_then(Membership::find_for_id);

        if let Some(mut membership) = membership {
            if !membership.remove_bid(payload.index) {
                return vec![Event::Message(Message::error(
                    session.tr("admin_bidding_error"),
                ))];
            }
            if let Err(err) = membership.save().await {
                return vec![wallaby::Event::Message(wallaby::Message::error(format!(
                    "Could not save membership: {err:?}"
                )))];
            }
            vec![Event::Message(Message::success(
                session.tr("admin_bidding_remove_success"),
            ))]
        } else {
            vec![Event::Message(Message::error(
                session.tr("admin_bidding_error"),
            ))]
        }
    }
}
