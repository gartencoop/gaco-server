use super::{AdminMembership, Deserialize, GacoSession, Membership, Subscription};
use wallaby::{ActionPayload, Event, Input, Message};

pub struct SetSubscription;
#[async_trait::async_trait]
impl Input<AdminMembership, GacoSession> for SetSubscription {
    async fn action(&self, session: &mut GacoSession, payload: &ActionPayload) -> Vec<Event> {
        if !session.access_level().await.admin() {
            return vec![Event::Message(Message::error(
                "You are not allowed to set the subscription of a membership".to_string(),
            ))];
        }

        #[derive(Deserialize)]
        struct Payload {
            membership_id: u32,
            subscription: Subscription,
        }

        let message = match payload.extra_fields::<Payload>() {
            Some(payload) => match Membership::find_for_id(payload.membership_id) {
                Some(mut membership) => {
                    membership.set_subscription(payload.subscription);
                    match membership.save().await {
                        Ok(_) => Message::success(
                            "Der Zustand der Mitgliedschaft wurde erfolgreich geändert".to_string(),
                        ),
                        Err(err) => Message::error(format!("Error saving membership: {err:?}")),
                    }
                }
                None => Message::error(format!(
                    "Could not find membership {}",
                    payload.membership_id
                )),
            },
            None => Message::error("Invalid data".to_string()),
        };

        vec![Event::Message(message)]
    }
}
