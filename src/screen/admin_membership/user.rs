use super::{AdminMembership, Deserialize, GacoSession, Membership};
use wallaby::{ActionPayload, Event, Input, Message};

pub struct AddUser;
#[async_trait::async_trait]
impl Input<AdminMembership, GacoSession> for AddUser {
    async fn action(&self, session: &mut GacoSession, payload: &ActionPayload) -> Vec<Event> {
        if !session.access_level().await.admin() {
            return vec![Event::Message(Message::error(
                "You are not allowed to add a user to a membership".to_string(),
            ))];
        }

        #[derive(Deserialize)]
        struct Payload {
            membership_id: u32,
            user_id: u32,
        }

        let message = match payload.extra_fields::<Payload>() {
            Some(payload) => {
                if let Some(membership) = Membership::find_for_user_id(payload.user_id) {
                    Message::error(format!(
                        "Die Benutzer_in {} ist schon der Mitgliedschaft {} zugeordnet.",
                        payload.user_id,
                        membership.id()
                    ))
                } else {
                    match Membership::find_for_id(payload.membership_id) {
                        Some(mut membership) => {
                            match membership.add_user(payload.user_id).await {
                                Ok(true) => {
                                match membership.save().await {
                            Ok(_) => Message::success(
                                "Die Benutzer_in wurde erfolgreich der Mitgliedschaft zugewiesen"
                                    .to_string(),
                            ),
                            Err(err) => Message::error(format!("Error saving membership: {err:?}")),
                        }
                            } Ok(false) => {
                                Message::warning(
                                    "Die Benutzer_in war schon teil der Mitgliedschaft".to_string(),
                                )
                            },
                            Err(err) => {
                                Message::error(format!("Could not find user with user_id {}: {err:?}", payload.user_id))
                            }}
                        }
                        None => Message::error(format!(
                            "Could not find membership {}",
                            payload.membership_id
                        )),
                    }
                }
            }
            None => Message::error("Invalid data".to_string()),
        };

        vec![Event::Message(message)]
    }
}

pub struct RemoveUser;
#[async_trait::async_trait]
impl Input<AdminMembership, GacoSession> for RemoveUser {
    async fn action(&self, session: &mut GacoSession, payload: &ActionPayload) -> Vec<Event> {
        if !session.access_level().await.admin() {
            return vec![Event::Message(Message::error(
                "You are not allowed to remove a user of a membership".to_string(),
            ))];
        }

        #[derive(Deserialize)]
        struct Payload {
            membership_id: u32,
            user_id: u32,
        }

        let message = match payload.extra_fields::<Payload>() {
            Some(payload) => match Membership::find_for_id(payload.membership_id) {
                Some(mut membership) => {
                    if membership.remove_user(payload.user_id) {
                        match membership.save().await {
                            Ok(_) => Message::success(
                                "Die Benutzer_in wurde erfolgreich aus der Mitgliedschaft entfernt"
                                    .to_string(),
                            ),
                            Err(err) => Message::error(format!("Error saving membership: {err:?}")),
                        }
                    } else {
                        Message::warning(
                            "Die Benutzer_in war nicht Teil der Mitgliedschaft".to_string(),
                        )
                    }
                }
                None => Message::error(format!(
                    "Could not find membership {}",
                    payload.membership_id
                )),
            },
            None => Message::error("Invalid data".to_string()),
        };

        vec![Event::Message(message)]
    }
}

pub struct SetMainUser;
#[async_trait::async_trait]
impl Input<AdminMembership, GacoSession> for SetMainUser {
    async fn action(&self, session: &mut GacoSession, payload: &ActionPayload) -> Vec<Event> {
        if !session.access_level().await.admin() {
            return vec![Event::Message(Message::error(
                "You are not allowed to set a user as main user of a membership".to_string(),
            ))];
        }

        #[derive(Deserialize)]
        struct Payload {
            membership_id: u32,
            user_id: u32,
        }

        let message = match payload.extra_fields::<Payload>() {
            Some(payload) => match Membership::find_for_id(payload.membership_id) {
                Some(mut membership) => {
                    if membership.set_main_user(payload.user_id) {
                        match membership.save().await {
                            Ok(_) => Message::success(
                                "Die Benutzer_in wurde erfolgreich als Hauptbenutzer festgelegt"
                                    .to_string(),
                            ),
                            Err(err) => Message::error(format!("Error saving membership: {err:?}")),
                        }
                    } else {
                        Message::warning(
                            "Die Benutzer_in ist nicht Teil der Mitgliedschaft".to_string(),
                        )
                    }
                }
                None => Message::error(format!(
                    "Could not find membership {}",
                    payload.membership_id
                )),
            },
            None => Message::error("Invalid data".to_string()),
        };

        vec![Event::Message(message)]
    }
}
