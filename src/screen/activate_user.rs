use super::common::Common;
use crate::{
    model::user::User,
    routing::{Page, Routing},
    GacoSession, CONFIG,
};
use serde_json::json;
use wallaby::{Event, Interaction, Message, Output, RenderedOutput, StringValuePayload};

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession")]
pub struct ActivateUser {
    #[screen]
    common: Common,

    #[interaction]
    password: Password,

    #[interaction(ty = "output")]
    texts: Texts,

    #[interaction(ty = "output")]
    token_valid: TokenValid,
}

struct Texts;

impl Output<ActivateUser, GacoSession> for Texts {
    fn render(
        &self,
        _interaction_name: String,
        _screen: &ActivateUser,
        session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            json!({
                "change_password": session.tr("change_profile_change_password"),
                "password": session.tr("change_profile_password"),
                "password2": session.tr("change_profile_password2"),
                "passwords_not_matching": session.tr("change_profile_passwords_not_matching"),
                "passwords_too_short": session.tr("change_profile_password_too_short"),
                "link_not_valid": session.tr("change_profile_link_not_valid"),
                "create_new_link": session.tr("change_profile_create_new_link"),
            }),
            (),
        )
    }
}

struct TokenValid;
impl Output<ActivateUser, GacoSession> for TokenValid {
    fn render(
        &self,
        _interaction_name: String,
        _screen: &ActivateUser,
        session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(check_token(session).is_ok(), ())
    }
}

struct Password;

#[async_trait::async_trait]
impl Interaction<ActivateUser, GacoSession> for Password {
    async fn action(
        &self,
        session: &mut GacoSession,
        payload: &wallaby::ActionPayload,
    ) -> Vec<wallaby::Event> {
        let Ok(mut user) = check_token(session) else {
            return vec![Event::Message(Message::error("Link not valid".to_string()))];
        };

        let Some(password) = payload
            .extra_fields::<StringValuePayload>()
            .map(|payload| payload.value)
        else {
            return vec![Event::Message(Message::error(
                "Password not found".to_string(),
            ))];
        };

        if let Err(err) = user.set_password(&password) {
            return vec![Event::Message(Message::error(format!(
                "Passwort konnte nicht gesetzt werden: {err:?}"
            )))];
        }

        let username = user.username().to_owned();

        if let Err(err) = user.save().await {
            return vec![Event::Message(Message::error(format!(
                "Benutzer konnte nicht gespeichert werden: {err:?}"
            )))];
        }

        let mut events = vec![Event::Message(Message::info(
            session.tr("password_changed"),
        ))];

        events.extend(session.login(&username, &password).await);

        session.set_routing(Routing::Page(Page::Start));

        events
    }
}

fn check_token(session: &GacoSession) -> Result<User, ()> {
    if !CONFIG.modify_users_or_documents {
        return Err(());
    }

    let Routing::Page(Page::ActivateUser {
        user,
        password_token,
    }) = session.routing()
    else {
        return Err(());
    };

    let Some(user) = User::find_for_uid(*user) else {
        return Err(());
    };

    if user.check_password_token(password_token).is_err() {
        return Err(());
    }

    Ok(user)
}
