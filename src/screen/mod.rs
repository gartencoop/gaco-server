mod common;

pub mod access_denied;
pub mod activate_user;
pub mod admin_bidding;
pub mod admin_deposits;
pub mod admin_incomes;
pub mod admin_membership;
pub mod admin_memberships;
pub mod admin_transactions;
pub mod admin_users;
pub mod bidding;
pub mod calendar;
pub mod calendar_import_ics;
pub mod document;
pub mod edit_event;
pub mod edit_vp;
pub mod event;
pub mod forgot_password;
pub mod impress;
pub mod income;
pub mod invitation;
pub mod membership;
pub mod news;
pub mod not_found;
pub mod privacy;
pub mod register_user;
pub mod start;
pub mod user;
pub mod vp;
pub mod vps;

use crate::GacoSession;
use wallaby::*;

pub async fn add(server: &mut WallabyServer<GacoSession, InMemorySessionStore<GacoSession>>) {
    server
        .add_screen(access_denied::AccessDenied::generator())
        .await;
    server.add_screen(calendar::Calendar::generator()).await;
    server
        .add_screen(calendar_import_ics::CalendarImportIcs::generator())
        .await;
    server.add_screen(document::Document::generator()).await;
    server.add_screen(event::Event::generator()).await;
    server.add_screen(not_found::NotFound::generator()).await;
    server.add_screen(news::News::generator()).await;
    server.add_screen(start::Start::generator()).await;
    server.add_screen(impress::Impress::generator()).await;
    server.add_screen(privacy::Privacy::generator()).await;
    server.add_screen(vp::VP::generator()).await;
    server.add_screen(edit_event::EditEvent::generator()).await;
    server.add_screen(edit_vp::EditVP::generator()).await;
    server.add_screen(membership::Membership::generator()).await;
    server.add_screen(bidding::BiddingPage::generator()).await;
    server
        .add_screen(admin_bidding::AdminBidding::generator())
        .await;
    server
        .add_screen(admin_memberships::AdminMemberships::generator())
        .await;
    server
        .add_screen(admin_membership::AdminMembership::generator())
        .await;
    server
        .add_screen(admin_transactions::AdminTransactions::generator())
        .await;
    server
        .add_screen(admin_users::AdminUsers::generator())
        .await;
    server
        .add_screen(admin_incomes::AdminIncomes::generator())
        .await;
    server.add_screen(income::Income::generator()).await;
    server
        .add_screen(admin_deposits::AdminDeposits::generator())
        .await;
    server.add_screen(invitation::Invitation::generator()).await;
    server.add_screen(user::User::generator()).await;
    server
        .add_screen(forgot_password::ForgotPassword::generator())
        .await;
    server
        .add_screen(activate_user::ActivateUser::generator())
        .await;
    server
        .add_screen(register_user::RegisterUser::generator())
        .await;
    server.add_screen(vps::VPs::generator()).await;
}
