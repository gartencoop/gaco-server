use super::common::Common;
use crate::{
    interaction::{membership::ScreenMembership, transactions::ScreenTransactions},
    model::{membership::Subscription, user::User},
    GacoSession,
};
use serde_derive::Deserialize;
use serde_json::json;
use wallaby::*;

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession", hooks(load_data))]
pub struct Membership {
    #[screen]
    common: Common,

    #[interaction(ty = "output")]
    membership: crate::interaction::membership::Membership,

    #[interaction(ty = "input")]
    change_user_data: ChangeUserData,

    #[interaction(ty = "output")]
    texts: Texts,

    user: Option<User>,
    data: Option<crate::model::membership::Membership>,
    invitation_url: Option<String>,
    invitation_url_valid_until: Option<String>,
}

#[async_trait::async_trait]
impl LoadData<GacoSession> for Membership {
    async fn load_data(&mut self, session: &GacoSession) -> Result<(), wallaby::Error> {
        self.user = session.user().await;
        self.data = self.user.as_ref().and_then(|user| user.active_membership());
        if let Some(membership) = self.data.as_ref() {
            self.invitation_url = membership.invitation_url().await.ok();
            self.invitation_url_valid_until =
                membership.invitation_valid_until().await.ok().map(|date| {
                    session.tr_args(
                        "invitation_validity",
                        &std::iter::once(("date".into(), date.into())).collect(),
                    )
                });
        }

        Ok(())
    }
}

impl ScreenMembership for Membership {
    fn membership(&self) -> Option<&crate::model::membership::Membership> {
        self.data.as_ref()
    }

    fn user(&self) -> Option<&crate::model::user::User> {
        self.user.as_ref()
    }

    fn membership_invitation_url(&self) -> Option<&str> {
        self.invitation_url.as_deref()
    }

    fn membership_invitation_url_valid_until(&self) -> Option<&str> {
        self.invitation_url_valid_until.as_deref()
    }
}

impl ScreenTransactions for Membership {
    fn transactions(&self) -> Vec<&crate::model::transaction::Transaction> {
        // We do not show transactions to the user, so always empty on this screen
        vec![]
    }
}

struct Texts;

impl Output<Membership, GacoSession> for Texts {
    fn render(
        &self,
        _interaction_name: String,
        screen: &Membership,
        session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            json!({
                "id": session.tr("membership_id"),
                "main_person": session.tr("membership_main_person"),
                "invoice_help": session.tr_args(
                    "membership_invoice_help",
                    &std::iter::once(("id".into(), screen.data.as_ref().map(|membership|membership.id()).unwrap_or_default().into())).collect(),
                ),
                "contact": session.tr("membership_contact"),
                "change_my_date" : session.tr("membership_change_my_date"),
                "membership" : session.tr("membership_membership"),
                "created_at" : session.tr("membership_created_at"),
                "started_at" : session.tr("membership_started_at"),
                "end_at" : session.tr("membership_end_at"),
                "type" : session.tr("membership_type"),
                "shares" : session.tr("membership_shares"),
                "current_bid": session.tr_args(
                    "membership_current_bid",
                    &std::iter::once(("bid".into(), screen.data.as_ref().and_then(|membership|membership.current_bid()).map(|bid| session.format_cents_as_euros(bid.cents(), 2)).unwrap_or_default().into())).collect(),
                ),
                "distribution_point" : session.tr("membership_distribution_point"),
                "members" : session.tr("membership_members"),
                "add_member" : session.tr("membership_add_member"),
                "copy_invitation_link" : session.tr("membership_copy_invitation_link"),
                "link_copied_to_clipboard" : session.tr("membership_link_copied_to_clipboard"),
                "name" : session.tr("membership_name"),
                "surname" : session.tr("membership_surname"),
                "street" : session.tr("membership_street"),
                "zip" : session.tr("membership_zip"),
                "city" : session.tr("membership_city"),
                "discard" : session.tr("membership_discard"),
                "save" : session.tr("membership_save"),
                "subscription": match screen.data.as_ref().map(|membership| membership.subscription()) {
                    Some(subscription) => match *subscription {
                        Subscription::WaitingList => session.tr("membership_type_waiting_list"),
                        Subscription::NoVegetables => session.tr("membership_type_no_vegetables"),
                        Subscription::Active => session.tr("membership_type_active"),
                        Subscription::TestMembership => session.tr("membership_type_test_membership"),
                        Subscription::Team => session.tr("membership_type_team"),
                        Subscription::FreeVegetables => session.tr("membership_type_free_vegetables"),
                    },
                    None => "".to_string(),
                },
            }),
            (),
        )
    }
}

struct ChangeUserData;
#[async_trait::async_trait]
impl Input<Membership, GacoSession> for ChangeUserData {
    async fn action(&self, session: &mut GacoSession, payload: &ActionPayload) -> Vec<Event> {
        if !session.access_level().await.admin() {
            return vec![Event::Message(Message::error(
                session.tr("admin_bidding_error"),
            ))];
        }

        #[derive(Deserialize)]
        struct Payload {
            city: String,
            name: String,
            street: String,
            surname: String,
            zip: String,
        }

        let Some(payload) = payload.extra_fields::<Payload>() else {
            return vec![Event::Message(Message::error("Invalid data".to_string()))];
        };
        let Some(user) = session.user().await else {
            return vec![Event::Message(Message::error("Not logged in".to_string()))];
        };
        let Some(mut membership) = user.membership() else {
            return vec![Event::Message(Message::error(
                "User has no membership".to_string(),
            ))];
        };

        membership.set_city(Some(payload.city));
        membership.set_name(Some(payload.name));
        membership.set_street(Some(payload.street));
        membership.set_surname(Some(payload.surname));
        membership.set_zip(Some(payload.zip));

        vec![Event::Message(match membership.save().await {
            Ok(_) => {
                Message::success("Deine Mitgliedschaft wurde erfolgreich angepasst.".to_string())
            }
            Err(err) => Message::error(format!(
                "Deine Mitglieschaft konnte nicht angepasst werden: {err:?}"
            )),
        })]
    }
}
