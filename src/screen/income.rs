use super::common::Common;
use crate::{
    routing::{Page, Routing},
    GacoSession,
};
use serde::Deserialize;
use serde_json::json;
use wallaby::*;

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession", hooks(load_data))]
pub struct Income {
    #[screen]
    common: Common,

    #[interaction(ty = "button")]
    submit: Submit,

    income: Option<crate::model::income::Income>,
}

#[async_trait::async_trait]
impl LoadData<GacoSession> for Income {
    async fn load_data(&mut self, session: &GacoSession) -> Result<(), Error> {
        let Routing::Page(Page::Income { year_pseudonym }) = session.routing() else {
            return Ok(());
        };

        self.income = crate::model::income::Income::find(year_pseudonym).await;

        Ok(())
    }
}

struct Submit;
#[async_trait::async_trait]
impl Button<Income, GacoSession> for Submit {
    fn render(
        &self,
        interaction_name: String,
        screen: &Income,
        _session: &GacoSession,
    ) -> RenderedButton {
        RenderedButton::new(
            None,
            interaction_name,
            true,
            json!({
                "exists": screen.income.is_some(),
                "has_submitted_income": screen.income.as_ref().map(|income|income.has_income_cents()).unwrap_or_default(),
            }),
        )
    }

    async fn action(&self, session: &mut GacoSession, payload: &ActionPayload) -> Vec<Event> {
        let Routing::Page(Page::Income { year_pseudonym }) = session.routing() else {
            return vec![Event::Message(Message::error(
                "Pseudonym ist nicht vorhanden!".to_string(),
            ))];
        };
        let Some(income) = crate::model::income::Income::find(year_pseudonym).await else {
            return vec![Event::Message(Message::error(
                "Pseudonym konnte nicht gefunden werden!".to_string(),
            ))];
        };

        #[derive(Deserialize)]
        struct Payload {
            cents: u32,
            workdays: u32,
        }
        let message = if let Some(payload) = payload.extra_fields::<Payload>() {
            match income
                .set_income_cents_and_workdays(payload.cents, payload.workdays)
                .await
            {
                Ok(_) => {
                    Message::success("Das Einkommen wurde erfolgreich abgespeichert.".to_string())
                }
                Err(err) => {
                    Message::error(format!("Fehler beim abspeichern des Einkommens: {err:?}"))
                }
            }
        } else {
            Message::error("Data error!".to_string())
        };

        vec![Event::Message(message)]
    }
}
