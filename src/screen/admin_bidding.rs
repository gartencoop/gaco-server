use super::common::Common;
use crate::{
    model::{
        bidding::{Bidding, BiddingCalc},
        membership::Membership,
        user::User,
    },
    GacoSession,
};
use chrono::NaiveDate;
use serde_derive::Deserialize;
use serde_json::json;
use wallaby::*;
#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession", hooks(load_data), ui = "BiddingAdmin")]
pub struct AdminBidding {
    #[screen]
    common: Common,

    #[interaction(ty = "output")]
    overview: Overview,

    #[interaction(ty = "input")]
    setup: Setup,

    user: Option<User>,
}

#[async_trait::async_trait]
impl LoadData<GacoSession> for AdminBidding {
    async fn load_data(&mut self, session: &GacoSession) -> Result<(), Error> {
        self.user = session.user().await;

        if let Some(user) = self.user.as_ref() {
            if !user.access_level().admin() {
                return Err(wallaby::Error::ScreenNotFound(
                    Self::screen_name().to_string(),
                ));
            }
        }

        Ok(())
    }
}

struct Overview;

impl Output<AdminBidding, GacoSession> for Overview {
    fn render(
        &self,
        _interaction_name: String,
        screen: &AdminBidding,
        session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            if screen
                .user
                .as_ref()
                .map(|user| user.access_level().admin())
                .unwrap_or_default()
            {
                let bidding_calc = BiddingCalc::get();

                json!({
                    "average": session.tr_args(
                        "bidding_average_euros",
                        &std::iter::once(("euros".into(), session.format_cents_as_euros(bidding_calc.avg_cents(), 2).into())).collect(),
                    ),
                    "extrapolated": session.tr_args(
                        "bidding_extrapolated",
                        &std::iter::once(("euros".into(), session.format_number(bidding_calc.extrapolated_budget() as f32 / 100.0, 0).into())).collect(),
                    ),
                    "bidding_shares": session.tr_args(
                        "bidding_shares",
                        &std::iter::once(("bidded_shares".into(), session.format_number(bidding_calc.sum_shares(), 1).into())).chain(std::iter::once(("total_shares".into(), session.format_number(Membership::bid_shares(), 1).into()))).collect(),
                    ),
                    "bidding_members": session.tr_args(
                        "bidding_members",
                        &std::iter::once(("bidded_members".into(), bidding_calc.sum_members().into())).chain(std::iter::once(("total_members".into(), Membership::bid_members().into()))).collect(),
                    ),
                    "distribution_plot": bidding_calc.distribution_plot().expect("Could not create distribution plot"),
                    "csv": format!("/csv/bidding")
                })
            } else {
                json!({})
            },
            (),
        )
    }
}

struct Setup;

#[async_trait::async_trait]
impl Input<AdminBidding, GacoSession> for Setup {
    fn render(
        &self,
        interaction_name: String,
        screen: &AdminBidding,
        _session: &GacoSession,
    ) -> RenderedInput {
        if !screen
            .user
            .as_ref()
            .map(|user| user.access_level().admin())
            .unwrap_or_default()
        {
            return RenderedInput::new((), interaction_name, true, false, ());
        }

        let bidding = Bidding::get();

        RenderedInput::new(
            serde_json::to_value(bidding).expect("Could not convert bidding to Value"),
            interaction_name,
            true,
            false,
            (),
        )
    }

    async fn action(&self, session: &mut GacoSession, payload: &ActionPayload) -> Vec<Event> {
        if !session.access_level().await.admin() {
            return vec![Event::Message(Message::error(
                session.tr("admin_bidding_error"),
            ))];
        }

        #[derive(Deserialize)]
        struct Payload {
            #[serde(default, deserialize_with = "crate::common::deserialize_optional_date")]
            start_at: Option<NaiveDate>,
            #[serde(default, deserialize_with = "crate::common::deserialize_optional_date")]
            end_at: Option<NaiveDate>,
            #[serde(default, deserialize_with = "crate::common::deserialize_optional_date")]
            correction_until: Option<NaiveDate>,
            #[serde(default)]
            goal_cents: Option<u32>,
            #[serde(default)]
            goal_members: Option<u32>,
        }

        let Some(payload) = payload.extra_fields::<Payload>() else {
            return vec![Event::Message(Message::error("Invalid data".to_string()))];
        };

        let mut bidding = Bidding::get();

        if let Some(start_at) = payload.start_at {
            bidding.set_start_at(start_at);
        }
        if let Some(end_at) = payload.end_at {
            bidding.set_end_at(end_at);
        }
        if let Some(correction_until) = payload.correction_until {
            bidding.set_correction_until(correction_until);
        }
        if let Some(goal_cents) = payload.goal_cents {
            bidding.set_goal_cents(goal_cents);
        }
        if let Some(goal_members) = payload.goal_members {
            bidding.set_goal_members(goal_members);
        }

        vec![Event::Message(match bidding.save().await {
            Ok(_) => {
                Message::success("Die Einstellungen wurden erfolgreich abgespeichert".to_string())
            }
            Err(_) => {
                Message::error("Die Einstellungen konnten nicht abgespeichert werden.".to_string())
            }
        })]
    }
}
