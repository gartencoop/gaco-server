use super::common::Common;
use crate::{model::event::Event, GacoSession};
use chrono::{DateTime, Duration};
use serde::{Deserialize, Serialize};
use serde_json::json;
use std::ops::Add;
use wallaby::{ActionPayload, Error, Input, LoadData, RenderedInput};

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession", hooks(load_data))]
pub struct Calendar {
    #[screen]
    common: Common,

    #[interaction(ty = "input")]
    calendar: CalendarView,

    events: Option<Vec<CalendarEvent>>,
}

struct CalendarView;
#[async_trait::async_trait]
impl Input<Calendar, GacoSession> for CalendarView {
    fn render(
        &self,
        interaction_name: String,
        screen: &Calendar,
        session: &GacoSession,
    ) -> RenderedInput {
        RenderedInput::new(
            json!({
                "view": session.calendar().view(),
                "events": screen.events.as_ref(),
            }),
            interaction_name,
            true,
            false,
            (),
        )
    }

    async fn action(
        &self,
        session: &mut GacoSession,
        payload: &ActionPayload,
    ) -> Vec<wallaby::Event> {
        #[derive(Deserialize)]
        struct CalendarAction {
            payload: CalendarActionPayload,
        }

        #[derive(Deserialize)]
        #[serde(untagged)]
        enum CalendarActionPayload {
            RangeChangeMonth { start: String, end: String },
            RangeChangeDays(Vec<String>),
            ViewChange(crate::session::calendar::CalendarView),
        }

        if let Some(calendar_action) = payload.extra_fields::<CalendarAction>() {
            let calendar = session.calendar_mut();
            match calendar_action.payload {
                CalendarActionPayload::RangeChangeMonth { start, end } => {
                    calendar.set_start(start);
                    calendar.set_end(end);
                }
                CalendarActionPayload::RangeChangeDays(days) if !days.is_empty() => {
                    let start = days.first().unwrap().to_owned();
                    let end = DateTime::parse_from_rfc3339(days.last().unwrap())
                        .map(|last| {
                            last.add(Duration::try_days(1).expect("Could not create duration"))
                                .to_rfc3339()
                        })
                        .unwrap_or_else(|_| days.last().unwrap().to_owned());

                    calendar.set_start(start);
                    calendar.set_end(end);
                }
                CalendarActionPayload::ViewChange(view) => {
                    calendar.set_view(view);
                }
                _ => (),
            }
        }

        vec![]
    }
}

#[derive(Serialize, Deserialize, Debug, Default)]
#[serde(default)]
pub struct CalendarEvent {
    title: String,
    #[serde(rename(serialize = "start"))]
    start_at: String,
    #[serde(rename(serialize = "end"))]
    end_at: String,
    #[serde(serialize_with = "crate::model::user::username_for_id")]
    created_by: u32,
    path: String,
    category: String,
    #[serde(skip_serializing)]
    private: bool,
}

#[async_trait::async_trait]
impl LoadData<GacoSession> for Calendar {
    async fn load_data(&mut self, session: &GacoSession) -> Result<(), Error> {
        let show_private_events = session.access_level().await.user();
        let id_selectors = {
            let mut selector = vec![json!({
                "$gt": format!("{}_", Event::id_prefix(false)),
                "$lt": format!("{}_{{}}", Event::id_prefix(false))
            })];
            if show_private_events {
                selector.push(json!({
                    "$gt": format!("{}_", Event::id_prefix(true)),
                    "$lt": format!("{}_{{}}", Event::id_prefix(true))
                }));
            }
            selector
        };
        self.events = crate::couchdb::find_documents::<CalendarEvent>(json!({
            "selector": {
                "_id": {
                    "$or": id_selectors,
                },
                "start_at": {
                    "$gte": session.calendar().start(),
                    "$lt": session.calendar().end(),
                }
            },
            "sort": [{"start_at": "asc"}],
            "fields": [
                "created_by",
                "start_at",
                "end_at",
                "title",
                "path",
                "category",
                "private"
            ],
            "limit": 10000
        }))
        .await
        .ok();

        Ok(())
    }
}
