use super::common::Common;
use crate::{model::user::User, GacoSession};
use serde_json::json;
use wallaby::{LoadData, Output, RenderedOutput};

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession", hooks(load_data))]
pub struct ForgotPassword {
    #[screen]
    common: Common,

    #[interaction]
    forgot_email: ForgotEmail,

    #[interaction(ty = "output")]
    texts: Texts,
}

struct Texts;

impl Output<ForgotPassword, GacoSession> for Texts {
    fn render(
        &self,
        _interaction_name: String,
        _screen: &ForgotPassword,
        session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            json!({
                "header": session.tr("forgot_password_header"),
                "text": session.tr("forgot_password_text"),
                "button": session.tr("forgot_password_button"),
            }),
            (),
        )
    }
}

#[async_trait::async_trait]
impl LoadData<GacoSession> for ForgotPassword {
    async fn load_data(&mut self, _session: &GacoSession) -> Result<(), wallaby::Error> {
        Ok(())
    }
}

struct ForgotEmail;

#[async_trait::async_trait]
impl wallaby::Interaction<ForgotPassword, GacoSession> for ForgotEmail {
    async fn action(
        &self,
        _session: &mut GacoSession,
        payload: &wallaby::ActionPayload,
    ) -> Vec<wallaby::Event> {
        let Some(email) = payload
            .extra_fields::<wallaby::StringValuePayload>()
            .map(|payload| payload.value)
        else {
            return vec![wallaby::Event::Message(wallaby::Message::error(
                "Email missing".to_string(),
            ))];
        };

        tokio::task::spawn(async move {
            if let Some(user) = User::find_for_username_or_email(&email) {
                match user.send_reset_password_email(true).await {
                    Ok(_) => log::info!("forgot password email sent"),
                    Err(e) => log::error!("Could not send forgot password email: {e}"),
                }
            };
        });

        vec![wallaby::Event::Message(wallaby::Message::success(
            "Passwort-vergessen Email wurde versendet".to_string(),
        ))]
    }
}
