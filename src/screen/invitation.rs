use super::common::Common;
use crate::{
    model::membership::Membership,
    routing::{Page, Routing},
    GacoSession,
};
use serde_json::json;
use wallaby::{ActionPayload, Button, Error, Event, LoadData, Message, RenderedButton};

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession", hooks(load_data))]
pub struct Invitation {
    #[screen]
    common: Common,

    #[interaction(ty = "button")]
    accept_invitation: AcceptInvitation,

    current_membership: Option<Membership>,
    membership: Option<Membership>,
}

#[async_trait::async_trait]
impl LoadData<GacoSession> for Invitation {
    async fn load_data(&mut self, session: &GacoSession) -> Result<(), Error> {
        let Some(membership) = checked_membership(session.routing()) else {
            return Ok(());
        };

        self.membership = Some(membership);
        if let Some(user) = session.user().await {
            self.current_membership = user.membership();
        }

        Ok(())
    }
}

struct AcceptInvitation;
#[async_trait::async_trait]
impl Button<Invitation, GacoSession> for AcceptInvitation {
    fn render(
        &self,
        interaction_name: String,
        screen: &Invitation,
        session: &GacoSession,
    ) -> RenderedButton {
        let (show_buttons, caption) = if let (Some(current_membership), Some(membership)) = (
            screen.current_membership.as_ref(),
            screen.membership.as_ref(),
        ) {
            if current_membership == membership {
                (
                    false,
                    session.tr_args(
                        "invitation_already",
                        &std::iter::once(("name".into(), membership.full_name().into())).collect(),
                    ),
                )
            } else {
                (
                    true,
                    session.tr_args(
                        "invitation_switch",
                        &std::iter::once((
                            "old_name".into(),
                            current_membership.full_name().into(),
                        ))
                        .chain(std::iter::once((
                            "name".into(),
                            membership.full_name().into(),
                        )))
                        .collect(),
                    ),
                )
            }
        } else if let Some(membership) = screen.membership.as_ref() {
            (
                true,
                session.tr_args(
                    "invitation_new",
                    &std::iter::once(("name".into(), membership.full_name().into())).collect(),
                ),
            )
        } else {
            (false, session.tr("invitation_invalid"))
        };

        RenderedButton::new(
            Some(caption),
            interaction_name,
            true,
            json!({
                "yes": session.tr("invitation_yes"),
                "no": session.tr("invitation_no"),
                "show_buttons": show_buttons
            }),
        )
    }

    async fn action(&self, session: &mut GacoSession, _payload: &ActionPayload) -> Vec<Event> {
        let Some(mut membership) = checked_membership(session.routing()) else {
            return vec![Event::Message(Message::error(
                session.tr("invitation_invalid"),
            ))];
        };

        let Some(uid) = session.uid().await else {
            return vec![Event::Message(Message::error(
                "You need to be logged in!".to_string(),
            ))];
        };

        let name = membership.full_name();
        match membership.add_user(uid).await {
            Ok(true) => {
                if let Err(err) = membership.save().await {
                    return vec![Event::Message(Message::error(
                        session.tr_args(
                            "invitation_error",
                            &std::iter::once(("name".into(), name.into()))
                                .chain(std::iter::once(("error".into(), format!("{err:?}").into())))
                                .collect(),
                        ),
                    ))];
                } else {
                    return vec![Event::Message(Message::success(session.tr_args(
                        "invitation_success",
                        &std::iter::once(("name".into(), name.into())).collect(),
                    )))];
                }
            }
            Ok(false) => {
                vec![]
            }
            Err(err) => {
                return vec![Event::Message(Message::error(
                    session.tr_args(
                        "invitation_error",
                        &std::iter::once(("name".into(), name.into()))
                            .chain(std::iter::once(("error".into(), format!("{err:?}").into())))
                            .collect(),
                    ),
                ))];
            }
        }
    }
}

fn checked_membership(routing: &Routing) -> Option<Membership> {
    let Routing::Page(Page::Invitation {
        membership_id,
        invitation_token,
    }) = routing
    else {
        return None;
    };

    let membership = Membership::find_for_id(*membership_id);
    if let Some(membership) = membership.as_ref() {
        if membership
            .verify_invitation_token(invitation_token)
            .is_err()
        {
            return None;
        }
    }

    membership
}
