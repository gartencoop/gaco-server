use super::common::Common;
use crate::{
    model::user::User,
    routing::{Page, Routing},
    GacoSession, CONFIG,
};
use serde::Deserialize;
use serde_json::json;
use wallaby::{Error, Event, Helo, HeloPayload, Interaction, Message, Output, RenderedOutput};

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession", hooks(helo))]
pub struct RegisterUser {
    #[screen]
    common: Common,

    #[interaction]
    form: Form,

    #[interaction(ty = "output")]
    texts: Texts,
}

struct Texts;

impl Output<RegisterUser, GacoSession> for Texts {
    fn render(
        &self,
        _interaction_name: String,
        _screen: &RegisterUser,
        session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            json!({
                "title": session.tr("register_user_title"),
                "description": session.tr("register_user_description"),
                "username": session.tr("register_user_username"),
                "name": session.tr("register_user_name"),
                "phone": session.tr("register_user_phone"),
                "email": session.tr("register_user_email"),
                "submit": session.tr("register_user_submit"),
            }),
            (),
        )
    }
}

struct Form;

#[async_trait::async_trait]
impl Interaction<RegisterUser, GacoSession> for Form {
    async fn action(
        &self,
        session: &mut GacoSession,
        payload: &wallaby::ActionPayload,
    ) -> Vec<wallaby::Event> {
        if !CONFIG.modify_users_or_documents {
            return vec![Event::Message(Message::error(
                "User modification is disabled".to_string(),
            ))];
        }

        #[derive(Deserialize)]
        struct FormPayload {
            pub email: String,
            pub user: String,
            pub name: Option<String>,
            pub phone: Option<String>,
        }

        let Some(form) = payload.extra_fields::<FormPayload>() else {
            return vec![Event::Message(Message::error("Missing data".to_string()))];
        };

        if User::find_for_username_or_email(&form.user).is_some() {
            return vec![Event::Message(Message::error(
                session.tr("register_user_username_exists").to_string(),
            ))];
        }

        if User::find_for_username_or_email(&form.email).is_some() {
            return vec![Event::Message(Message::error(
                session.tr("register_user_email_exists").to_string(),
            ))];
        }

        let message = if let Err(err) = User::create(
            form.email,
            form.user,
            form.name.filter(|name| !name.is_empty()),
            form.phone.filter(|phone: &String| !phone.is_empty()),
        )
        .await
        {
            Message::error(format!("Could not create user: {err:?}"))
        } else {
            session.set_routing(Routing::Page(Page::Start));

            Message::success("Du bekommst eine Email mit weiteren Anweisungen zum Aktivieren und Passwort Setzen.".to_owned())
        };

        session.set_routing(Routing::Page(Page::Start));

        vec![Event::Message(message)]
    }
}

#[async_trait::async_trait]
impl Helo<GacoSession> for RegisterUser {
    async fn helo(
        &self,
        _session: &mut GacoSession,
        _payload: &HeloPayload,
    ) -> Result<Vec<Event>, Error> {
        if !CONFIG.modify_users_or_documents {
            return Ok(vec![Event::Message(Message::error(
                "User modification is disabled".to_string(),
            ))]);
        }

        Ok(vec![])
    }
}
