use super::common::Common;
use crate::{
    couchdb::{save_batch, save_batch_owned},
    model::{income::Income, membership::Membership},
    GacoSession, CONFIG,
};
use anyhow::Result;
use chrono::Datelike;
use lettre::{
    message::{header::ContentType, Attachment, MultiPart, SinglePart},
    transport::smtp::authentication::Credentials,
    AsyncSmtpTransport, AsyncTransport, Tokio1Executor,
};
use mime::TEXT_PLAIN_UTF_8;
use serde_json::json;
use std::sync::atomic::{AtomicUsize, Ordering::Relaxed};
use wallaby::*;

static MAIL_QUEUE: AtomicUsize = AtomicUsize::new(0);

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession", hooks(load_data))]
pub struct AdminIncomes {
    #[screen]
    common: Common,

    #[interaction(ty = "button")]
    start: Start,

    #[interaction(ty = "button")]
    delete: Delete,

    #[interaction(ty = "output")]
    submitted: Submitted,

    #[interaction(ty = "output")]
    total: Total,

    #[interaction(ty = "output")]
    mail_queue: MailQueue,

    incomes: Option<Vec<Income>>,
}

#[async_trait::async_trait]
impl LoadData<GacoSession> for AdminIncomes {
    async fn load_data(&mut self, session: &GacoSession) -> Result<(), Error> {
        if !session
            .user()
            .await
            .map(|user| user.access_level().admin())
            .unwrap_or_default()
        {
            return Err(wallaby::Error::ScreenNotFound(
                Self::screen_name().to_string(),
            ));
        }

        self.incomes = Some(Income::find_all_for_year(chrono::Utc::now().year() as u16).await);

        Ok(())
    }
}

struct Start;
#[async_trait::async_trait]
impl Button<AdminIncomes, GacoSession> for Start {
    fn render(
        &self,
        interaction_name: String,
        screen: &AdminIncomes,
        _session: &GacoSession,
    ) -> RenderedButton {
        RenderedButton::new(
            None,
            interaction_name,
            true,
            json!({"enabled": screen.incomes.as_ref().map(|incomes|incomes.is_empty()).unwrap_or_default()}),
        )
    }

    async fn action(&self, session: &mut GacoSession, _payload: &ActionPayload) -> Vec<Event> {
        if !session
            .user()
            .await
            .map(|user| user.access_level().admin())
            .unwrap_or_default()
        {
            return vec![];
        }

        let year = chrono::Utc::now().year() as u16;
        if !Income::find_all_for_year(year).await.is_empty() {
            return vec![Event::Message(Message::error("Es kann kein neues Einkommensverfahren gestartet werden, da für dieses Jahr schon eines gestartet wurde!".to_string()))];
        }

        // never save on the server, is discarded at end of function!
        let mut membership_income: Vec<(Membership, Income, String)> = Membership::find_all()
            .into_iter()
            .filter(|membership| membership.can_bid())
            .map(|membership| {
                let shares = membership.shares();
                let income = Income::new(year, shares);
                let body = session.tr_args(
                    "mail_income_body",
                    &std::iter::once((
                        "link".into(),
                        format!(
                            "https://gaco.uber.space/income_{}_{}",
                            income.year(),
                            income.pseudonym()
                        )
                        .into(),
                    ))
                    .collect(),
                );
                (membership, income, body)
            })
            .collect();

        let message = match save_batch(
            membership_income
                .iter_mut()
                .map(|(_membership, income, _body)| income)
                .collect::<Vec<_>>(),
        )
        .await
        {
            Ok(_) => Message::success(
                "Die Pseudonyme wurden erfolgreich angelegt. Sende Emails..".to_string(),
            ),
            Err(err) => Message::error(format!("Fehler beim abspeichern der Pseudonyme: {err:?}")),
        };

        let subject = session.tr("mail_income_subject");

        tokio::task::spawn(async move {
            MAIL_QUEUE.store(membership_income.len() + 1, Relaxed);

            let csv = membership_income
                .iter()
                .map(|(membership, income, _body)| {
                    format!(
                        "{}; {}; {}",
                        membership.id(),
                        membership.emails().join(", "),
                        income.pseudonym()
                    )
                })
                .collect::<Vec<_>>()
                .join("\n")
                .into_bytes();
            if let Err(err) = send_income_csv(csv).await {
                log::error!("Error sending email: {err:?}");
            }

            MAIL_QUEUE.store(membership_income.len(), Relaxed);

            for (mail_queue, (membership, _income, body)) in
                membership_income.into_iter().enumerate().rev()
            {
                if let Err(err) = membership.send_email(&subject, body).await {
                    log::error!("Error sending email: {err:?}");
                }

                MAIL_QUEUE.store(mail_queue, Relaxed);
            }
        });

        vec![Event::Message(message)]
    }
}

async fn send_income_csv(csv: Vec<u8>) -> Result<()> {
    let smtp_credentials =
        Credentials::new(CONFIG.email.username.clone(), CONFIG.email.password.clone());
    let mailer = AsyncSmtpTransport::<Tokio1Executor>::relay(&CONFIG.email.host)?
        .credentials(smtp_credentials)
        .build();
    let address = CONFIG.email.income_csv_to.parse()?;

    let year = chrono::Utc::now().year() as u16;
    let csv_attachment = Attachment::new(format!("mitglieder_pseudonyme_{year}.csv"))
        .body(csv, ContentType::parse("text/csv")?);
    let email = lettre::Message::builder()
        .from(CONFIG.email.membership_from.parse()?)
        .to(address)
        .subject(format!("Zuordnung Mitgliedsnummer - Pseudonym {year}"))
        .multipart(
            MultiPart::mixed()
            .singlepart(
            SinglePart::builder()
                .header(ContentType::from(TEXT_PLAIN_UTF_8))
                .body(format!("Die Zuordnung von Mitgliedsnummern zu Pseudonymen für das Jahr {year} ist im Anhang.\n\nBitte diese Email niemals an die Webseitadministratoren schicken, da diese sonst das Einkommen einer Mitgliedschaft kennen!")),
            )
            .singlepart(csv_attachment)
        )?;

    mailer.send(email).await?;

    Ok(())
}

struct Delete;
#[async_trait::async_trait]
impl Button<AdminIncomes, GacoSession> for Delete {
    async fn action(&self, session: &mut GacoSession, _payload: &ActionPayload) -> Vec<Event> {
        if !session
            .user()
            .await
            .map(|user| user.access_level().admin())
            .unwrap_or_default()
        {
            return vec![];
        }

        let year = chrono::Utc::now().year() as u16;

        let message = match save_batch_owned(
            Income::find_all_for_year(year)
                .await
                .into_iter()
                .map(|income| income.into_deleted_document())
                .collect(),
        )
        .await
        {
            Ok(_) => Message::success("Die Pseudonyme wurden erfolgreich gelöscht.".to_string()),
            Err(err) => Message::error(format!("Fehler beim löschen der Pseudonyme: {err:?}")),
        };

        vec![Event::Message(message)]
    }
}

struct Submitted;
impl Output<AdminIncomes, GacoSession> for Submitted {
    fn render(
        &self,
        _interaction_name: String,
        screen: &AdminIncomes,
        _session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            screen
                .incomes
                .as_ref()
                .map(|incomes| {
                    incomes
                        .iter()
                        .filter(|income| income.has_income_cents())
                        .count()
                })
                .unwrap_or_default(),
            (),
        )
    }
}

struct Total;
impl Output<AdminIncomes, GacoSession> for Total {
    fn render(
        &self,
        _interaction_name: String,
        screen: &AdminIncomes,
        _session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            screen
                .incomes
                .as_ref()
                .map(|incomes| incomes.len())
                .unwrap_or_default(),
            (),
        )
    }
}

struct MailQueue;
impl Output<AdminIncomes, GacoSession> for MailQueue {
    fn render(
        &self,
        _interaction_name: String,
        _screen: &AdminIncomes,
        _session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(MAIL_QUEUE.load(Relaxed), ())
    }
}
