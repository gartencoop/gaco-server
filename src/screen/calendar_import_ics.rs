use super::common::Common;
use crate::GacoSession;
use wallaby::*;

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession", ui = "HtmlText", hooks(load_data))]
pub struct CalendarImportIcs {
    #[screen]
    common: Common,

    #[interaction(ty = "output")]
    title: Title,

    #[interaction(ty = "output")]
    text: Text,

    ics_url: Option<String>,
}

struct Title;

impl Output<CalendarImportIcs, GacoSession> for Title {
    fn render(
        &self,
        _interaction_name: String,
        _screen: &CalendarImportIcs,
        session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(session.tr("calendar_import_ics_title"), ())
    }
}

#[async_trait::async_trait]
impl LoadData<GacoSession> for CalendarImportIcs {
    async fn load_data(&mut self, session: &GacoSession) -> Result<(), Error> {
        let Some(user) = session.user().await else {
            return Ok(());
        };
        self.ics_url = user.ics_url().await.ok();

        Ok(())
    }
}

struct Text;

impl Output<CalendarImportIcs, GacoSession> for Text {
    fn render(
        &self,
        _interaction_name: String,
        screen: &CalendarImportIcs,
        session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            session.tr_args(
                "calendar_import_ics",
                &std::iter::once((
                    "ics_url".into(),
                    screen.ics_url.clone().unwrap_or_default().into(),
                ))
                .collect(),
            ),
            (),
        )
    }
}
