use super::Common;
use crate::{model::event::Category, GacoSession};
use strum::IntoEnumIterator;
use wallaby::{Output, RenderedOutput};

pub struct EventCategories;
impl Output<Common, GacoSession> for EventCategories {
    fn render(
        &self,
        _interaction_name: String,
        _screen: &Common,
        _session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            Category::iter()
                .map(|category| category.into())
                .collect::<Vec<&'static str>>(),
            (),
        )
    }
}
