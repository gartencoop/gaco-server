use super::Common;
use crate::{GacoSession, CONFIG};
use anyhow::Result;
use lettre::{
    message::header::ContentType, transport::smtp::authentication::Credentials, AsyncSmtpTransport,
    AsyncTransport, Tokio1Executor,
};
use mime::TEXT_PLAIN_UTF_8;
use serde::Deserialize;
pub use wallaby::*;

pub struct Contact;

#[async_trait::async_trait]
impl Input<Common, GacoSession> for Contact {
    async fn action(&self, session: &mut GacoSession, payload: &ActionPayload) -> Vec<Event> {
        #[derive(Deserialize)]
        struct ContactPayload {
            name: String,
            email: String,
            message: String,
        }

        let mut events = vec![];

        if let Some(payload) = payload.extra_fields::<ContactPayload>() {
            let user = session.user().await;

            let body = format!(
                "Kontaktaufnahme\n\nName: {}\nEmail: {}{}\n\nNachricht:\n\n{}",
                payload.name,
                payload.email,
                user.as_ref()
                    .map(|user| format!("\nUser: {}", user.username()))
                    .unwrap_or_default(),
                payload.message
            );

            events.push(Event::Message(
                match send_email(
                    body,
                    user.and_then(|user| user.active_membership())
                        .map(|membership| membership.id()),
                )
                .await
                {
                    Ok(_) => Message::success(session.tr("contact_message_send")),
                    Err(err) => {
                        log::error!("Error sending message: {err:?}");
                        Message::error(session.tr("contact_message_error"))
                    }
                },
            ));
        }

        events
    }
}

async fn send_email(body: String, membership_id: Option<u32>) -> Result<()> {
    let smtp_credentials =
        Credentials::new(CONFIG.email.username.clone(), CONFIG.email.password.clone());

    let mailer = AsyncSmtpTransport::<Tokio1Executor>::relay(&CONFIG.email.host)?
        .credentials(smtp_credentials)
        .build();

    let mut builder = lettre::Message::builder()
        .header(ContentType::from(TEXT_PLAIN_UTF_8))
        .from(CONFIG.email.contact_form_from.parse()?)
        .to(if membership_id.is_some() {
            &CONFIG.email.contact_form_to
        } else {
            &CONFIG.email.contact_form_to_guests
        }
        .parse()?)
        .subject(format!(
            "{} - {}",
            CONFIG.email.contact_form_subject,
            membership_id.map_or_else(
                || "Aktuell keine Mitgliedschaft".to_owned(),
                |m| format!("Mitgliedschaft #{m}")
            )
        ));

    if let Some(cc) = CONFIG.email.contact_form_cc.as_ref() {
        builder = builder.cc(cc.parse()?);
    }

    let email = builder.body(body)?;

    mailer.send(email).await?;

    Ok(())
}
