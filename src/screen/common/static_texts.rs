use super::Common;
use crate::GacoSession;
use serde_json::{Map, Value};
use wallaby::*;

pub struct StaticTexts;

impl Output<Common, GacoSession> for StaticTexts {
    fn render(
        &self,
        _interaction_name: String,
        _screen: &Common,
        session: &GacoSession,
    ) -> RenderedOutput {
        let mut map = Map::new();

        for text_id in &[
            "login",
            "logout",
            "access_denied_title",
            "access_denied_text",
            "access_denied_login",
            "culture",
            "mailing_address",
            "mailing_address_line_0",
            "mailing_address_line_1",
            "mailing_address_line_2",
            "farmstead",
            "farmstead_line_0",
            "farmstead_line_1",
            "farmstead_line_2",
            "bank_details",
            "bank_details_line_0",
            "bank_details_line_1",
            "bank_details_line_2",
            "bank_details_line_3",
            "contact",
            "contact_line_0",
            "contact_line_1",
            "contact_line_2",
            "contact_form_title",
            "contact_form_name",
            "contact_form_email",
            "contact_form_message",
            "contact_form_send",
            "impress",
            "privacy",
            "login_username",
            "login_password",
            "login_register",
            "login_forgot_password",
            "search_type",
            "search_type_all",
            "search_type_documents",
            "search_type_events",
            "search_input",
            "search_event_category",
            "search_document_team",
            "search_document_topic",
        ] {
            map.insert(text_id.to_string(), Value::String(session.tr(text_id)));
        }

        RenderedOutput::new(Value::Object(map), ())
    }
}
