mod bidding;
pub mod contact;
mod document;
mod event;
mod menu;
mod static_texts;
mod user;

use crate::{model::user::User, routing::Routing, GacoSession, CONFIG};
use document::{DocumentTeams, DocumentTopics};
use event::EventCategories;
use serde::Deserialize;
use static_texts::StaticTexts;
use wallaby::*;

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession", hooks(load_data, helo))]
pub struct Common {
    #[interaction(ty = "output")]
    menu: menu::Menu,

    #[interaction(ty = "input")]
    bidding: bidding::Bidding,

    #[interaction(ty = "input")]
    contact: contact::Contact,

    #[interaction(ty = "input")]
    login: user::Login,

    #[interaction(ty = "button")]
    logout: user::Logout,

    #[interaction(ty = "output")]
    logged_in: user::LoggedIn,

    #[interaction(ty = "output")]
    username: user::Name,

    #[interaction(ty = "output")]
    no_membership: user::NoMembership,

    #[interaction(ty = "output")]
    static_texts: StaticTexts,

    #[interaction(ty = "output")]
    rss_link: RssLink,

    #[interaction(ty = "output")]
    modify_users: ModifyUsers,

    /// for search
    #[interaction(ty = "output")]
    document_teams: DocumentTeams,

    /// for search
    #[interaction(ty = "output")]
    document_topics: DocumentTopics,

    /// for search
    #[interaction(ty = "output")]
    event_categories: EventCategories,

    #[interaction(ty = "output")]
    search_available: SearchAvailable,

    user: Option<User>,
    rss_url: Option<String>,
}

#[async_trait::async_trait]
impl LoadData<GacoSession> for Common {
    async fn load_data(&mut self, session: &GacoSession) -> Result<(), wallaby::Error> {
        self.user = session.user().await;
        if let Some(user) = self.user.as_ref() {
            self.rss_url = user.rss_url().await.ok();
        }

        Ok(())
    }
}

#[async_trait::async_trait]
impl Helo<GacoSession> for Common {
    async fn helo(
        &self,
        session: &mut GacoSession,
        payload: &HeloPayload,
    ) -> Result<Vec<Event>, Error> {
        #[derive(Deserialize)]
        struct LoginIdPayload {
            login_id: String,
            pathname: String,
        }

        if let Some(login_id_payload) = payload.extra_fields::<LoginIdPayload>() {
            session.set_login_id(login_id_payload.login_id);

            let access_level = session.access_level().await;
            session.set_routing(Routing::by_path(&login_id_payload.pathname, access_level).await);
        }

        Ok(vec![])
    }
}

struct RssLink;

impl Output<Common, GacoSession> for RssLink {
    fn render(
        &self,
        _interaction_name: String,
        screen: &Common,
        _session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(screen.rss_url.clone().unwrap_or_default(), ())
    }
}

struct ModifyUsers;
impl Output<Common, GacoSession> for ModifyUsers {
    fn render(
        &self,
        _interaction_name: String,
        _screen: &Common,
        _session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(CONFIG.modify_users_or_documents, ())
    }
}

struct SearchAvailable;
impl Output<Common, GacoSession> for SearchAvailable {
    fn render(
        &self,
        _interaction_name: String,
        _screen: &Common,
        _session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(crate::search::CLIENT.get().is_some(), ())
    }
}
