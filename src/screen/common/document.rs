use super::Common;
use crate::{
    model::document::{Team, Topic},
    GacoSession,
};
use strum::IntoEnumIterator;
use wallaby::{Output, RenderedOutput};

pub struct DocumentTeams;
impl Output<Common, GacoSession> for DocumentTeams {
    fn render(
        &self,
        _interaction_name: String,
        _screen: &Common,
        _session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            Team::iter()
                .map(|team| {
                    let caption: &'static str = team.into();
                    serde_json::json!({
                        "caption": caption,
                        "value": team,
                    })
                })
                .collect::<serde_json::Value>(),
            (),
        )
    }
}

pub struct DocumentTopics;
impl Output<Common, GacoSession> for DocumentTopics {
    fn render(
        &self,
        _interaction_name: String,
        _screen: &Common,
        _session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            Topic::iter()
                .map(|topic| topic.into())
                .collect::<Vec<&'static str>>(),
            (),
        )
    }
}
