use super::Common;
use crate::{routing::Page, GacoSession, CONFIG};
use chrono::Datelike;
use serde::Serialize;
use serde_json::json;
use wallaby::*;

pub struct Menu;

#[async_trait::async_trait]
impl Output<Common, GacoSession> for Menu {
    fn render(
        &self,
        _interaction_name: String,
        screen: &Common,
        session: &GacoSession,
    ) -> RenderedOutput {
        let access_level = screen
            .user
            .as_ref()
            .map(|user| user.access_level())
            .unwrap_or_default();
        let mut menu = vec![];

        if access_level.admin() {
            let entries = vec![
                MenuEntry::new(
                    "Mitgliedschaften".to_string(),
                    Page::AdminMemberships.into_path(),
                ),
                MenuEntry::new(
                    "Beiträge".to_string(),
                    Page::AdminTransactions {
                        year: chrono::Utc::now().year() as u16,
                    }
                    .into_path(),
                ),
                MenuEntry::new("Einlagen".to_string(), Page::AdminDeposits.into_path()),
                MenuEntry::new("Gebote".to_string(), Page::AdminBidding.into_path()),
                MenuEntry::new("Einkommen".to_string(), Page::AdminIncomes.into_path()),
            ];

            menu.push(MenuTop {
                name: "Büro".to_string(),
                entries,
            });
        }

        menu.push(MenuTop {
            name: session.tr("menu_calendar"),
            entries: {
                let mut entries = vec![MenuEntry::new(
                    session.tr("menu_calendar"),
                    Page::Calendar.into_path(),
                )];

                if access_level.user() {
                    entries.push(MenuEntry::new(
                        session.tr("menu_my_events"),
                        "/todo".to_string(), // TODO
                    ));
                    entries.push(MenuEntry::new(
                        session.tr("menu_calendar_import"),
                        Page::CalendarImport.into_path(),
                    ));
                }

                entries
            },
        });

        if access_level.user() {
            menu.push(MenuTop {
                name: "Dokumente".to_string(),
                entries: vec![
                    MenuEntry::new(session.tr("menu_new_content"), "/todo".to_string()), // TODO
                    MenuEntry::new(session.tr("menu_documents"), "/todo".to_string()), // TODO: Sortierung nach Aktualität der letzten Bearbeitung
                    MenuEntry::new(session.tr("menu_protocols"), "/todo".to_string()), // TODO: Liste mit Filter
                    MenuEntry::new(session.tr("menu_teams"), "/todo".to_string()), // TODO: Liste mit Filter
                    MenuEntry::new(session.tr("menu_decisions"), "/node/4693".to_string()), // TODO: Test
                    MenuEntry::new(session.tr("menu_trailer_rent"), "/node/1145".to_string()), // TODO: Test
                    MenuEntry::new(session.tr("menu_ads"), "/todo".to_string()), // TODO: Liste mit Filter
                    MenuEntry::new(session.tr("menu_recipes"), "/todo".to_string()), // TODO: Liste mit Filter
                    MenuEntry::new(session.tr("menu_newsletters"), "/todo".to_string()), // TODO
                    MenuEntry::new(session.tr("menu_harvest_stats"), "/node/195".to_string()), // TODO: Test
                    MenuEntry::new(session.tr("menu_financial_status"), "/todo".to_string()), // TODO
                ],
            });
        }

        menu.push(MenuTop {
            name: "Infos".to_string(),
            entries: {
                let mut entries = vec![
                    MenuEntry::new(session.tr("menu_way_to_tunsel"), "/node/989".to_string()),
                    MenuEntry::new(session.tr("menu_vps"), Page::VPs.into_path()),
                ];
                if access_level.user() {
                    entries.push(
                        MenuEntry::new(
                            session.tr("menu_internal_contact"),
                            "/communication".to_string(),
                        ), // TODO: Test
                    );
                    entries.push(
                        MenuEntry::new(session.tr("menu_handbook"), "/node/7422".to_string()), // TODO: Test
                    );
                }
                entries
            },
        });

        //TODO: Remove
        menu.push(MenuTop {
            name: "Beispieldokumente".to_string(),
            entries: vec![
                MenuEntry::new("Tabelle".to_string(), "/node/6945".to_string()),
                MenuEntry::new("Große Tabelle".to_string(), "/node/1145".to_string()),
                MenuEntry::new("Bilder".to_string(), "/node/7121".to_string()),
                MenuEntry::new("Dateien".to_string(), "/node/6455".to_string()),
            ],
        });

        let user_menu = if access_level.user() {
            let membership = screen
                .user
                .as_ref()
                .and_then(|user| user.active_membership());

            let mut entries = vec![];
            if let Some(membership) = membership {
                if let Some(vp) = membership.vp() {
                    entries.push(MenuEntry::new(
                        format!("{} ({})", session.tr("menu_my_vp"), vp.code()),
                        vp.path().to_string(),
                    ));
                }
                entries.push(MenuEntry::new(
                    session.tr("menu_my_membership"),
                    Page::UserMembership.into_path(),
                ));
            }
            if CONFIG.modify_users_or_documents {
                entries.push(MenuEntry::new(
                    session.tr("menu_my_account"),
                    Page::User.into_path(),
                ));
            }

            entries
        } else {
            vec![]
        };

        RenderedOutput::new(
            json!(menu),
            json!({"user": MenuTop { name: session.tr("menu_manage_profile")
            , entries: user_menu}}),
        )
    }
}

#[derive(Serialize)]
pub struct MenuTop {
    pub name: String,
    pub entries: Vec<MenuEntry>,
}

#[derive(Serialize)]
pub struct MenuEntry {
    pub name: String,
    pub pathname: String,
}

impl MenuEntry {
    pub fn new(name: String, pathname: String) -> Self {
        Self { name, pathname }
    }
}
