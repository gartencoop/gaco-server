use super::Common;
use crate::{
    change_path::change_path_event,
    routing::{Page, Routing},
    GacoSession,
};
use serde::Deserialize;
pub use wallaby::*;

pub struct Login;

#[async_trait::async_trait]
impl Input<Common, GacoSession> for Login {
    async fn action(&self, session: &mut GacoSession, payload: &ActionPayload) -> Vec<Event> {
        #[derive(Deserialize)]
        struct LoginPayload {
            username: String,
            password: String,
        }

        let mut events = vec![];

        if let Some(payload) = payload.extra_fields::<LoginPayload>() {
            events.extend(session.login(&payload.username, &payload.password).await);
        }

        if !session.logged_in().await {
            events.push(Event::Message(Message::error(session.tr("login_failed"))));
        }

        events
    }
}

pub struct LoggedIn;

impl Output<Common, GacoSession> for LoggedIn {
    fn render(
        &self,
        _interaction_name: String,
        screen: &Common,
        _session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(screen.user.is_some(), ())
    }
}

pub struct Logout;

#[async_trait::async_trait]
impl Button<Common, GacoSession> for Logout {
    async fn action(&self, session: &mut GacoSession, _payload: &ActionPayload) -> Vec<Event> {
        session.logout().await;
        session.set_routing(Routing::Page(Page::Start));
        vec![change_path_event("/")]
    }
}

pub struct Name;

impl Output<Common, GacoSession> for Name {
    fn render(
        &self,
        _interaction_name: String,
        screen: &Common,
        _session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            screen
                .user
                .as_ref()
                .map(|user| user.name().to_string())
                .unwrap_or_default(),
            (),
        )
    }
}

pub struct NoMembership;

impl Output<Common, GacoSession> for NoMembership {
    fn render(
        &self,
        _interaction_name: String,
        screen: &Common,
        session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            screen
                .user
                .as_ref()
                .filter(|user| user.active_membership().is_none() && !user.access_level().manager())
                .map(|_| session.tr("user_without_membership")),
            (),
        )
    }
}
