use super::Common;
use crate::{
    model::{
        bidding::BiddingCalc,
        membership::{Bid, Membership, Shares},
    },
    GacoSession,
};
use chrono::{Datelike, NaiveDate, Utc};
use serde::Deserialize;
use serde_json::json;
use wallaby::*;

pub struct Bidding;

#[async_trait::async_trait]
impl Input<Common, GacoSession> for Bidding {
    fn render(
        &self,
        interaction_name: String,
        screen: &Common,
        session: &GacoSession,
    ) -> RenderedInput {
        RenderedInput::new(
            {
                let bidding_calc = BiddingCalc::get();
                let bidding = crate::model::bidding::Bidding::get();

                let membership = screen
                    .user
                    .as_ref()
                    .and_then(|user| user.active_membership())
                    .filter(|membership| membership.can_bid());

                let bid = membership.map(|membership| {
                        json!({
                            "title": match (membership.bid_for_current_bidding(), membership.shares()) {
                                (Some(bid), Shares::One) => session.tr_args(
                                    "bidding_current_bid",
                                    &std::iter::once(("euros".into(), session.format_cents_as_euros(bid.cents(), 2).into())).chain(std::iter::once(("euros_year".into(), session.format_cents_as_euros(bid.cents() * 12, 2).into()))).collect(),
                                ),
                                (Some(bid), shares) => session.tr_args(
                                    "bidding_current_bid_n_shares",
                                    &std::iter::once(("euros".into(), session.format_cents_as_euros(bid.cents(), 2).into())).chain(std::iter::once(("euros_year".into(), session.format_cents_as_euros(bid.cents() * 12, 2).into()))).chain(std::iter::once(("shares_euros".into(), (bid.cents() as f32 / 100.0 / f32::from(shares)).into()))).collect(),
                                ),
                                (None, _) => session.tr("bidding_no_bid"),
                            },
                            "euros": membership.bid_for_current_bidding().map(|bid|bid.cents() as f32 / 100.0),
                            "shares": match membership.shares() {
                                Shares::Half => session.tr("bidding_half_share"),
                                Shares::One => session.tr("bidding_one_share"),
                                Shares::OneAndHalf => session.tr("bidding_oneandhalf_shares"),
                                Shares::Two => session.tr("bidding_two_shares"),
                            },
                            "bid_label": session.tr("bidding_bid"),
                        })
                    });

                let admin = screen
                        .user
                        .as_ref()
                        .filter(|user| user.access_level().admin())
                        .map(|_user| {
                            json!({
                                "average": session.tr_args(
                                    "bidding_average_euros",
                                    &std::iter::once(("euros".into(), session.format_cents_as_euros(bidding_calc.avg_cents(), 2).into())).collect(),
                                ),
                                "extrapolated": session.tr_args(
                                    "bidding_extrapolated",
                                    &std::iter::once(("euros".into(), session.format_number(bidding_calc.extrapolated_budget() as f32 / 100.0, 0).into())).collect(),
                                ),
                                "bidding_shares": session.tr_args(
                                    "bidding_shares",
                                    &std::iter::once(("bidded_shares".into(), session.format_number(bidding_calc.sum_shares(), 1).into())).chain(std::iter::once(("total_shares".into(), session.format_number(Membership::bid_shares(), 1).into()))).collect(),
                                ),
                                "distribution_plot": bidding_calc.distribution_plot().expect("Could not create distribution plot"),
                                "csv": "/csv/bidding"
                            })
                        });

                if !bidding.is_active() || (admin.is_none() && bid.is_none()) {
                    ().into()
                } else {
                    json!({
                        "title": session.tr_args(
                            "bidding_title",
                            &std::iter::once(("year".into(), bidding.year().into())).collect(),
                        ),
                        "bid": bid,
                        "admin": admin,
                        "end_at": crate::common::date_string_de(&bidding.end_at()),
                    })
                }
            },
            interaction_name,
            true,
            false,
            (),
        )
    }

    async fn action(
        &self,
        session: &mut GacoSession,
        payload: &wallaby::ActionPayload,
    ) -> Vec<wallaby::Event> {
        #[derive(Deserialize)]
        struct BidPayload {
            bid: String,
        }

        let bidding = crate::model::bidding::Bidding::get();

        if let (Some(mut membership), Some(euros)) = (
            session
                .user()
                .await
                .and_then(|user| user.active_membership())
                .filter(|membership| membership.can_bid()),
            payload
                .extra_fields::<BidPayload>()
                .and_then(|payload| payload.bid.replace(',', ".").parse::<f32>().ok())
                .filter(|bid| *bid > 0.0),
        ) {
            let cents = (euros * 100.0).round() as u32;
            let now = crate::common::now_date();
            match (bidding.end_at() >= now, bidding.correction_until() >= now) {
                (true, _) => (),
                (false, true) => {
                    if let Some(bid) = membership.bid_for_current_bidding() {
                        if bid.cents() > cents {
                            return vec![Event::Message(Message::error(
                                session.tr("bidding_error_correction"),
                            ))];
                        }
                    }
                }
                (false, false) => {
                    return vec![Event::Message(Message::error(
                        session.tr("bidding_error_ended"),
                    ))];
                }
            }
            let now = Utc::now().naive_utc().date();
            let start_of_next_year =
                NaiveDate::from_ymd_opt(now.year() + 1, 1, 1).expect("Could not create NaiveDate");
            membership.add_bid(Bid::new(cents, start_of_next_year));
            if let Err(err) = membership.save().await {
                return vec![wallaby::Event::Message(wallaby::Message::error(format!(
                    "Could not save bidding: {err:?}"
                )))];
            }

            wallaby::update_all_screens().await;
            return vec![Event::Message(Message::success(
                session.tr("bidding_success"),
            ))];
        }

        vec![]
    }
}
