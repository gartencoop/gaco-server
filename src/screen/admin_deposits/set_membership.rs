use super::AdminDeposits;
use crate::{
    model::{deposit::Deposit, membership::Membership},
    GacoSession,
};
use serde::Deserialize;
use wallaby::*;

pub struct SetMembership;

#[async_trait::async_trait]
impl Input<AdminDeposits, GacoSession> for SetMembership {
    async fn action(&self, session: &mut GacoSession, payload: &ActionPayload) -> Vec<Event> {
        if !session.access_level().await.admin() {
            return vec![Event::Message(Message::error(
                session.tr("admin_bidding_error"),
            ))];
        }

        #[derive(Deserialize)]
        struct Payload {
            #[serde(default)]
            deposit_ids: Vec<String>,
            membership_id: u32,
        }

        match payload.extra_fields::<Payload>() {
            Some(payload) => {
                let mut deposits = vec![];
                for deposit_id in payload.deposit_ids.into_iter() {
                    let Some(deposit) = Deposit::find_by_id(&deposit_id).await else {
                        return vec![Event::Message(Message::error(format!(
                            "Could not find deposit \"{}\"",
                            deposit_id
                        )))];
                    };
                    deposits.push(deposit);
                }

                if deposits.is_empty() {
                    return vec![Event::Message(Message::error(
                        "Could not find any deposits".to_string(),
                    ))];
                }

                if Membership::find_for_id(payload.membership_id).is_none() {
                    return vec![Event::Message(Message::error(format!(
                        "Could not find membership \"{}\"",
                        payload.membership_id
                    )))];
                };

                for deposit in deposits.iter_mut() {
                    deposit.set_membership_id(payload.membership_id);
                }

                match crate::couchdb::save_batch_owned(deposits).await {
                    Ok(_) => vec![Event::Message(Message::success(
                        "Saved deposits".to_string(),
                    ))],
                    Err(err) => vec![Event::Message(Message::error(format!(
                        "Could not save deposits: {err:?}"
                    )))],
                }
            }
            _ => vec![Event::Message(Message::error(
                "Data is not valid".to_string(),
            ))],
        }
    }
}
