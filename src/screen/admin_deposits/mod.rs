mod set_membership;
mod upload_csv;

use super::common::Common;
use crate::{
    interaction::{
        deposits::{Deposits, DepositsSum, ScreenDeposits},
        memberships::{Memberships, ScreenMemberships},
        transactions::ScreenTransactions,
    },
    model::{deposit::Deposit, membership::Membership},
    GacoSession,
};
use set_membership::SetMembership;
use upload_csv::UploadCsv;
use wallaby::*;

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession", hooks(load_data))]
pub struct AdminDeposits {
    #[screen]
    common: Common,

    #[interaction(ty = "output")]
    memberships: Memberships,

    #[interaction(ty = "output")]
    deposits: Deposits,

    #[interaction(ty = "output")]
    deposits_sum: DepositsSum,

    #[interaction(ty = "input")]
    set_membership: SetMembership,

    #[interaction(ty = "input")]
    upload_csv: UploadCsv,

    memberships_data: Option<Vec<Membership>>,
    deposits_data: Option<Vec<Deposit>>,
}

#[async_trait::async_trait]
impl LoadData<GacoSession> for AdminDeposits {
    async fn load_data(&mut self, session: &GacoSession) -> Result<(), wallaby::Error> {
        if !session.access_level().await.admin() {
            return Err(wallaby::Error::ScreenNotFound(
                "AdminMemberships".to_string(),
            ));
        }

        self.memberships_data = Some(Membership::find_all());
        self.deposits_data = Some(Deposit::find_all().await);

        Ok(())
    }
}

impl ScreenMemberships for AdminDeposits {
    fn memberships(&self) -> Vec<&Membership> {
        self.memberships_data
            .as_ref()
            .map(|memberships| memberships.iter().collect())
            .unwrap_or_default()
    }
}

impl ScreenDeposits for AdminDeposits {
    fn deposits(&self) -> Vec<&Deposit> {
        self.deposits_data
            .as_ref()
            .map(|deposits| deposits.iter().collect())
            .unwrap_or_default()
    }
}

impl ScreenTransactions for AdminDeposits {
    fn transactions(&self) -> Vec<&crate::model::transaction::Transaction> {
        vec![]
    }
}
