use super::AdminTransactions;
use crate::{model::transaction::Transaction, GacoSession};
use serde::Deserialize;
use wallaby::*;

pub struct SetYear;

#[async_trait::async_trait]
impl Input<AdminTransactions, GacoSession> for SetYear {
    async fn action(&self, session: &mut GacoSession, payload: &ActionPayload) -> Vec<Event> {
        if !session.access_level().await.admin() {
            return vec![Event::Message(Message::error(
                session.tr("admin_bidding_error"),
            ))];
        }

        #[derive(Deserialize)]
        struct Payload {
            transaction_id: String,
            year: u16,
        }

        match payload.extra_fields::<Payload>() {
            Some(payload) => {
                let Some(mut transaction) = Transaction::find_by_id(&payload.transaction_id).await else {
                    return vec![Event::Message(Message::error(format!(
                        "Could not find transaction \"{}\"",
                        payload.transaction_id
                    )))];
                };

                transaction.set_year(payload.year);

                match transaction.save().await {
                    Ok(_) => vec![Event::Message(Message::success(
                        "Saved transaction".to_string(),
                    ))],
                    Err(err) => vec![Event::Message(Message::error(format!(
                        "Could not save transaction: {err:?}"
                    )))],
                }
            }
            _ => vec![Event::Message(Message::error(
                "Data is not valid".to_string(),
            ))],
        }
    }
}
