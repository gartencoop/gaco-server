use super::AdminTransactions;
use crate::{model::transaction::csv::import, GacoSession};
use wallaby::*;

pub struct UploadCsv;

#[async_trait::async_trait]
impl Input<AdminTransactions, GacoSession> for UploadCsv {
    async fn action(&self, session: &mut GacoSession, payload: &ActionPayload) -> Vec<Event> {
        if !session.access_level().await.admin() {
            return vec![Event::Message(Message::error(
                session.tr("admin_bidding_error"),
            ))];
        }

        let message = match payload.extra_fields::<StringValuePayload>() {
            Some(payload) => {
                let csv = payload.value.into_bytes();
                match import(csv).await {
                    Ok((inserted_documents, removed_documents)) => Message::success(format!(
                        "Es wurden {inserted_documents} neue Datensätze importiert, {removed_documents} wurden gelöscht."
                    )),
                    Err(err) => Message::error(format!("Fehler beim importieren: {err:?}")),
                }
            }
            _ => Message::error("Data is not valid".to_string()),
        };

        vec![Event::Message(message)]
    }
}
