mod select_year;
mod set_year;
mod upload_csv;

use super::common::Common;
use crate::{
    interaction::{
        deposits::ScreenDeposits,
        memberships::{Memberships, ScreenMemberships},
        transaction_set_membership::TransactionSetMembership,
        transactions::{ScreenTransactions, Transactions, TransactionsSum},
    },
    model::{membership::Membership, transaction::Transaction},
    routing::{Page, Routing},
    GacoSession,
};
use chrono::Datelike;
use select_year::SelectYear;
use set_year::SetYear;
use upload_csv::UploadCsv;
use wallaby::*;

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession", hooks(load_data))]
pub struct AdminTransactions {
    #[screen]
    common: Common,

    #[interaction(ty = "output")]
    memberships: Memberships,

    #[interaction(ty = "output")]
    transactions: Transactions,

    #[interaction(ty = "output")]
    transactions_sum: TransactionsSum,

    #[interaction(ty = "input")]
    transaction_set_membership: TransactionSetMembership,

    #[interaction(ty = "input")]
    set_year: SetYear,

    #[interaction(ty = "selection")]
    select_year: SelectYear,

    #[interaction(ty = "input")]
    upload_csv: UploadCsv,

    memberships_data: Option<Vec<Membership>>,
    transactions_data: Option<Vec<Transaction>>,
    years_data: Option<Vec<u16>>,
}

#[async_trait::async_trait]
impl LoadData<GacoSession> for AdminTransactions {
    async fn load_data(&mut self, session: &GacoSession) -> Result<(), wallaby::Error> {
        if !session.access_level().await.admin() {
            return Err(wallaby::Error::ScreenNotFound(
                "AdminMemberships".to_string(),
            ));
        }

        let year = match session.routing() {
            Routing::Page(Page::AdminTransactions { year }) => *year,
            _ => chrono::Utc::now().year() as u16,
        };

        self.memberships_data = Some(Membership::find_all());
        self.transactions_data = Some(Transaction::find_for_year(year).await);
        self.years_data = Some(Transaction::find_years().await);

        Ok(())
    }
}

impl ScreenMemberships for AdminTransactions {
    fn memberships(&self) -> Vec<&Membership> {
        self.memberships_data
            .as_ref()
            .map(|memberships| memberships.iter().collect())
            .unwrap_or_default()
    }
}

impl ScreenTransactions for AdminTransactions {
    fn transactions(&self) -> Vec<&Transaction> {
        self.transactions_data
            .as_ref()
            .map(|transactions| transactions.iter().collect())
            .unwrap_or_default()
    }
}

impl ScreenDeposits for AdminTransactions {
    fn deposits(&self) -> Vec<&crate::model::deposit::Deposit> {
        // not needed for this screen
        vec![]
    }
}
