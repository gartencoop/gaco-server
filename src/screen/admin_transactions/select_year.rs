use super::AdminTransactions;
use crate::{
    routing::{Page, Routing},
    GacoSession,
};
use chrono::Datelike;
use serde::Deserialize;
use wallaby::*;

pub struct SelectYear;

#[async_trait::async_trait]
impl Selection<AdminTransactions, GacoSession> for SelectYear {
    fn render(
        &self,
        interaction_name: String,
        screen: &AdminTransactions,
        session: &GacoSession,
    ) -> RenderedSelection {
        let current_year = chrono::Utc::now().year() as u16;
        let year = match session.routing() {
            Routing::Page(Page::AdminTransactions { year }) => *year,
            _ => current_year,
        };

        let options = match screen.years_data.as_ref() {
            None => vec![],
            Some(years) => {
                let years_iter = years
                    .iter()
                    .map(|year| SelectionOption::new(*year, format!("{year}")));
                if years.contains(&current_year) {
                    years_iter.collect()
                } else {
                    years_iter
                        .chain(std::iter::once(SelectionOption::new(
                            current_year,
                            format!("{current_year}"),
                        )))
                        .collect()
                }
            }
        };

        RenderedSelection::new(year, interaction_name, options, ())
    }

    async fn action(&self, session: &mut GacoSession, payload: &ActionPayload) -> Vec<Event> {
        if !session.access_level().await.admin() {
            return vec![Event::Message(Message::error(
                session.tr("admin_bidding_error"),
            ))];
        }

        #[derive(Deserialize)]
        struct Payload {
            year: u16,
        }

        match payload.extra_fields::<Payload>() {
            Some(payload) => {
                session.set_routing(Routing::Page(Page::AdminTransactions {
                    year: payload.year,
                }));
                vec![]
            }
            _ => vec![Event::Message(Message::error(
                "Data is not valid".to_string(),
            ))],
        }
    }
}
