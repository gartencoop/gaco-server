use super::common::Common;
use crate::model::event::{Category, Location};
use crate::model::html::Html;
use crate::model::user::User;
use crate::screen::document::DocumentLock;
use crate::GacoSession;
use crate::{couchdb::CouchdbDocument, CONFIG};
use chrono::{DateTime, Utc};
use log::info;
use serde_json::{json, Value};
use wallaby::*;

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession", hooks(load_data))]
pub struct Event {
    #[screen]
    common: Common,

    #[interaction]
    event: EventInteraction,

    data: Option<crate::model::event::Event>,
    template: Option<crate::model::document::Document>,
    user: Option<User>,
}

#[async_trait::async_trait]
impl LoadData<GacoSession> for Event {
    async fn load_data(&mut self, session: &GacoSession) -> Result<(), Error> {
        if let Some(id) = session.routing().event_id() {
            self.data = crate::model::event::Event::find_for_id(id).await;
            if let Some(event) = self.data.as_ref() {
                self.template = event.template().await;
            }
        }
        self.user = session.user().await;

        Ok(())
    }
}

pub struct EventInteraction;

#[async_trait::async_trait]
impl Interaction<Event, GacoSession> for EventInteraction {
    fn render(
        &self,
        _interaction_name: String,
        screen: &Event,
        session: &GacoSession,
    ) -> RenderedInteraction {
        let show_creator_names = screen
            .user
            .as_ref()
            .map(|user| user.access_level().user())
            .unwrap_or_default();

        let value = if let Some(event) = screen.data.as_ref() {
            json!({
                "title": event.title().to_owned(),
                "category": event.category(),
                "created_at": event.created_at().to_rfc3339(),
                "created_by": event.created_by_name().filter(|_|show_creator_names),
                "start_at": event.start_at().to_rfc3339(),
                "end_at": event.end_at().to_rfc3339(),
                "body": event.description(),
                "takealong": event.takealong(),
                "location": event.location(),
                "images": event.image_paths(),
                "files": event.files(show_creator_names),
                "comments": event.comments().iter().map(|comment| {
                    comment.view(event.meta().id_number(), show_creator_names, session.locale())
                }).collect::<Vec<_>>(),
                "template_id": screen.data.as_ref().and_then(|event| event.template_id()),
                "template_title": screen.template.as_ref().map(|template| template.title()),
                "template_body": screen.template.as_ref().map(|template| template.body()),
            })
        } else {
            Value::Null
        };

        RenderedInteraction::new(value, self.action_types())
    }

    async fn action(
        &self,
        session: &mut GacoSession,
        payload: &ActionPayload,
    ) -> Vec<wallaby::Event> {
        if !CONFIG.modify_users_or_documents {
            return vec![wallaby::Event::Message(Message::error(
                "Document modification is disabled".to_string(),
            ))];
        }
        let Some(user_id) = session.uid().await else {
            return vec![wallaby::Event::Message(Message::error(
                "Unauthorized access".to_string(),
            ))];
        };
        let Some(event_id) = session.routing().event_id() else {
            return vec![wallaby::Event::Message(Message::error(
                "Missing event id".to_string(),
            ))];
        };
        let Some(mut event) = crate::model::event::Event::find_for_id(event_id).await else {
            return vec![wallaby::Event::Message(wallaby::Message::error(
                "Event not found".to_string(),
            ))];
        };
        if payload.action_type() == Some("edit") {
            info!("Entering edit mode");
            session.edit_mode = true;
            return vec![];
        }
        if payload.action_type() == Some("close") {
            info!("Closing edit mode");
            DocumentLock::unlock(event_id, user_id).await;
            session.edit_mode = false;
            return vec![];
        }

        if DocumentLock::lock(event_id, user_id).await.is_err() {
            return vec![wallaby::Event::Message(Message::error(
                "Document is locked by another user".to_string(),
            ))];
        }

        match payload.action_type() {
            Some("touch") => {
                info!("Touching document");
                vec![]
            }
            Some("save") => {
                #[derive(Default, serde::Deserialize)]
                #[serde(default)]
                struct Payload {
                    html: Option<Html>,
                    title: Option<String>,
                    start_at: Option<DateTime<Utc>>,
                    end_at: Option<DateTime<Utc>>,
                    takealong: Option<Html>,
                    location: Option<Location>,
                    category: Option<Category>,
                    template_id: Option<u32>,
                }

                let Some(payload) = payload.extra_fields::<Payload>() else {
                    return vec![wallaby::Event::Message(wallaby::Message::error(
                        "Invalid payload".to_string(),
                    ))];
                };

                if let Some(html) = payload.html {
                    info!("Setting body to {html:?}");
                    event.set_description(html);
                }

                if let Some(title) = payload.title {
                    info!("Setting title to {title}");
                    event.set_title(title);
                }

                if let Some(start_at) = payload.start_at {
                    info!("Setting start_at to {start_at:?}");
                    event.set_start_at(start_at);
                }

                if let Some(end_at) = payload.end_at {
                    info!("Setting end_at to {end_at:?}");
                    event.set_end_at(end_at);
                }

                if let Some(takealong) = payload.takealong {
                    info!("Setting takealong to {takealong:?}");
                    event.set_takealong(takealong);
                }

                if let Some(location) = payload.location {
                    info!("Setting location to {location:?}");
                    event.set_location(location);
                }

                if let Some(category) = payload.category {
                    info!("Setting category to {category:?}");
                    event.set_category(category);
                }

                if let Some(template_id) = payload.template_id {
                    info!("Setting template_id to {template_id}");
                    event.set_template_id(Some(template_id));
                }

                if let Err(err) = event.save().await {
                    return vec![wallaby::Event::Message(wallaby::Message::error(
                        err.to_string(),
                    ))];
                };

                vec![]
            }
            Some("add_file") => {
                #[derive(Default, serde::Deserialize)]
                #[serde(default)]
                struct Payload {
                    name: String,
                    #[serde(deserialize_with = "crate::common::deserialize_data_url")]
                    data: Vec<u8>,
                }

                let Some(payload) = payload.extra_fields::<Payload>() else {
                    return vec![wallaby::Event::Message(wallaby::Message::error(
                        "Invalid payload".to_string(),
                    ))];
                };

                info!("Adding file: {}", payload.name);

                if let Err(err) = event.add_file(user_id, payload.name, payload.data).await {
                    return vec![wallaby::Event::Message(wallaby::Message::error(
                        err.to_string(),
                    ))];
                }

                vec![]
            }
            Some("remove_file") => {
                #[derive(Default, serde::Deserialize)]
                #[serde(default)]
                struct Payload {
                    path: String,
                }

                let Some(payload) = payload.extra_fields::<Payload>() else {
                    return vec![wallaby::Event::Message(wallaby::Message::error(
                        "Invalid payload".to_string(),
                    ))];
                };

                info!("Removing file: {}", payload.path);
                if let Err(err) = event.remove_file(&payload.path).await {
                    return vec![wallaby::Event::Message(wallaby::Message::error(
                        err.to_string(),
                    ))];
                };

                vec![]
            }
            Some("remove_template") => {
                info!("Removing template");
                event.set_template_id(None);
                event.save().await.ok();
                vec![]
            }
            Some(action) => vec![wallaby::Event::Message(Message::error(format!(
                "Invalid action: {action}"
            )))],
            None => vec![wallaby::Event::Message(Message::error(
                "Missing action".to_string(),
            ))],
        }
    }

    fn actions(&self) -> Vec<Action> {
        vec![
            Action::new("edit", None),
            Action::new("close", None),
            Action::new("save", None),
            Action::new("touch", None),
            Action::new("add_file", None),
            Action::new("remove_file", None),
            Action::new("remove_template", None),
        ]
    }
}
