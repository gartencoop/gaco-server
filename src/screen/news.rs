use super::common::Common;
use crate::model::document::{Document, DocumentType};
use crate::GacoSession;
use wallaby::*;

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession", hooks(load_data))]
pub struct News {
    #[screen]
    common: Common,

    #[table(data = "news_data")]
    news: super::document::DocumentView,

    news_data: Option<Vec<Document>>,
}

#[async_trait::async_trait]
impl LoadData<GacoSession> for News {
    async fn load_data(&mut self, _session: &GacoSession) -> Result<(), wallaby::Error> {
        self.news_data = Document::find_newest(vec![DocumentType::News], None, None)
            .await
            .ok();

        Ok(())
    }
}
