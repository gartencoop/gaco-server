use super::common::Common;
use crate::{
    interaction::users::{ScreenUsers, Users},
    model::user::User,
    GacoSession,
};
use wallaby::*;

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession", hooks(load_data))]
pub struct AdminUsers {
    #[screen]
    common: Common,

    #[interaction(ty = "output")]
    users: Users,

    user: Option<User>,
    users_data: Option<Vec<User>>,
}

#[async_trait::async_trait]
impl LoadData<GacoSession> for AdminUsers {
    async fn load_data(&mut self, session: &GacoSession) -> Result<(), Error> {
        self.user = session.user().await;

        if let Some(user) = self.user.as_ref() {
            if !user.access_level().admin() {
                return Err(wallaby::Error::ScreenNotFound(
                    "AdminMembership".to_string(),
                ));
            }
        }

        self.users_data = Some(User::find_all());

        Ok(())
    }
}

impl ScreenUsers for AdminUsers {
    fn users(&self, _interaction_name: &str) -> Vec<&User> {
        self.users_data
            .as_ref()
            .map(|users| users.iter().collect())
            .unwrap_or_default()
    }
}
