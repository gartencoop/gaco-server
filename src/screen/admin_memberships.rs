use super::common::Common;
use crate::{
    change_path::change_path_event,
    interaction::{
        deposits::ScreenDeposits,
        memberships::{Memberships, ScreenMemberships},
        transactions::ScreenTransactions,
    },
    model::{
        deposit::Deposit,
        membership::{Membership, Shares, Subscription},
        transaction::Transaction,
    },
    routing::{Page, Routing},
    GacoSession,
};
use serde_json::{Map, Value};
use std::collections::HashMap;
use wallaby::*;

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession", hooks(load_data))]
pub struct AdminMemberships {
    #[screen]
    common: Common,

    #[interaction(ty = "button")]
    create_membership: CreateMembership,

    #[interaction(ty = "output")]
    memberships: Memberships,

    #[interaction(ty = "output")]
    stats: MembershipStats,

    #[interaction(ty = "output")]
    total_bid_cents_for_current_year: TotalBidCentsForCurrentYear,

    data: Option<Vec<Membership>>,
    transactions_data: Option<Vec<Transaction>>,
    deposits_data: Option<Vec<Deposit>>,
}

#[async_trait::async_trait]
impl LoadData<GacoSession> for AdminMemberships {
    async fn load_data(&mut self, session: &GacoSession) -> Result<(), wallaby::Error> {
        if !session.access_level().await.admin() {
            return Err(wallaby::Error::ScreenNotFound(
                "AdminMemberships".to_string(),
            ));
        }

        self.data = Some(Membership::find_all());
        self.transactions_data = Some(Transaction::find_all().await);
        self.deposits_data = Some(Deposit::find_all().await);

        Ok(())
    }
}

impl ScreenMemberships for AdminMemberships {
    fn memberships(&self) -> Vec<&crate::model::membership::Membership> {
        self.data
            .as_ref()
            .map(|memberships| memberships.iter().collect())
            .unwrap_or_default()
    }
}

impl ScreenTransactions for AdminMemberships {
    fn transactions(&self) -> Vec<&crate::model::transaction::Transaction> {
        self.transactions_data
            .as_ref()
            .map(|transaction| transaction.iter().collect())
            .unwrap_or_default()
    }
}

impl ScreenDeposits for AdminMemberships {
    fn deposits(&self) -> Vec<&crate::model::deposit::Deposit> {
        self.deposits_data
            .as_ref()
            .map(|deposits| deposits.iter().collect())
            .unwrap_or_default()
    }
}

struct MembershipStats;

impl Output<AdminMemberships, GacoSession> for MembershipStats {
    fn render(
        &self,
        _interaction_name: String,
        screen: &AdminMemberships,
        session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            screen.data.as_ref().map(|memberships| {
                let mut waiting_list = 0;
                let mut no_vegetables = 0;
                let mut half_shares = 0;
                let mut shares = 0;
                let mut one_and_half_shares = 0;
                let mut two_shares = 0;
                let mut free_half_shares = 0;
                let mut free_shares = 0;
                let mut free_one_and_half_shares = 0;
                let mut free_two_shares = 0;
                let mut test_half_shares = 0;
                let mut test_shares = 0;
                let mut test_one_and_half_shares = 0;
                let mut test_two_shares = 0;

                for membership in memberships
                    .iter()
                    .filter(|membership| membership.is_active())
                {
                    match (membership.subscription(), membership.shares()) {
                        (Subscription::WaitingList, _) => waiting_list += 1,
                        (Subscription::NoVegetables, _) => no_vegetables += 1,
                        (Subscription::Active, Shares::Half) => half_shares += 1,
                        (Subscription::Active, Shares::One) => shares += 1,
                        (Subscription::Active, Shares::OneAndHalf) => one_and_half_shares += 1,
                        (Subscription::Active, Shares::Two) => two_shares += 1,
                        (Subscription::FreeVegetables | Subscription::Team, Shares::Half) => {
                            free_half_shares += 1
                        }
                        (Subscription::FreeVegetables | Subscription::Team, Shares::One) => {
                            free_shares += 1
                        }
                        (Subscription::FreeVegetables | Subscription::Team, Shares::OneAndHalf) => {
                            free_one_and_half_shares += 1
                        }
                        (Subscription::FreeVegetables | Subscription::Team, Shares::Two) => {
                            free_two_shares += 1
                        }
                        (Subscription::TestMembership, Shares::Half) => test_half_shares += 1,
                        (Subscription::TestMembership, Shares::One) => test_shares += 1,
                        (Subscription::TestMembership, Shares::OneAndHalf) => {
                            test_one_and_half_shares += 1
                        }
                        (Subscription::TestMembership, Shares::Two) => test_two_shares += 1,
                    }
                }

                let mut stats = HashMap::new();

                stats.insert(session.tr("stats_waiting_list"), waiting_list);
                stats.insert(session.tr("stats_no_vegetables"), no_vegetables);
                stats.insert(session.tr("stats_half_shares"), half_shares);
                stats.insert(session.tr("stats_shares"), shares);
                stats.insert(session.tr("stats_one_and_half_shares"), one_and_half_shares);
                stats.insert(session.tr("stats_two_shares"), two_shares);
                stats.insert(session.tr("stats_free_half_shares"), free_half_shares);
                stats.insert(session.tr("stats_free_shares"), free_shares);
                stats.insert(
                    session.tr("stats_free_one_and_half_shares"),
                    free_one_and_half_shares,
                );
                stats.insert(session.tr("stats_free_two_shares"), free_two_shares);
                stats.insert(session.tr("stats_test_half_shares"), test_half_shares);
                stats.insert(session.tr("stats_test_shares"), test_shares);
                stats.insert(
                    session.tr("stats_test_one_and_half_shares"),
                    test_one_and_half_shares,
                );
                stats.insert(session.tr("stats_test_two_shares"), test_two_shares);

                stats
                    .into_iter()
                    .filter(|(_k, v)| *v > 0)
                    .map(|(k, v)| (k, v.into()))
                    .collect::<Map<_, Value>>()
            }),
            (),
        )
    }
}

struct CreateMembership;

#[async_trait::async_trait]
impl Button<AdminMemberships, GacoSession> for CreateMembership {
    async fn action(&self, session: &mut GacoSession, _payload: &ActionPayload) -> Vec<Event> {
        let mut membership = Membership::new();
        session.set_routing(Routing::Page(Page::AdminMembership {
            member_id: membership.id(),
        }));
        let member_id = membership.id();
        if let Err(err) = membership.save().await {
            return vec![Event::Message(Message::error(format!(
                "Es konnte keine neue Mitgliedschaft angelegt werden: {err:?}"
            )))];
        }

        vec![change_path_event(&format!("/admin_membership_{member_id}"))]
    }
}

struct TotalBidCentsForCurrentYear;

impl Output<AdminMemberships, GacoSession> for TotalBidCentsForCurrentYear {
    fn render(
        &self,
        _interaction_name: String,
        screen: &AdminMemberships,
        _session: &GacoSession,
    ) -> RenderedOutput {
        let cents: u32 = screen
            .data
            .as_ref()
            .map(|memberships| {
                memberships
                    .iter()
                    .map(|membership| membership.bid_cents_for_current_year())
                    .sum()
            })
            .unwrap_or_default();
        RenderedOutput::new(cents, ())
    }
}
