use super::common::Common;
use crate::{model::bidding::Bidding, GacoSession};
use wallaby::*;

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession")]
pub struct BiddingPage {
    #[screen]
    common: Common,

    #[interaction(ty = "output")]
    title: Title,

    #[interaction(ty = "output")]
    subtitle: Subtitle,

    #[interaction(ty = "output")]
    text: Text,
}

struct Title;

impl Output<BiddingPage, GacoSession> for Title {
    fn render(
        &self,
        _interaction_name: String,
        _screen: &BiddingPage,
        session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(session.tr("bidding_page_title"), ())
    }
}

struct Subtitle;

impl Output<BiddingPage, GacoSession> for Subtitle {
    fn render(
        &self,
        _interaction_name: String,
        _screen: &BiddingPage,
        session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(session.tr("bidding_page_subtitle"), ())
    }
}

struct Text;

impl Output<BiddingPage, GacoSession> for Text {
    fn render(
        &self,
        _interaction_name: String,
        _screen: &BiddingPage,
        session: &GacoSession,
    ) -> RenderedOutput {
        let bidding = Bidding::get();
        RenderedOutput::new(
            session.tr_args(
                "bidding_page_text",
                &std::iter::once(("year".into(), bidding.year().into()))
                    .chain(std::iter::once((
                        "goal_members".into(),
                        bidding.goal_members().into(),
                    )))
                    .chain(std::iter::once((
                        "goal_euros".into(),
                        session.format_number(bidding.goal_cents() / 100, 0).into(),
                    )))
                    .chain(std::iter::once((
                        "goal_euros_total".into(),
                        session
                            .format_number((bidding.goal_cents_total() / 100000) * 1000, 0)
                            .into(),
                    )))
                    .collect(),
            ),
            (),
        )
    }
}
