use super::common::Common;
use crate::{GacoSession, CONFIG};
use serde::Deserialize;
use serde_json::json;
use wallaby::{
    Event, Interaction, LoadData, Message, Output, RenderedInteraction, RenderedOutput,
    StringValuePayload,
};

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession", hooks(load_data))]
pub struct User {
    #[screen]
    common: Common,

    #[interaction]
    email: Email,

    #[interaction]
    password: Password,

    #[interaction]
    language: Language,

    #[interaction(ty = "output")]
    texts: Texts,

    user: Option<crate::model::user::User>,
}

#[async_trait::async_trait]
impl LoadData<GacoSession> for User {
    async fn load_data(&mut self, session: &GacoSession) -> Result<(), wallaby::Error> {
        self.user = session.user().await;

        Ok(())
    }
}

struct Texts;

impl Output<User, GacoSession> for Texts {
    fn render(
        &self,
        _interaction_name: String,
        _screen: &User,
        session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(
            json!({
                "change_profile_header": session.tr("change_profile_header"),
                "user": session.tr("change_profile_user"),
                "change_language": session.tr("change_profile_change_language"),
                "language": session.tr("change_profile_language"),
                "change_password": session.tr("change_profile_change_password"),
                "password": session.tr("change_profile_password"),
                "password2": session.tr("change_profile_password2"),
                "passwords_not_matching": session.tr("change_profile_passwords_not_matching"),
            }),
            (),
        )
    }
}

struct Email;

#[async_trait::async_trait]
impl Interaction<User, GacoSession> for Email {
    fn render(
        &self,
        _interaction_name: String,
        screen: &User,
        _session: &GacoSession,
    ) -> RenderedInteraction {
        RenderedInteraction::new(
            json!({"value": screen.user.as_ref().map(|user| user.email().to_string())}),
            self.action_types(),
        )
    }

    /*
    async fn action(
        &self,
        session: &mut GacoSession,
        payload: &wallaby::ActionPayload,
    ) -> Vec<wallaby::Event> {
        if !CONFIG.modify_users {
            return vec![Event::Message(Message::error(
                "User modification is disabled".to_string(),
            ))];
        }

        let Some(mut user) = session.user().await else {
            return vec![Event::Message(Message::error("User not found".to_string()))];
        };

        let Some(email) = payload
            .extra_fields::<StringValuePayload>()
            .map(|payload| payload.value)
        else {
            return vec![Event::Message(Message::error(
                "Email not found".to_string(),
            ))];
        };

        user.set_email(email);
        if let Err(err) = user.save().await {
            return vec![Event::Message(Message::error(format!(
                "Benutzer konnte nicht gespeichert werden: {err:?}"
            )))];
        }

        vec![Event::Message(Message::info(
            "Email wurde geändert".to_string(),
        ))]
    }
    */
}

struct Password;

#[async_trait::async_trait]
impl Interaction<User, GacoSession> for Password {
    async fn action(
        &self,
        session: &mut GacoSession,
        payload: &wallaby::ActionPayload,
    ) -> Vec<wallaby::Event> {
        if !CONFIG.modify_users_or_documents {
            return vec![Event::Message(Message::error(
                "User modification is disabled".to_string(),
            ))];
        }

        let Some(mut user) = session.user().await else {
            return vec![Event::Message(Message::error("User not found".to_string()))];
        };

        let Some(password) = payload
            .extra_fields::<StringValuePayload>()
            .map(|payload| payload.value)
        else {
            return vec![Event::Message(Message::error(
                "Password not found".to_string(),
            ))];
        };

        if let Err(err) = user.set_password(&password) {
            return vec![Event::Message(Message::error(format!(
                "Passwort konnte nicht gesetzt werden: {err:?}"
            )))];
        }

        if let Err(err) = user.save().await {
            return vec![Event::Message(Message::error(format!(
                "Benutzer konnte nicht gespeichert werden: {err:?}"
            )))];
        }

        vec![Event::Message(Message::info(
            session.tr("password_changed"),
        ))]
    }
}

struct Language;

#[async_trait::async_trait]
impl Interaction<User, GacoSession> for Language {
    fn render(
        &self,
        _interaction_name: String,
        screen: &User,
        _session: &GacoSession,
    ) -> RenderedInteraction {
        RenderedInteraction::new(
            json!({"value": screen.user.as_ref().map(|user|user.language())}),
            self.action_types(),
        )
    }

    async fn action(
        &self,
        session: &mut GacoSession,
        payload: &wallaby::ActionPayload,
    ) -> Vec<wallaby::Event> {
        if !CONFIG.modify_users_or_documents {
            return vec![Event::Message(Message::error(
                "User modification is disabled".to_string(),
            ))];
        }

        let Some(mut user) = session.user().await else {
            return vec![Event::Message(Message::error("User not found".to_string()))];
        };

        #[derive(Deserialize)]
        struct Payload {
            value: crate::model::language::Language,
        }

        let Some(language) = payload
            .extra_fields::<Payload>()
            .map(|payload| payload.value)
        else {
            return vec![Event::Message(Message::error(
                "Language not found".to_string(),
            ))];
        };

        session.set_language(&language);
        user.set_language(language);
        if let Err(err) = user.save().await {
            return vec![Event::Message(Message::error(format!(
                "Benutzer konnte nicht gespeichert werden: {err:?}"
            )))];
        }

        vec![Event::Message(Message::info(
            session.tr("language_changed"),
        ))]
    }
}
