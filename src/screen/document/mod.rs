mod lock;
mod view;

use super::common::Common;
use crate::GacoSession;

pub use lock::DocumentLock;
pub use view::DocumentView;

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession")]
pub struct Document {
    #[screen]
    common: Common,

    #[screen]
    view: DocumentView,
}
