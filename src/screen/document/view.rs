use super::DocumentLock;
use crate::{
    model::{
        document::{Team, Topic},
        html::Html,
        user::User,
    },
    GacoSession, CONFIG,
};
use chrono::{DateTime, Utc};
use log::info;
use serde::Serialize;
use serde_json::{json, Value};
use wallaby::{
    Action, ActionPayload, Error, Event, Interaction, LoadData, Message, RenderedInteraction,
    SetData,
};

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession", hooks(load_data))]
pub struct DocumentView {
    #[interaction]
    document: Document,

    edit_mode: EditMode,
    data: Option<crate::model::document::Document>,
    user: Option<User>,
}

#[derive(Default, Serialize)]
#[serde(rename_all = "camelCase", tag = "value", content = "until")]
pub enum EditMode {
    #[default]
    No,
    LockedByOtherUser(#[serde(serialize_with = "crate::common::serialize_datetime")] DateTime<Utc>),
    Yes,
}

impl SetData<crate::model::document::Document> for DocumentView {
    fn set_data(&mut self, data: &crate::model::document::Document) {
        self.data = Some(data.clone());
    }
}

#[async_trait::async_trait]
impl LoadData<GacoSession> for DocumentView {
    async fn load_data(&mut self, session: &GacoSession) -> Result<(), Error> {
        self.user = session.user().await;
        if let Some(document_id) = session.routing().document_id() {
            self.data = crate::model::document::Document::find_for_id(document_id).await;
            if let Some(user) = self.user.as_ref().filter(|_| session.edit_mode) {
                self.edit_mode = match DocumentLock::lock(document_id, user.id()).await {
                    Ok(_) => EditMode::Yes,
                    Err(until) => EditMode::LockedByOtherUser(until),
                };
            }
        }

        Ok(())
    }
}

pub struct Document;

#[async_trait::async_trait]
impl Interaction<DocumentView, GacoSession> for Document {
    fn render(
        &self,
        _interaction_name: String,
        screen: &DocumentView,
        _session: &GacoSession,
    ) -> RenderedInteraction {
        let show_creator_names = screen
            .user
            .as_ref()
            .map(|user| user.access_level().user())
            .unwrap_or_default();

        let value = if let Some(document) = screen.data.as_ref() {
            json!({
                "id": document.nid(),
                "title": document.title().to_owned(),
                "link": document.link(),
                "edit_link": document.edit_link(),
                "topic": document.topic(),
                "teams": document.teams(),
                "created_at": document.created_at().to_rfc3339(),
                "created_by": document.created_by_name().filter(|_|show_creator_names),
                "body": document.body(),
                "images": document.image_paths(),
                "files": document.files(show_creator_names),
                "edit_mode": screen.edit_mode,
            })
        } else {
            Value::Null
        };

        RenderedInteraction::new(value, self.action_types())
    }

    async fn action(&self, session: &mut GacoSession, payload: &ActionPayload) -> Vec<Event> {
        if !CONFIG.modify_users_or_documents {
            return vec![Event::Message(Message::error(
                "Document modification is disabled".to_string(),
            ))];
        }
        let Some(user_id) = session.uid().await else {
            return vec![Event::Message(Message::error(
                "Unauthorized access".to_string(),
            ))];
        };
        let Some(document_id) = session.routing().document_id() else {
            return vec![Event::Message(Message::error(
                "Missing document id".to_string(),
            ))];
        };
        let Some(mut document) = crate::model::document::Document::find_for_id(document_id).await
        else {
            return vec![wallaby::Event::Message(wallaby::Message::error(
                "Document not found".to_string(),
            ))];
        };
        if payload.action_type() == Some("edit") {
            info!("Entering edit mode");
            session.edit_mode = true;
            return vec![];
        }
        if payload.action_type() == Some("close") {
            info!("Closing edit mode");
            DocumentLock::unlock(document_id, user_id).await;
            session.edit_mode = false;
            return vec![];
        }

        if DocumentLock::lock(document_id, user_id).await.is_err() {
            return vec![Event::Message(Message::error(
                "Document is locked by another user".to_string(),
            ))];
        }

        match payload.action_type() {
            Some("touch") => {
                info!("Touching document");
                vec![]
            }
            Some("save") => {
                #[derive(Default, serde::Deserialize)]
                #[serde(default)]
                struct Payload {
                    html: Option<Html>,
                    title: Option<String>,
                    topic: Option<Topic>,
                    teams: Option<Vec<Team>>,
                }

                let Some(payload) = payload.extra_fields::<Payload>() else {
                    return vec![wallaby::Event::Message(wallaby::Message::error(
                        "Invalid payload".to_string(),
                    ))];
                };

                if let Some(title) = payload.title {
                    info!("Setting title to {title}");
                    document.set_title(title);
                }

                if let Some(html) = payload.html {
                    info!("Setting body to {html:?}");
                    document.set_body(html);
                }

                if let Some(topic) = payload.topic {
                    info!("Setting topic to {topic:?}");
                    document.set_topic(topic);
                }

                if let Some(teams) = payload.teams {
                    info!("Setting teams to {teams:?}");
                    document.set_teams(teams);
                }

                if let Err(err) = document.save().await {
                    return vec![wallaby::Event::Message(wallaby::Message::error(
                        err.to_string(),
                    ))];
                };

                vec![]
            }
            Some("add_file") => {
                #[derive(Default, serde::Deserialize)]
                #[serde(default)]
                struct Payload {
                    name: String,
                    #[serde(deserialize_with = "crate::common::deserialize_data_url")]
                    data: Vec<u8>,
                }

                let Some(payload) = payload.extra_fields::<Payload>() else {
                    return vec![wallaby::Event::Message(wallaby::Message::error(
                        "Invalid payload".to_string(),
                    ))];
                };

                info!("Adding file: {}", payload.name);

                if let Err(err) = document.add_file(user_id, payload.name, payload.data).await {
                    return vec![wallaby::Event::Message(wallaby::Message::error(
                        err.to_string(),
                    ))];
                }

                vec![]
            }
            Some("remove_file") => {
                #[derive(Default, serde::Deserialize)]
                #[serde(default)]
                struct Payload {
                    path: String,
                }

                let Some(payload) = payload.extra_fields::<Payload>() else {
                    return vec![wallaby::Event::Message(wallaby::Message::error(
                        "Invalid payload".to_string(),
                    ))];
                };

                info!("Removing file: {}", payload.path);
                if let Err(err) = document.remove_file(&payload.path).await {
                    return vec![wallaby::Event::Message(wallaby::Message::error(
                        err.to_string(),
                    ))];
                };

                vec![]
            }
            Some(action) => vec![Event::Message(Message::error(format!(
                "Invalid action: {action}"
            )))],
            None => vec![Event::Message(Message::error("Missing action".to_string()))],
        }
    }

    fn actions(&self) -> Vec<Action> {
        vec![
            Action::new("edit", None),
            Action::new("close", None),
            Action::new("save", None),
            Action::new("touch", None),
            Action::new("add_file", None),
            Action::new("remove_file", None),
        ]
    }
}
