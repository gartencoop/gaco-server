use chrono::{DateTime, Duration, Utc};
use tokio::sync::Mutex;

const MAX_LOCK_DURATION_MINUTES: i64 = 15;
static LOCKS: Mutex<Vec<DocumentLock>> = Mutex::const_new(Vec::new());

#[derive(Debug, Clone, Copy)]
pub struct DocumentLock {
    updated_at: DateTime<Utc>,
    user: u32,
    document: u32,
}

impl DocumentLock {
    pub fn periodically_clean_old_locks() {
        tokio::spawn(async move {
            loop {
                tokio::time::sleep(std::time::Duration::from_secs(1)).await;
                let mut locks = LOCKS.lock().await;
                let now = Utc::now();
                locks.retain(|lock| {
                    now - lock.updated_at < Duration::minutes(MAX_LOCK_DURATION_MINUTES)
                });
            }
        });
    }

    async fn find(document: u32) -> Option<DocumentLock> {
        let locks = LOCKS.lock().await;
        locks.iter().find(|lock| lock.document == document).cloned()
    }

    async fn touch(mut self) {
        self.updated_at = Utc::now();
        let mut locks = LOCKS.lock().await;
        if let Some(pos) = locks.iter().position(|lock| lock.document == self.document) {
            locks[pos] = self;
        } else {
            log::error!(
                "Lock not found for touching. Document: {}. User: {}",
                self.document,
                self.user
            );
        }
    }

    pub async fn lock(document: u32, user: u32) -> Result<(), DateTime<Utc>> {
        match Self::find(document).await {
            Some(lock) => {
                if lock.user == user {
                    lock.touch().await;
                    Ok(())
                } else {
                    Err(lock.updated_at + Duration::minutes(MAX_LOCK_DURATION_MINUTES))
                }
            }
            None => {
                LOCKS.lock().await.push(DocumentLock {
                    updated_at: Utc::now(),
                    user,
                    document,
                });
                Ok(())
            }
        }
    }

    pub async fn unlock(document: u32, user: u32) {
        let mut locks = LOCKS.lock().await;
        if let Some(pos) = locks
            .iter()
            .position(|lock| lock.document == document && lock.user == user)
        {
            locks.remove(pos);
        } else {
            log::error!("Lock not found for unlock. Document: {document}. User: {user}")
        }
    }
}
