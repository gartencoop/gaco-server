use super::common::Common;
use crate::GacoSession;
use wallaby::*;

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession", ui = "HtmlText")]
pub struct Impress {
    #[screen]
    common: Common,

    #[interaction(ty = "output")]
    title: Title,

    #[interaction(ty = "output")]
    text: Text,
}

struct Title;

impl Output<Impress, GacoSession> for Title {
    fn render(
        &self,
        _interaction_name: String,
        _screen: &Impress,
        session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(session.tr("impress_title"), ())
    }
}

struct Text;

impl Output<Impress, GacoSession> for Text {
    fn render(
        &self,
        _interaction_name: String,
        _screen: &Impress,
        session: &GacoSession,
    ) -> RenderedOutput {
        RenderedOutput::new(session.tr("impress_text"), ())
    }
}
