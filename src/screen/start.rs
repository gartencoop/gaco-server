use super::common::Common;
use crate::GacoSession;

#[derive(wallaby::derive::Screen)]
#[screen(session = "GacoSession")]
pub struct Start {
    #[screen]
    common: Common,
}
