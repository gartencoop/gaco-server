use super::{CouchdbDocument, CLIENT};
use anyhow::{Context, Result};
use bytes::Bytes;
use reqwest::StatusCode;
use serde::Deserialize;

#[derive(Debug)]
pub enum AttachmentResponse {
    NotFound,
    Found { etag: String, data: Bytes },
    NotModified,
    ServerError,
}

pub async fn download_attachment(id: &str, name: &str, etag: Option<&str>) -> AttachmentResponse {
    if id.contains('/') || name.contains('/') {
        return AttachmentResponse::ServerError;
    }
    let mut request = CLIENT.get(format!("{}/{id}/{name}", crate::CONFIG.couchdb_url));

    if let Some(etag) = etag {
        request = request.header(reqwest::header::IF_NONE_MATCH, etag);
    }

    let response = match request.send().await {
        Ok(request) => request,
        Err(_err) => return AttachmentResponse::ServerError,
    };

    match response.status() {
        StatusCode::OK => {
            let Some(etag) = response
                .headers()
                .get("etag")
                .and_then(|header| header.to_str().map(|str| str.to_string()).ok())
            else {
                return AttachmentResponse::ServerError;
            };
            match response.bytes().await {
                Ok(data) => AttachmentResponse::Found { etag, data },
                Err(_err) => AttachmentResponse::ServerError,
            }
        }
        StatusCode::NOT_MODIFIED => AttachmentResponse::NotModified,
        StatusCode::NOT_FOUND => AttachmentResponse::NotFound,
        _ => AttachmentResponse::ServerError,
    }
}

pub async fn upload_attachment<T: CouchdbDocument>(
    document: &mut T,
    name: &str,
    data: Vec<u8>,
) -> Result<()> {
    #[derive(Deserialize, Debug)]
    #[serde(untagged)]
    enum CouchdbResult {
        Ok {
            #[serde(rename = "rev")]
            _rev: String,
        },
        Error {
            id: String,
            error: String,
            reason: String,
        },
    }
    let id = document.id();
    let Some(revision) = document.revision() else {
        anyhow::bail!("Can not add attachment to document without revision");
    };
    let res: CouchdbResult = CLIENT
        .put(format!(
            "{}/{id}/{name}?rev={revision}",
            crate::CONFIG.couchdb_url
        ))
        .body(data)
        .send()
        .await
        .context("Could not upload attachment")?
        .error_for_status()?
        .json()
        .await
        .context("Could not load/parse json")?;

    log::info!("Uploaded attachment for document \"{id}\": \"{name}\": {res:?}");

    match res {
        CouchdbResult::Ok { .. } => {
            document.refresh().await?;
            Ok(())
        }
        CouchdbResult::Error { id, error, reason } => {
            anyhow::bail!("Could not upload attachment for document \"{id}\": {error} - {reason}");
        }
    }
}
