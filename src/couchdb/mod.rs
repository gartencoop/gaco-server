mod attachment;
mod bulk;
mod find;
mod meta;
mod remove;

use once_cell::sync::Lazy;
use reqwest::Client;

pub use attachment::*;
pub use bulk::*;
pub use find::*;
pub use meta::*;
pub use remove::*;

static CLIENT: Lazy<Client> = Lazy::new(Client::new);
