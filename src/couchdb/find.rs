use std::fmt::Debug;

use super::CLIENT;
use crate::{couchdb::meta::CouchdbMeta, model::user::User};
use anyhow::{Context, Result};
use serde::{de::DeserializeOwned, Deserialize};
use serde_json::{json, Value};

pub async fn view<T: DeserializeOwned>(path: &str) -> Result<Vec<T>> {
    #[derive(Deserialize)]
    struct Response<T> {
        rows: Vec<T>,
    }
    let response: Response<T> = CLIENT
        .get(format!("{}/{path}", crate::CONFIG.couchdb_url))
        .send()
        .await
        .context("Could not query {path}")?
        .error_for_status()
        .context("Error querying {path}")?
        .json()
        .await
        .context("Could not deserialize _find response")?;

    Ok(response.rows)
}

pub async fn find_documents<T: DeserializeOwned + Debug>(query: Value) -> Result<Vec<T>> {
    #[derive(Deserialize, Debug)]
    struct Response<T> {
        docs: Vec<T>,
    }
    let response: Result<Response<T>> = CLIENT
        .post(format!("{}/_find", crate::CONFIG.couchdb_url))
        .json(&query)
        .send()
        .await
        .context("Could not query _find")?
        .error_for_status()
        .context("Error querying _find")?
        .json()
        .await
        .with_context(|| format!("Could not deserialize _find response of query: {query:?}"));

    match response {
        Err(err) => {
            eprintln!("{err:?}");
            if let Ok(body) = CLIENT
                .post(format!("{}/_find", crate::CONFIG.couchdb_url))
                .json(&query)
                .send()
                .await
                .context("Could not query _find")?
                .error_for_status()
                .context("Error querying _find")?
                .text()
                .await
            {
                eprintln!("{body}");
            }
            Err(err)
        }
        Ok(res) => Ok(res.docs),
    }
}

pub async fn find_single_document<T: DeserializeOwned + Debug>(query: Value) -> Result<T> {
    find_documents(query)
        .await?
        .into_iter()
        .next()
        .ok_or_else(|| anyhow::format_err!("Could not find a single document"))
}

pub async fn find_next_id() -> Result<u32> {
    #[derive(Deserialize, Debug)]
    struct ResponseRow {
        nid: u32,
    }
    find_single_document::<ResponseRow>(json!({
        "selector": {
            "nid": {
                "$gt": 0u8
            }
        },
        "sort": [
            {
                "nid": "desc"
            }
        ],
        "fields": [
            "nid"
        ],
        "limit": 1u8
    }))
    .await
    .map(|row| row.nid + 1)
}

pub async fn find_next_user_id() -> Result<u32> {
    #[derive(Deserialize, Debug)]
    struct ResponseRow {
        id: u32,
    }
    find_single_document::<ResponseRow>(json!({
        "selector": {
            "_id": {
                "$gt": format!("{}_", User::ID_PREFIX),
                "$lt": format!("{}_{{}}", User::ID_PREFIX)
            }
        },
        "sort": [
            {
                "id": "desc"
            }
        ],
        "fields": [
            "id"
        ],
        "limit": 1u8
    }))
    .await
    .map(|response| response.id + 1)
}

pub async fn find_meta_for_path(path: &str) -> Result<CouchdbMeta> {
    if let Some(nid) = path
        .strip_prefix("node/")
        .and_then(|nid_string| nid_string.parse().ok())
    {
        return find_document_by_nid(nid).await;
    }

    #[derive(Deserialize, Debug)]
    struct ResponseDoc {
        #[serde(flatten)]
        couchdb_meta: CouchdbMeta,
    }

    find_single_document::<ResponseDoc>(json!({
        "selector": {
            "path": {
                "$eq": path
            }
        },
        "fields": [
            "_id",
            "_rev",
        ],
        "limit": 1u8
    }))
    .await
    .map(|doc| doc.couchdb_meta)
}

pub async fn find_document_by_nid<T: DeserializeOwned + Debug>(nid: u32) -> Result<T> {
    find_single_document(json!({
        "selector": {
            "nid": {
                "$eq": nid
            }
        },
        "limit": 1u8
    }))
    .await
}

pub async fn find_document_by_id<T: DeserializeOwned>(id: &str) -> Result<T> {
    // Sometimes links are broken up by email programms. This is a workaround for that.
    let id = id.replace("%20", "").replace(' ', "");

    let response: T = CLIENT
        .get(format!("{}/{id}", crate::CONFIG.couchdb_url))
        .send()
        .await
        .context("Could not query document")?
        .error_for_status()
        .context("Error querying document")?
        .json()
        .await
        .context("Could not deserialize _find response")?;

    Ok(response)
}
