use super::{CouchdbDocument, CLIENT};
use anyhow::{Context, Result};

pub async fn remove<T: CouchdbDocument>(doc: T) -> Result<()> {
    let Some(revision) = doc.revision() else {
        anyhow::bail!("Document does not have a revision")
    };

    CLIENT
        .delete(format!(
            "{}/{}?rev={revision}",
            crate::CONFIG.couchdb_url,
            doc.id(),
        ))
        .send()
        .await
        .context("Could not remove document")?
        .error_for_status()
        .context("Error removing document")?;

    Ok(())
}
