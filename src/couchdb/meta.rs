use once_cell::sync::Lazy;
use regex::{Captures, Regex};
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use serde_json::Value;
use std::{collections::HashMap, fmt::Display};

static ID_REGEX: Lazy<Regex> = Lazy::new(|| Regex::new(r"(?m)^(.*)_(\d+)$").unwrap());

#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq, Eq)]
#[serde(default)]
pub struct CouchdbMeta {
    #[serde(rename = "_id")]
    id: String,
    #[serde(rename = "_rev", skip_serializing_if = "Option::is_none")]
    revision: Option<String>,
    #[serde(rename = "_attachments", skip_serializing_if = "HashMap::is_empty")]
    attachments: HashMap<String, Value>,
}

impl CouchdbMeta {
    pub fn new<T: Display>(id_prefix: &str, name: T) -> Self {
        Self {
            id: format!("{id_prefix}_{name}"),
            ..Default::default()
        }
    }

    pub fn new_without_prefix(id: String) -> Self {
        Self {
            id,
            ..Default::default()
        }
    }

    pub fn id(&self) -> &str {
        &self.id
    }

    fn id_captures(&self) -> Option<Captures> {
        ID_REGEX.captures_iter(&self.id).next()
    }

    pub fn id_prefix(&self) -> Option<String> {
        self.id_captures().map(|captures| captures[1].to_string())
    }

    pub fn id_number(&self) -> u32 {
        self.id_captures()
            .and_then(|captures| captures[2].parse().ok())
            .unwrap_or_default()
    }

    pub fn id_parts(&self) -> Option<(String, u32)> {
        self.id_captures().and_then(|captures| {
            captures[2]
                .parse()
                .ok()
                .map(|number| (captures[1].to_string(), number))
        })
    }

    pub fn set_revision(&mut self, revision: String) {
        self.revision = Some(revision);
    }

    pub fn revision(&self) -> Option<&str> {
        self.revision.as_deref()
    }

    pub fn remove_attachment(&mut self, attachment: &str) {
        self.attachments.remove(attachment);
    }
}

pub trait CouchdbDocument: Serialize + DeserializeOwned + Sized + Send {
    fn meta(&self) -> &CouchdbMeta;
    fn meta_mut(&mut self) -> &mut CouchdbMeta;

    fn id(&self) -> &str {
        self.meta().id()
    }

    fn set_revision(&mut self, revision: String) {
        self.meta_mut().set_revision(revision);
    }

    fn revision(&self) -> Option<&str> {
        self.meta().revision()
    }

    /// Refresh document from database.
    ///
    /// Must be done after uploading attachments so that the otherwise missing stub wouldn't delete the attachment again.
    async fn refresh(&mut self) -> anyhow::Result<()> {
        let document = crate::couchdb::find_document_by_id(self.id()).await?;
        *self = document;
        Ok(())
    }
}

#[derive(Debug, Default, Serialize, Deserialize, Clone)]
pub struct DeletedDocument {
    #[serde(flatten)]
    couchdb_meta: CouchdbMeta,

    #[serde(rename = "_deleted")]
    deleted: bool,
}

impl From<CouchdbMeta> for DeletedDocument {
    fn from(couchdb_meta: CouchdbMeta) -> Self {
        Self {
            couchdb_meta,
            deleted: true,
        }
    }
}

impl CouchdbDocument for DeletedDocument {
    fn meta(&self) -> &CouchdbMeta {
        &self.couchdb_meta
    }

    fn meta_mut(&mut self) -> &mut CouchdbMeta {
        &mut self.couchdb_meta
    }
}
