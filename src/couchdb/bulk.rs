use super::{CouchdbDocument, CLIENT};
use anyhow::{Context, Result};
use serde::Deserialize;
use serde_json::json;
use std::collections::HashMap;

pub async fn save_batch_owned<T: CouchdbDocument>(mut batch: Vec<T>) -> Result<()> {
    save_batch(batch.iter_mut().collect()).await
}

/// Save all given documents
/// Calls set_revision on all saved documents
pub async fn save_batch<T: CouchdbDocument>(batch: Vec<&mut T>) -> Result<()> {
    #[derive(Deserialize)]
    #[serde(untagged)]
    enum BulkResponse {
        Ok {
            id: String,
            rev: String,
        },
        Error {
            id: String,
            error: String,
            reason: String,
        },
    }

    let res: Vec<BulkResponse> = CLIENT
        .post(format!("{}/_bulk_docs", crate::CONFIG.couchdb_url))
        .json(&json!({ "docs": batch }))
        .send()
        .await
        .context("Could not save bulk")?
        .error_for_status()
        .context("Error saving bulk")?
        .json()
        .await
        .context("Could not parse response of bulk")?;

    let mut docs_by_id: HashMap<String, &mut T> = batch
        .into_iter()
        .map(|doc| (doc.id().to_string(), doc))
        .collect::<HashMap<_, _>>();

    for row in res {
        match row {
            BulkResponse::Error { id, error, reason } => {
                anyhow::bail!(
                    "Error inserting/updating document {}: {} - {}",
                    id,
                    error,
                    reason
                );
            }
            BulkResponse::Ok { id, rev } => {
                docs_by_id
                    .remove(&id)
                    .ok_or_else(|| anyhow::format_err!("Could not find document"))?
                    .set_revision(rev);
            }
        }
    }

    Ok(())
}

/// Save all given documents, ignore already existing documents
pub async fn save_batch_ignore_existing<T: CouchdbDocument>(batch: Vec<T>) -> Result<usize> {
    #[derive(Deserialize)]
    #[serde(untagged)]
    enum BulkResponse {
        Ok {
            #[allow(dead_code)]
            id: String,
            #[allow(dead_code)]
            rev: String,
        },
        Error {
            id: String,
            error: String,
            reason: String,
        },
    }

    let res: Vec<BulkResponse> = CLIENT
        .post(format!("{}/_bulk_docs", crate::CONFIG.couchdb_url))
        .json(&json!({ "docs": batch }))
        .send()
        .await
        .context("Could not save bulk")?
        .error_for_status()
        .context("Error saving bulk")?
        .json()
        .await
        .context("Could not parse response of bulk")?;

    let mut inserted_documents = 0;
    for row in res {
        match row {
            BulkResponse::Ok { .. } => {
                inserted_documents += 1;
            }
            BulkResponse::Error { error, .. } if error == "conflict" => (),
            BulkResponse::Error { id, error, reason } => {
                anyhow::bail!(
                    "Error inserting/updating document {}: {} - {}",
                    id,
                    error,
                    reason
                );
            }
        }
    }

    Ok(inserted_documents)
}
