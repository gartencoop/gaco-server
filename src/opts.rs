use clap::Parser;
use once_cell::sync::Lazy;
use std::path::PathBuf;

pub static OPTS: Lazy<Opts> = Lazy::new(Opts::parse);

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Opts {
    #[clap(default_value = "config.json")]
    pub config: PathBuf,
}
