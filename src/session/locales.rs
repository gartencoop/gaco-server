use crate::model::language::Language;

use super::GacoSession;
use fluent_templates::{fluent_bundle::FluentValue, Loader};
use std::{borrow::Cow, collections::HashMap, fmt::Display};
use wallaby::Session;

fluent_templates::static_loader! {
    pub static LOCALES = {
        locales: "./locales",
        fallback_language: "de-DE",
        customise: |bundle| bundle.set_use_isolating(false),
    };
}

impl GacoSession {
    pub fn set_language(&mut self, language: &Language) {
        self.lang_id = language.id();
    }

    /// Get translation (see ftl files)
    pub fn tr(&self, text_id: &str) -> String {
        LOCALES.lookup(self.locale(), text_id)
    }

    /// Get translation with arguments (see ftl files)
    pub fn tr_args(&self, text_id: &str, args: &HashMap<Cow<'static, str>, FluentValue>) -> String {
        LOCALES.lookup_with_args(self.locale(), text_id, args)
    }

    pub fn format_number<N: Display>(&self, number: N, decimals: usize) -> String {
        let comma = self
            .tr("comma")
            .chars()
            .next()
            .expect("Could not find comma");
        let thousand_separator = self
            .tr("thousand_separator")
            .chars()
            .next()
            .expect("Could not find thousand_separator");

        let mut s = String::new();
        let num = format!("{number:.decimals$}");
        for (idx, val) in num.chars().rev().enumerate() {
            if idx > decimals + 1
                && if decimals == 0 {
                    idx
                } else {
                    idx - decimals - 1
                } % 3
                    == 0
            {
                s.insert(0, thousand_separator);
            }
            if idx == decimals && decimals != 0 {
                s.insert(0, comma);
            } else {
                s.insert(0, val);
            }
        }
        s
    }

    pub fn format_cents_as_euros(&self, cents: u32, decimals: usize) -> String {
        let comma = self
            .tr("comma")
            .chars()
            .next()
            .expect("Could not find comma");

        crate::common::format_cents_as_euros(cents, comma, decimals)
    }
}
