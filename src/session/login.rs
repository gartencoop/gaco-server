use super::GacoSession;
use crate::{
    model::{
        bidding::Bidding,
        user::{AccessLevel, User},
    },
    routing::{Page, Routing},
};
use once_cell::sync::Lazy;
use std::collections::HashMap;
use tokio::sync::RwLock;
use wallaby::Event;

static LOGGED_IN_UIDS: Lazy<RwLock<HashMap<String, u32>>> =
    Lazy::new(|| RwLock::new(HashMap::new()));

impl GacoSession {
    pub fn set_login_id(&mut self, login_id: String) {
        self.login_id = Some(login_id);
    }

    pub async fn uid_for_login_id(login_id: &str) -> Option<u32> {
        let logged_in_uids = &*LOGGED_IN_UIDS.read().await;
        logged_in_uids.get(login_id).copied()
    }

    pub async fn login(&mut self, username: &str, password: &str) -> Vec<Event> {
        if let (Some(login_id), Some(user)) =
            (self.login_id.as_ref(), User::login(username, password))
        {
            LOGGED_IN_UIDS
                .write()
                .await
                .insert(login_id.to_string(), user.uid());
            self.lang_id = user.language().id();

            if user
                .active_membership()
                .map(|membership| {
                    Bidding::get().is_active()
                        && membership.can_bid()
                        && membership.bid_for_current_bidding().is_none()
                })
                .unwrap_or_default()
            {
                self.routing = Routing::Page(Page::Bidding);
            }

            self.routing.changed_access_level(user.access_level()).await
        } else {
            vec![]
        }
    }

    pub async fn logout(&mut self) {
        if let Some(login_id) = self.login_id.as_ref() {
            LOGGED_IN_UIDS.write().await.remove(login_id);
        }

        self.routing = Routing::default();
    }

    pub async fn uid(&self) -> Option<u32> {
        match self.login_id.as_ref() {
            Some(login_id) => Self::uid_for_login_id(login_id).await,
            None => None,
        }
    }

    pub async fn logged_in(&self) -> bool {
        self.uid().await.is_some()
    }

    pub async fn user(&self) -> Option<User> {
        User::find_for_uid(self.uid().await?)
    }

    pub async fn access_level(&self) -> AccessLevel {
        match self.user().await {
            Some(user) => user.access_level(),
            None => AccessLevel::Guest,
        }
    }
}
