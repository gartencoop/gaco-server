use super::GacoSession;
use chrono::{Datelike, Timelike, Utc};
use serde::{Deserialize, Serialize};

impl GacoSession {
    pub fn calendar(&self) -> &Calendar {
        &self.calendar
    }

    pub fn calendar_mut(&mut self) -> &mut Calendar {
        &mut self.calendar
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Calendar {
    start: String,
    end: String,
    view: CalendarView,
}

impl Calendar {
    pub fn start(&self) -> &str {
        &self.start
    }

    pub fn end(&self) -> &str {
        &self.end
    }

    pub fn view(&self) -> &CalendarView {
        &self.view
    }

    pub fn set_start(&mut self, start: String) {
        self.start = start;
    }

    pub fn set_end(&mut self, end: String) {
        self.end = end;
    }

    pub fn set_view(&mut self, view: CalendarView) {
        self.view = view;
    }
}

impl Default for Calendar {
    fn default() -> Self {
        let start = Utc::now()
            .with_day0(0)
            .unwrap()
            .with_hour(0)
            .unwrap()
            .with_minute(0)
            .unwrap()
            .with_second(0)
            .unwrap()
            .with_nanosecond(0)
            .unwrap();
        let end = match start.month() {
            12 => start
                .with_month(1)
                .unwrap()
                .with_year(start.year() + 1)
                .unwrap(),
            month => start.with_month(month + 1).unwrap(),
        };

        Self {
            start: start.to_rfc3339(),
            end: end.to_rfc3339(),
            view: Default::default(),
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub enum CalendarView {
    Month,
    Week,
    WorkWeek,
    Day,
    Agenda,
}

impl Default for CalendarView {
    fn default() -> Self {
        Self::Month
    }
}
