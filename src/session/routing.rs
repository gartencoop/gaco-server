use super::GacoSession;
use crate::routing::Routing;

impl GacoSession {
    pub fn set_routing(&mut self, routing: Routing) {
        self.routing = routing;
    }

    pub fn routing(&self) -> &Routing {
        &self.routing
    }
}
