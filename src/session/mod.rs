pub mod calendar;
pub mod locales;
mod login;
mod routing;

use crate::{model::language::Language, routing::Routing};
use calendar::Calendar;
use serde_derive::{Deserialize, Serialize};
use unic_langid::LanguageIdentifier;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct GacoSession {
    routing: Routing,
    pub edit_mode: bool,
    login_id: Option<String>,
    #[serde(skip)]
    lang_id: LanguageIdentifier,
    calendar: Calendar,
}

impl Default for GacoSession {
    fn default() -> Self {
        Self {
            login_id: None,
            edit_mode: false,
            lang_id: Language::default().id(),
            routing: Default::default(),
            calendar: Default::default(),
        }
    }
}

impl wallaby::Session for GacoSession {
    fn screen_name(&self) -> &str {
        self.routing.screen_name()
    }

    fn set_screen_name(&mut self, _screen_name: String) {}

    fn locale(&self) -> &LanguageIdentifier {
        &self.lang_id
    }
}
