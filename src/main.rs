pub use gaco_server::*;
use gaco_server::{
    model::{bidding::Bidding, membership::Membership, user::User, vp::VP},
    screen::admin_incomes::AdminIncomes,
};
use screen::document::DocumentLock;
use search::handler::SearchHandler;
use std::time::Duration;
use wallaby::*;
//use warp::Filter;

#[tokio::main]
async fn main() {
    pretty_env_logger::try_init_timed().ok();

    Bidding::load().await;
    User::load_all().await.expect("Error loading users");
    Membership::load_all()
        .await
        .expect("Error loading memberships");
    VP::load_all().await.expect("Error loading vps");
    DocumentLock::periodically_clean_old_locks();
    search::init().await.expect("Could not initialize search");

    let session_store = InMemorySessionStore::new();
    let mut server = WallabyServer::new(
        WallabyServerConfig {
            port: CONFIG.port,
            assets: CONFIG.assets.clone(),
            default_asset: CONFIG.assets.join("index.html"),
        },
        session_store,
    );
    screen::add(&mut server).await;

    // Update AdminIncomes screen every 2 seconds so that the progress of mail sending is updated
    tokio::spawn(async move {
        loop {
            tokio::time::sleep(Duration::from_secs(2)).await;
            wallaby::update_screen(AdminIncomes::screen_name().into()).await;
        }
    });

    server.add_handler::<SearchHandler>().await;

    println!("starting server..");
    server.start(crate::files::filter()).await;
}
