use crate::GacoSession;
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize, Clone)]
pub enum VPStatus {
    Unknown,
    Impossible,
    Possible,
    Operational,
}

impl Default for VPStatus {
    fn default() -> Self {
        Self::Unknown
    }
}

impl VPStatus {
    pub fn tr(&self, session: &GacoSession) -> String {
        session.tr(match self {
            VPStatus::Unknown => "vp_status_unknown",
            VPStatus::Impossible => "vp_status_impossible",
            VPStatus::Possible => "vp_status_possible",
            VPStatus::Operational => "vp_status_operational",
        })
    }

    pub fn operational(&self) -> bool {
        matches!(self, Self::Operational)
    }
}
