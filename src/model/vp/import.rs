use chrono::{DateTime, Utc};

use crate::{
    couchdb::CouchdbMeta,
    model::{file::File, html::Html, language::Language},
};

use super::VP;

impl VP {
    #[allow(clippy::too_many_arguments)]
    pub fn import(
        nid: u32,
        path: String,
        language: Language,
        title: String,
        created_at: DateTime<Utc>,
        created_by: u32,
        body: Html,
        code: String,
        status: super::VPStatus,
        contact: String,
        case_storage: String,
        opening: String,
        location: String,
        files: Vec<File>,
    ) -> Self {
        let couchdb_meta = CouchdbMeta::new(Self::ID_PREFIX, nid);

        #[allow(clippy::needless_update)]
        Self {
            couchdb_meta,
            nid,
            path,
            language,
            title,
            created_at,
            created_by,
            body,
            code,
            status,
            contact,
            case_storage,
            opening,
            location,
            files,
            ..Default::default()
        }
    }
}
