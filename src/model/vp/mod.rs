mod db;
mod status;

#[cfg(feature = "import")]
mod import;

use super::{file::File, html::Html, language::Language, membership::Membership};
use crate::couchdb::CouchdbMeta;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

use serde_json::{json, Value};
pub use status::VPStatus;

#[derive(Debug, Default, Deserialize, Serialize, Clone)]
#[serde(default)]
pub struct VP {
    #[serde(flatten)]
    couchdb_meta: CouchdbMeta,

    nid: u32,
    path: String,
    language: Language,
    #[serde(
        deserialize_with = "crate::common::deserialize_datetime",
        serialize_with = "crate::common::serialize_datetime"
    )]
    created_at: DateTime<Utc>,
    created_by: u32,
    title: String,

    body: Html,
    code: String,
    status: VPStatus,
    contact: String,
    case_storage: String,
    opening: String,
    location: String,

    #[serde(skip_serializing_if = "Vec::is_empty")]
    files: Vec<File>,
}

impl VP {
    pub fn nid(&self) -> u32 {
        self.nid
    }

    pub fn path(&self) -> &str {
        &self.path
    }

    pub fn code(&self) -> &str {
        &self.code
    }

    pub fn title(&self) -> &str {
        &self.title
    }

    pub fn created_by_name(&self) -> Option<String> {
        crate::model::user::User::name_for_uid(self.created_by)
    }

    pub fn created_at(&self) -> DateTime<Utc> {
        self.created_at
    }

    pub fn status(&self) -> &VPStatus {
        &self.status
    }

    pub fn contact(&self) -> &str {
        &self.contact
    }

    pub fn case_storage(&self) -> &str {
        &self.case_storage
    }

    pub fn opening(&self) -> &str {
        &self.opening
    }

    pub fn location(&self) -> &str {
        &self.location
    }

    pub fn members(&self) -> Vec<Membership> {
        Membership::find_all_for_vp_id(self.nid)
    }

    pub fn waiting_members(&self) -> Vec<Membership> {
        Membership::find_all_for_waiting_vp_id(self.nid)
    }

    pub fn body(&self) -> &str {
        self.body.as_ref()
    }

    pub fn image_paths(&self) -> Vec<String> {
        self.files
            .iter()
            .filter(|file| file.is_image())
            .map(|file| file.path(self.couchdb_meta.id_number()))
            .collect()
    }

    pub fn files(&self) -> Vec<Value> {
        self.files
            .iter()
            .filter(|file| !file.is_image())
            .map(|file| {
                json!({
                    "name": file.name(),
                    "created_by": file.created_by_name(),
                    "path": file.path(self.couchdb_meta.id_number()),
                })
            })
            .collect()
    }
}
