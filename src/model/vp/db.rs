use super::VP;
use crate::couchdb::{CouchdbDocument, CouchdbMeta};
use anyhow::Result;
use log::debug;
use once_cell::sync::Lazy;
use serde_json::json;
use std::{collections::HashMap, sync::RwLock};

static NID_TO_VP: Lazy<RwLock<HashMap<u32, VP>>> = Lazy::new(|| RwLock::new(HashMap::new()));

impl VP {
    pub const ID_PREFIX: &'static str = "vp";

    pub fn find_for_nid(id: u32) -> Option<Self> {
        NID_TO_VP.read().ok()?.get(&id).cloned()
    }

    pub fn find_all() -> Vec<Self> {
        let mut vps = NID_TO_VP
            .read()
            .map(|nid_to_vp| {
                nid_to_vp
                    .values()
                    .filter(|vp| vp.status.operational())
                    .cloned()
                    .collect::<Vec<_>>()
            })
            .unwrap_or_default();
        vps.sort_by_key(|vp| vp.code().to_owned());
        vps
    }

    pub async fn load_all() -> Result<()> {
        let vps: Vec<VP> = crate::couchdb::find_documents(json!({
            "selector": {
                "_id": {
                    "$gt": format!("{}_", Self::ID_PREFIX),
                    "$lt": format!("{}_{{}}", Self::ID_PREFIX),
                }
            },
            "limit": 10_000u32
        }))
        .await?;

        let nid_to_vp = vps
            .into_iter()
            .map(|vp| (vp.nid, vp))
            .collect::<HashMap<u32, Self>>();

        debug!("Loaded {} vps", nid_to_vp.len());

        *NID_TO_VP
            .write()
            .map_err(|_| anyhow::format_err!("ID_TO_VP is poisoned"))? = nid_to_vp;

        Ok(())
    }

    pub async fn save(&mut self) -> Result<()> {
        crate::couchdb::save_batch(vec![self]).await?;
        Self::load_all().await?;
        Ok(())
    }
}

impl CouchdbDocument for VP {
    fn meta(&self) -> &CouchdbMeta {
        &self.couchdb_meta
    }

    fn meta_mut(&mut self) -> &mut CouchdbMeta {
        &mut self.couchdb_meta
    }
}
