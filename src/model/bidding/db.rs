use super::Bidding;
use crate::couchdb::{CouchdbDocument, CouchdbMeta};
use anyhow::Result;
use chrono::NaiveDate;
use std::sync::RwLock;

static ID: &str = "bidding";
static BIDDING: RwLock<Option<Bidding>> = RwLock::new(None);

impl Bidding {
    pub async fn load() {
        let bidding = crate::couchdb::find_single_document(serde_json::json!({
            "selector": {
                "_id": {
                    "$eq": ID
                }
            }
        }))
        .await
        .unwrap_or_else(|_| Bidding {
            couchdb_meta: CouchdbMeta::new_without_prefix(ID.to_string()),
            start_at: NaiveDate::from_ymd_opt(2022, 11, 4).expect("Could not create start_at"),
            end_at: NaiveDate::from_ymd_opt(2022, 11, 17).expect("Could not create end_at"),
            correction_until: NaiveDate::from_ymd_opt(2022, 11, 30)
                .expect("Could not create correction_until"),
            goal_cents: 11_340,
            goal_members: 280,
        });

        *BIDDING.write().expect("BIDDING was poisoned") = Some(bidding);
    }

    pub fn get() -> Self {
        BIDDING
            .read()
            .expect("BIDDING was poisoned")
            .clone()
            .expect("Bidding::load() was not called")
    }

    pub async fn save(&mut self) -> Result<()> {
        crate::couchdb::save_batch(vec![self]).await?;

        Ok(())
    }
}

impl CouchdbDocument for Bidding {
    fn meta(&self) -> &CouchdbMeta {
        &self.couchdb_meta
    }

    fn meta_mut(&mut self) -> &mut CouchdbMeta {
        &mut self.couchdb_meta
    }

    fn set_revision(&mut self, revision: String) {
        self.meta_mut().set_revision(revision);

        *BIDDING.write().expect("BIDDING was poisoned") = Some(self.to_owned());
    }
}
