use super::BiddingCalc;
use crate::{common::format_cents_as_euros, model::membership::Membership};

impl BiddingCalc {
    pub fn csv(&self) -> String {
        std::iter::once("Mitgliednummer;Nachname;Vorname;Anteile;Gebot".to_string())
            .chain(std::iter::once(format!(
                "Abgegeben;;;{};{}",
                self.sum_shares(),
                format_cents_as_euros(self.sum_cents() * 12, ',', 2)
            )))
            .chain(std::iter::once(format!(
                "Gesamt;;;{};{}",
                Membership::bid_shares(),
                format_cents_as_euros(self.extrapolated_budget(), ',', 2)
            )))
            .chain(self.memberships().iter().map(|membership| {
                let name = membership.name().unwrap_or_default();
                let surname = membership.surname().unwrap_or_default();
                let id = membership.id();
                let bid = self.bids().get(&id);
                let shares = f32::from(membership.shares());
                let euros = match bid {
                    Some(bid) => format_cents_as_euros(bid.cents(), ',', 2),
                    None => "-".to_string(),
                };
                format!("{id};{surname};{name};{shares};{euros}")
            }))
            .collect::<Vec<_>>()
            .join("\n")
    }
}
