use super::BiddingCalc;
use anyhow::Result;
use std::collections::BTreeMap;

impl BiddingCalc {
    pub fn distribution(&self) -> Vec<(u32, u32)> {
        let mut distributed_bids = BTreeMap::new();

        for membership in self.memberships() {
            if let Some(bid) = membership.bid_for_current_bidding() {
                let slot = (bid.cents() as f32 / f32::from(membership.shares()) / 1000.0).round()
                    as u32
                    * 10;
                *distributed_bids.entry(slot).or_insert(0u32) += 1;
            }
        }

        let mut it = distributed_bids.iter();
        match (it.next(), it.last()) {
            (Some((first, _)), Some((last, _))) => {
                for euros in { 10.max(*first) - 10..=*last + 10 }.step_by(10) {
                    distributed_bids.entry(euros).or_insert(0);
                }
            }
            (Some((only, _)), None) => {
                for euros in { 10.max(*only) - 10..=*only + 10 }.step_by(10) {
                    distributed_bids.entry(euros).or_insert(0);
                }
            }
            _ => (),
        }

        distributed_bids.into_iter().collect()
    }

    pub fn distribution_plot(&self) -> Result<String> {
        let distribution = self
            .distribution()
            .into_iter()
            .map(|(euros, bids)| (euros as i128 - 5, bids as i128));

        let plot = poloto::build::plot("").histogram(distribution);
        #[allow(deprecated)]
        Ok(poloto::data(poloto::plots!(plot))
            .build_and_label(("Gebotsverteilung", "€/Monat", "Mitgliedschaften"))
            .append_to(poloto::header().light_theme())
            .render_string()?)
    }
}
