mod calc;
mod db;

use crate::couchdb::CouchdbMeta;
use chrono::{Datelike, NaiveDate, Utc};
use serde::{Deserialize, Serialize};

pub use calc::BiddingCalc;

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
#[serde(default)]
pub struct Bidding {
    #[serde(flatten)]
    couchdb_meta: CouchdbMeta,

    /// Date when the bidding should start
    #[serde(
        deserialize_with = "crate::common::deserialize_date",
        serialize_with = "crate::common::serialize_date"
    )]
    start_at: NaiveDate,

    /// Date until the bidding should run
    #[serde(
        deserialize_with = "crate::common::deserialize_date",
        serialize_with = "crate::common::serialize_date"
    )]
    end_at: NaiveDate,

    /// Date until the bidding should run
    #[serde(
        deserialize_with = "crate::common::deserialize_date",
        serialize_with = "crate::common::serialize_date"
    )]
    correction_until: NaiveDate,

    /// Monthly sum we would like to reach ("Deckungsbeitrag")
    goal_cents: u32,

    /// Members we need
    goal_members: u32,
}

impl Bidding {
    pub fn start_at(&self) -> NaiveDate {
        self.start_at
    }

    pub fn set_start_at(&mut self, start_at: NaiveDate) {
        self.start_at = start_at;
    }

    pub fn end_at(&self) -> NaiveDate {
        self.end_at
    }

    pub fn set_end_at(&mut self, end_at: NaiveDate) {
        self.end_at = end_at;
    }

    pub fn correction_until(&self) -> NaiveDate {
        self.correction_until
    }

    pub fn set_correction_until(&mut self, correction_until: NaiveDate) {
        self.correction_until = correction_until;
    }

    pub fn is_active(&self) -> bool {
        let now = Utc::now().naive_utc().date();

        self.start_at <= now && self.correction_until >= now
    }

    pub fn year(&self) -> u16 {
        self.start_at.year() as u16 + 1
    }

    pub fn goal_cents(&self) -> u32 {
        self.goal_cents
    }

    pub fn set_goal_cents(&mut self, goal_cents: u32) {
        self.goal_cents = goal_cents;
    }

    pub fn goal_members(&self) -> u32 {
        self.goal_members
    }

    pub fn set_goal_members(&mut self, goal_members: u32) {
        self.goal_members = goal_members;
    }

    pub fn goal_cents_total(&self) -> u32 {
        self.goal_cents * self.goal_members * 12
    }
}
