mod csv;
mod distribution;

use crate::model::membership::{Bid, Membership};
use std::collections::HashMap;

pub struct BiddingCalc {
    memberships: Vec<Membership>,
    bids: HashMap<u32, Bid>,
}
impl BiddingCalc {
    pub fn get() -> Self {
        let mut memberships = Membership::find_all()
            .into_iter()
            .filter(|membership| membership.can_bid())
            .collect::<Vec<_>>();
        memberships.sort_by_key(|membership| {
            membership
                .surname()
                .or_else(|| membership.name())
                .map(|name| name.to_owned())
        });
        let bids = memberships
            .iter()
            .filter_map(|membership| {
                membership
                    .bid_for_current_bidding()
                    .map(|bid| (membership.id(), bid))
            })
            .collect();

        Self { memberships, bids }
    }

    pub fn memberships(&self) -> &Vec<Membership> {
        &self.memberships
    }

    pub fn bids(&self) -> &HashMap<u32, Bid> {
        &self.bids
    }

    pub fn sum_shares(&self) -> f32 {
        let sum = self
            .memberships
            .iter()
            .filter(|membership| self.bids.contains_key(&membership.id()))
            .map(|membership| f32::from(membership.shares()))
            .sum();
        if sum == -0.0 {
            0.0
        } else {
            sum
        }
    }

    pub fn sum_members(&self) -> usize {
        self.memberships
            .iter()
            .filter(|membership| self.bids.contains_key(&membership.id()))
            .count()
    }

    pub fn sum_cents(&self) -> u32 {
        self.bids.values().map(|bid| bid.cents()).sum()
    }

    pub fn avg_cents(&self) -> u32 {
        if self.memberships().is_empty() {
            0
        } else {
            (self.sum_cents() as f32 / self.sum_shares()) as u32
        }
    }

    pub fn extrapolated_budget(&self) -> u32 {
        (self.avg_cents() as f32 * Membership::bid_shares() * 12.0).round() as u32
    }
}
