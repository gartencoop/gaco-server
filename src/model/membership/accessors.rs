use super::{
    bid::BiddingYear, Bid, Membership, Shares, Subscription, VpMembership, VpMembershipState,
};
use crate::model::{bidding::Bidding, user::User, vp::VP};
use anyhow::Result;
use chrono::{DateTime, Datelike, NaiveDate, Utc};
use std::collections::BTreeMap;

impl Membership {
    pub fn id(&self) -> u32 {
        self.id
    }

    pub fn created_at(&self) -> DateTime<Utc> {
        self.created_at
    }

    pub fn start_at(&self) -> NaiveDate {
        self.start_at
    }

    pub fn set_start_at(&mut self, start_at: NaiveDate) {
        self.start_at = start_at;
    }

    pub fn end_at(&self) -> Option<NaiveDate> {
        self.end_at
    }

    pub fn set_end_at(&mut self, end_at: Option<NaiveDate>) {
        self.end_at = end_at;
    }

    pub fn surname(&self) -> Option<&str> {
        self.surname.as_deref()
    }

    pub fn set_surname(&mut self, surname: Option<String>) {
        self.surname = surname;
    }

    pub fn name(&self) -> Option<&str> {
        self.name.as_deref()
    }

    pub fn set_name(&mut self, name: Option<String>) {
        self.name = name;
    }

    pub fn street(&self) -> Option<&str> {
        self.street.as_deref()
    }

    pub fn set_street(&mut self, street: Option<String>) {
        self.street = street;
    }

    pub fn zip(&self) -> Option<&str> {
        self.zip.as_deref()
    }

    pub fn set_zip(&mut self, zip: Option<String>) {
        self.zip = zip;
    }

    pub fn city(&self) -> Option<&str> {
        self.city.as_deref()
    }

    pub fn set_city(&mut self, city: Option<String>) {
        self.city = city;
    }

    pub fn subscription(&self) -> &Subscription {
        &self.subscription
    }

    pub fn set_subscription(&mut self, subscription: Subscription) {
        self.subscription = subscription;
    }

    pub fn vp_membership_history(&self) -> Vec<VpMembership> {
        self.vp_membership_history.clone()
    }

    pub fn vp_id(&self) -> Option<u32> {
        self.vp_membership_history
            .last()
            .and_then(|vp_membership| match vp_membership.state() {
                VpMembershipState::None | VpMembershipState::Waitinglist { .. } => None,
                VpMembershipState::Member { member_id }
                | VpMembershipState::MemberWaitinglist { member_id, .. } => Some(member_id),
            })
    }

    pub fn waiting_vp_id(&self) -> Option<u32> {
        self.vp_membership_history
            .last()
            .and_then(|vp_membership| match vp_membership.state() {
                VpMembershipState::None | VpMembershipState::Member { .. } => None,
                VpMembershipState::Waitinglist { waiting_id }
                | VpMembershipState::MemberWaitinglist { waiting_id, .. } => Some(waiting_id),
            })
    }

    pub fn vp(&self) -> Option<VP> {
        self.vp_id().and_then(VP::find_for_nid)
    }

    pub fn bidding_history(&self) -> Vec<Bid> {
        self.bidding_history.clone()
    }

    pub fn bidding_years(&self) -> BTreeMap<i32, BiddingYear> {
        let mut bids = self.bidding_history();
        bids.reverse();

        let mut end_at = NaiveDate::from_ymd_opt(chrono::Utc::now().year() + 1, 12, 31)
            .expect("Could not create date");
        if let Some(membership_end_at) = self.end_at() {
            end_at = end_at.min(membership_end_at);
        }

        let mut bidding_years = BTreeMap::new();

        for bid in bids.into_iter() {
            let mut year = bid.start_at().year();
            let mut month = bid.start_at().month();
            if bid.start_at().day() != 1 {
                month += 1;
                if month == 13 {
                    month = 1;
                    year += 1;
                }
            }

            loop {
                let current =
                    NaiveDate::from_ymd_opt(year, month, 1).expect("Could not create naive date");

                if current >= end_at {
                    break;
                }

                let year_entry = bidding_years
                    .entry(year)
                    .or_insert_with(BiddingYear::default);
                year_entry.cents += bid.cents();
                year_entry.months += 1;

                month += 1;
                if month == 13 {
                    month = 1;
                    year += 1;
                }
            }
            end_at = bid.start_at();
        }

        bidding_years
    }

    pub fn add_bid(&mut self, bid: Bid) {
        self.bidding_history.push(bid);
    }

    pub fn remove_bid(&mut self, index: usize) -> bool {
        if index >= self.bidding_history.len() {
            return false;
        }
        self.bidding_history.remove(index);
        true
    }

    pub fn current_bid(&self) -> Option<Bid> {
        self.bidding_history.last().cloned()
    }

    pub fn bid_for_current_bidding(&self) -> Option<Bid> {
        self.current_bid()
            .filter(|bid| bid.change_at().date_naive() >= Bidding::get().start_at())
    }

    pub fn bid_cents_for_current_year(&self) -> u32 {
        self.bidding_years()
            .get(&chrono::Utc::now().year())
            .map(|bidding_year| bidding_year.cents)
            .unwrap_or_default()
    }

    pub fn add_vp_membership(&mut self, vp_membership: VpMembership) {
        self.vp_membership_history.push(vp_membership);
    }

    pub fn remove_vp_membership(&mut self, index: usize) -> bool {
        if index >= self.vp_membership_history.len() {
            return false;
        }
        self.vp_membership_history.remove(index);
        true
    }

    pub fn shares(&self) -> Shares {
        self.shares
    }

    pub fn set_shares(&mut self, shares: Shares) {
        self.shares = shares;
    }

    pub fn main_user_id(&self) -> Option<u32> {
        self.main_user
    }

    pub fn main_user(&self) -> Option<User> {
        self.main_user_id().and_then(User::find_for_uid)
    }

    pub fn users(&self) -> Vec<User> {
        self.users
            .iter()
            .filter_map(|uid| User::find_for_uid(*uid))
            .collect()
    }

    /// returns if the user was added
    pub async fn add_user(&mut self, user_id: u32) -> Result<bool> {
        let Some(user) = User::find_for_uid(user_id) else {
            anyhow::bail!("Could not find user for uid");
        };
        if let Some(mut current_membership) = user.membership() {
            current_membership.remove_user(user_id);
            current_membership.save().await?;
        }

        if self.users.contains(&user_id) {
            return Ok(false);
        }

        if self.users.is_empty() {
            self.main_user = Some(user_id);
        }
        self.users.push(user_id);
        Ok(true)
    }

    /// returns if the user was removed
    pub fn remove_user(&mut self, user_id: u32) -> bool {
        if !self.users.contains(&user_id) {
            return false;
        }

        self.users.retain(|id| *id != user_id);
        if self.main_user == Some(user_id) {
            self.main_user = self.users.first().copied();
        }
        true
    }

    /// returns if the user could be set as main user
    pub fn set_main_user(&mut self, user_id: u32) -> bool {
        if !self.users.contains(&user_id) {
            return false;
        }

        self.main_user = Some(user_id);

        true
    }

    pub fn notes(&self) -> Option<&str> {
        self.notes.as_deref()
    }

    pub fn set_notes(&mut self, notes: Option<String>) {
        self.notes = notes;
    }

    pub fn active_months(&self) -> u8 {
        if self.subscription != Subscription::Active {
            return 0;
        }

        let year = Utc::now().year();
        match (
            self.start_at.year() == year,
            self.end_at.map(|d| d.year() == year).unwrap_or_default(),
        ) {
            (true, true) => (self.end_at.map(|d| d.month0()).unwrap_or_default() as u8)
                .saturating_sub(self.start_at.month0() as u8),
            (true, false) => 12 - self.start_at.month0() as u8,
            (false, true) => self.end_at.map(|d| d.month0()).unwrap_or_default() as u8,
            (false, false)
                if self
                    .end_at
                    .map(|d| d < Utc::now().date_naive())
                    .unwrap_or_default() =>
            {
                0
            }
            (false, false) => 12,
        }
    }
}
