use super::Membership;
use crate::common::now_date;
use anyhow::{bail, Result};
use chrono::Utc;

impl Membership {
    async fn invitation_token(&self) -> Result<String> {
        let invitation_token = match self.invitation_token.clone().filter(|_| {
            self.invitation_token_created_at
                .map(|created_at| {
                    Utc::now().date_naive().signed_duration_since(created_at)
                        < chrono::Duration::try_weeks(1).expect("Could not create duration")
                })
                .unwrap_or_default()
        }) {
            None => {
                let invitation_token = uuid::Uuid::new_v4().to_string();

                let mut membership = self.clone();
                membership.invitation_token = Some(invitation_token.clone());
                membership.invitation_token_created_at = Some(now_date());
                membership.save().await?;

                invitation_token
            }
            Some(invitation_token) => invitation_token,
        };

        Ok(invitation_token)
    }

    pub async fn invitation_url(&self) -> Result<String> {
        Ok(format!(
            "/invitation_{}_{}",
            self.id,
            self.invitation_token().await?
        ))
    }

    pub async fn invitation_valid_until(&self) -> Result<String> {
        self.invitation_token().await?;

        let Some(created_at) = self.invitation_token_created_at else {
            anyhow::bail!("internal error")
        };
        Ok(
            (created_at + chrono::Duration::try_weeks(1).expect("Could not create duration"))
                .format("%d.%m.%Y")
                .to_string(),
        )
    }

    pub fn verify_invitation_token(&self, token: &str) -> Result<()> {
        if self
            .invitation_token_created_at
            .filter(|created_at| now_date().signed_duration_since(*created_at).num_weeks() == 0)
            .is_none()
        {
            bail!("invitation token too old");
        }
        if self.invitation_token.as_deref() != Some(token) {
            bail!("invitation token does not match");
        }

        Ok(())
    }
}
