use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Deserialize, Serialize, PartialEq, Eq)]
pub enum Subscription {
    /// Warteliste
    WaitingList,
    /// Kein Gemüse
    NoVegetables,
    /// Aktive Mitgliedschaft
    Active,
    /// Im Team der Gartencoop
    Team,
    /// Umsonstanteile (Gärtner_innen und ko)
    FreeVegetables,
    /// Probemitgliedschaft
    TestMembership,
}

impl Default for Subscription {
    fn default() -> Self {
        Self::TestMembership
    }
}
