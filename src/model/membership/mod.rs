mod accessors;
mod bid;
mod db;
mod email;
mod finance;
mod invitation;
mod shares;
mod subscription;
mod vp_membership;

use crate::couchdb::CouchdbMeta;
use chrono::{DateTime, Datelike, NaiveDate, TimeZone, Utc};
use serde::{Deserialize, Serialize};

pub use bid::Bid;
pub use shares::Shares;
pub use subscription::Subscription;
pub use vp_membership::{VpMembership, VpMembershipState};

#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq, Eq)]
#[serde(default)]
pub struct Membership {
    #[serde(flatten)]
    couchdb_meta: CouchdbMeta,

    id: u32,
    #[serde(
        deserialize_with = "crate::common::deserialize_datetime",
        serialize_with = "crate::common::serialize_datetime"
    )]
    created_at: DateTime<Utc>,
    #[serde(
        deserialize_with = "crate::common::deserialize_date",
        serialize_with = "crate::common::serialize_date"
    )]
    start_at: NaiveDate,
    #[serde(
        deserialize_with = "crate::common::deserialize_optional_date",
        serialize_with = "crate::common::serialize_optional_date"
    )]
    end_at: Option<NaiveDate>,

    surname: Option<String>,
    name: Option<String>,
    street: Option<String>,
    zip: Option<String>,
    city: Option<String>,

    shares: Shares,
    subscription: Subscription,
    vp_membership_history: Vec<VpMembership>,
    bidding_history: Vec<Bid>,

    /// Imported memberships can exist without any users
    main_user: Option<u32>,
    users: Vec<u32>,
    invitation_token: Option<String>,
    #[serde(
        deserialize_with = "crate::common::deserialize_optional_date",
        serialize_with = "crate::common::serialize_optional_date"
    )]
    invitation_token_created_at: Option<NaiveDate>,

    notes: Option<String>,
}

impl Membership {
    pub fn new() -> Self {
        let id = Self::next_id();
        Self {
            couchdb_meta: CouchdbMeta::new(Self::ID_PREFIX, id),
            id,
            created_at: crate::common::now_datetime(),
            start_at: crate::common::now_date(),
            ..Default::default()
        }
    }

    pub fn is_active(&self) -> bool {
        let now = crate::common::now_date();

        self.start_at() <= now && !self.end_at().map(|end_at| end_at < now).unwrap_or_default()
    }

    /// Whether this membership is active on the first of january next year
    pub fn can_bid(&self) -> bool {
        if self.subscription != Subscription::Active {
            return false;
        }

        let next_january = Utc
            .with_ymd_and_hms(Utc::now().year() + 1, 1, 1, 0, 0, 0)
            .single()
            .expect("Could not generate date")
            .date_naive();

        self.start_at() <= next_january
            && !self
                .end_at()
                .map(|end_at| end_at < next_january)
                .unwrap_or_default()
    }

    pub fn full_name(&self) -> String {
        match (self.name(), self.surname()) {
            (Some(name), Some(surname)) => format!("{name} {surname}"),
            (Some(name), None) | (None, Some(name)) => name.to_owned(),
            _ => "-".to_owned(),
        }
    }
}
