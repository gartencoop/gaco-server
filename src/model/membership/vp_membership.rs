use crate::common::now_datetime;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq, Eq)]
pub struct VpMembership {
    #[serde(
        deserialize_with = "crate::common::deserialize_datetime",
        serialize_with = "crate::common::serialize_datetime"
    )]
    change_at: DateTime<Utc>,
    state: VpMembershipState,
}

impl VpMembership {
    pub fn new(state: VpMembershipState) -> Self {
        Self {
            change_at: crate::common::now_datetime(),
            state,
        }
    }

    pub fn change_at(&self) -> DateTime<Utc> {
        self.change_at
    }

    pub fn state(&self) -> VpMembershipState {
        self.state
    }
}

#[derive(Clone, Copy, Debug, Default, Deserialize, Serialize, PartialEq, Eq)]
pub enum VpMembershipState {
    #[default]
    None,
    /// membership is in the vp `member_id`
    Member { member_id: u32 },
    /// membership is on the waitinglist for vp `waiting_id`
    Waitinglist { waiting_id: u32 },
    /// membership is in the vp `member_id` and on the waitinglist for vp `waiting_id`
    MemberWaitinglist { member_id: u32, waiting_id: u32 },
}

impl From<VpMembershipState> for VpMembership {
    fn from(state: VpMembershipState) -> Self {
        VpMembership {
            change_at: now_datetime(),
            state,
        }
    }
}
