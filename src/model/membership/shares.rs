use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize, Copy, PartialEq, Eq)]
pub enum Shares {
    Half,
    One,
    OneAndHalf,
    Two,
}

impl Default for Shares {
    fn default() -> Self {
        Self::One
    }
}

impl From<Shares> for f32 {
    fn from(shares: Shares) -> Self {
        match shares {
            Shares::Half => 0.5,
            Shares::One => 1.0,
            Shares::OneAndHalf => 1.5,
            Shares::Two => 2.0,
        }
    }
}
