use super::Membership;
use crate::model::transaction::Transaction;
use serde_json::json;

impl Membership {
    pub fn finance<'a, T: IntoIterator<Item = &'a Transaction>>(
        &self,
        transactions: T,
    ) -> serde_json::Value {
        let bidding_years = self.bidding_years();
        let current_transactions: Vec<_> = transactions
            .into_iter()
            .filter(|transaction| transaction.membership_id() == Some(self.id()))
            .collect();
        let transactions_count = current_transactions.len() as i32;
        let transactions_sum: i32 = current_transactions
            .into_iter()
            .map(|transaction| transaction.cents() as i32)
            .sum();

        let mut months = 0;
        let mut cents = 0;
        let years = bidding_years.len().saturating_sub(1); // ignore next year
        for (_, bidding_year) in bidding_years.into_iter().take(years) {
            months += bidding_year.months;
            cents += bidding_year.cents;
        }

        json!({
            "months": months,
            "current_sum": transactions_sum,
            "count": transactions_count,
            "expected_sum": cents,
            "diff": transactions_sum - cents as i32,
        })
    }
}
