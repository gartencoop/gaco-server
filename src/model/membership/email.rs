use super::Membership;
use crate::CONFIG;
use anyhow::Result;
use lettre::{
    message::header::ContentType, transport::smtp::authentication::Credentials, AsyncSmtpTransport,
    AsyncTransport, Tokio1Executor,
};
use mime::TEXT_PLAIN_UTF_8;

impl Membership {
    pub fn emails(&self) -> Vec<String> {
        self.users()
            .iter()
            .map(|user| user.email().to_owned())
            .collect()
    }

    pub async fn send_email(&self, subject: &str, body: String) -> Result<()> {
        let smtp_credentials =
            Credentials::new(CONFIG.email.username.clone(), CONFIG.email.password.clone());

        let mailer = AsyncSmtpTransport::<Tokio1Executor>::relay(&CONFIG.email.host)?
            .credentials(smtp_credentials)
            .build();

        for user in self.users() {
            let Some(address) = CONFIG
                .email
                .membership_to_overwrite
                .as_ref()
                .and_then(|email| email.parse().ok())
                .or_else(|| user.email().parse().ok())
            else {
                log::error!("Could not parse email address: {}", user.email());
                continue;
            };

            let email = lettre::Message::builder()
                .header(ContentType::from(TEXT_PLAIN_UTF_8))
                .from(CONFIG.email.membership_from.parse()?)
                .to(address)
                .subject(subject)
                .body(body.clone())?;

            mailer.send(email).await?;
        }

        Ok(())
    }
}
