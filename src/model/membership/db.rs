use super::Membership;
use crate::couchdb::{CouchdbDocument, CouchdbMeta};
use anyhow::Result;
use log::debug;
use once_cell::sync::Lazy;
use serde_json::json;
use std::{collections::HashMap, sync::RwLock};

static ID_TO_MEMBERSHIP: Lazy<RwLock<HashMap<u32, Membership>>> =
    Lazy::new(|| RwLock::new(HashMap::new()));
static UID_TO_ID: Lazy<RwLock<HashMap<u32, u32>>> = Lazy::new(|| RwLock::new(HashMap::new()));
static VP_ID_TO_IDS: Lazy<RwLock<HashMap<u32, Vec<u32>>>> =
    Lazy::new(|| RwLock::new(HashMap::new()));
static WAITING_VP_ID_TO_IDS: Lazy<RwLock<HashMap<u32, Vec<u32>>>> =
    Lazy::new(|| RwLock::new(HashMap::new()));

impl Membership {
    pub const ID_PREFIX: &'static str = "membership";

    pub fn find_for_id(id: u32) -> Option<Self> {
        ID_TO_MEMBERSHIP.read().ok()?.get(&id).cloned()
    }

    pub fn find_for_user_id(uid: u32) -> Option<Self> {
        Self::find_for_id(Self::id_for_uid(uid)?)
    }

    pub fn find_all() -> Vec<Self> {
        ID_TO_MEMBERSHIP
            .read()
            .map(|id_to_membership| id_to_membership.values().cloned().collect())
            .unwrap_or_default()
    }

    pub fn find_all_for_vp_id(vp_id: u32) -> Vec<Self> {
        Self::ids_for_vp_id(vp_id)
            .into_iter()
            .filter_map(Self::find_for_id)
            .collect()
    }

    pub fn find_all_for_waiting_vp_id(waiting_vp_id: u32) -> Vec<Self> {
        Self::ids_for_waiting_vp_id(waiting_vp_id)
            .into_iter()
            .filter_map(Self::find_for_id)
            .collect()
    }

    pub fn next_id() -> u32 {
        ID_TO_MEMBERSHIP
            .read()
            .map(|id_to_membership| id_to_membership.keys().max().copied().unwrap_or_default() + 1)
            .expect("Could not find next membership id")
    }

    pub fn count() -> usize {
        ID_TO_MEMBERSHIP
            .read()
            .map(|id_to_membership| id_to_membership.len())
            .unwrap_or_default()
    }

    /// Shares which are active on the first of january next year
    pub fn bid_shares() -> f32 {
        ID_TO_MEMBERSHIP
            .read()
            .map(|id_to_membership| {
                id_to_membership
                    .values()
                    .filter(|membership| membership.can_bid())
                    .map(|membership| f32::from(membership.shares()))
                    .sum()
            })
            .unwrap_or_default()
    }

    /// Members which are active on the first of january next year
    pub fn bid_members() -> usize {
        ID_TO_MEMBERSHIP
            .read()
            .map(|id_to_membership| {
                id_to_membership
                    .values()
                    .filter(|membership| membership.can_bid())
                    .count()
            })
            .unwrap_or_default()
    }

    pub fn id_for_uid(uid: u32) -> Option<u32> {
        UID_TO_ID.read().ok()?.get(&uid).copied()
    }

    pub fn ids_for_vp_id(vp_id: u32) -> Vec<u32> {
        VP_ID_TO_IDS
            .read()
            .ok()
            .and_then(|vp_id_to_ids| vp_id_to_ids.get(&vp_id).cloned())
            .unwrap_or_default()
    }

    pub fn ids_for_waiting_vp_id(vp_id: u32) -> Vec<u32> {
        WAITING_VP_ID_TO_IDS
            .read()
            .ok()
            .and_then(|waiting_vp_id_to_ids| waiting_vp_id_to_ids.get(&vp_id).cloned())
            .unwrap_or_default()
    }

    pub async fn load_all() -> Result<()> {
        let memberships: Vec<Membership> = crate::couchdb::find_documents(json!({
            "selector": {
                "_id": {
                    "$gt": format!("{}_", Self::ID_PREFIX),
                    "$lt": format!("{}_{{}}", Self::ID_PREFIX),
                }
            },
            "limit": 10_000u32
        }))
        .await?;

        let mut uid_to_id = HashMap::new();
        let mut vp_id_to_ids = HashMap::new();
        let mut waiting_vp_id_to_ids = HashMap::new();
        for membership in memberships.iter() {
            for uid in membership.users.iter() {
                uid_to_id.insert(*uid, membership.id);
            }
            if let Some(vp_id) = membership.vp_id() {
                vp_id_to_ids
                    .entry(vp_id)
                    .or_insert_with(Vec::new)
                    .push(membership.id);
            }
            if let Some(waiting_vp_id) = membership.waiting_vp_id() {
                waiting_vp_id_to_ids
                    .entry(waiting_vp_id)
                    .or_insert_with(Vec::new)
                    .push(membership.id);
            }
        }
        let id_to_membership = memberships
            .into_iter()
            .map(|membership| (membership.id, membership))
            .collect::<HashMap<u32, Self>>();

        debug!("Loaded {} memberships", id_to_membership.len());

        *UID_TO_ID
            .write()
            .map_err(|_| anyhow::format_err!("UID_TO_ID is poisoned"))? = uid_to_id;
        *VP_ID_TO_IDS
            .write()
            .map_err(|_| anyhow::format_err!("VP_ID_TO_IDS is poisoned"))? = vp_id_to_ids;
        *WAITING_VP_ID_TO_IDS
            .write()
            .map_err(|_| anyhow::format_err!("WAITING_VP_ID_TO_IDS is poisoned"))? =
            waiting_vp_id_to_ids;
        *ID_TO_MEMBERSHIP
            .write()
            .map_err(|_| anyhow::format_err!("ID_TO_MEMBERSHIP is poisoned"))? = id_to_membership;

        Ok(())
    }

    pub async fn save(&mut self) -> Result<()> {
        crate::couchdb::save_batch(vec![self]).await?;
        Self::load_all().await?;
        Ok(())
    }

    pub async fn remove(self) -> Result<()> {
        crate::couchdb::remove(self).await?;
        Self::load_all().await?;
        Ok(())
    }
}

impl CouchdbDocument for Membership {
    fn meta(&self) -> &CouchdbMeta {
        &self.couchdb_meta
    }

    fn meta_mut(&mut self) -> &mut CouchdbMeta {
        &mut self.couchdb_meta
    }

    fn set_revision(&mut self, revision: String) {
        self.meta_mut().set_revision(revision);

        let mut uid_to_id = UID_TO_ID.write().expect("UID_TO_ID is poisoned");
        for uid in self.users.iter() {
            uid_to_id.insert(*uid, self.id);
        }

        let mut vp_id_to_ids = VP_ID_TO_IDS.write().expect("VPID_TO_IDS is poisoned");
        vp_id_to_ids.iter_mut().for_each(|(vp_id, ids)| {
            let vp = Some(*vp_id) == self.vp_id();
            let contained = ids.contains(&self.id);
            if vp && !contained {
                ids.push(self.id);
            } else if !vp && contained {
                ids.retain(|id| id != &self.id)
            }
        });

        ID_TO_MEMBERSHIP
            .write()
            .expect("ID_TO_MEMBERSHIP is poisoned")
            .insert(self.id, self.to_owned());
    }
}
