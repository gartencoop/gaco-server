use chrono::{DateTime, NaiveDate, Utc};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq, Eq)]
pub struct Bid {
    #[serde(
        deserialize_with = "crate::common::deserialize_datetime",
        serialize_with = "crate::common::serialize_datetime"
    )]
    change_at: DateTime<Utc>,

    #[serde(
        deserialize_with = "crate::common::deserialize_date",
        serialize_with = "crate::common::serialize_date"
    )]
    start_at: NaiveDate,

    cents: u32,
}

impl Bid {
    pub fn new(cents: u32, start_at: NaiveDate) -> Self {
        Self {
            change_at: crate::common::now_datetime(),
            start_at,
            cents,
        }
    }

    pub fn start_at(&self) -> NaiveDate {
        self.start_at
    }

    pub fn change_at(&self) -> DateTime<Utc> {
        self.change_at
    }

    pub fn cents(&self) -> u32 {
        self.cents
    }
}

#[derive(Default)]
pub struct BiddingYear {
    pub months: u8,
    pub cents: u32,
}
