use super::Event;
use crate::{
    couchdb::{upload_attachment, CouchdbDocument, CouchdbMeta},
    model::file::File,
};
use anyhow::Result;
use uuid::Uuid;

impl Event {
    pub fn id_prefix(private: bool) -> &'static str {
        if private {
            "private_event"
        } else {
            "public_event"
        }
    }

    pub async fn find_for_id(id: u32) -> Option<Self> {
        crate::couchdb::find_document_by_nid(id).await.ok()
    }

    pub async fn save(&mut self) -> Result<()> {
        crate::couchdb::save_batch(vec![self]).await?;

        Ok(())
    }

    /// Upload attachment, add file structure to document and save document.
    pub async fn add_file(&mut self, user_id: u32, filename: String, data: Vec<u8>) -> Result<()> {
        let attachment = Uuid::new_v4().to_string();

        upload_attachment(self, &attachment, data).await?;

        self.files.push(File::new(user_id, filename, attachment));

        self.save().await
    }

    /// Delete attachment, remove file structure of document and save document.
    pub async fn remove_file(&mut self, path: &str) -> Result<()> {
        let Some(index) = self
            .files
            .iter()
            .position(|file| file.path(self.nid) == path)
        else {
            log::debug!("Could not delete file as it was not found: {path}");
            return Ok(());
        };

        let file = self.files.remove(index);
        self.meta_mut().remove_attachment(file.attachment());
        self.save().await
    }
}

impl CouchdbDocument for Event {
    fn meta(&self) -> &CouchdbMeta {
        &self.couchdb_meta
    }

    fn meta_mut(&mut self) -> &mut CouchdbMeta {
        &mut self.couchdb_meta
    }
}
