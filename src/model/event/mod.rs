mod category;
mod db;
mod location;

#[cfg(feature = "import")]
mod import;

use super::{comment::Comment, document::Document, file::File, html::Html, language::Language};
use crate::couchdb::CouchdbMeta;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};

pub use category::Category;
pub use location::Location;

#[derive(Debug, Default, Deserialize, Serialize)]
#[serde(default)]
pub struct Event {
    #[serde(flatten)]
    couchdb_meta: CouchdbMeta,

    nid: u32,
    private: bool,
    path: String,
    language: Language,

    title: String,
    #[serde(
        deserialize_with = "crate::common::deserialize_datetime",
        serialize_with = "crate::common::serialize_datetime"
    )]
    created_at: DateTime<Utc>,
    created_by: u32,

    #[serde(
        deserialize_with = "crate::common::deserialize_datetime",
        serialize_with = "crate::common::serialize_datetime"
    )]
    start_at: DateTime<Utc>,
    #[serde(
        deserialize_with = "crate::common::deserialize_datetime",
        serialize_with = "crate::common::serialize_datetime"
    )]
    end_at: DateTime<Utc>,

    #[serde(skip_serializing_if = "Option::is_none")]
    description: Option<Html>,
    #[serde(skip_serializing_if = "Option::is_none")]
    takealong: Option<Html>,

    location: Location,
    category: Category,
    #[serde(skip_serializing_if = "Option::is_none")]
    template: Option<u32>,

    #[serde(skip_serializing_if = "Vec::is_empty")]
    files: Vec<File>,
    #[serde(skip_serializing_if = "Vec::is_empty")]
    comments: Vec<Comment>,
}

impl Event {
    pub fn title(&self) -> &str {
        &self.title
    }

    pub fn set_title(&mut self, title: String) {
        self.title = title;
    }

    pub fn path(&self) -> &str {
        &self.path
    }

    pub fn created_by_name(&self) -> Option<String> {
        crate::model::user::User::name_for_uid(self.created_by)
    }

    pub fn created_at(&self) -> DateTime<Utc> {
        self.created_at
    }

    pub fn start_at(&self) -> DateTime<Utc> {
        self.start_at
    }

    pub fn set_start_at(&mut self, start_at: DateTime<Utc>) {
        self.start_at = start_at;
    }

    pub fn end_at(&self) -> DateTime<Utc> {
        self.end_at
    }

    pub fn set_end_at(&mut self, end_at: DateTime<Utc>) {
        self.end_at = end_at;
    }

    pub fn image_paths(&self) -> Vec<String> {
        self.files
            .iter()
            .filter(|file| file.is_image())
            .map(|file| file.path(self.couchdb_meta.id_number()))
            .collect()
    }

    pub fn files(&self, show_creator_names: bool) -> Vec<Value> {
        self.files
            .iter()
            .filter(|file| !file.is_image())
            .map(|file| {
                json!({
                    "name": file.name(),
                    "created_by": file.created_by_name().filter(|_|show_creator_names),
                    "path": file.path(self.couchdb_meta.id_number()),
                })
            })
            .collect()
    }

    pub fn comments(&self) -> &Vec<Comment> {
        &self.comments
    }

    pub fn description(&self) -> Option<&Html> {
        self.description.as_ref()
    }

    pub fn set_description(&mut self, description: Html) {
        self.description = Some(description);
    }

    pub fn takealong(&self) -> Option<&str> {
        self.takealong.as_deref()
    }

    pub fn set_takealong(&mut self, takealong: Html) {
        self.takealong = Some(takealong);
    }

    pub fn location(&self) -> &Location {
        &self.location
    }

    pub fn set_location(&mut self, location: Location) {
        self.location = location;
    }

    pub fn category(&self) -> &Category {
        &self.category
    }

    pub fn set_category(&mut self, category: Category) {
        self.category = category;
    }

    pub fn template_id(&self) -> Option<u32> {
        self.template
    }

    pub fn set_template_id(&mut self, template_id: Option<u32>) {
        self.template = template_id;
    }

    pub async fn template(&self) -> Option<Document> {
        Document::find_for_id(self.template?).await
    }
}
