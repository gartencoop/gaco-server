use super::{Category, Event, Location};
use crate::{
    couchdb::CouchdbMeta,
    model::{comment::Comment, file::File, html::Html, language::Language},
};
use chrono::{DateTime, Utc};

impl Event {
    #[allow(clippy::too_many_arguments)]
    pub fn import(
        nid: u32,
        private: bool,
        path: String,
        language: Language,
        title: String,
        created_at: DateTime<Utc>,
        created_by: u32,
        description: Option<Html>,
        takealong: Option<Html>,
        location: Location,
        category: Category,
        start_at: DateTime<Utc>,
        end_at: DateTime<Utc>,
        template: Option<u32>,
        files: Vec<File>,
        comments: Vec<Comment>,
    ) -> Self {
        let couchdb_meta = CouchdbMeta::new(Self::id_prefix(private), nid);

        #[allow(clippy::needless_update)]
        Self {
            couchdb_meta,
            nid,
            private,
            path,
            language,
            title,
            created_at,
            created_by,
            description,
            takealong,
            location,
            category,
            start_at,
            end_at,
            template,
            files,
            comments,
            ..Default::default()
        }
    }
}
