use serde::{Deserialize, Serialize};
use strum_macros::{EnumIter, EnumString, IntoStaticStr};

#[derive(
    Clone, Copy, Debug, Deserialize, Serialize, Default, IntoStaticStr, EnumIter, EnumString,
)]
pub enum Category {
    Fahrradverteilung,
    Sprinter,
    Koko,
    Tunsel,
    #[default]
    Sonstige,
}
