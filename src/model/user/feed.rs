use super::User;
use anyhow::{bail, Result};

impl User {
    async fn feed_token(&self) -> Result<String> {
        let feed_token = match self.feed_token.clone() {
            None => {
                let feed_token = uuid::Uuid::new_v4().to_string();

                let mut user = self.clone();
                user.feed_token = Some(feed_token.clone());
                user.save().await?;

                feed_token
            }
            Some(feed_token) => feed_token,
        };

        Ok(feed_token)
    }

    pub async fn rss_url(&self) -> Result<String> {
        Ok(format!("/rss/{}/{}", self.id, self.feed_token().await?))
    }

    pub async fn ics_url(&self) -> Result<String> {
        Ok(format!("/ics/{}/{}", self.id, self.feed_token().await?))
    }

    pub fn verify_feed_token(&self, token: &str) -> Result<()> {
        if self.feed_token.as_deref() != Some(token) {
            bail!("feed token does not match");
        }

        Ok(())
    }
}
