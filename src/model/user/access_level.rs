use serde::{Deserialize, Serialize};

use crate::GacoSession;

use super::User;

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Debug, Deserialize, Serialize)]
pub enum AccessLevel {
    Guest,
    User,
    Member,
    Manager,
    Admin,
}

impl AccessLevel {
    pub async fn for_login_id(login_id: Option<String>) -> Option<Self> {
        let uid = GacoSession::uid_for_login_id(&login_id?).await?;
        let user = User::find_for_uid(uid)?;

        Some(user.access_level())
    }

    /// Returns true if the access level is at least `User`.
    pub fn user(&self) -> bool {
        *self >= Self::User
    }

    /// Returns true if the access level is at least `Member`.
    pub fn member(&self) -> bool {
        *self >= Self::Member
    }

    /// Returns true if the access level is at least `Manager`.
    pub fn manager(&self) -> bool {
        *self >= Self::Manager
    }

    /// Returns true if the access level is at least `Admin`.
    pub fn admin(&self) -> bool {
        *self >= Self::Admin
    }

    pub fn to_string(&self, session: &GacoSession) -> String {
        session.tr(match self {
            AccessLevel::Guest => "access_level_guest",
            AccessLevel::User => "access_level_user",
            AccessLevel::Member => "access_level_member",
            AccessLevel::Manager => "access_level_manager",
            AccessLevel::Admin => "access_level_admin",
        })
    }
}

impl Default for AccessLevel {
    fn default() -> Self {
        Self::Guest
    }
}
