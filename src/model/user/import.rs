use super::{AccessLevel, User};
use crate::{couchdb::CouchdbMeta, model::language::Language};
use chrono::{DateTime, Utc};

impl User {
    #[allow(clippy::too_many_arguments)]
    pub fn import(
        id: u32,
        username: String,
        password: String,
        created_at: DateTime<Utc>,
        email: String,
        language: Language,
        feed_token: Option<String>,
        overwrite_access_level: Option<AccessLevel>,
    ) -> Self {
        let couchdb_meta = CouchdbMeta::new(Self::ID_PREFIX, id);

        Self {
            couchdb_meta,
            id,
            username,
            password: Some(password),
            created_at,
            email,
            language,
            feed_token,
            overwrite_access_level,
            ..Default::default()
        }
    }

    pub fn set_name(&mut self, name: String) {
        self.name = Some(name);
    }

    pub fn set_phone(&mut self, phone: String) {
        self.phone = Some(phone);
    }

    pub fn set_feed_token(&mut self, feed_token: Option<String>) {
        self.feed_token = feed_token;
    }

    pub fn set_password_token(&mut self, password_token: Option<String>) {
        self.password_token = password_token;
    }
}
