mod crypto;

use super::User;
use crate::{session::locales::LOCALES, CONFIG};
use anyhow::Result;
use fluent_templates::Loader;
use lettre::{
    message::header::ContentType, transport::smtp::authentication::Credentials, AsyncSmtpTransport,
    AsyncTransport, Tokio1Executor,
};
use mime::TEXT_PLAIN_UTF_8;

impl User {
    pub fn check_password(&self, password: &str) -> Result<()> {
        if option_env!("BYPASS_LOGIN").is_some() {
            log::error!("Login bypass is active!");
            return Ok(());
        }
        let Some(stored) = self.password.as_ref() else {
            anyhow::bail!("No password set for user {}", self.username());
        };

        crypto::check_password(stored, password)
    }

    pub fn check_password_token(&self, password_token: &str) -> Result<()> {
        if self.password_token.as_deref() != Some(password_token) {
            anyhow::bail!("Invalid password token");
        }

        Ok(())
    }

    /// Also invalidates the password token
    pub fn set_password(&mut self, password: &str) -> Result<()> {
        let password = crypto::new(password)?;

        self.password = Some(password);
        self.password_token.take();

        Ok(())
    }

    pub async fn send_reset_password_email(&self, forgot: bool) -> Result<()> {
        let password_token = match self.password_token.clone() {
            Some(token) => token,
            None => {
                let token = uuid::Uuid::new_v4().to_string();
                let mut user = self.clone();
                user.password_token = Some(token.clone());
                user.save().await?;
                token
            }
        };

        let smtp_credentials =
            Credentials::new(CONFIG.email.username.clone(), CONFIG.email.password.clone());

        let mailer = AsyncSmtpTransport::<Tokio1Executor>::relay(&CONFIG.email.host)?
            .credentials(smtp_credentials)
            .build();

        let Some(address) = CONFIG
            .email
            .membership_to_overwrite
            .as_ref()
            .and_then(|email| email.parse().ok())
            .or_else(|| self.email().parse().ok())
        else {
            anyhow::bail!("Could not parse email address: {}", self.email());
        };

        let lang_id = self.language().id();
        let subject = LOCALES.lookup(
            &lang_id,
            if forgot {
                "password_forgot_email_subject"
            } else {
                "new_user_email_subject"
            },
        );
        let body = LOCALES.lookup_with_args(
            &lang_id,
            if forgot {
                "password_forgot_email_body"
            } else {
                "new_user_email_body"
            },
            &std::iter::once(("token".into(), password_token.into()))
                .chain(std::iter::once(("user".into(), self.id.into())))
                .collect(),
        );

        let email = lettre::Message::builder()
            .header(ContentType::from(TEXT_PLAIN_UTF_8))
            .from(CONFIG.email.membership_from.parse()?)
            .to(address)
            .subject(subject)
            .body(body)?;

        mailer.send(email).await?;

        Ok(())
    }
}
