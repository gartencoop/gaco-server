use super::User;
use crate::couchdb::{find_next_user_id, CouchdbDocument, CouchdbMeta};
use anyhow::Result;
use log::debug;
use once_cell::sync::Lazy;
use serde_json::json;
use std::{collections::HashMap, sync::RwLock};

static UID_TO_USER: Lazy<RwLock<HashMap<u32, User>>> = Lazy::new(|| RwLock::new(HashMap::new()));
static USERNAME_TO_UID: Lazy<RwLock<HashMap<String, u32>>> =
    Lazy::new(|| RwLock::new(HashMap::new()));
static EMAIL_TO_UID: Lazy<RwLock<HashMap<String, u32>>> = Lazy::new(|| RwLock::new(HashMap::new()));

impl User {
    pub const ID_PREFIX: &'static str = "user";

    pub fn exists_for_username(username: &str) -> bool {
        Self::uid_for_username(username)
            .map(Self::exists_for_uid)
            .unwrap_or_default()
    }

    pub fn find_for_username(username: &str) -> Option<Self> {
        Self::find_for_uid(Self::uid_for_username(username)?)
    }

    pub fn find_for_username_or_email(username: &str) -> Option<Self> {
        Self::find_for_uid(Self::uid_for_username_or_email(username)?)
    }

    pub fn exists_for_uid(uid: u32) -> bool {
        UID_TO_USER
            .read()
            .ok()
            .map(|uid_to_user| uid_to_user.contains_key(&uid))
            .unwrap_or_default()
    }

    pub fn find_for_uid(uid: u32) -> Option<Self> {
        UID_TO_USER.read().ok()?.get(&uid).cloned()
    }

    pub fn find_all() -> Vec<Self> {
        UID_TO_USER
            .read()
            .ok()
            .map(|uid_to_user| uid_to_user.values().cloned().collect())
            .unwrap_or_default()
    }

    pub fn uid_for_username(username: &str) -> Option<u32> {
        USERNAME_TO_UID.read().ok()?.get(username).copied()
    }

    pub fn uid_for_username_or_email(username: &str) -> Option<u32> {
        let username = username.to_lowercase();
        USERNAME_TO_UID
            .read()
            .ok()?
            .get(&username)
            .copied()
            .or_else(|| EMAIL_TO_UID.read().ok()?.get(&username).copied())
    }

    pub fn name_for_uid(uid: u32) -> Option<String> {
        UID_TO_USER
            .read()
            .ok()?
            .get(&uid)
            .map(|user| user.name().to_owned())
    }

    pub async fn load_all() -> Result<()> {
        let users: Vec<User> = crate::couchdb::find_documents(json!({
            "selector": {
                "_id": {
                    "$gt": format!("{}_", Self::ID_PREFIX),
                    "$lt": format!("{}_{{}}", Self::ID_PREFIX),
                }
            },
            "limit": 10_000u32
        }))
        .await?;

        let username_to_uid: HashMap<String, u32> = users
            .iter()
            .map(|user| (user.username.to_lowercase(), user.uid()))
            .collect();
        let email_to_uid: HashMap<String, u32> = users
            .iter()
            .map(|user| (user.email.clone(), user.uid()))
            .collect();
        let uid_to_user = users
            .into_iter()
            .map(|user: Self| (user.uid(), user))
            .collect::<HashMap<u32, Self>>();

        debug!("Loaded {} users", uid_to_user.len());

        *UID_TO_USER
            .write()
            .map_err(|_| anyhow::format_err!("UID_TO_USER is poisoned"))? = uid_to_user;
        *USERNAME_TO_UID
            .write()
            .map_err(|_| anyhow::format_err!("USERNAME_TO_UID is poisoned"))? = username_to_uid;
        *EMAIL_TO_UID
            .write()
            .map_err(|_| anyhow::format_err!("EMAIL_TO_UID is poisoned"))? = email_to_uid;

        Ok(())
    }

    pub async fn save(&mut self) -> Result<()> {
        crate::couchdb::save_batch(vec![self]).await?;
        Self::load_all().await?;
        Ok(())
    }

    pub async fn create(
        email: String,
        username: String,
        name: Option<String>,
        phone: Option<String>,
    ) -> Result<()> {
        let id = find_next_user_id().await?;
        let mut user = Self {
            couchdb_meta: CouchdbMeta::new(Self::ID_PREFIX, id),
            id,
            email,
            username,
            name,
            phone,
            created_at: chrono::Utc::now(),
            ..Default::default()
        };
        user.save().await?;
        Self::load_all().await?;

        let user = Self::find_for_uid(id).ok_or_else(|| anyhow::format_err!("User not found"))?;
        user.send_reset_password_email(false).await?;

        Ok(())
    }
}

impl CouchdbDocument for User {
    fn meta(&self) -> &CouchdbMeta {
        &self.couchdb_meta
    }

    fn meta_mut(&mut self) -> &mut CouchdbMeta {
        &mut self.couchdb_meta
    }

    fn set_revision(&mut self, revision: String) {
        self.meta_mut().set_revision(revision);

        UID_TO_USER
            .write()
            .expect("UID_TO_USER is poisoned")
            .insert(self.id, self.to_owned());
    }
}
