mod table;

use self::table::{CRYPT_DECODE, CRYPT_ENCODE, INVALID_VALUE};
use anyhow::Result;
use rand::Rng;
use sha2::{Digest, Sha512};

pub fn check_password(stored: &str, password: &str) -> Result<()> {
    if stored.len() != 55 {
        anyhow::bail!("Password hash has wrong length");
    }

    let settings = &stored[..12];
    let crypted = crypt(settings, password)?;

    if stored != crypted {
        anyhow::bail!("Password does not match!");
    }

    Ok(())
}

pub fn new(password: &str) -> Result<String> {
    let salt = drupal_base64(&rand::rng().random::<u64>().to_be_bytes()[..6]);
    let settings = format!("$S$D{salt}");
    crypt(&settings, password)
}

fn crypt(settings: &str, password: &str) -> Result<String> {
    if settings.len() != 12 {
        anyhow::bail!("Wrong settings length!");
    }

    if !settings.starts_with("$S$") {
        anyhow::bail!("Settings is in wrong format!");
    }

    let log_2_rounds = CRYPT_DECODE[settings.chars().nth(3).unwrap() as usize];
    if log_2_rounds == INVALID_VALUE {
        anyhow::bail!("Invalid value for log_2_rounds");
    }
    let count = 1u64 << log_2_rounds;
    let salt = &settings[4..];
    let mut hasher = Sha512::new();
    hasher.update(salt);
    hasher.update(password);
    let mut hash = hasher.finalize().to_vec();
    for _ in 0..count {
        let mut hasher = Sha512::new();
        hasher.update(hash);
        hasher.update(password);
        hash = hasher.finalize().to_vec();
    }

    let hash_base64 = drupal_base64(&hash);
    let crypted = format!("{settings}{}", &hash_base64[..43]);
    if crypted.len() != 55 {
        anyhow::bail!("Wrong crypted length!");
    }

    Ok(crypted)
}

// Close enough port of wrongly implemented base64 from drupal code (possibly missing chars at the end)
fn drupal_base64(input: &[u8]) -> String {
    let mut output = vec![];
    for i in 0..input.len() / 3 {
        let value: u32 = ((input[i * 3 + 2] as u32) << 16)
            | ((input[i * 3 + 1] as u32) << 8)
            | input[i * 3] as u32;
        output.push(CRYPT_ENCODE[(value & 0x3F) as usize]);
        output.push(CRYPT_ENCODE[((value >> 6) & 0x3F) as usize]);
        output.push(CRYPT_ENCODE[((value >> 12) & 0x3F) as usize]);
        output.push(CRYPT_ENCODE[((value >> 18) & 0x3F) as usize]);
    }

    output.into_iter().map(|u| u as char).collect()
}
