mod db;

use super::membership::Shares;
use crate::couchdb::{CouchdbMeta, DeletedDocument};
use anyhow::Result;
use serde::{Deserialize, Serialize};

#[derive(Debug, Default, Deserialize, Serialize, Clone)]
#[serde(default)]
pub struct Income {
    #[serde(flatten)]
    couchdb_meta: CouchdbMeta,

    year: u16,
    pseudonym: String,
    shares: Shares,

    income_cents: Option<u32>,
    workdays: u32,
}

impl Income {
    pub fn new(year: u16, shares: Shares) -> Self {
        let pseudonym = uuid::Uuid::new_v4().to_string();

        Self {
            couchdb_meta: CouchdbMeta::new(Self::ID_PREFIX, format!("{year}_{pseudonym}")),
            year,
            pseudonym,
            shares,
            income_cents: None,
            workdays: 0,
        }
    }

    pub fn year(&self) -> u16 {
        self.year
    }

    pub fn pseudonym(&self) -> &str {
        &self.pseudonym
    }

    pub fn into_pseudonym(self) -> String {
        self.pseudonym
    }

    pub fn shares(&self) -> Shares {
        self.shares
    }

    pub fn income_cents(&self) -> Option<u32> {
        self.income_cents
    }

    pub fn workdays(&self) -> u32 {
        self.workdays
    }

    pub fn has_income_cents(&self) -> bool {
        self.income_cents.is_some()
    }

    pub async fn set_income_cents_and_workdays(
        mut self,
        income_cents: u32,
        workdays: u32,
    ) -> Result<()> {
        self.income_cents = Some(income_cents);
        self.workdays = workdays;
        self.save().await
    }

    pub fn into_deleted_document(self) -> DeletedDocument {
        self.couchdb_meta.into()
    }
}
