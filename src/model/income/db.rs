use super::Income;
use crate::couchdb::{CouchdbDocument, CouchdbMeta};
use anyhow::Result;
use serde_json::json;

impl Income {
    pub const ID_PREFIX: &'static str = "income";

    pub async fn find(year_pseudonym: &str) -> Option<Self> {
        crate::couchdb::find_document_by_id(&format!("{}_{year_pseudonym}", Self::ID_PREFIX))
            .await
            .ok()
    }

    pub async fn find_all_for_year(year: u16) -> Vec<Self> {
        crate::couchdb::find_documents(json!({
            "selector": {
                "_id": {
                    "$gt": format!("{}_{year}_", Self::ID_PREFIX),
                    "$lt": format!("{}_{year}_{{}}", Self::ID_PREFIX),
                }
            },
            "limit": 10_000u32
        }))
        .await
        .map_err(|err| {
            log::error!("Error while finding incomes: {:?}", err);
            err
        })
        .ok()
        .unwrap_or_default()
    }

    pub async fn save(&mut self) -> Result<()> {
        crate::couchdb::save_batch(vec![self]).await?;

        Ok(())
    }
}

impl CouchdbDocument for Income {
    fn meta(&self) -> &CouchdbMeta {
        &self.couchdb_meta
    }

    fn meta_mut(&mut self) -> &mut CouchdbMeta {
        &mut self.couchdb_meta
    }
}
