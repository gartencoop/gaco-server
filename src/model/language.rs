use serde::{Deserialize, Serialize};
use unic_langid::{langid, LanguageIdentifier};

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq, Eq)]
pub enum Language {
    German,
    English,
}

impl Default for Language {
    fn default() -> Self {
        Self::German
    }
}

impl Language {
    pub fn id(&self) -> LanguageIdentifier {
        match self {
            Self::German => langid!("de-DE"),
            Self::English => langid!("en-GB"),
        }
    }
}
