use super::Comment;
use crate::model::{file::File, html::Html};
use chrono::{DateTime, Utc};

impl Comment {
    pub fn import(
        created_by: u32,
        created_at: DateTime<Utc>,
        body: Html,
        files: Vec<File>,
    ) -> Self {
        Self {
            created_by,
            created_at,
            body,
            files,
        }
    }
}
