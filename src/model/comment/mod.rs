#[cfg(feature = "import")]
mod import;
mod view;

use super::{file::File, html::Html};
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
#[serde(default)]
pub struct Comment {
    created_by: u32,
    #[serde(
        deserialize_with = "crate::common::deserialize_datetime",
        serialize_with = "crate::common::serialize_datetime"
    )]
    created_at: DateTime<Utc>,
    body: Html,
    #[serde(skip_serializing_if = "Vec::is_empty")]
    files: Vec<File>,
}

impl Comment {
    pub fn created_by_name(&self) -> Option<String> {
        crate::model::user::User::name_for_uid(self.created_by)
    }

    pub fn created_at(&self) -> DateTime<Utc> {
        self.created_at
    }

    pub fn body(&self) -> &str {
        self.body.as_ref()
    }

    pub fn image_paths(&self, id_number: u32) -> Vec<String> {
        self.files
            .iter()
            .filter(|file| file.is_image())
            .map(|file| file.path(id_number))
            .collect()
    }

    pub fn files(&self, id_number: u32, show_creator_names: bool) -> Vec<Value> {
        self.files
            .iter()
            .filter(|file| !file.is_image())
            .map(|file| {
                json!({
                    "name": file.name(),
                    "created_by": file.created_by_name().filter(|_|show_creator_names),
                    "path": file.path(id_number),
                })
            })
            .collect()
    }
}
