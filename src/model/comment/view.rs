use super::Comment;
use fluent_templates::LanguageIdentifier;
use serde_json::{json, Value};

impl Comment {
    pub fn view(
        &self,
        id_number: u32,
        show_creator_names: bool,
        _locale: &LanguageIdentifier,
    ) -> Value {
        json!({
            "created_at": self.created_at().to_rfc3339(),
            "created_by": self.created_by_name().filter(|_|show_creator_names),
            "body": self.body(),
            "images": self.image_paths(id_number),
            "files": self.files(id_number, show_creator_names),
        })
    }
}
