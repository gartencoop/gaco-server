use super::Transaction;
use crate::{
    common::date_string,
    couchdb::{CouchdbDocument, CouchdbMeta},
};
use anyhow::Result;
use chrono::NaiveDate;
use serde::Deserialize;
use serde_json::json;

impl Transaction {
    pub const ID_PREFIX: &'static str = "transaction";

    pub async fn find_years() -> Vec<u16> {
        #[derive(Deserialize)]
        struct Row {
            #[serde(rename = "key")]
            year: u16,
        }
        crate::couchdb::view::<Row>("_design/transaction/_view/years?reduce=true&group_level=1")
            .await
            .unwrap_or_default()
            .into_iter()
            .map(|row| row.year)
            .collect()
    }

    pub async fn find_for_year(year: u16) -> Vec<Self> {
        crate::couchdb::find_documents(json!({
            "selector": {
                "_id": {
                    "$gt": format!("{}_", Self::ID_PREFIX),
                    "$lt": format!("{}_{{}}", Self::ID_PREFIX),
                },
                "year": {
                    "$eq": year
                }
            },
            "limit": 10_000u32
        }))
        .await
        .ok()
        .unwrap_or_default()
    }

    pub async fn find_all() -> Vec<Self> {
        crate::couchdb::find_documents(json!({
            "selector": {
                "_id": {
                    "$gt": format!("{}_", Self::ID_PREFIX),
                    "$lt": format!("{}_{{}}", Self::ID_PREFIX),
                },
            },
            "limit": 10_000u32
        }))
        .await
        .ok()
        .unwrap_or_default()
    }

    pub async fn find_for_membership_id(membership_id: u32) -> Vec<Self> {
        crate::couchdb::find_documents(json!({
            "selector": {
                "_id": {
                    "$gt": format!("{}_", Self::ID_PREFIX),
                    "$lt": format!("{}_{{}}", Self::ID_PREFIX),
                },
                "membership_id": {
                    "$eq": membership_id
                }
            },
            "limit": 10_000u32
        }))
        .await
        .ok()
        .unwrap_or_default()
    }

    pub async fn find_by_id(transaction_id: &str) -> Option<Self> {
        crate::couchdb::find_single_document(json!({
            "selector": {
                "_id": {
                    "$eq": transaction_id,
                }
            }
        }))
        .await
        .ok()
        .flatten()
    }

    pub async fn find_for_date_range(oldest: NaiveDate, newest: NaiveDate) -> Vec<Self> {
        crate::couchdb::find_documents(json!({
            "selector": {
                "_id": {
                    "$gt": format!("{}_", Self::ID_PREFIX),
                    "$lt": format!("{}_{{}}", Self::ID_PREFIX),
                },
                "date": {
                    "$gte": date_string(&oldest),
                    "$lte": date_string(&newest),
                }
            },
            "limit": 10_000u32
        }))
        .await
        .ok()
        .unwrap_or_default()
    }

    pub async fn save_new_transactions(transaction: Vec<Self>) -> Result<usize> {
        let inserted_documents = crate::couchdb::save_batch_ignore_existing(transaction).await?;

        Ok(inserted_documents)
    }

    pub async fn save(&mut self) -> Result<()> {
        crate::couchdb::save_batch(vec![self]).await?;

        Ok(())
    }
}

impl CouchdbDocument for Transaction {
    fn meta(&self) -> &CouchdbMeta {
        &self.couchdb_meta
    }

    fn meta_mut(&mut self) -> &mut CouchdbMeta {
        &mut self.couchdb_meta
    }
}
