pub mod csv;
mod db;

use crate::couchdb::CouchdbMeta;
use chrono::NaiveDate;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
#[serde(default)]
pub struct Transaction {
    #[serde(flatten)]
    couchdb_meta: CouchdbMeta,

    /// Date of transaction
    #[serde(
        deserialize_with = "crate::common::deserialize_date",
        serialize_with = "crate::common::serialize_date"
    )]
    date: NaiveDate,

    /// Year this transaction pays for
    year: u16,

    description: String,
    cents: u32,
    membership_id: Option<u32>,
}

impl Transaction {
    pub fn id(&self) -> &str {
        self.couchdb_meta.id()
    }

    pub fn date(&self) -> NaiveDate {
        self.date
    }

    pub fn year(&self) -> u16 {
        self.year
    }

    pub fn set_year(&mut self, year: u16) {
        self.year = year;
    }

    pub fn description(&self) -> &str {
        &self.description
    }

    pub fn cents(&self) -> u32 {
        self.cents
    }

    pub fn membership_id(&self) -> Option<u32> {
        self.membership_id
    }

    pub fn set_membership_id(&mut self, membership_id: u32) {
        self.membership_id = Some(membership_id);
    }
}
