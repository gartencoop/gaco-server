mod access_level;
mod db;
mod feed;
mod password;

#[cfg(feature = "import")]
mod import;

use super::{language::Language, membership::Membership};
use crate::couchdb::CouchdbMeta;
pub use access_level::AccessLevel;
use anyhow::Result;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq, Eq)]
#[serde(default)]
pub struct User {
    #[serde(flatten)]
    couchdb_meta: CouchdbMeta,

    id: u32,
    #[serde(
        deserialize_with = "crate::common::deserialize_datetime",
        serialize_with = "crate::common::serialize_datetime"
    )]
    created_at: DateTime<Utc>,

    username: String,
    password: Option<String>,
    email: String,

    #[serde(skip_serializing_if = "Option::is_none")]
    name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    phone: Option<String>,

    language: Language,

    password_token: Option<String>,
    feed_token: Option<String>,

    overwrite_access_level: Option<AccessLevel>,
}

impl User {
    pub fn id(&self) -> u32 {
        self.id
    }

    pub fn membership(&self) -> Option<Membership> {
        Membership::find_for_user_id(self.id)
    }

    /// Only return a Membership if it is active
    pub fn active_membership(&self) -> Option<Membership> {
        self.membership()
            .filter(|membership| membership.is_active())
    }

    pub fn access_level(&self) -> AccessLevel {
        if let Some(access_level) = self.overwrite_access_level {
            return access_level;
        }

        if self
            .active_membership()
            .filter(|membership| membership.is_active())
            .is_some()
        {
            AccessLevel::Member
        } else {
            AccessLevel::User
        }
    }

    pub fn login(username: &str, password: &str) -> Option<User> {
        let user = Self::find_for_username_or_email(username)?;
        if user.check_password(password).is_err() {
            return None;
        }

        Some(user)
    }

    pub fn username(&self) -> &str {
        &self.username
    }

    pub fn name(&self) -> &str {
        self.name.as_deref().unwrap_or(self.username.as_str())
    }

    pub fn uid(&self) -> u32 {
        self.id
    }

    pub fn language(&self) -> &Language {
        &self.language
    }

    pub fn set_language(&mut self, language: Language) {
        self.language = language;
    }

    pub fn email(&self) -> &str {
        &self.email
    }

    pub fn set_email(&mut self, email: String) {
        self.email = email;
    }

    pub fn phone(&self) -> Option<&str> {
        self.phone.as_deref()
    }

    pub fn created_at(&self) -> DateTime<Utc> {
        self.created_at
    }
}

pub fn username_for_id<S>(uid: &u32, s: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    match User::name_for_uid(*uid) {
        None => s.serialize_str(&format!("User not found ({uid})")),
        Some(name) => s.serialize_str(&name),
    }
}
