use serde::{Deserialize, Serialize};
use strum_macros::{EnumIter, EnumString, IntoStaticStr};
#[derive(Clone, Copy, Debug, Deserialize, Serialize, IntoStaticStr, EnumIter, EnumString)]
pub enum Topic {
    #[serde(rename = "AG")]
    #[strum(serialize = "AG")]
    Ag,
    #[serde(rename = "Anbau")]
    #[strum(serialize = "Anbau")]
    Anbau,
    #[serde(rename = "Archiv")]
    #[strum(serialize = "Archiv")]
    Archiv,
    #[serde(rename = "Aufteilung")]
    #[strum(serialize = "Aufteilung")]
    Aufteilung,
    #[serde(rename = "Baugruppe")]
    #[strum(serialize = "Baugruppe")]
    Baugruppe,
    #[serde(rename = "Bauwoche")]
    #[strum(serialize = "Bauwoche")]
    Bauwoche,
    #[serde(rename = "Beschlüsse")]
    #[strum(serialize = "Beschlüsse")]
    Beschluesse,
    #[serde(rename = "Car-Sharing")]
    #[strum(serialize = "Car-Sharing")]
    CarSharing,
    #[serde(rename = "Ernte")]
    #[strum(serialize = "Ernte")]
    Ernte,
    #[serde(rename = "Fahrrad")]
    #[strum(serialize = "Fahrrad")]
    Fahrrad,
    #[serde(rename = "Finanzen")]
    #[strum(serialize = "Finanzen")]
    Finanzen,
    #[serde(rename = "Forum")]
    #[strum(serialize = "Forum")]
    Forum,
    #[serde(rename = "Frühjahrswerkstatt")]
    #[strum(serialize = "Frühjahrswerkstatt")]
    Fruehjahrswerkstatt,
    #[serde(rename = "Gurken & Zucchini")]
    #[strum(serialize = "Gurken & Zucchini")]
    GurkenUndZucchini,
    #[serde(rename = "Hoffest")]
    #[strum(serialize = "Hoffest")]
    Hoffest,
    #[serde(rename = "Hofstelle Tunsel")]
    #[strum(serialize = "Hofstelle Tunsel")]
    HofstelleTunsel,
    #[serde(rename = "Homepage")]
    #[strum(serialize = "Homepage")]
    Homepage,
    #[serde(rename = "Hygiene")]
    #[strum(serialize = "Hygiene")]
    Hygiene,
    #[serde(rename = "Interne Dokumente")]
    #[strum(serialize = "Interne Dokumente")]
    InterneDokumente,
    #[serde(rename = "Kochen")]
    #[strum(serialize = "Kochen")]
    Kochen,
    #[serde(rename = "KoKo")]
    #[strum(serialize = "KoKo")]
    KoKo,
    #[serde(rename = "Kreislaufwirtschaft")]
    #[strum(serialize = "Kreislaufwirtschaft")]
    Kreislaufwirtschaft,
    #[serde(rename = "Landkauf")]
    #[strum(serialize = "Landkauf")]
    Landkauf,
    #[serde(rename = "Mitgliedereinsatz")]
    #[strum(serialize = "Mitgliedereinsatz")]
    Mitgliedereinsatz,
    #[serde(rename = "Mitgliedschaft")]
    #[strum(serialize = "Mitgliedschaft")]
    Mitgliedschaft,
    #[serde(rename = "MV")]
    #[strum(serialize = "MV")]
    Mv,
    #[serde(rename = "Netzwerk SoLaWi")]
    #[strum(serialize = "Netzwerk SoLaWi")]
    NetzwerkSoLaWi,
    #[serde(rename = "Newsletter")]
    #[strum(serialize = "Newsletter")]
    Newsletter,
    #[serde(rename = "ÖA")]
    #[strum(serialize = "ÖA")]
    OeA,
    #[serde(rename = "Orga- und Entscheidungsstruktur")]
    #[strum(serialize = "Orga- und Entscheidungsstruktur")]
    OrgaUndEntscheidungsstruktur,
    #[serde(rename = "Politisches")]
    #[strum(serialize = "Politisches")]
    Politisches,
    #[serde(rename = "Protokoll")]
    #[strum(serialize = "Protokoll")]
    Protokoll,
    #[serde(rename = "Räume")]
    #[strum(serialize = "Räume")]
    Raeume,
    #[serde(rename = "Rechtsform")]
    #[strum(serialize = "Rechtsform")]
    Rechtsform,
    #[serde(rename = "Veranstaltungen")]
    #[strum(serialize = "Veranstaltungen")]
    Veranstaltungen,
    #[serde(rename = "Verteilpunkt")]
    #[strum(serialize = "Verteilpunkt")]
    Verteilpunkt,
    #[serde(rename = "Verteilung")]
    #[strum(serialize = "Verteilung")]
    Verteilung,
}
#[cfg(feature = "import")]
impl Topic {
    #[allow(dead_code)]
    pub fn from(topic: &str) -> Option<Self> {
        match topic {
"AG Arbeitswelt"|"AG-Besuch der Koko"|"AG Bildung"|"AG-Feedback"|"AG Selbstverständnis"|"AG Verteilung"|"AG-Verteilung"|"AG Verteilung 02.12.2020"|"AG-Verteilung 20.08.2020"|"AG-Vorbereitung"|"Diversity AG 2. Treffen"|"Heilkräuter"|"Heilkräuter-AG"|"Heilkräuterbeet"|"Heilkräuter-Beet"|"Heilpflanzen"|"Heilpflanzen AG"|"Konservieren-AG"|"Kräuterbeet"|"Rechtsfragen-AG"|"Support-Team"
=> Some(Self::Ag),
"Anbau"|"Bienen"|"Bioland"|"Düngekonzept"|"Elektrotraktor"|"Ernterhythmus"|"Erntetag"|"E-Traktor"|"Flächenverbrauch"|"Folientunnel"|"Futsu Black"|"garbanzo negro"|"Gemüseinfo"|"Getreide"|"Honig"|"Kürbis"|"Kürbis nachpflanzen"|"Navet"|"Pflanzen"|"Rückmeldung Gemüsemengen"|"Saatgut"|"Schädlinge"|"Schwermetallbelastung"|"Sonderkulturen"|"Sorten in der coop"|"Spaghettibohne"|"Tatsoi"|"Thymian"|"tomaten"|"Tomatensorten"|"Tunnelpflanzung"|"Tunnelpflanzungen"|"Übergabe"|"Unkraut"|"Vereinzeln"|"Vereinzeln Möhre und Chicoree"|"Vogelmiere"|"Weizen"|"Wildkräuter"|"Zucchini"
=> Some(Self::Anbau),
"2018"|"Arbeistwelt"|"Arbeitsaufwand"|"Arbeitsplan"|"Arbeitsspitzen"|"Archiv"|"Atomfrei - Rüstungsfrei - SoLaWi verpflichtet"|"Aufbau"|"Ausbildung"|"Austritte"|"Avocados"|"Bäckerei"|"Backtag"|"Bad Boll"|"Band"|"BBZ"|"Beratung"|"Bericht"|"Besuch"|"Bildband"|"Bilder"|"Bildung"|"Blanchierter und dann eingefrorener Rosenkohl innen verfärbt"|"Blei"|"Blog"|"Brot"|"buffet"|"Cooperative"|"Das Neue Haus"|"Diskussion"|"Diy"|"EFD"|"einfaches eintragen"|"E-mail"|"Erinnerung an eine Möglichkeit"|"Erlebnisbericht"|"EVS"|"Fastenzeit"|"Feedback"|"Feedback Bogen"|"FIBL"|"Fortbildung"|"Foto"|"FREE"|"Gesprächsthemen"|"Heizkosten sparen"|"Hühnerstall ausmisten"|"Ideen für eine Weiterentwicklung der Gartencoop"|"Info Tisch"|"Klimaschutzpreis 2020"|"Konflikt"|"Kündigung"|"Lagern"|"Lagerung"|"LED-Strahler"|"Lehrling"|"Lied"|"Luft zum Atmen"|"Marktwert"|"Mitfahrgelegenheiten"|"Mobilfunk"|"Schrot & Korn"|"Schützenallee"|"Spaß"|"Telefonat mit Landwirtschaftsamt Freiburg"|"Theke"|"Treffen vom 16.02.12"|"Trennung"|"Übernachtung"|"Umweltanalyse"|"Umweltanlaysen"|"Umwelt-Recherche"|"Weitergabe"|"wissenschaftliche Arbeit"|"wissenschaftliche Arbeiten und Ressourcen"|"Zeit für eine Würdigung der Arbeit des Kernteams"|"Zimmer"
=> Some(Self::Archiv),
"Aufteilung"|"Getreide Mehl Aufteilung"|"Ernteaufteilung"
=> Some(Self::Aufteilung),
"AG Baugruppe"
=> Some(Self::Baugruppe),
"Abwasserleitung"|"Aufräumen"|"Bauwoche"|"Bauwoche Baustelle Fahrrad AG Baugruppe"
=> Some(Self::Bauwoche),
"Corona"
=> Some(Self::Beschluesse),
"car"|"Car-Sharing Transporter"|"Sprinterverterteilung"|"Sprinterverterteilung 2020"|"Transporterfahrt - Mitfahrer_innen"
=> Some(Self::CarSharing),
"Ernte"|"Kartoffel-Ernte"|"KartoffelErnte"|"Zucchini Ernte"|"Zwiebellagerernte"
=> Some(Self::Ernte),
"Coopräder"|"EBike"|"Fahrrad"|"Fahrradanhänger"|"Fahrradreparaturen"|"Fahrradweg"|"Radeltour"
=> Some(Self::Fahrrad),
"Beitrag 2019"|"Betriebsspiegel"|"Buchführung"|"Budget"|"Budget 2012"|"Budget 2014"|"Budget 2020"|"Budgetauffüllung"|"Direktkredite"|"Finanzen"|"Finanzierungslücke"|"Finanzupdate"|"Finanzupdates"|"Fundraising"|"Gebote"|"Haushalt"|"Haushalt 2013 auffüllen"|"Haushalt 2014"|"Haushalt 2018"|"Herbstspende"|"Investitionen 2021"|"Kleinanlegerschutzgesetz"|"Konto 26.02.2019"|"Körperschaftssteuer"|"Preisstudie"|"Quartalsupdate"|"Spende"|"Spenden"|"Steuern"|"Umsatzsteuer"|"Zwischenbilanztreffen"|"Grundkonsens und konkrete Vorschläge zum Budget"|"Finanzupdate 2022"|"Bieteverfahren"|"Bietverfahren"
=> Some(Self::Finanzen),
"Forum"|"Forum 11.02.2020"|"Forum 24.03.2020"|"Forum 24.09.2019"
=> Some(Self::Forum),
"frühjahrscamp"|"Frühjahrshütte 2020"|"Frühjahrshütte 2022"|"Frühjahrswerkstatt"|"Frühjahrswerkstatt 2019"|"Frühjahrswerkstatt 2020"
=> Some(Self::Fruehjahrswerkstatt),
"﻿.."|"13.11.2018"|"18.12.18"|"26.05.2020"|"Allgemeine To-do-Liste"|"Altholz"|"Buch"|"Die"|"KW 10"|"KW 12"|"KW 15"|"KW 16"|"KW 17"|"KW 48"|"KW 49 2018"|"Liste"|"Mediation"|"Ob-Kart"|"Orga"|"Ort steht noch nicht fest!"|"Plakate und Flyer im Strandi abzuholen"|"Regiokarte zu vergeben"|"suchen"|"Szenario 1. Verteilpunktverantwortung:"|"Vorbereitung"|"Zusammenfassung"|"Zwischenstandstreffen"
=> None,
"Gurken"|"Zucchinis"|"Zucchini Ernte 2022"|"Zucchini und Gurken Ernte 2023"|"Zucchini und Gurken Ernte 2024"
=> Some(Self::GurkenUndZucchini),
"Hoffest"|"Hoffest 2014"|"Hoffest 2015"|"Hoffest 2016"|"Hoffest 2017"|"Hoffest 2018"|"Hoffest 2020"|"Hoffest 2021"|"Hoffest 2021 Hoffest"|"Hoffest 2022"|"Solifest"|"Soli-Hoffest"|"Hoffest 2023"|"Hoffest 2024"
=> Some(Self::Hoffest),
"3 Tage Tunseln"|"Bodenanalysen"|"Bodenproben"|"Bodenuntersuchungen"|"Container"|"Donnerstag"|"Hof"|"Hofstelle"|"Hofstelle Infrastruktur Baugruppe Baustellen Technik"|"Kinder"|"Kirschen"|"Klo"|"Küche"|"Lager"|"Napfkisten"|"NAPF Kisten"|"Naturschutz"|"Reparatur"|"T1"|"T2 Abbau"|"Tagebuch; Hofstelle"|"Tunnel"|"Tunsel"|"Überschwemmung"|"Wasseranalyse"|"Wasseranalysen"|"Wasserleitung"|"Landwirtschaft"|"Beregnungsverband"|"Sommerwerkstatt 2024"|"Sommerwerkstatt"|"Hofkauf"
=> Some(Self::HofstelleTunsel),
"Dokumentation"|"Drupal"|"Handbuch"|"Homepage"|"Homepage-How-to"|"Selbstverständnis"|"Tagebuch"|"Webspace"|"Stellenausschreibung"|"Stellenangebote"
=> Some(Self::Homepage),
"covid19"|"EHEC"
=> Some(Self::Hygiene),
"Allmende"|"Allmende Breisgau"|"Alternatiba"|"Anbauplan"|"Anbauplanung"|"Büro"|"Checkliste"|"Entscheidungsstruktur"|"GC-Adressliste"|"HowTo"|"Infomaterial"|"Infomaterialien"|"Internet Telefonanschluss"|"Jahresplanung"|"Kalender"|"Karten. Infoblatt für Mitglieder"|"Kisten"|"Mailinglisten"|"Neu-Mitglieder"|"öffentlichkeit"|"Organisationsstruktur"|"Orte"|"Regionale Ernährung"|"Schlachtung"|"Solidarität"|"Wichtige Termine"|"Wissen"|"Zahlenschloss"|"Corona Gartencoop Mitglieder"|"Jahresplanung 2023"|"Beschluss Bieteverfahren"|"Jahresplanung 2024"
=> Some(Self::InterneDokumente),
"Als Hauptmahlzeit oder Nachspeise"|"Aufstriche"|"Boretsch"|"Einkochaktion"|"Einkochaktion für Sommergemüse"|"Einkochen"|"Einmachen"|"Essblüten"|"Essen und Trinken"|"Haltbar machen"|"Kaltauszug"|"Kochaktion"|"Kochbuch"|"Kochen"|"Konservieren"|"Konservieren und einmachen"|"Kräuter"|"Kürbisse schälen"|"Rezept"|"Rezepte"|"Sauerkraut"|"Tomateneinkochen"|"Weiterverarbeitung"|"Wildgemüse"|"Zwiebelkuchen vegan"
=> Some(Self::Kochen),
"Koko"|"KoKo 01.02.2022"|"Koko 01.12.2020"|"KoKo 02.02.2020"|"koko 02.02.2021 protokoll"|"KoKo 02.03.2021"|"KoKo 02.06.2020"|"KoKo 03.08.2021"|"KoKo 04.08.2020"|"KoKo 06.07.2021"|"KoKo 07.04.2020"|"Koko 07.05.2019"|"KoKo 07.07.2020"|"KoKo 08.06.2021"|"Koko 09.03.2020"|"KoKo 09-06.2020"|"KoKo 09.10.18"|"KoKo 11.05.2021"|"KoKo 11.09. 2018"|"KoKo 12.01.2021"|"KoKo 12.03.2019"|"KoKo 12.05.2020"|"Koko 12.10.21"|"KoKo 12.11.2019"|"KoKo 13.04.2021"|"KoKo 13.06.2018"|"KoKo 14.01.2020"|"KoKo 14.09.2021"|"KoKo 15.09.2020"|"KoKo 15.12.2020"|"KoKo 16.02.2021"|"KoKo 16.03.2021"|"KoKo 17.08.2021"|"KoKo 17.08.21"|"KoKo 17.11.2020"|"KoKo 18.12.2018"|"KoKo 20.07.2021"|"KoKo 2019"|"KoKo 21.04.2020"|"KoKo 21.08.2021"|"KoKo 22.05.18"|"KoKo 22.12.2020"|"KoKo 23.04.2019"|"KoKo 24.05.18"|"KoKo 25.02.2020"|"KoKo 25.03.2021"|"Koko 25.05.2021"|"KoKo 25.09.2018"|"Koko 25.4.17"|"KoKo 26.10.2021"|"KoKo 27.08.2019"|"KoKo 28.01.2020"|"KoKo 28.04.20"|"Koko 28.08.2018"|"Koko 29.03.2022"|"KoKo 29.09.2020"|"KoKo 30.03.2021"|"KoKo 30.06.2020"|"KoKo 30.07.19"|"KoKo 31.07.2018"|"Koko + davor Abendessen"|"Koko Protokoll"|"KoKo Struktur"|"KoKo-Struktur"|"KoKo virtuell 05.05.2020"|"KoKo virtuell 21.04.2020"|"Konsens"|"Nachfolger'in für Koko-Mitarbeit und weitere Interessierte gesucht"|"Sonder-KoKo zum Thema"|"virtuelle KoKo 13.04.20"|"virtuelle KoKo 24.03.20"|"virtuelle KoKo 31.03.20"|"virtuelle KoKo 31.03.2020"|"Perspektiven"|"Koko Protokoll 17.12.2022"|"Protokoll KoKo 14.01.2023"|"Protokoll Koko 31.01.2023"|"Protokoll KoKo 12.09.2023"|"Koko; Mitgliederversammlung"|"KoKo Protokoll 30.01.2024"|"KoKo-Struktur neu"|"KoKo Protokoll 13.08.2024"
=> Some(Self::KoKo),
"Äpfel"|"Bienencoop"|"Fleisch"|"Hühner"|"Imkerei"|"Rinder"|"Tiere"
=> Some(Self::Kreislaufwirtschaft),
"Äckersyndikat"|"Bodensyndikat"|"Kauf der Äcker"|"Landkauf"
=> Some(Self::Landkauf),
"Anmeldung zu Arbeitseinsätzen"|"Arbeitseinsätze"|"Einsätze"|"Ernteeinsatz"|"Freiwuscheln."|"Jäteinsatz"|"Jäten"|"jäten un so"|"Lagerernte"|"Lagerernte am Freitag"|"Lagerernten"|"Migliedereinsätze"|"Mitgliedereinsatz"|"Mitgliedereinsatz Donnerstag"|"Mitgliedseinsätze"|"Samstag: Lagerernte"|"Sonntagseinsatz"|"Wochenend-Einsatz"|"Wuschel- und Jäteeinsatz"
=> Some(Self::Mitgliedereinsatz),
"Beteiligung"|"Binnenvertrag"|"Fördermitgliedschaften"|"Fragebogen"|"Gesprächskultur"|"Mitglieder"|"Mitgliedsbeitrag"|"neue Mitglieder"|"Partizipation"|"Umfrage"
=> Some(Self::Mitgliedschaft),
"Einladung MV 2019"|"Einladung MV 2020"|"Mitgliederversammlung"|"Mitgliederversammlung 2018"|"Mitgliederversammlung 2020"|"Mitgliederversammlung 2021"|"MV"|"MV 2018"|"MV 2019"|"MV 2021"|"MV -Nachbereitung"|"MV Nachbereitung"|"MV November 2018"|"MVs"|"MV-Vorbereitung"|"MV Vorbereitung 2021"|"MV Vorbereitung Checkliste"|"MVV Rechtsform Betrieb"|"Vorbereitung MV"|"MV Vorbereitung"|"MV 2023"|"MV Vorbereitung 2022"|"MV 2022"|"Sonder MV"|"MV Einladung"
=> Some(Self::Mv),
"CSA"|"CSA for Europe"|"Direktkredit"|"eine gemeinsame Kochaktion und Infostand der SoLaWis"|"Kooperativen"|"Luzernenhof"|"Netzwerk solawi"|"Netzwerk Solidarische Landwirtschaft"|"Olivenöl"|"pro Spezies rara"|"Seefelden"|"Seefelden Milch Landwirtschaft"|"SoLaWi"|"Solawi-Verteiler"|"solidarische Landwirtschaft"|"solidarische Lanwirtschaft crowdfunding"|"Streuobstwiese"
=> Some(Self::NetzwerkSoLaWi),
"Newasletter KW 27"|"Newsletter"|"Newsletter 2018 KW 31"|"Newsletter 2018 KW 32"|"Newsletter KW 05 2019"|"Newsletter KW 09 2019"|"Newsletter KW 10 2019"|"Newsletter KW 10 2020"|"Newsletter KW 11 2019"|"Newsletter KW 12 2020"|"Newsletter KW 13 2019"|"Newsletter KW 14 2019"|"Newsletter KW 19"|"Newsletter KW 19 2019"|"Newsletter KW 20"|"Newsletter KW 21"|"Newsletter KW 22"|"Newsletter KW 23"|"Newsletter KW 24"|"Newsletter KW 25"|"Newsletter KW 26"|"Newsletter KW 26 2020"|"Newsletter KW 27 2019"|"Newsletter KW 28"|"Newsletter KW 29"|"Newsletter KW 30"|"Newsletter KW 31 2020"|"Newsletter KW 3 2019"|"Newsletter KW 33 2018"|"Newsletter KW 34 2018"|"Newsletter KW 34 2021"|"Newsletter KW 35"|"Newsletter KW 35 2018"|"Newsletter KW 35 2020"|"Newsletter KW 36 2018"|"Newsletter KW 36 2019"|"Newsletter KW 36 2020"|"Newsletter KW 37 2018"|"Newsletter KW 37 2019"|"Newsletter KW 37 2020"|"Newsletter KW 38 2018"|"Newsletter KW 39"|"Newsletter KW 40 2018"|"Newsletter KW 41 2018"|"Newsletter KW 4 2019"|"Newsletter KW 4 2020"|"Newsletter KW 47 2018"|"Newsletter KW 47 2019"|"Newsletter KW 50 2018"|"Newsletter KW 5 2020"|"Newsletter KW 6 2019"|"Newsletter KW6 2022"|"Newsletter KW 6 : erste Pflanzungen und Aussaaten - 11.2 GaCo Forum - 18.2 : Buchvorstellung"|"Newsletter KW 8:"|"Newsletter KW 9 2020"|"Newsletter Nachtrag"|"Newslketter KW 23 2020"|"Sondernewsletter"
=> Some(Self::Newsletter),
"Abschlussarbeit"|"Anfragen"|"article"|"Einführung"|"Einladung"|"Film"|"Flyer"|"Infostand"|"Kulturinformationen"|"Mitgliederwerbung"|"Oeffentlichkeitsarbeit"|"Öffentlichkeit"|"Öffentlichkeitsarbeit"|"Praxisjahr"|"Presse"|"Radio"|"Selbstdarstellung"|"Südkurier"|"Veranstaltung"|"Vernetzung"|"Werbematerialien"|"Werbung"|"Werbung für den 23.9.12. Plakate"|"Vortrag"|"PodCast"|"Radio PodCast"|"Forschungsprojekt"|"Resilienz"|"Frankreich"|"GartenCoopCafé"|"AG Kommunikation SoMe und Außerdarstellung"
=> Some(Self::OeA),
"Arbeitswelt"|"basisdemokratie"|"bedarfslohn"|"Beschluss"|"Beschlüsse"|"Bildungs-Aktivitäten z.T. durch steuerlich abzugsfähige Spenden finanzieren"|"Büro-Team"|"C Gruppe"|"C-Gruppe"|"C-Gruppenwahl"|"Differenzierte Stufen im Konsensverfahren bei der Entscheidungsfindung"|"Entscheidungen"|"Entscheidungsstrukturen"|"Hütte"|"Jahresplanung 2019"|"Kommunikation"|"Konferenz"|"Logo"|"Lohnpolitik"|"Moderation"|"Orga & Entscheidungsstrukturen"|"Organisation"|"Prozess"|"Prozessvorschlag"|"Reflexion"|"Satzung"|"Selbstorganisation"|"Selbstverständnis Gartencoop"|"supervision"|"Verwaltung"|"Wohnen"
=> Some(Self::OrgaUndEntscheidungsstruktur),
"1. Mai 2020"|"AfD"|"Briançon"|"Cecosesola"|"confédération paysanne"|"Demo"|"Demonstration"|"Eine Cooperative iun Venezuela mit 20 000 Mitgliedern - hierarchiefrei"|"Elsass"|"Endgültige Stilllegung von Fessenheim"|"Energiewende"|"Ernährungssouveränitat"|"Fessenheim"|"Flüchtlingspolitik"|"Fukushima"|"Gentechnik"|"Gesundheitssouveränität"|"Getränke"|"Internationalismus"|"Kampagne Olivenöl aus Palästina"|"Klima"|"Klimacamp"|"kollektives Eigentum"|"Landgrabbing"|"Lebensmittelskandale"|"Mietshäusersyndikat"|"Mietshäuser Syndikat"|"Monsanto"|"Nach Israels Krieg gegen Gaza  Soldarität mit Palästina"|"Nach Israels Krieg gegen Gaza Soldarität mit Palästina"|"Plädoyer"|"Politik"|"politisches Klima"|"Raumfahrt"|"rechte Tendenzen"|"Reclaim The Fields"|"Repression"|"Solidarität mit palästinensischen Kleinbauern und  Familien aus Gaza"|"Solidarität mit palästinensischen Kleinbauern und Familien aus Gaza"|"Syngenta"|"Umweltpsychologie"|"Venezuela"|"Arbeitsbedingungen in Europas Landwirtschaft"|"Georgien...die"|"Kampagne"|"Wie den globalen Systemwechsel schaffen. Erfahrungen aus der Lebensgemeinschaft Tamera"|"Mediathek Arte"
=> Some(Self::Politisches),
"Hütenwochenende"|"Jahresplan"|"Jahresplanung 2021"|"Jahresübersicht"|"Koko- Protokoll"|"Koko-Protokoll"|"KoKo Protokoll 01.02.2022"|"KoKo Protokoll 04.08.2020"|"Koko Protokoll 05.11.2019"|"Koko Protokoll 07.05.19"|"KoKo Protokoll 07.07.2020"|"KoKo Protokoll 09.06.2020"|"KoKo Protokoll 14.09.2021"|"KoKo Protokoll 16.02.2021"|"KoKo Protokoll 18.01.2022"|"KoKo Protokoll 18.08.2020"|"KoKo Protokoll 21.07.2020"|"Koko Protokoll 21.12.2021"|"KoKo Protokoll 31.08.2021"|"KoKo Protokoll vom 26.02.2019"|"Praktika"|"Protokoll"|"Protokoll 11.03.2021"|"Protokoll 19.01.2021"|"Protokoll 27.04.2021"|"Protokoll AG-Verteilung"|"Protokoll Forum 19.11.2019"|"Protokoll Koko"
=> Some(Self::Protokoll),
"Raum"|"Raummiete"|"Veranstaltungsorte"
=> Some(Self::Raeume),
"GbR"|"GmbH"|"Rechtsform"|"Rechtsformstruktur"|"Versicherung"|"Versicherungen"
=> Some(Self::Rechtsform),
"Agrikulturfest"|"Agrikulturfestival"|"Benefizkonzert"|"Connecting Day"|"connecting day 2021"|"Ein gemeinsamer Infostand beim Agrikulturfestival"|"Exkursion"|"Filmvorführung"|"Flohmarkt"|"Früchte des Zorns"|"Führung"|"Führungen"|"Hofführung"|"Infoveranstaltung"|"Kennenlernen und Spaß haben"|"Kleinkunstabend"|"Kneipe"|"Konzert"|"Mitgliederforum 2021"|"Mitgliederversammlung 2014"|"Mundenhof"|"Offbeat"|"on Air"|"Sanie-Dienst"|"Setzlingsmarkt"|"setzlingsmarkt 2021"|"Skill Sharing"|"Soliveranstaltung"|"Strandcafé"|"Strandi"|"Tag des kleinbäuerlichen Widerstands"|"TdkW 2020"|"Variété"|"Variete`Theater"|"Veranstaltung; solidarität"|"Vokü"|"Wildkräuter-Spaziergang"|"World Café 10.7"
=> Some(Self::Veranstaltungen),
"Knöpfle"|"Ölmühle"|"Paten"|"Patenschaften"|"Patensystem"|"Stühlinger"|"SUSI"|"Treffen der VP Koordinator*innen vorbereiten..."|"Verteilpunkt"|"Verteilpunkte"|"Verteilpunktsuche"|"VP Knöpfle"|"VP Koordinator*innen Treffen"|"VPs"|"VP Schwarz120"|"VP Wiehre"|"Zähringen"|"VP-Koordination"
=> Some(Self::Verteilpunkt),
"Alles rund um die Gemüseverteilung mit Rad und Transporter"|"Anhänger"|"Gemüse-Transport-Technik"|"Koordination"|"Koordination Verteilung"|"Kürbisse"|"Kürbisverteilung"|"selbstorganisierte Fahrradverteilung"|"Umschlagplatz"|"Verteilpause 2022"|"Verteiltag"|"Verteilung"|"Verteilung neu"|"Verteilungsfreie Woche"|"Was gibts wann"|"Wiesentalstr."|"Sicherheit Umschlagplatz"|"Mobilität"
=> Some(Self::Verteilung),
topic => {println!("Could not find topic \"{topic}\"");None} ,}
    }
}
