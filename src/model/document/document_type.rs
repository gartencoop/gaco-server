use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum DocumentType {
    News,
    Media,
    Internal,
    Offtopic,
    Page,
    Template,
}

impl Default for DocumentType {
    fn default() -> Self {
        Self::Internal
    }
}

impl DocumentType {
    pub fn as_str(&self) -> &str {
        match self {
            DocumentType::News => "news",
            DocumentType::Media => "media",
            DocumentType::Internal => "internal",
            DocumentType::Offtopic => "offtopic",
            DocumentType::Page => "page",
            DocumentType::Template => "template",
        }
    }

    pub fn try_from_str(s: &str) -> Option<Self> {
        Some(match s {
            "news" => Self::News,
            "media" => Self::Media,
            "internal" => Self::Internal,
            "offtopic" => Self::Offtopic,
            "page" => Self::Page,
            "template" => Self::Template,
            _ => return None,
        })
    }
}
