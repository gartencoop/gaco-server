mod db;
mod document_type;
mod team;
mod topic;

#[cfg(feature = "import")]
mod import;

use super::{file::File, html::Html};
use crate::couchdb::CouchdbMeta;
use chrono::{DateTime, Utc};
pub use document_type::DocumentType;
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};

pub use team::Team;
pub use topic::Topic;

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
#[serde(default)]
pub struct Document {
    #[serde(flatten)]
    couchdb_meta: CouchdbMeta,

    /// types:
    /// * `news`
    /// * `media`
    /// * `internal`
    /// * `offtopic`
    /// * `page`
    /// * `template`
    ty: DocumentType,
    nid: u32,
    path: String,

    title: String,
    #[serde(
        deserialize_with = "crate::common::deserialize_datetime",
        serialize_with = "crate::common::serialize_datetime"
    )]
    created_at: DateTime<Utc>,
    created_by: u32,
    #[serde(skip_serializing_if = "Vec::is_empty")]
    teams: Vec<Team>,
    #[serde(skip_serializing_if = "Option::is_none")]
    topic: Option<Topic>,
    body: Html,
    #[serde(skip_serializing_if = "Vec::is_empty")]
    files: Vec<File>,
}

impl Document {
    pub fn created_by_name(&self) -> Option<String> {
        crate::model::user::User::name_for_uid(self.created_by)
    }

    pub fn created_at(&self) -> DateTime<Utc> {
        self.created_at
    }

    pub fn link(&self) -> String {
        format!("/{}", self.path)
    }

    pub fn edit_link(&self) -> String {
        format!("/{}/edit", self.path)
    }

    pub fn image_paths(&self) -> Vec<String> {
        self.files
            .iter()
            .filter(|file| file.is_image())
            .map(|file| file.path(self.couchdb_meta.id_number()))
            .collect()
    }

    pub fn files(&self, show_creator_names: bool) -> Vec<Value> {
        self.files
            .iter()
            .filter(|file| !file.is_image())
            .map(|file| {
                json!({
                    "name": file.name(),
                    "created_by": file.created_by_name().filter(|_|show_creator_names),
                    "path": file.path(self.couchdb_meta.id_number()),
                })
            })
            .collect()
    }

    pub fn title(&self) -> &str {
        &self.title
    }

    pub fn path(&self) -> &str {
        &self.path
    }

    pub fn body(&self) -> &str {
        self.body.as_ref()
    }

    pub fn set_title(&mut self, title: String) {
        self.title = title;
    }

    pub fn set_body(&mut self, body: Html) {
        self.body = body;
    }

    pub fn teams(&self) -> &[Team] {
        &self.teams
    }

    pub fn set_teams(&mut self, teams: Vec<Team>) {
        self.teams = teams;
    }

    pub fn topic(&self) -> Option<&Topic> {
        self.topic.as_ref()
    }

    pub fn set_topic(&mut self, topic: Topic) {
        self.topic = Some(topic);
    }

    pub fn nid(&self) -> u32 {
        self.nid
    }
}
