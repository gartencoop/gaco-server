use serde::{Deserialize, Serialize};
use strum_macros::{EnumIter, EnumString, IntoStaticStr};

/// Arbeitsgruppe
#[derive(
    Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize, IntoStaticStr, EnumIter, EnumString,
)]
#[serde(rename_all = "camelCase")]
pub enum Team {
    ///Kooperativen-Koordination
    #[strum(serialize = "Kooperativen-Koordination (KoKo)")]
    KoKo,
    Seefelden,
    Anbau,
    Baugruppe,
    Werkstatt,
    Finanzen,
    Rechtsform,
    Verteilung,
    Konservieren,
    #[strum(serialize = "Öffentlichkeit")]
    Oeffentlichkeit,
    Homepage,
    Hoffest,
    Spenden,
    #[strum(serialize = "Keins davon")]
    Other,
}
