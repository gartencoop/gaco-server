use super::{team::Team, Document, DocumentType, Topic};
use crate::{
    couchdb::CouchdbMeta,
    model::{file::File, html::Html},
};
use chrono::{DateTime, Utc};

impl Document {
    #[allow(clippy::too_many_arguments)]
    pub fn import(
        nid: u32,
        ty: DocumentType,
        path: String,
        title: String,
        created_at: DateTime<Utc>,
        created_by: u32,
        teams: Vec<Team>,
        topic: Option<Topic>,
        body: Html,
        files: Vec<File>,
    ) -> Self {
        let couchdb_meta = CouchdbMeta::new(Self::id_prefix(&ty), nid);

        #[allow(clippy::needless_update)]
        Self {
            couchdb_meta,
            nid,
            ty,
            path,
            title,
            created_at,
            created_by,
            teams,
            topic,
            body,
            files,
            ..Default::default()
        }
    }
}
