use super::{Document, DocumentType};
use crate::{
    couchdb::{upload_attachment, CouchdbDocument, CouchdbMeta},
    model::file::File,
};
use anyhow::Result;
use serde_json::json;
use uuid::Uuid;

impl Document {
    pub fn id_prefix(ty: &DocumentType) -> &str {
        ty.as_str()
    }

    pub async fn find_for_id(id: u32) -> Option<Self> {
        crate::couchdb::find_document_by_nid(id).await.ok()
    }

    pub async fn find_newest(
        ty: Vec<DocumentType>,
        skip: Option<u32>,
        limit: Option<u32>,
    ) -> Result<Vec<Self>> {
        crate::couchdb::find_documents(json!({
            "selector": {
                "ty": if ty.is_empty() {
                    json!({"$gt": ""})
                } else {
                    json!({
                        "$or": ty.into_iter().map(|ty| json!({"$eq": ty})).collect::<Vec<_>>()
                    })
                }
            },
            "sort": [
                {
                    "created_at": "desc"
                }
            ],
            "skip": skip.unwrap_or(0),
            "limit": limit.unwrap_or(10)
        }))
        .await
    }

    pub async fn save(&mut self) -> Result<()> {
        crate::couchdb::save_batch(vec![self]).await?;

        Ok(())
    }

    /// Upload attachment, add file structure to document and save document.
    pub async fn add_file(&mut self, user_id: u32, filename: String, data: Vec<u8>) -> Result<()> {
        let attachment = Uuid::new_v4().to_string();

        upload_attachment(self, &attachment, data).await?;

        self.files.push(File::new(user_id, filename, attachment));

        self.save().await
    }

    /// Delete attachment, remove file structure of document and save document.
    pub async fn remove_file(&mut self, path: &str) -> Result<()> {
        let Some(index) = self
            .files
            .iter()
            .position(|file| file.path(self.nid) == path)
        else {
            log::debug!("Could not delete file as it was not found: {path}");
            return Ok(());
        };

        let file = self.files.remove(index);
        self.meta_mut().remove_attachment(file.attachment());
        self.save().await
    }
}

impl CouchdbDocument for Document {
    fn meta(&self) -> &CouchdbMeta {
        &self.couchdb_meta
    }

    fn meta_mut(&mut self) -> &mut CouchdbMeta {
        &mut self.couchdb_meta
    }
}
