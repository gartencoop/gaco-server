use super::File;

impl File {
    pub fn import(created_by: u32, name: String, attachment: String) -> Self {
        Self {
            created_by,
            name,
            attachment,
        }
    }

    pub fn set_attachment(&mut self, attachment: String) {
        self.attachment = attachment;
    }
}
