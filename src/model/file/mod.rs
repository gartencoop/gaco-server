#[cfg(feature = "import")]
mod import;

use once_cell::sync::Lazy;
use regex::Regex;
use serde::{Deserialize, Serialize};

static EXTENSION: Lazy<Regex> = Lazy::new(|| Regex::new(r"(?m).([a-zA-Z]+)$").unwrap());

#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq, Eq, PartialOrd, Ord)]
#[serde(default)]
pub struct File {
    created_by: u32,
    name: String,
    attachment: String,
}

impl File {
    pub fn new(created_by: u32, name: String, attachment: String) -> Self {
        Self {
            created_by,
            name,
            attachment,
        }
    }

    pub fn is_image(&self) -> bool {
        let extension = EXTENSION
            .captures_iter(&self.name)
            .next()
            .map(|captures| captures[1].to_lowercase());
        matches!(
            extension.as_deref(),
            Some("jpg" | "jpeg" | "png" | "bmp" | "gif")
        )
    }

    pub fn created_by_name(&self) -> Option<String> {
        crate::model::user::User::name_for_uid(self.created_by)
    }

    pub fn path(&self, doc_number: u32) -> String {
        format!("/file/{}/{}", doc_number, self.attachment)
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn attachment(&self) -> &str {
        &self.attachment
    }

    pub fn into_name(self) -> String {
        self.name
    }
}
