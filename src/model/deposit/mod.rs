pub mod csv;
mod db;
pub mod donation;

use crate::couchdb::CouchdbMeta;
use chrono::NaiveDate;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
#[serde(default)]
pub struct Deposit {
    #[serde(flatten)]
    couchdb_meta: CouchdbMeta,

    /// Date of deposit
    #[serde(
        deserialize_with = "crate::common::deserialize_date",
        serialize_with = "crate::common::serialize_date"
    )]
    date: NaiveDate,

    description: String,
    cents: i32,
    membership_id: Option<u32>,
    deposit_type: DepositType,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub enum DepositType {
    #[default]
    Payment,
    Donation,
}

impl Deposit {
    pub fn id(&self) -> &str {
        self.couchdb_meta.id()
    }

    pub fn date(&self) -> NaiveDate {
        self.date
    }

    pub fn description(&self) -> &str {
        &self.description
    }

    pub fn cents(&self) -> i32 {
        self.cents
    }

    pub fn membership_id(&self) -> Option<u32> {
        self.membership_id
    }

    pub fn deposit_type(&self) -> &DepositType {
        &self.deposit_type
    }

    pub fn set_membership_id(&mut self, membership_id: u32) {
        self.membership_id = Some(membership_id);
    }

    pub fn set_description(&mut self, description: String) {
        self.description = description;
    }
}
