#![allow(unused)]

use super::Deposit;
use crate::{
    common::parse_date,
    couchdb::{remove, CouchdbMeta},
    model::membership::Membership,
};
use anyhow::Result;
use chrono::{Datelike, NaiveDate};
use once_cell::sync::Lazy;
use openssl::hash;
use regex::Regex;
use serde::Deserialize;
use std::{collections::HashMap, io::Cursor};

static GACO_MEMBERSHIP_REGEX: Lazy<Regex> =
    Lazy::new(|| Regex::new(r"(?m)gaco\s*(\d+)").expect("Could not parse regex"));
static NAME_REGEX: Lazy<Regex> =
    Lazy::new(|| Regex::new(r"(?m);\s*([a-zäüöß ]+)$").expect("Could not parse regex"));

//Date,Account Name,Number,Description,Full Category Path,Reconcile,Amount With Sym,Amount Num.,Rate/Price
#[derive(Debug, Deserialize)]
struct Record {
    #[serde(rename = "Date", deserialize_with = "deserialize_date")]
    date: NaiveDate,
    #[serde(rename = "Description")]
    description: String,
    #[serde(rename = "Amount Num.", deserialize_with = "deserialize_cents")]
    cents: i32,
    #[serde(rename = "Account Name")]
    account_name: String,

    #[serde(skip)]
    name: Option<String>,
    /*
    #[serde(rename = "Account Name")]
    account_name: String,
    #[serde(rename = "Number")]
    number: String,
    #[serde(rename = "Full Category Path")]
    full_category_path: String,
    #[serde(rename = "Reconcile")]
    reconcile: String,
    #[serde(rename = "Amount With Sym")]
    amount_with_symbol: String,
    #[serde(rename = "Rate/Price")]
    rate_price: String,
    */
}

fn deserialize_date<'de, D>(deserializer: D) -> Result<NaiveDate, D::Error>
where
    D: serde::Deserializer<'de>,
{
    let s = String::deserialize(deserializer)?;

    parse_date(&s.split('.').rev().collect::<Vec<_>>().join("-"))
        .ok_or_else(|| serde::de::Error::custom(format!("Error parsing date: \"{s}\"")))
}

fn deserialize_cents<'de, D>(deserializer: D) -> Result<i32, D::Error>
where
    D: serde::Deserializer<'de>,
{
    let s = String::deserialize(deserializer)?;

    s.replace('.', "")
        .replace(',', ".")
        .parse::<f32>()
        .map(|amount| (amount * 100.0).round() as i32)
        .map_err(|err| serde::de::Error::custom(format!("Error parsing number: \"{s}\": {err:?}")))
}

impl Record {
    pub fn parse(buf: Vec<u8>) -> Result<Vec<Self>> {
        let cursor = Cursor::new(buf);
        Ok(csv::Reader::from_reader(cursor)
            .deserialize::<Record>()
            .map(|mut record| {
                if let Ok(mut record) = record.as_mut() {
                    let mut hasher = blake3::Hasher::new();
                    hasher.update(record.date.to_string().as_bytes());
                    hasher.update(record.description.as_bytes());
                    hasher.update(record.cents.to_be_bytes().as_slice());
                    record.name = Some(format!("{}", hasher.finalize()))
                }
                record
            })
            .collect::<Result<_, _>>()?)
    }
}

impl From<Record> for Deposit {
    fn from(record: Record) -> Self {
        let lowercase_description = record.description.to_lowercase();
        let membership_id = GACO_MEMBERSHIP_REGEX
            .captures(&lowercase_description)
            .and_then(|captures| captures.get(1))
            .and_then(|m| m.as_str().parse::<u32>().ok())
            .or_else(|| {
                NAME_REGEX
                    .captures(&lowercase_description)
                    .and_then(|captures| captures.get(1))
                    .map(|m| m.as_str().to_string())
                    .and_then(|name| {
                        let memberships = Membership::find_all();
                        memberships
                            .into_iter()
                            .find(|membership| {
                                name.split(" und ")
                                    .any(|name| membership.full_name().to_lowercase() == name)
                            })
                            .map(|membership| membership.id())
                    })
            });

        Self {
            couchdb_meta: CouchdbMeta::new(
                Self::ID_PREFIX,
                record.name.expect("Record.name is not set"),
            ),
            date: record.date,
            description: record.description,
            cents: record.cents,
            membership_id,
            ..Default::default()
        }
    }
}

fn parse(csv: Vec<u8>) -> Result<Vec<Deposit>> {
    Ok(Record::parse(csv)?
        .into_iter()
        .filter(|record| record.account_name.starts_with("GLS"))
        .map(|record| record.into())
        .collect())
}

pub async fn import(csv: Vec<u8>) -> Result<(usize, usize)> {
    let mut deposits = parse(csv)?;
    deposits.sort_by_key(|deposit| deposit.date());

    let mut removed_deposits = 0;
    if let (Some(oldest), Some(newest)) = (deposits.first(), deposits.last()) {
        let mut old_deposits: HashMap<String, Deposit> =
            Deposit::find_for_date_range(oldest.date(), newest.date())
                .await
                .into_iter()
                .map(|deposit| (deposit.id().to_owned(), deposit))
                .collect();
        for deposit in deposits.iter() {
            old_deposits.remove(deposit.id());
        }

        removed_deposits = old_deposits.len();
        for (_id, deposit) in old_deposits {
            remove(deposit).await?;
        }
    }

    let new_transactions = Deposit::save_new_deposits(deposits).await?;

    Ok((new_transactions, removed_deposits))
}
