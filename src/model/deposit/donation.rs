use crate::{
    common::{date_string, now_date},
    couchdb::CouchdbMeta,
};

use super::Deposit;

impl Deposit {
    pub fn create_donation(cents: u32, membership_id: u32, description: String) -> Self {
        let date = now_date();
        let name = format!("donation_{}_{membership_id}_{cents}", date_string(&date));

        Self {
            couchdb_meta: CouchdbMeta::new(Self::ID_PREFIX, name),
            date,
            description,
            cents: -(cents as i32),
            membership_id: Some(membership_id),
            ..Default::default()
        }
    }
}
