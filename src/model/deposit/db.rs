use super::Deposit;
use crate::{
    common::date_string,
    couchdb::{CouchdbDocument, CouchdbMeta},
};
use anyhow::Result;
use chrono::NaiveDate;
use serde_json::json;

impl Deposit {
    pub const ID_PREFIX: &'static str = "deposit";

    pub async fn find_all() -> Vec<Self> {
        crate::couchdb::find_documents(json!({
            "selector": {
                "_id": {
                    "$gt": format!("{}_", Self::ID_PREFIX),
                    "$lt": format!("{}_{{}}", Self::ID_PREFIX),
                }
            },
            "limit": 10_000u32
        }))
        .await
        .ok()
        .unwrap_or_default()
    }

    pub async fn find_for_membership_id(membership_id: u32) -> Vec<Self> {
        crate::couchdb::find_documents(json!({
            "selector": {
                "_id": {
                    "$gt": format!("{}_", Self::ID_PREFIX),
                    "$lt": format!("{}_{{}}", Self::ID_PREFIX),
                },
                "membership_id": {
                    "$eq": membership_id
                },
            },
            "sort": [{"date": "asc"}],
            "limit": 10_000u32
        }))
        .await
        .ok()
        .unwrap_or_default()
    }

    pub async fn find_by_id(deposit_id: &str) -> Option<Self> {
        crate::couchdb::find_single_document(json!({
            "selector": {
                "_id": {
                    "$eq": deposit_id,
                }
            }
        }))
        .await
        .ok()
        .flatten()
    }

    pub async fn find_for_date_range(oldest: NaiveDate, newest: NaiveDate) -> Vec<Self> {
        crate::couchdb::find_documents(json!({
            "selector": {
                "_id": {
                    "$gt": format!("{}_", Self::ID_PREFIX),
                    "$lt": format!("{}_{{}}", Self::ID_PREFIX),
                },
                "date": {
                    "$gte": date_string(&oldest),
                    "$lte": date_string(&newest),
                }
            },
            "limit": 10_000u32
        }))
        .await
        .ok()
        .unwrap_or_default()
    }

    pub async fn save_new_deposits(deposits: Vec<Self>) -> Result<usize> {
        let inserted_documents = crate::couchdb::save_batch_ignore_existing(deposits).await?;

        Ok(inserted_documents)
    }

    pub async fn remove(self) -> Result<()> {
        crate::couchdb::remove(self).await?;

        Ok(())
    }

    pub async fn save(&mut self) -> Result<()> {
        crate::couchdb::save_batch(vec![self]).await?;

        Ok(())
    }
}

impl CouchdbDocument for Deposit {
    fn meta(&self) -> &CouchdbMeta {
        &self.couchdb_meta
    }

    fn meta_mut(&mut self) -> &mut CouchdbMeta {
        &mut self.couchdb_meta
    }
}
