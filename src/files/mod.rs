mod attachment;
mod csv_bidding;
mod csv_income_entries;
mod csv_income_pseudonyms;
mod csv_membership_emails;
mod ics;
mod rss;

use warp::{filters::BoxedFilter, Filter, Reply};

pub fn filter() -> BoxedFilter<(impl Reply + 'static,)> {
    warp::path!("file" / u32 / String)
        .and(warp::header::optional::<String>("if-none-match"))
        .and(warp::filters::cookie::optional("login_id"))
        .and_then(attachment::get)
        .or(warp::path!("ics" / u32 / String).and_then(ics::handle))
        .or(warp::path!("rss" / u32 / String).and_then(rss::handle))
        .or(warp::path!("csv" / "bidding")
            .and(warp::filters::cookie::optional("login_id"))
            .and_then(csv_bidding::handle))
        .or(warp::path!("csv" / "income" / "pseudonyms")
            .and(warp::filters::cookie::optional("login_id"))
            .and_then(csv_income_pseudonyms::handle))
        .or(warp::path!("csv" / "income" / "entries")
            .and(warp::filters::cookie::optional("login_id"))
            .and_then(csv_income_entries::handle))
        .or(warp::path!("csv" / "membership" / "emails")
            .and(warp::filters::cookie::optional("login_id"))
            .and_then(csv_membership_emails::handle))
        .boxed()
}
