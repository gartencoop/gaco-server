use crate::{
    common::format_cents_as_euros,
    model::{income::Income, user::AccessLevel},
};
use chrono::Datelike;
use headers::{ContentLength, HeaderMapExt, HeaderValue};
use warp::{http::StatusCode, hyper::Response};

pub async fn handle(login_id: Option<String>) -> Result<Box<dyn warp::Reply>, warp::Rejection> {
    if !AccessLevel::for_login_id(login_id)
        .await
        .map(|access_level| access_level.admin())
        .unwrap_or_default()
    {
        return Ok(Box::new(StatusCode::FORBIDDEN));
    }

    let mut incomes = Income::find_all_for_year(chrono::Utc::now().year() as u16)
        .await
        .into_iter()
        .filter_map(|income| {
            income
                .income_cents()
                .map(|cents| (cents, f32::from(income.shares()), income.workdays()))
        })
        .collect::<Vec<_>>();

    if incomes.len() < 10 {
        return Ok(Box::new(StatusCode::FORBIDDEN));
    }

    incomes.sort_by_key(|(cents, _, _)| *cents);
    let buf = std::iter::once("Einkommen;Anteile;Achereinsaetze".to_string())
        .chain(incomes.into_iter().map(|(cents, shares, workdays)| {
            format!(
                "{};{shares};{workdays}",
                format_cents_as_euros(cents, ',', 2)
            )
        }))
        .collect::<Vec<_>>()
        .join("\n")
        .into_bytes();
    let len = buf.len() as u64;
    let mut response = Response::new(buf);
    let headers = response.headers_mut();
    headers.typed_insert(ContentLength(len));
    headers.insert(
        "Content-Type",
        HeaderValue::from_static("text/csv; charset=utf-8"),
    );
    headers.insert(
        "Content-Disposition",
        HeaderValue::from_static("attachment;filename=\"Einkommen.csv\""),
    );

    Ok(Box::new(response))
}
