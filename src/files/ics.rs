use crate::model::user::User;
use bytes::{BufMut, BytesMut};
use chrono::{DateTime, Datelike, Timelike, Utc};
use headers::{ContentLength, HeaderMapExt, HeaderValue};
use ics::{
    escape_text,
    properties::{Categories, Description, DtEnd, DtStart, Summary, URL},
    Event, ICalendar,
};
use log::{debug, info};
use serde::Deserialize;
use serde_json::json;
use warp::{http::StatusCode, hyper::Response};

pub async fn handle(uid: u32, feed_token: String) -> Result<Box<dyn warp::Reply>, warp::Rejection> {
    let Some(user) = User::find_for_uid(uid) else {
        return Ok(Box::new(StatusCode::FORBIDDEN));
    };
    if user.verify_feed_token(&feed_token).is_err() {
        info!("Wrong feed token for uid {uid}");
        return Ok(Box::new(StatusCode::FORBIDDEN));
    }

    let show_private_events = user.access_level().user();
    debug!("Loading events for uid {uid}. private: {show_private_events}");

    let start = Utc::now()
        .with_day0(0)
        .unwrap()
        .with_hour(0)
        .unwrap()
        .with_minute(0)
        .unwrap()
        .with_second(0)
        .unwrap()
        .with_nanosecond(0)
        .unwrap();
    let start = match start.month() {
        1 => start
            .with_month(12)
            .unwrap()
            .with_year(start.year() - 1)
            .unwrap(),
        month => start.with_month(month - 1).unwrap(),
    };
    let id_selectors = {
        let mut selector = vec![json!({
            "$gt": format!("{}_", crate::model::event::Event::id_prefix(false)),
            "$lt": format!("{}_{{}}", crate::model::event::Event::id_prefix(false))
        })];
        if show_private_events {
            selector.push(json!({
                "$gt": format!("{}_", crate::model::event::Event::id_prefix(true)),
                "$lt": format!("{}_{{}}", crate::model::event::Event::id_prefix(true))
            }));
        }
        selector
    };
    let query = json!({
        "selector": {
            "_id": {
                "$or": id_selectors
            },
            "start_at": {
                "$gte": start.to_rfc3339(),
            }
        },
        "sort": [{"start_at": "asc"}],
        "fields": [
            "_id",
            "created_by",
            "start_at",
            "end_at",
            "title",
            "path",
            "body",
            "category",
            "private"
        ],
        "limit": 10000u32
    });
    debug!("query: {:?}", query);

    let events: Vec<CalendarEvent> = match crate::couchdb::find_documents(query).await {
        Ok(events) => events,
        Err(_) => return Ok(Box::new(StatusCode::INTERNAL_SERVER_ERROR)),
    };

    debug!("Found {} events", events.len());

    // Create new iCalendar object
    // An iCalendar object must at least consist a component and the VERSION and
    // PRODID property.
    let mut calendar = ICalendar::new("2.0", "-//xyz Corp//NONSGML PDA Calendar Version 1.0//EN");

    for event in events {
        let start = event.start_at.format("%Y%m%dT%H%M%SZ").to_string();
        let end = event.end_at.format("%Y%m%dT%H%M%SZ").to_string();

        let mut ics_event = Event::new(format!("{}:{}", feed_token, event.id), start.clone());
        ics_event.push(DtStart::new(start));
        ics_event.push(DtEnd::new(end));
        ics_event.push(Categories::new(event.category));
        ics_event.push(Summary::new(escape_text(event.title)));
        ics_event.push(Description::new(escape_text(event.body)));
        ics_event.push(URL::new(format!(
            "https://www.gartencoop.org/tunsel/{}",
            event.path
        )));

        calendar.add_event(ics_event);
    }

    let mut writer = BytesMut::new().writer();
    if calendar.write(&mut writer).is_err() {
        Ok(Box::new(StatusCode::INTERNAL_SERVER_ERROR))
    } else {
        let buffer = writer.into_inner().to_vec();
        let len = buffer.len() as u64;
        let mut response = Response::new(buffer);
        let headers = response.headers_mut();
        headers.typed_insert(ContentLength(len));
        headers.insert(
            "Content-Type",
            HeaderValue::from_static("text/calendar; charset=utf-8"),
        );
        headers.insert(
            "Content-Disposition",
            HeaderValue::from_static("attachment;filename=\"Gartencoop.ics\""),
        );

        Ok(Box::new(response))
    }
}

#[derive(Deserialize, Debug, Default)]
#[serde(default)]
pub struct CalendarEvent {
    #[serde(rename = "_id")]
    id: String,
    title: String,
    #[serde(deserialize_with = "crate::common::deserialize_datetime")]
    start_at: DateTime<Utc>,
    #[serde(deserialize_with = "crate::common::deserialize_datetime")]
    end_at: DateTime<Utc>,
    body: String,
    path: String,
    category: String,
    private: bool,
}
