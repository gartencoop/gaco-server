use crate::model::{bidding::BiddingCalc, user::AccessLevel};
use headers::{ContentLength, HeaderMapExt, HeaderValue};
use warp::{http::StatusCode, hyper::Response};

pub async fn handle(login_id: Option<String>) -> Result<Box<dyn warp::Reply>, warp::Rejection> {
    if !AccessLevel::for_login_id(login_id)
        .await
        .map(|access_level| access_level.admin())
        .unwrap_or_default()
    {
        return Ok(Box::new(StatusCode::FORBIDDEN));
    }

    let buf = BiddingCalc::get().csv().into_bytes();
    let len = buf.len() as u64;
    let mut response = Response::new(buf);
    let headers = response.headers_mut();
    headers.typed_insert(ContentLength(len));
    headers.insert(
        "Content-Type",
        HeaderValue::from_static("text/csv; charset=utf-8"),
    );
    headers.insert(
        "Content-Disposition",
        HeaderValue::from_static("attachment;filename=\"Gebote.csv\""),
    );

    Ok(Box::new(response))
}
