use crate::model::{
    document::{Document, DocumentType},
    user::User,
};
use chrono::{DateTime, Utc};
use headers::{ContentLength, HeaderMapExt, HeaderValue};
use rss::{Channel, Item};
use serde::Deserialize;
use warp::{http::StatusCode, hyper::Response};

pub async fn handle(uid: u32, feed_token: String) -> Result<Box<dyn warp::Reply>, warp::Rejection> {
    let Some(user) = User::find_for_uid(uid) else {
        return Ok(Box::new(StatusCode::FORBIDDEN));
    };
    if user.verify_feed_token(&feed_token).is_err() {
        return Ok(Box::new(StatusCode::FORBIDDEN));
    }
    let show_private_documents = user.access_level().user();

    let document_types = if show_private_documents {
        vec![
            DocumentType::News,
            DocumentType::Media,
            DocumentType::Internal,
            DocumentType::Offtopic,
        ]
    } else {
        vec![DocumentType::News]
    };
    let documents = match Document::find_newest(document_types, None, Some(100)).await {
        Ok(documents) => documents,
        Err(_) => return Ok(Box::new(StatusCode::INTERNAL_SERVER_ERROR)),
    };

    let items = documents
        .into_iter()
        .map(|document| Item {
            title: Some(document.title().to_owned()),
            link: Some(format!(
                "https://www.gartencoop.org/tunsel/{}",
                document.path()
            )),
            content: Some(document.body().to_owned()),
            pub_date: Some(document.created_at().to_rfc3339()),
            ..Default::default()
        })
        .collect();
    let channel = Channel {
        title: "Gartencoop".to_owned(),
        link: "https://www.gartencoop.org".to_owned(),
        items,
        ..Default::default()
    };
    let feed = channel.to_string();

    let len = feed.len() as u64;
    let mut response = Response::new(feed);
    let headers = response.headers_mut();
    headers.typed_insert(ContentLength(len));
    headers.insert(
        "Content-Type",
        HeaderValue::from_static("application/rss+xml; charset=utf-8"),
    );

    Ok(Box::new(response))
}

#[derive(Deserialize, Debug, Default)]
#[serde(default)]
pub struct CalendarEvent {
    #[serde(rename = "_id")]
    id: String,
    title: String,
    #[serde(deserialize_with = "crate::common::deserialize_datetime")]
    start_at: DateTime<Utc>,
    #[serde(deserialize_with = "crate::common::deserialize_datetime")]
    end_at: DateTime<Utc>,
    body: String,
    path: String,
    category: String,
    private: bool,
}
