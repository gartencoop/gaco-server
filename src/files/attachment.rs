use crate::{
    couchdb::{AttachmentResponse, CouchdbMeta},
    model::{file::File, user::AccessLevel},
};
use headers::{CacheControl, ContentLength, ContentType, ETag, HeaderMapExt, HeaderValue};
use log::debug;
use serde::Deserialize;
use std::time::Duration;
use warp::{http::StatusCode, hyper::Response};

pub async fn get(
    id: u32,
    attachment: String,
    etag: Option<String>,
    login_id: Option<String>,
) -> Result<Box<dyn warp::Reply>, warp::Rejection> {
    debug!("Trying access for {login_id:?} to {id}/{attachment}");
    let show_private_files = AccessLevel::for_login_id(login_id)
        .await
        .map(|access_level| access_level.user())
        .unwrap_or_default();

    #[derive(Deserialize, Default, Debug)]
    #[serde(default)]
    struct Doc {
        #[serde(flatten)]
        couchdb_meta: CouchdbMeta,
        files: Vec<File>,
    }
    let doc: Doc = match crate::couchdb::find_document_by_nid(id).await {
        Ok(doc) => doc,
        Err(_) => {
            log::info!("Could not find document by nid: {id}");
            return Ok(Box::new(StatusCode::NOT_FOUND));
        }
    };

    match doc.couchdb_meta.id_prefix().as_deref() {
        Some("news" | "public_event") => (),
        Some("media" | "internal" | "offtopic" | "page" | "template" | "private_event" | "vp") => {
            if !show_private_files {
                match tokio::fs::read(crate::CONFIG.assets.join("index.html")).await {
                    Ok(index) => {
                        debug!("Access forbidden to {id}/{attachment}");
                        let mut response = Response::new(index);
                        *response.status_mut() = StatusCode::FORBIDDEN;
                        let headers = response.headers_mut();
                        headers.typed_insert(ContentType::html());

                        return Ok(Box::new(response));
                    }
                    Err(_) => return Ok(Box::new(StatusCode::INTERNAL_SERVER_ERROR)),
                }
            }
        }
        Some(id_prefix) => {
            log::warn!("Unhandeled id_prefix: {id_prefix}");
            return Ok(Box::new(StatusCode::NOT_FOUND));
        }
        None => {
            log::warn!("No id_prefix found");
            return Ok(Box::new(StatusCode::NOT_FOUND));
        }
    }

    let file = doc
        .files
        .into_iter()
        .find(|file| file.attachment() == attachment);
    let file_is_image = file
        .as_ref()
        .map(|file| file.is_image())
        .unwrap_or_default();
    let file_name = file.map(|file| file.into_name());

    let image =
        crate::couchdb::download_attachment(doc.couchdb_meta.id(), &attachment, etag.as_deref())
            .await;
    match image {
        AttachmentResponse::NotFound => Ok(Box::new(StatusCode::NOT_FOUND)),
        AttachmentResponse::Found { etag, data } => {
            let etag = match etag.parse::<ETag>() {
                Err(_err) => return Ok(Box::new(StatusCode::INTERNAL_SERVER_ERROR)),
                Ok(etag) => etag,
            };
            let len = data.len() as u64;
            let mut response = Response::new(data);
            let headers = response.headers_mut();
            headers.typed_insert(ContentLength(len));
            if file_is_image {
                headers.typed_insert(ContentType::jpeg());
            } else {
                headers.typed_insert(ContentType::octet_stream());
            }
            headers.typed_insert(etag);

            headers.typed_insert(
                CacheControl::new()
                    .with_public()
                    .with_max_age(Duration::from_secs(60 * 60 * 24 * 365)),
            );

            if let Some(content_disposition) = file_name.and_then(|file_name| {
                HeaderValue::from_str(&format!("attachment;filename=\"{file_name}\"")).ok()
            }) {
                headers.insert("Content-Disposition", content_disposition);
            }

            Ok(Box::new(response))
        }
        AttachmentResponse::NotModified => Ok(Box::new(StatusCode::NOT_MODIFIED)),
        AttachmentResponse::ServerError => Ok(Box::new(StatusCode::INTERNAL_SERVER_ERROR)),
    }
}
