use wallaby::{CustomEvent, Event};

pub fn change_path_event(path: &str) -> Event {
    Event::Custom(CustomEvent::new(
        "change_path".to_owned(),
        std::iter::once(("path".to_owned(), path.into())).collect(),
    ))
}
