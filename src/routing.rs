use crate::{
    change_path::change_path_event,
    model::{document::DocumentType, user::AccessLevel},
};
use anyhow::Result;
use serde::{Deserialize, Serialize};
use strum_macros::IntoStaticStr;
use wallaby::{CustomEvent, Event};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum Routing {
    NotFound,
    AccessDenied(String),
    Page(Page),
    Document(DocumentType, u32),
    NewDocument(DocumentType),
    PrivateEvent(u32),
    PublicEvent(u32),
    EditPublicEvent(u32),
    EditPrivateEvent(u32),
    VP(u32),
    EditVP(u32),
}

impl Default for Routing {
    fn default() -> Self {
        Self::Page(Page::Start)
    }
}

impl Routing {
    // A session logged in a user so we might be able to show a page we where not allowed to show prior.
    pub async fn changed_access_level(&mut self, access_level: AccessLevel) -> Vec<Event> {
        if let (Self::Page(Page::Start), true) = (&self, access_level.user()) {
            *self = Self::Page(Page::News);
            return vec![change_path_event("/News")];
        } else if let Self::AccessDenied(path) = self.clone() {
            if path.starts_with("/file/") {
                if let Some(id) = path.split('/').nth(2) {
                    let doc_path = format!("/node/{id}");
                    *self = Self::by_path(&doc_path, access_level).await;

                    return vec![
                        Event::Custom(CustomEvent::new(
                            "load_file".to_owned(),
                            std::iter::once(("path".to_owned(), path.to_owned().into())).collect(),
                        )),
                        change_path_event(&doc_path),
                    ];
                }
            } else {
                *self = Self::by_path(&path, access_level).await;
            }
        }

        vec![]
    }

    // Find routing by path
    pub async fn by_path(path: &str, access_level: AccessLevel) -> Self {
        // support old paths which start with "/tunsel"
        let path = match path.strip_prefix("/tunsel") {
            Some(path) => path,
            None => path,
        };

        // Remove slash at start of path and check if it ends with "/edit"
        let stripped_path = match path.strip_prefix('/') {
            None | Some("") => return Self::Page(Page::Start),
            Some(stripped_path) if stripped_path.starts_with("file/") => {
                return Self::AccessDenied(path.to_owned())
            }
            Some(stripped_path) => stripped_path,
        };

        match Page::try_from(stripped_path) {
            Ok(page) => page.into_routing(access_level),
            Err(_) => match crate::couchdb::find_meta_for_path(stripped_path)
                .await
                .ok()
                .and_then(|meta| meta.id_parts())
            {
                Some((ty, id)) => match ty.as_str() {
                    "public_event" => Self::PublicEvent(id),
                    "private_event" if access_level.user() => Self::PrivateEvent(id),
                    "vp" if access_level.user() => Self::VP(id),
                    "private_event" | "vp" => Self::AccessDenied(path.to_owned()),
                    ty => match DocumentType::try_from_str(ty) {
                        Some(DocumentType::News) => Self::Document(DocumentType::News, id),
                        Some(document_type) if access_level.user() => {
                            Self::Document(document_type, id)
                        }
                        Some(_) => Self::AccessDenied(path.to_owned()),
                        None => Self::NotFound,
                    },
                },
                None => Self::NotFound,
            },
        }
    }

    pub fn screen_name(&self) -> &str {
        match self {
            Self::Page(page) => page.screen_name(),
            Self::NewDocument(..) | Self::Document(..) => {
                crate::screen::document::Document::screen_name()
            }
            Self::PrivateEvent(_) | Routing::PublicEvent(_) => {
                crate::screen::event::Event::screen_name()
            }
            Self::EditPrivateEvent(_) | Self::EditPublicEvent(_) => {
                crate::screen::edit_event::EditEvent::screen_name()
            }
            Self::VP(_) => crate::screen::vp::VP::screen_name(),
            Self::EditVP(_) => crate::screen::edit_vp::EditVP::screen_name(),
            Self::NotFound => crate::screen::not_found::NotFound::screen_name(),
            Self::AccessDenied(_) => crate::screen::access_denied::AccessDenied::screen_name(),
        }
    }

    pub fn document_id(&self) -> Option<u32> {
        match self {
            Self::Document(_, id) => Some(*id),
            _ => None,
        }
    }

    pub fn event_id(&self) -> Option<u32> {
        match self {
            Self::PublicEvent(id)
            | Self::PrivateEvent(id)
            | Self::EditPrivateEvent(id)
            | Self::EditPublicEvent(id) => Some(*id),
            _ => None,
        }
    }
}

#[derive(IntoStaticStr, Serialize, Deserialize, Debug, Clone)]
pub enum Page {
    Start,
    News,
    Calendar,
    CalendarImport,
    Imprint,
    Privacy,
    User,
    ForgotPassword,
    UserMembership, // membership of the user
    Bidding,
    AdminMemberships,
    AdminMembership {
        member_id: u32,
    },
    AdminTransactions {
        year: u16,
    },
    AdminUsers,
    AdminBidding,
    AdminIncomes,
    AdminDeposits,
    Income {
        year_pseudonym: String,
    },
    Invitation {
        membership_id: u32,
        invitation_token: String,
    },
    ActivateUser {
        user: u32,
        password_token: String,
    },
    RegisterUser,
    VPs,
}

impl TryFrom<&str> for Page {
    type Error = anyhow::Error;

    fn try_from(path: &str) -> Result<Self> {
        Ok(
            if let Some(id) = path
                .strip_prefix("admin_membership_")
                .and_then(|path| path.parse().ok())
            {
                Self::AdminMembership { member_id: id }
            } else if let Some(year) = path
                .strip_prefix("admin_transactions_")
                .and_then(|path| path.parse().ok())
            {
                Self::AdminTransactions { year }
            } else if let Some(year_pseudonym) = path
                .strip_prefix("income_")
                .map(|year_pseudonym| year_pseudonym.to_string())
            {
                Self::Income { year_pseudonym }
            } else if let Some((membership_id, invitation_token)) =
                path.strip_prefix("invitation_").and_then(|path| {
                    let mut path_iter = path.split('_');
                    Some((
                        path_iter.next()?.parse().ok()?,
                        path_iter.next()?.to_owned(),
                    ))
                })
            {
                Self::Invitation {
                    membership_id,
                    invitation_token,
                }
            } else if let Some((user, password_token)) =
                path.strip_prefix("activate_user_").and_then(|path| {
                    let mut parts = path.split('_');
                    parts
                        .next()
                        .and_then(|user| user.parse().ok())
                        .and_then(|user| {
                            parts
                                .next()
                                .map(|password_token| (user, password_token.to_string()))
                        })
                })
            {
                Self::ActivateUser {
                    user,
                    password_token,
                }
            } else {
                match path {
                    "Start" => Self::Start,
                    "News" => Self::News,
                    "Kalender" => Self::Calendar,
                    "KalenderImport" | "CalendarImport" => Self::CalendarImport,
                    "Impressum" => Self::Imprint,
                    "Datenschutz" => Self::Privacy,
                    "Benutzer" | "User" => Self::User,
                    "forgot_password" => Self::ForgotPassword,
                    "Mitgliedschaft" | "UserMembership" => Self::UserMembership,
                    "Bieteverfahren" | "Bidding" => Self::Bidding,
                    "Mitgliedschaften" => Self::AdminMemberships,
                    "admin_bidding" => Self::AdminBidding,
                    "admin_memberships" => Self::AdminMemberships,
                    "admin_users" => Self::AdminUsers,
                    "admin_incomes" => Self::AdminIncomes,
                    "admin_deposits" => Self::AdminDeposits,
                    "register_user" => Self::RegisterUser,
                    "vps" => Self::VPs,
                    _ => anyhow::bail!("Could not find page"),
                }
            },
        )
    }
}

impl Page {
    fn into_routing(self, access_level: AccessLevel) -> Routing {
        match self {
            Self::AdminMembership { member_id: id } if !access_level.admin() => {
                Routing::AccessDenied(format!("/admin_membership_{id}"))
            }
            Self::AdminDeposits if !access_level.admin() => {
                Routing::AccessDenied("/admin_deposits".to_string())
            }
            Self::AdminMemberships if !access_level.admin() => {
                Routing::AccessDenied("/admin_memberships".to_string())
            }
            Self::AdminTransactions { year } if !access_level.admin() => {
                Routing::AccessDenied(format!("/admin_transactions_{year}"))
            }
            Self::AdminUsers if !access_level.admin() => {
                Routing::AccessDenied("/admin_users".to_string())
            }
            Self::AdminBidding if !access_level.admin() => {
                Routing::AccessDenied("/admin_bidding".to_string())
            }
            Self::AdminIncomes if !access_level.admin() => {
                Routing::AccessDenied("/admin_incomes".to_string())
            }
            Self::CalendarImport
            | Self::UserMembership
            | Self::Bidding
            | Self::Invitation { .. }
            | Self::User
                if !access_level.user() =>
            {
                let name: &'static str = self.into();
                Routing::AccessDenied(format!("/{name}"))
            }
            Self::VPs if !access_level.member() => {
                let name: &'static str = self.into();
                Routing::AccessDenied(format!("/{name}"))
            }
            page => Routing::Page(page),
        }
    }

    pub fn into_path(self) -> String {
        match self {
            Self::Start => "/".to_owned(),
            Self::News => "/news".to_owned(),
            Self::Calendar => "/Kalender".to_owned(),
            Self::CalendarImport => "/KalenderImport".to_owned(),
            Self::Imprint => "/Impressum".to_owned(),
            Self::Privacy => "/Datenschutz".to_owned(),
            Self::User => "/Benutzer".to_owned(),
            Self::UserMembership => "/Mitgliedschaft".to_owned(),
            Self::Bidding => "/Bieteverfahren".to_owned(),
            Self::AdminDeposits => "/admin_deposits".to_owned(),
            Self::AdminMemberships => "/admin_memberships".to_owned(),
            Self::AdminMembership { member_id } => format!("/admin_membership_{member_id}"),
            Self::AdminTransactions { year } => format!("/admin_transactions_{year}"),
            Self::AdminUsers => "/admin_users".to_string(),
            Self::AdminBidding => "/admin_bidding".to_string(),
            Self::AdminIncomes => "/admin_incomes".to_string(),
            Self::Income { year_pseudonym } => format!("/income_{year_pseudonym}"),
            Self::Invitation {
                membership_id,
                invitation_token,
            } => format!("/invitation_{membership_id}_{invitation_token}"),
            Self::ForgotPassword => "/forgot_password".to_string(),
            Self::ActivateUser {
                user,
                password_token,
            } => format!("/activate_user_{user}_{password_token}"),
            Self::RegisterUser => "/register_user".to_string(),
            Self::VPs => "/vps".to_string(),
        }
    }

    fn screen_name(&self) -> &str {
        match self {
            Self::Start => crate::screen::start::Start::screen_name(),
            Self::News => crate::screen::news::News::screen_name(),
            Self::Calendar => crate::screen::calendar::Calendar::screen_name(),
            Self::CalendarImport => {
                crate::screen::calendar_import_ics::CalendarImportIcs::screen_name()
            }
            Self::Imprint => crate::screen::impress::Impress::screen_name(),
            Self::Privacy => crate::screen::privacy::Privacy::screen_name(),
            Self::User => crate::screen::user::User::screen_name(),
            Self::UserMembership => crate::screen::membership::Membership::screen_name(),
            Self::Bidding => crate::screen::bidding::BiddingPage::screen_name(),
            Self::AdminMemberships => {
                crate::screen::admin_memberships::AdminMemberships::screen_name()
            }
            Self::AdminMembership { .. } => {
                crate::screen::admin_membership::AdminMembership::screen_name()
            }
            Self::AdminBidding => crate::screen::admin_bidding::AdminBidding::screen_name(),
            Self::AdminTransactions { .. } => {
                crate::screen::admin_transactions::AdminTransactions::screen_name()
            }
            Self::AdminUsers => crate::screen::admin_users::AdminUsers::screen_name(),
            Self::AdminIncomes => crate::screen::admin_incomes::AdminIncomes::screen_name(),
            Self::Income { .. } => crate::screen::income::Income::screen_name(),
            Self::AdminDeposits => crate::screen::admin_deposits::AdminDeposits::screen_name(),
            Self::Invitation { .. } => crate::screen::invitation::Invitation::screen_name(),
            Self::ForgotPassword => crate::screen::forgot_password::ForgotPassword::screen_name(),
            Self::ActivateUser { .. } => crate::screen::activate_user::ActivateUser::screen_name(),
            Self::RegisterUser => crate::screen::register_user::RegisterUser::screen_name(),
            Self::VPs => crate::screen::vps::VPs::screen_name(),
        }
    }
}
