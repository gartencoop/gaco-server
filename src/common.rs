use base64::{engine::general_purpose::STANDARD, Engine as _};
use chrono::{DateTime, NaiveDate, Utc};
use serde::{de, Deserialize, Deserializer, Serializer};
use std::fmt;

static DATE_FORMAT: &str = "%Y-%m-%d";
static DATE_FORMAT_DE: &str = "%d.%m.%Y";

pub fn format_cents_as_euros(cents: u32, comma: char, decimals: usize) -> String {
    let cents = cents.to_string();
    let mut euros = String::with_capacity(if decimals == 0 {
        cents.len().max(3) - 2
    } else {
        cents.len().max(3) + decimals - 1
    });
    for (i, c) in std::iter::repeat('0')
        .take(3 - cents.len().min(3))
        .chain(cents.chars())
        .enumerate()
    {
        euros.push(c);
        if decimals != 0 && i == cents.len().max(3) - 3 {
            euros.push(comma);
        }
    }
    euros
}

pub fn now_date() -> NaiveDate {
    Utc::now().date_naive()
}

pub fn now_datetime() -> DateTime<Utc> {
    Utc::now()
}

pub fn date_string(date: &NaiveDate) -> String {
    date.format(DATE_FORMAT).to_string()
}

pub fn date_string_de(date: &NaiveDate) -> String {
    date.format(DATE_FORMAT_DE).to_string()
}

pub fn parse_date(date: &str) -> Option<NaiveDate> {
    NaiveDate::parse_from_str(date, DATE_FORMAT).ok()
}

pub fn serialize_date<S>(date: &NaiveDate, s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    s.serialize_str(&date_string(date))
}

pub fn deserialize_date<'de, D>(deserializer: D) -> Result<NaiveDate, D::Error>
where
    D: Deserializer<'de>,
{
    deserializer.deserialize_str(DateStringVisitor)
}

pub fn serialize_optional_date<S>(date: &Option<NaiveDate>, s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    match date {
        Some(date) => s.serialize_str(&date_string(date)),
        None => s.serialize_none(),
    }
}

pub fn deserialize_optional_date<'de, D>(deserializer: D) -> Result<Option<NaiveDate>, D::Error>
where
    D: Deserializer<'de>,
{
    deserializer.deserialize_option(OptionalDateStringVisitor)
}

pub fn serialize_datetime<S>(datetime: &DateTime<Utc>, s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    s.serialize_str(&datetime.to_rfc3339())
}

pub fn deserialize_datetime<'de, D>(deserializer: D) -> Result<DateTime<Utc>, D::Error>
where
    D: Deserializer<'de>,
{
    deserializer.deserialize_str(DateTimeStringVisitor)
}

struct DateTimeStringVisitor;
impl de::Visitor<'_> for DateTimeStringVisitor {
    type Value = DateTime<Utc>;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("a string containing a date in rfc3339 format")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        DateTime::parse_from_rfc3339(v)
            .map_err(E::custom)
            .map(|dt| dt.into())
    }
}

struct DateStringVisitor;
impl de::Visitor<'_> for DateStringVisitor {
    type Value = NaiveDate;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("a string containing a date in the format Y-m-d")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        parse_date(v).ok_or_else(|| E::custom("Could not parse date"))
    }
}

struct OptionalDateStringVisitor;
impl<'de> de::Visitor<'de> for OptionalDateStringVisitor {
    type Value = Option<NaiveDate>;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("a optional string containing a date in the format Y-m-d")
    }

    fn visit_none<E>(self) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(None)
    }

    fn visit_some<D>(self, deserializer: D) -> Result<Self::Value, D::Error>
    where
        D: Deserializer<'de>,
    {
        Ok(Some(deserializer.deserialize_any(DateStringVisitor)?))
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(Some(
            parse_date(v).ok_or_else(|| E::custom("Could not parse date"))?,
        ))
    }
}

pub fn deserialize_data_url<'de, D>(deserializer: D) -> Result<Vec<u8>, D::Error>
where
    D: Deserializer<'de>,
{
    let data_url = String::deserialize(deserializer)?;
    let parts: Vec<&str> = data_url.splitn(2, ',').collect();
    if parts.len() != 2 {
        return Err(serde::de::Error::custom("Invalid data url"));
    }
    let data = STANDARD
        .decode(parts[1])
        .map_err(serde::de::Error::custom)?;
    Ok(data)
}
