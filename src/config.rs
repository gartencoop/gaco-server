use anyhow::Context;
use once_cell::sync::Lazy;
use serde::Deserialize;
use std::{fs::File, path::PathBuf};

pub static CONFIG: Lazy<Config> = Lazy::new(|| {
    File::open(&crate::OPTS.config)
        .with_context(|| {
            format!(
                "Could not open config file \"{}\"",
                crate::OPTS.config.display()
            )
        })
        .and_then(|config_json| {
            serde_json::from_reader(config_json).with_context(|| {
                format!(
                    "Could not parse config file \"{}\"",
                    crate::OPTS.config.display()
                )
            })
        })
        .expect("Could not load config file")
});

#[derive(Debug, Deserialize, Default)]
#[serde(default)]
pub struct Config {
    /// not active in production yet as we still get the data from the old website
    pub modify_users_or_documents: bool,
    pub couchdb_url: String,
    pub port: u16,
    pub assets: PathBuf,
    pub email: EmailConfig,
    pub search_index_prefix: Option<String>,
    pub search_api_key: Option<String>,
}

#[derive(Debug, Deserialize, Default)]
pub struct EmailConfig {
    pub contact_form_from: String,
    pub contact_form_to: String,
    /// When guests (not users) use the contact form, this is the email address to send the message to
    pub contact_form_to_guests: String,
    #[serde(default)]
    pub contact_form_cc: Option<String>,
    pub contact_form_subject: String,
    pub membership_from: String,
    #[serde(default)]
    pub membership_to_overwrite: Option<String>,
    pub income_csv_to: String,
    pub host: String,
    pub username: String,
    pub password: String,
}
