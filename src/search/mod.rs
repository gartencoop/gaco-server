pub mod handler;

use crate::{
    model::{
        document::{Document, DocumentType},
        event::Event,
        vp::VP,
    },
    CONFIG,
};
use anyhow::{Context, Result};
use meilisearch_sdk::client::Client;
use once_cell::sync::Lazy;
use serde_json::json;
use tokio::sync::OnceCell;

pub static CLIENT: OnceCell<Client> = OnceCell::const_new();
pub static INDEX_NEWS: Lazy<String> = Lazy::new(|| {
    format!(
        "{}_news",
        CONFIG.search_index_prefix.as_deref().unwrap_or_default()
    )
});
pub static INDEX_DOCUMENTS: Lazy<String> = Lazy::new(|| {
    format!(
        "{}_documents",
        CONFIG.search_index_prefix.as_deref().unwrap_or_default()
    )
});
pub static INDEX_EVENTS: Lazy<String> = Lazy::new(|| {
    format!(
        "{}_events",
        CONFIG.search_index_prefix.as_deref().unwrap_or_default()
    )
});
static PRIMARY_KEY: Option<&str> = Some("_id");

pub async fn init() -> Result<()> {
    if let Some(api_key) = CONFIG.search_api_key.as_deref() {
        let client = Client::new("http://localhost:7700", Some(api_key))?;
        CLIENT
            .set(client.clone())
            .expect("Could not set search client");

        {
            let index = client.index(&*INDEX_NEWS);

            index
                .set_searchable_attributes(&["title", "topic", "body"])
                .await?;
            index
                .set_filterable_attributes(vec!["teams", "topic"])
                .await?;

            index
                .set_ranking_rules(&[
                    "words",
                    "typo",
                    "proximity",
                    "attribute",
                    "sort",
                    "exactness",
                    "created_at:desc",
                ])
                .await?;

            let id_prefix = Document::id_prefix(&DocumentType::News);

            index
                .add_documents(
                    &crate::couchdb::find_documents::<Document>(json!({
                        "selector": {
                            "_id": {
                                "$gt": format!("{id_prefix}_"),
                                "$lt": format!("{id_prefix}_{{}}")
                            }
                        },
                        "limit": 10000
                    }))
                    .await?,
                    PRIMARY_KEY,
                )
                .await?;
        }

        {
            let index = client.index(&*INDEX_DOCUMENTS);
            index
                .set_searchable_attributes(&["title", "topic", "body"])
                .await?;
            index
                .set_filterable_attributes(vec!["teams", "topic"])
                .await?;

            for document_type in [
                DocumentType::News,
                DocumentType::Internal,
                DocumentType::Media,
                DocumentType::Offtopic,
                DocumentType::Page,
            ] {
                let id_prefix = Document::id_prefix(&document_type);
                index
                    .add_documents(
                        &crate::couchdb::find_documents::<Document>(json!({
                            "selector": {
                                "_id": {
                                    "$gt": format!("{id_prefix}_"),
                                    "$lt": format!("{id_prefix}_{{}}")
                                }
                            },
                            "limit": 10000
                        }))
                        .await?,
                        PRIMARY_KEY,
                    )
                    .await?;
            }
        }

        // VPs are searchable by all logged in users
        {
            let id_prefix = VP::ID_PREFIX;
            let index = client.index(id_prefix);

            index
                .set_searchable_attributes(&["code", "title", "body"])
                .await?;

            index.add_documents(&VP::find_all(), PRIMARY_KEY).await?;
        }

        // Events are only searchable by logged in users with membership
        {
            let index = client.index(&*INDEX_EVENTS);

            index
                .set_searchable_attributes(&["title", "location", "description"])
                .await
                .context("Set searchable attributes of event index")?;
            index
                .set_sortable_attributes(&["start_at"])
                .await
                .context("set sortable attributs of event index")?;
            index
                .set_filterable_attributes(vec!["category"])
                .await
                .context("set_filterable attributes of event index")?;

            for private in [true, false] {
                let id_prefix = Event::id_prefix(private);
                index
                    .add_documents(
                        &crate::couchdb::find_documents::<Event>(json!({
                            "selector": {
                                "_id": {
                                    "$gt": format!("{id_prefix}_"),
                                    "$lt": format!("{id_prefix}_{{}}")
                                }
                            },
                            "limit": 10000
                        }))
                        .await?,
                        PRIMARY_KEY,
                    )
                    .await?;
            }
        }
    }

    Ok(())
}
