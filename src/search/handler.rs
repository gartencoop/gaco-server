use super::{CLIENT, INDEX_DOCUMENTS, INDEX_EVENTS, INDEX_NEWS};
use crate::{
    model::{
        document::{Document, Team, Topic},
        event::{Category, Event},
    },
    GacoSession,
};
use futures_util::future::{join, Either};
use meilisearch_sdk::search::{SearchQuery, SearchResult};
use serde::{Deserialize, Deserializer, Serialize};
use serde_json::Value;
use std::cmp::Ordering;
use wallaby::{
    CustomEvent, EventHandler, InMemorySessionStore, ServerEvent, Storage, StoredSession,
};

#[derive(Debug, Deserialize, Clone)]
#[serde(default)]
pub struct Query {
    pub term: String,
    pub page: usize,
    pub document_team: Option<Team>,
    #[serde(deserialize_with = "deserialize_str_enum")]
    pub document_topic: Option<Topic>,
    #[serde(deserialize_with = "deserialize_str_enum")]
    pub event_category: Option<Category>,
    #[serde(rename = "type")]
    pub search_type: SearchType,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase", tag = "type")]
pub enum SearchResultEntry {
    Document(Value),
    Event(Value),
}

#[derive(Debug, Deserialize, Default, Clone)]
#[serde(rename_all = "camelCase")]
pub enum SearchType {
    #[default]
    Both,
    Documents,
    Events,
}

impl Default for Query {
    fn default() -> Self {
        Self {
            term: Default::default(),
            page: 1,
            document_team: Default::default(),
            document_topic: Default::default(),
            event_category: Default::default(),
            search_type: Default::default(),
        }
    }
}

fn deserialize_str_enum<'de, D, T>(deserializer: D) -> Result<Option<T>, D::Error>
where
    D: Deserializer<'de>,
    T: for<'a> TryFrom<&'a str>,
{
    let Some(str) = Option::<String>::deserialize(deserializer)? else {
        return Ok(None);
    };

    if str.is_empty() {
        return Ok(None);
    }

    Ok(Some(T::try_from(str.as_str()).map_err(|_err| {
        serde::de::Error::custom(format!("Could not parse enum: {str}"))
    })?))
}

pub struct SearchHandler;
#[async_trait::async_trait]
impl EventHandler<GacoSession, InMemorySessionStore<GacoSession>> for SearchHandler {
    fn events() -> Vec<&'static str> {
        vec!["search"]
    }

    async fn handle_event(
        _storage: &mut Storage<GacoSession, InMemorySessionStore<GacoSession>>,
        session: &mut StoredSession<GacoSession>,
        _name: &str,
        payload: &serde_json::Value,
    ) {
        let (pages, values) = async {
            let Some(client) = CLIENT.get().cloned() else {
                log::warn!("No search client registered");
                return (0, Vec::new());
            };
            let search = match serde_json::from_value::<Query>(payload.clone()) {
                Ok(search) => search,
                Err(err) => {
                    log::warn!("Could not parse search query: {err:?}");
                    return (0, Vec::new());
                }
            };

            let documents = {
                let client = client.clone();
                let search = search.clone();
                move |index, hits_per_page| async move {
                    let index = client.index(index);
                    let mut query = SearchQuery::new(&index);
                    query
                        .with_query(&search.term)
                        .with_page(search.page)
                        .with_show_ranking_score(true)
                        .with_hits_per_page(hits_per_page);
                    let filter: Option<String> = match (
                        search
                            .document_team
                            .as_ref()
                            .and_then(|team| serde_json::to_string(team).ok()),
                        search
                            .document_topic
                            .as_ref()
                            .and_then(|topic| serde_json::to_string(topic).ok()),
                    ) {
                        (Some(team), Some(topic)) => {
                            Some(format!("teams = {team} AND topic = {topic}"))
                        }
                        (Some(team), _) => Some(format!("teams = {team}")),
                        (_, Some(topic)) => Some(format!("topic = {topic}")),
                        _ => None,
                    };
                    if let Some(filter) = filter.as_ref() {
                        query.with_filter(filter);
                    }
                    query
                        .execute::<Document>()
                        .await
                        .map(|results| {
                            (
                                results.total_pages.unwrap_or_default(),
                                results
                                    .hits
                                    .into_iter()
                                    .map(Either::<_, SearchResult<Event>>::Left)
                                    .collect::<Vec<_>>(),
                            )
                        })
                        .map_err(|err| {
                            log::warn!("Could not search documents: {err:?}");
                        })
                        .unwrap_or_default()
                }
            };

            let events = {
                let client = client.clone();
                let search = search.clone();
                move |hits_per_page| async move {
                    let index = client.index(&*INDEX_EVENTS);
                    let mut query = SearchQuery::new(&index);
                    query
                        .with_query(&search.term)
                        .with_page(search.page)
                        .with_show_ranking_score(true)
                        .with_hits_per_page(hits_per_page)
                        .with_sort(&["start_at:desc"]);
                    let filter = search
                        .event_category
                        .as_ref()
                        .and_then(|category| serde_json::to_string(category).ok())
                        .map(|category| format!("category = {category}"));
                    if let Some(filter) = filter.as_ref() {
                        query.with_filter(filter);
                    }
                    query
                        .execute::<Event>()
                        .await
                        .map(|results| {
                            (
                                results.total_pages.unwrap_or_default(),
                                results
                                    .hits
                                    .into_iter()
                                    .map(|result| {
                                        Either::<SearchResult<Document>, _>::Right(result)
                                    })
                                    .collect::<Vec<_>>(),
                            )
                        })
                        .map_err(|err| {
                            log::warn!("Could not search events: {err:?}");
                        })
                        .unwrap_or_default()
                }
            };

            let (pages, mut results) = if session.session().access_level().await.member() {
                match search.search_type {
                    SearchType::Both => {
                        let documents = documents(&*INDEX_DOCUMENTS, 5);
                        let events = events(5);
                        let (documents, events) = join(documents, events).await;
                        (
                            documents.0.max(events.0),
                            documents
                                .1
                                .into_iter()
                                .chain(events.1.into_iter())
                                .collect(),
                        )
                    }
                    SearchType::Documents => documents(&*INDEX_DOCUMENTS, 10).await,
                    SearchType::Events => events(10).await,
                }
            } else {
                documents(&*INDEX_NEWS, 10).await
            };

            results.sort_by(|a, b| {
                let a = match a {
                    Either::Left(a) => a.ranking_score,
                    Either::Right(a) => a.ranking_score,
                };
                let b = match b {
                    Either::Left(b) => b.ranking_score,
                    Either::Right(b) => b.ranking_score,
                };
                match (a, b) {
                    (Some(a), Some(b)) => a.partial_cmp(&b).unwrap_or(Ordering::Equal),
                    (Some(_), None) => Ordering::Greater,
                    (None, Some(_)) => Ordering::Less,
                    (None, None) => Ordering::Equal,
                }
            });
            results.reverse();
            let values = results
                .into_iter()
                .filter_map(|result| match result {
                    Either::Left(result) => serde_json::to_value(result.result)
                        .ok()
                        .map(SearchResultEntry::Document),
                    Either::Right(result) => serde_json::to_value(result.result)
                        .ok()
                        .map(SearchResultEntry::Event),
                })
                .collect::<Vec<_>>();

            (pages, values)
        }
        .await;

        session
            .send_events(vec![ServerEvent::Custom(CustomEvent::new(
                "searchResult".to_owned(),
                std::iter::once(("pages".into(), pages.into()))
                    .chain(std::iter::once((
                        "results".into(),
                        serde_json::to_value(values).unwrap_or_else(|_| Value::Array(vec![])),
                    )))
                    .collect(),
            ))])
            .await;
    }
}
