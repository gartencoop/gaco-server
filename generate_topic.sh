#!/bin/bash

set -e

declare -A names
names["AG"]="Ag"
names["Beschlüsse"]="Beschluesse"
names["Car-Sharing"]="CarSharing"
names["Frühjahrswerkstatt"]="Fruehjahrswerkstatt"
names["Gurken & Zucchini"]="GurkenUndZucchini"
names["Hofstelle Tunsel"]="HofstelleTunsel"
names["Interne Dokumente"]="InterneDokumente"
names["MV"]="Mv"
names["Netzwerk SoLaWi"]="NetzwerkSoLaWi"
names["ÖA"]="OeA"
names["Orga- und Entscheidungsstruktur"]="OrgaUndEntscheidungsstruktur"
names["Räume"]="Raeume"

IFS=$'\n'       # make newlines the only separator

echo "use serde::{Deserialize, Serialize};use strum_macros::{IntoStaticStr, EnumIter, EnumString};#[derive(Clone, Copy, Debug, Deserialize, Serialize, IntoStaticStr, EnumIter, EnumString)]pub enum Topic {" > src/model/document/topic.rs
for topic in `ls topics/`; do
    if [[ "$topic" != "GARBAGE" ]]; then
        echo "#[serde(rename=\"$topic\")]" >> src/model/document/topic.rs
        echo "#[strum(serialize = \"$topic\")]" >> src/model/document/topic.rs
        if [[ "${names[$topic]}" ]]; then
            echo "${names[$topic]}," >> src/model/document/topic.rs
        else
            echo "$topic," >> src/model/document/topic.rs
        fi
    fi
done
echo "}" >> src/model/document/topic.rs

function join_by { local IFS="$1"; shift; echo "$*"; }

echo "#[cfg(feature = \"import\")]" >> src/model/document/topic.rs
echo "impl Topic { #[allow(dead_code)] pub fn from(topic: &str) -> Option<Self> {match topic {" >> src/model/document/topic.rs
for topic in `ls topics/`; do
    old_topics=`cat "topics/$topic"|awk 'NF{print "\"" $0 "\""}'`
    join_by \| $old_topics >> src/model/document/topic.rs
    
    if [[ "$topic" == "GARBAGE" ]]; then
        echo "=> None," >> src/model/document/topic.rs
    else
        if [[ "${names[$topic]}" ]]; then
            echo "=> Some(Self::${names[$topic]})," >> src/model/document/topic.rs
        else
            echo "=> Some(Self::$topic)," >> src/model/document/topic.rs
        fi
    fi
done
echo "topic => {println!(\"Could not find topic \\\"{topic}\\\"\");None} ,}}}" >> src/model/document/topic.rs
rustfmt src/model/document/topic.rs
