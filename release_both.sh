set -e

cargo build --release --target x86_64-unknown-linux-musl
rsync -vz ../target/x86_64-unknown-linux-musl/release/gaco-server gaco@erinome.uberspace.de:
ssh gaco@erinome.uberspace.de "supervisorctl stop gaco-server-test && supervisorctl stop gaco-server-live && cp gaco-server bin/gaco-server-test && mv gaco-server bin/gaco-server-live && supervisorctl start gaco-server-live && supervisorctl start gaco-server-test"
