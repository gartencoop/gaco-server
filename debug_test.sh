set -e

cargo build --target x86_64-unknown-linux-musl
rsync -vz target/x86_64-unknown-linux-musl/debug/gaco-server gaco@erinome.uberspace.de:
ssh gaco@erinome.uberspace.de "supervisorctl stop gaco-server-test && mv gaco-server bin/gaco-server-test && supervisorctl start gaco-server-test"