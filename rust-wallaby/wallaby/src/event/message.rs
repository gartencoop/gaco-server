use std::time::Duration;

use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Message {
    #[serde(rename = "type")]
    level: MessageLevel,
    #[serde(skip_serializing_if = "Option::is_none")]
    title: Option<String>,
    #[serde(rename = "msg")]
    text: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    duration: Option<u64>,
}

impl Message {
    pub fn new(level: MessageLevel, title: Option<String>, text: String) -> Self {
        Self {
            level,
            title,
            text,
            duration: None,
        }
    }

    pub fn error(text: String) -> Self {
        Self::new(MessageLevel::Error, None, text)
    }

    pub fn warning(text: String) -> Self {
        Self::new(MessageLevel::Warning, None, text)
    }

    pub fn info(text: String) -> Self {
        Self::new(MessageLevel::Info, None, text)
    }

    pub fn success(text: String) -> Self {
        Self::new(MessageLevel::Success, None, text)
    }

    pub fn set_duration(&mut self, duration: Duration) {
        self.duration = Some(duration.as_millis() as u64);
    }

    pub fn level(&self) -> MessageLevel {
        self.level
    }

    pub fn title(&self) -> Option<&str> {
        self.title.as_deref()
    }

    pub fn text(&self) -> &str {
        &self.text
    }

    pub fn duration(&self) -> Option<Duration> {
        self.duration.map(Duration::from_millis)
    }
}

#[derive(Clone, Copy, Debug, Deserialize, Serialize)]
pub enum MessageLevel {
    #[serde(rename = "error")]
    Error,
    #[serde(rename = "warn")]
    Warning,
    #[serde(rename = "info")]
    Info,
    #[serde(rename = "success")]
    Success,
}
