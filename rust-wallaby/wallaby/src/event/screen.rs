mod action;

use crate::{RenderedInteraction, Ui};
pub use action::ScreenAction;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[cfg(all(
    feature = "backend",
    feature = "screen_update",
    not(feature = "transport_ffi")
))]
type ScreenStore = HashMap<String, ScreenComplete>;
#[cfg(all(
    feature = "backend",
    feature = "screen_update",
    not(feature = "transport_ffi")
))]
static SCREENS: once_cell::sync::Lazy<std::sync::Arc<tokio::sync::Mutex<ScreenStore>>> =
    once_cell::sync::Lazy::new(|| std::sync::Arc::new(tokio::sync::Mutex::new(HashMap::new())));

#[cfg(all(
    feature = "backend",
    feature = "screen_update",
    not(feature = "transport_ffi")
))]
pub(crate) async fn start<SESSION: crate::Session, SESSIONSTORE: crate::SessionStore<SESSION>>(
    session_store: SESSIONSTORE,
) {
    tokio::spawn(async move {
        loop {
            tokio::time::sleep(std::time::Duration::from_secs(10)).await;
            let session_ids: std::collections::HashSet<_> =
                session_store.session_ids().await.collect();

            let mut screens = SCREENS.lock().await;
            let len = screens.len();
            screens.retain(|session_id, _screen| session_ids.contains(session_id));
            log::trace!("Removed {} of {} cached screens", len - screens.len(), len);
        }
    });
}

#[cfg(all(
    feature = "backend",
    any(not(feature = "screen_update"), feature = "transport_ffi")
))]
pub(crate) async fn start<SESSION: crate::Session, SESSIONSTORE: crate::SessionStore<SESSION>>(
    _session_store: SESSIONSTORE,
) {
}

#[allow(clippy::large_enum_variant)]
#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(untagged)]
pub enum Screen {
    Complete(ScreenComplete),
    #[cfg(all(feature = "screen_update", not(feature = "transport_ffi")))]
    Patch(ScreenPatch),
    #[cfg(all(feature = "screen_update", not(feature = "transport_ffi")))]
    Diff(ScreenDiff),
}

#[derive(Debug, Default, Deserialize, Serialize, Clone)]
#[serde(default)]
pub struct ScreenComplete {
    #[serde(rename = "edid")]
    pub session_id: String,
    pub name: String,
    #[serde(rename = "_requestedScreen")]
    pub requested_screen: String,
    pub update: bool,
    pub locale: String,
    pub ui: Ui,
    #[serde(rename = "queryParams", skip_serializing_if = "Option::is_none")]
    pub query_params: Option<String>,
    #[serde(rename = "action")]
    pub actions: HashMap<String, ScreenAction>,

    #[serde(rename = "interaction")]
    pub interactions: HashMap<String, RenderedInteraction>,
}

#[cfg(all(feature = "screen_update", not(feature = "transport_ffi")))]
#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct ScreenPatch {
    update: String,
    #[serde(rename = "edid")]
    session_id: String,
    name: String,
    patch: json_patch::Patch,
}

#[cfg(all(feature = "screen_update", not(feature = "transport_ffi")))]
impl ScreenPatch {
    #[cfg(feature = "backend")]
    fn new(session_id: String, name: String, patch: json_patch::Patch) -> Self {
        Self {
            update: "patch".to_owned(),
            session_id,
            name,
            patch,
        }
    }

    #[cfg(feature = "frontend")]
    fn apply(self, mut screen: serde_json::Value) -> Option<ScreenComplete> {
        json_patch::patch(&mut screen, &self.patch).ok()?;
        serde_json::from_value(screen).ok()
    }
}

#[cfg(all(feature = "screen_update", not(feature = "transport_ffi")))]
#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct ScreenDiff {
    update: String,
    #[serde(rename = "edid")]
    session_id: String,
    name: String,

    #[serde(flatten)]
    diff: serde_json::Value,
}

#[cfg(all(feature = "screen_update", not(feature = "transport_ffi")))]
impl ScreenDiff {
    #[cfg(feature = "backend")]
    fn new(session_id: String, name: String, diff: serde_json::Value) -> Self {
        Self {
            update: "diff".to_owned(),
            session_id,
            name,
            diff,
        }
    }

    #[cfg(feature = "frontend")]
    fn apply(self, mut screen: serde_json::Value) -> Option<ScreenComplete> {
        apply_diff(&mut screen, self.diff);
        serde_json::from_value(screen).ok()
    }
}

impl Screen {
    #[cfg(feature = "backend")]
    pub async fn from_rendered_screen<SESSION: crate::Session>(
        screen: crate::RenderedScreen,
        session: &crate::StoredSession<SESSION>,
        update: bool,
        exclude_interactions: Vec<String>,
    ) -> Option<Self> {
        let mut complete = ScreenComplete {
            session_id: session.session_id().to_owned(),
            locale: session.session.locale().language.as_str().to_owned(),
            name: screen.name.clone(),
            requested_screen: screen.name,
            update,
            ui: screen.ui,
            query_params: screen.query_params,
            actions: screen
                .actions
                .into_iter()
                .map(|(name, action)| (name, action.into()))
                .collect(),
            interactions: screen.interactions,
        };
        for interaction in exclude_interactions.iter() {
            complete.interactions.remove(interaction);
        }

        #[cfg(any(not(feature = "screen_update"), feature = "transport_ffi"))]
        {
            Some(Screen::Complete(complete))
        }

        #[cfg(all(feature = "screen_update", not(feature = "transport_ffi")))]
        {
            let mut previous_complete = SCREENS
                .lock()
                .await
                .insert(session.session_id().to_owned(), complete.clone());

            if let Some(previous_complete) = previous_complete.as_mut() {
                for interaction in exclude_interactions.iter() {
                    previous_complete.interactions.remove(interaction);
                }
            }

            let res = match (
                previous_complete
                    .filter(|_| update)
                    .and_then(|previous_complete| serde_json::to_value(previous_complete).ok()),
                serde_json::to_value(&complete).ok(),
            ) {
                (Some(previous_complete_value), Some(complete_value)) => {
                    let mut diff = diff(&previous_complete_value, &complete_value)?;
                    diff.as_object_mut().map(|diff| diff.remove("update"));
                    if diff.as_object().map(|obj| obj.len()) == Some(0) {
                        return None;
                    }
                    let patch = json_patch::diff(&previous_complete_value, &complete_value);

                    if serde_json::to_string(&diff)
                        .map(|json| json.len())
                        .unwrap_or_default()
                        < serde_json::to_string(&patch)
                            .map(|json| json.len())
                            .unwrap_or_default()
                    {
                        Screen::Diff(ScreenDiff::new(complete.session_id, complete.name, diff))
                    } else {
                        Screen::Patch(ScreenPatch::new(complete.session_id, complete.name, patch))
                    }
                }
                _ => Screen::Complete(complete),
            };

            Some(res)
        }
    }
}

#[cfg(all(
    feature = "backend",
    feature = "screen_update",
    not(feature = "transport_ffi")
))]
fn diff(old: &serde_json::Value, new: &serde_json::Value) -> Option<serde_json::Value> {
    if old == new {
        return None;
    }

    match (old, new) {
        (serde_json::Value::Object(old), serde_json::Value::Object(new)) => {
            let keys = old
                .keys()
                .chain(new.keys())
                .collect::<std::collections::HashSet<_>>();
            let obj_diff = keys
                .into_iter()
                .filter_map(|key| {
                    let old = old.get(key);
                    let new = new.get(key);
                    match (old, new) {
                        (Some(old), Some(new)) => diff(old, new),
                        (Some(_), None) => Some(serde_json::Value::Null),
                        (None, Some(new)) => Some(new.clone()),
                        (None, None) => None,
                    }
                    .map(|value| (key.clone(), value))
                })
                .collect::<serde_json::Map<_, _>>();
            if obj_diff.is_empty() {
                None
            } else {
                Some(serde_json::Value::Object(obj_diff))
            }
        }
        (_old, new) => Some(new.clone()),
    }
}

#[cfg(all(feature = "screen_update", feature = "frontend"))]
fn apply_diff(value: &mut serde_json::Value, diff: serde_json::Value) {
    match (value, diff) {
        (serde_json::Value::Object(value), serde_json::Value::Object(diff)) => {
            diff.into_iter().for_each(|(key, diff)| match diff {
                serde_json::Value::Null => {
                    value.remove(&key);
                }
                diff => {
                    if let Some(value) = value.get_mut(&key) {
                        apply_diff(value, diff);
                    } else {
                        value.insert(key.to_owned(), diff);
                    }
                }
            });
        }
        (value, diff) => *value = diff,
    }
}

#[cfg(feature = "frontend")]
impl Screen {
    #[cfg(not(feature = "screen_update"))]
    pub fn into_complete_screen(self, _previous: Option<ScreenComplete>) -> Option<ScreenComplete> {
        match self {
            Screen::Complete(complete) => Some(complete.into()),
        }
    }

    #[cfg(feature = "screen_update")]
    pub fn into_complete_screen(self, previous: Option<&ScreenComplete>) -> Option<ScreenComplete> {
        if let Screen::Complete(complete) = self {
            return Some(complete);
        }

        let previous = previous?;
        let name = previous.name.clone();
        let previous_value = serde_json::to_value(previous).ok()?;

        match self {
            Screen::Patch(patch) => {
                if patch.name != name {
                    return None;
                }

                patch.apply(previous_value)
            }
            Screen::Diff(diff) => {
                if diff.name != name {
                    return None;
                }

                diff.apply(previous_value)
            }
            Screen::Complete(_) => unreachable!(),
        }
    }
}
