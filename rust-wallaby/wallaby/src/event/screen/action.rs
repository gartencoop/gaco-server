use crate::RenderedAction;
use serde::{Deserialize, Serialize};
use serde_json::Value;

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct ScreenAction {
    code: String,
    row: String,
    #[serde(rename = "interaction")]
    interaction_name: String,
    #[serde(rename = "type")]
    action_type: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    table: Option<String>,
    #[serde(flatten)]
    extra_fields: Value,
}

impl ScreenAction {
    pub fn code(&self) -> &str {
        &self.code
    }

    pub fn row(&self) -> &str {
        &self.row
    }

    pub fn interaction_name(&self) -> &str {
        &self.interaction_name
    }

    pub fn action_type(&self) -> &str {
        &self.action_type
    }

    pub fn table(&self) -> Option<&str> {
        self.table.as_deref()
    }
}

impl From<RenderedAction> for ScreenAction {
    fn from(action: RenderedAction) -> Self {
        Self {
            code: "action/genericAction".to_string(),
            row: "{{{rowIndex}}}".to_string(),
            interaction_name: action.interaction_name,
            action_type: action.action_type,
            table: action.table,
            extra_fields: action.extra_fields,
        }
    }
}
