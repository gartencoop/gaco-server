use serde::{Deserialize, Serialize};
use serde_json::{Map, Value};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct CustomEvent {
    pub name: String,
    pub payload: Map<String, Value>,
}

impl CustomEvent {
    pub fn new(name: String, payload: Map<String, Value>) -> Self {
        Self { name, payload }
    }
}
