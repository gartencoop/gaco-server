use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Alert {
    #[serde(skip_serializing_if = "Option::is_none")]
    caption: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    message: Option<String>,
    #[serde(skip_serializing_if = "Vec::is_empty")]
    buttons: Vec<AlertButton>,
}

impl Alert {
    pub fn new(
        caption: Option<String>,
        message: Option<String>,
        buttons: Vec<AlertButton>,
    ) -> Self {
        Self {
            caption,
            message,
            buttons,
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct AlertButton {
    caption: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    action: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    class: Option<AlertButtonClass>,
}

impl AlertButton {
    pub fn new(caption: String, action: Option<String>, class: Option<AlertButtonClass>) -> Self {
        Self {
            caption,
            action,
            class,
        }
    }

    pub fn warning(caption: String, action: String) -> Self {
        Self::new(caption, Some(action), Some(AlertButtonClass::Warning))
    }

    pub fn warning_without_action(caption: String) -> Self {
        Self::new(caption, None, Some(AlertButtonClass::Warning))
    }

    pub fn danger(caption: String, action: String) -> Self {
        Self::new(caption, Some(action), Some(AlertButtonClass::Danger))
    }

    pub fn danger_without_action(caption: String) -> Self {
        Self::new(caption, None, Some(AlertButtonClass::Danger))
    }

    pub fn error(caption: String, action: String) -> Self {
        Self::new(caption, Some(action), Some(AlertButtonClass::Error))
    }

    pub fn error_without_action(caption: String) -> Self {
        Self::new(caption, None, Some(AlertButtonClass::Error))
    }

    pub fn primary(caption: String, action: String) -> Self {
        Self::new(caption, Some(action), Some(AlertButtonClass::Primary))
    }

    pub fn primary_without_action(caption: String) -> Self {
        Self::new(caption, None, Some(AlertButtonClass::Primary))
    }

    pub fn success(caption: String, action: String) -> Self {
        Self::new(caption, Some(action), Some(AlertButtonClass::Success))
    }

    pub fn success_without_action(caption: String) -> Self {
        Self::new(caption, None, Some(AlertButtonClass::Success))
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum AlertButtonClass {
    #[serde(rename = "btn-warning")]
    Warning,
    #[serde(rename = "btn-danger")]
    Danger,
    #[serde(rename = "btn-error")]
    Error,
    #[serde(rename = "btn-primary")]
    Primary,
    #[serde(rename = "btn-success")]
    Success,
}
