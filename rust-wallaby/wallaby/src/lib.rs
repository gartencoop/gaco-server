// Forbid warnings in release builds:
#![cfg_attr(not(debug_assertions), deny(warnings))]
#![warn(
    clippy::all,
    clippy::await_holding_lock,
    clippy::char_lit_as_u8,
    clippy::checked_conversions,
    clippy::dbg_macro,
    clippy::debug_assert_with_mut_call,
    clippy::disallowed_methods,
    clippy::doc_markdown,
    clippy::empty_enum,
    clippy::enum_glob_use,
    clippy::exit,
    clippy::expl_impl_clone_on_copy,
    clippy::explicit_deref_methods,
    clippy::explicit_into_iter_loop,
    clippy::fallible_impl_from,
    clippy::filter_map_next,
    clippy::flat_map_option,
    clippy::float_cmp_const,
    clippy::fn_params_excessive_bools,
    clippy::from_iter_instead_of_collect,
    clippy::if_let_mutex,
    clippy::implicit_clone,
    clippy::imprecise_flops,
    clippy::inefficient_to_string,
    clippy::invalid_upcast_comparisons,
    clippy::large_digit_groups,
    clippy::large_stack_arrays,
    clippy::large_types_passed_by_value,
    clippy::let_unit_value,
    clippy::linkedlist,
    clippy::lossy_float_literal,
    clippy::macro_use_imports,
    clippy::manual_ok_or,
    clippy::map_err_ignore,
    clippy::map_flatten,
    clippy::map_unwrap_or,
    clippy::match_on_vec_items,
    clippy::match_same_arms,
    clippy::match_wild_err_arm,
    clippy::match_wildcard_for_single_variants,
    clippy::mem_forget,
    clippy::missing_errors_doc,
    clippy::missing_safety_doc,
    clippy::mut_mut,
    clippy::mutex_integer,
    clippy::needless_borrow,
    clippy::needless_continue,
    clippy::needless_for_each,
    clippy::option_option,
    clippy::path_buf_push_overwrite,
    clippy::ptr_as_ptr,
    clippy::ref_option_ref,
    clippy::rest_pat_in_fully_bound_structs,
    clippy::same_functions_in_if_condition,
    clippy::semicolon_if_nothing_returned,
    clippy::single_match_else,
    clippy::string_add_assign,
    clippy::string_add,
    clippy::string_lit_as_bytes,
    clippy::string_to_string,
    clippy::todo,
    clippy::trait_duplication_in_bounds,
    clippy::unimplemented,
    clippy::unnested_or_patterns,
    clippy::unused_self,
    clippy::useless_transmute,
    clippy::verbose_file_reads,
    clippy::zero_sized_map_values,
    future_incompatible,
    nonstandard_style,
    rust_2018_idioms,
    rustdoc::missing_crate_level_docs
)]

#[cfg(feature = "bridge")]
mod bridge;
mod error;
mod event;
mod handler;
mod interaction;
mod screen;
#[cfg(feature = "backend")]
mod session;
#[cfg(feature = "backend")]
mod storage;
#[cfg(feature = "backend")]
mod transport;
#[cfg(feature = "backend")]
mod updater;

pub use error::Error;
pub use event::{
    alert::{Alert, AlertButton, AlertButtonClass},
    custom::CustomEvent,
    message::{Message, MessageLevel},
    play_sound::PlaySound,
    screen::{ScreenAction, ScreenComplete},
    Event, ServerEvent, ServerEvents,
};
pub use handler::{action::ActionPayload, client_event::ClientEvent};
#[cfg(feature = "backend")]
pub use handler::{
    action::{
        bool_value_payload::BoolValuePayload, f32_value_payload::F32ValuePayload,
        f64_value_payload::F64ValuePayload, string_value_payload::StringValuePayload,
        u8_value_payload::U8ValuePayload, value_payload::ValuePayload,
    },
    helo::HeloPayload,
    EventHandler,
};
pub use interaction::{
    button::RenderedButton,
    configure::ConfigureInteraction,
    container::RenderedContainer,
    generic::GenericInteraction,
    input::RenderedInput,
    output::RenderedOutput,
    selection::{RenderedSelection, SelectionOption},
    table::RenderedTable,
    Action, RenderedInteraction,
};
#[cfg(feature = "backend")]
pub use interaction::{
    button::{
        change_screen_button::ChangeScreenButton, static_button::StaticButton, Button,
        ButtonInteraction,
    },
    container::{Container, ContainerInteraction},
    input::{Input, InputInteraction},
    output::{static_output::StaticOutput, Output, OutputInteraction},
    selection::{Selection, SelectionInteraction},
    Interaction,
};
pub use log;
#[cfg(all(feature = "bridge", feature = "backend"))]
pub use screen::bridge::BridgeScreen;
#[cfg(feature = "backend")]
pub use screen::{
    configure::ConfigureScreen, helo::Helo, load_data::LoadData, query_params::QueryParams,
    set_data::SetData, BoxedScreen, Screen,
};
pub use screen::{RenderedAction, RenderedScreen, Ui};
#[cfg(feature = "backend")]
pub use session::Session;
#[cfg(feature = "backend")]
pub use storage::{
    screen_store::ScreenStore,
    session_store::{
        in_memory::InMemorySessionStore, single::SingleSessionStore, SessionStore, StoredSession,
    },
    Storage,
};
#[cfg(feature = "backend")]
pub use transport::*;
#[cfg(feature = "backend")]
pub use updater::{
    send_events, send_events_for_session, send_events_on_screen, send_events_on_screens,
    update_all_screens, update_all_screens_with_events, update_screen, update_screen_for_session,
    update_screen_with_events, update_screens, update_screens_with_events,
};

#[cfg(feature = "backend")]
pub mod derive {
    pub use wallaby_derive::Screen;
}

#[cfg(feature = "backend")]
// is run from transport and starts common tasks
async fn start<SESSION: Session, SESSIONSTORE: SessionStore<SESSION>>(
    mut storage: Storage<SESSION, SESSIONSTORE>,
) {
    storage
        .handler_store_mut()
        .add::<crate::handler::helo::Helo>()
        .await;
    storage
        .handler_store_mut()
        .add::<crate::handler::action::Action>()
        .await;
    event::screen::start(storage.session_store().clone()).await;
    updater::register(storage).await;
}
