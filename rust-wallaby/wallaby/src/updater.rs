use crate::{Event, ServerEvents, Session, SessionStore, Storage, StoredSession};
use log::{debug, error, info, trace};
use std::borrow::Cow;
use tokio::sync::mpsc::{channel, Sender};
use tokio::sync::Mutex;

static SENDER: Mutex<Option<Sender<UpdateRequest>>> = Mutex::const_new(None);

#[derive(Debug)]
enum UpdateRequest {
    AllScreensWithEvents {
        events: Vec<Event>,
    },
    ScreensWithEvents {
        screen_names: Vec<Cow<'static, str>>,
        events: Vec<Event>,
    },
    ScreenForSession {
        session_id: String,
    },
    EventsOnScreens {
        screen_names: Vec<Cow<'static, str>>,
        events: ServerEvents,
    },
    EventsForSession {
        session_id: String,
        events: ServerEvents,
    },
    Events {
        events: ServerEvents,
    },
}

/// Update screen for all sessions on a screen if `session.should_update_screen() == true`
pub async fn update_screen(screen_name: Cow<'static, str>) {
    update_screens_with_events(vec![screen_name], vec![]).await;
}

/// Update screen for all sessions on screens if `session.should_update_screen() == true`
pub async fn update_screens(screen_names: Vec<Cow<'static, str>>) {
    update_screens_with_events(screen_names, vec![]).await;
}

/// Update all screen for all sessions on screens if `session.should_update_screen() == true`
pub async fn update_all_screens() {
    update_all_screens_with_events(vec![]).await;
}

/// Update screen with extra events for all sessions on a screen if `session.should_update_screen() == true`
pub async fn update_screen_with_events(screen_name: Cow<'static, str>, events: Vec<Event>) {
    update_screens_with_events(vec![screen_name], events).await;
}

/// Update screens with extra events for all sessions on screens if `session.should_update_screen() == true`
pub async fn update_screens_with_events(screen_names: Vec<Cow<'static, str>>, events: Vec<Event>) {
    send(UpdateRequest::ScreensWithEvents {
        screen_names,
        events,
    })
    .await;
}

/// Update all screens with extra events for all sessions on screens if `session.should_update_screen() == true`
pub async fn update_all_screens_with_events(events: Vec<Event>) {
    send(UpdateRequest::AllScreensWithEvents { events }).await;
}

/// Update screen of session identified by `session_id`
pub async fn update_screen_for_session(session_id: String) {
    send(UpdateRequest::ScreenForSession { session_id }).await;
}

/// Send events to all sessions on a screen
pub async fn send_events_on_screen(screen_name: Cow<'static, str>, events: ServerEvents) {
    send_events_on_screens(vec![screen_name], events).await;
}

/// Send events to all sessions on screens
pub async fn send_events_on_screens(screen_names: Vec<Cow<'static, str>>, events: ServerEvents) {
    send(UpdateRequest::EventsOnScreens {
        screen_names,
        events,
    })
    .await;
}

/// Send events to all sessions
pub async fn send_events(events: ServerEvents) {
    send(UpdateRequest::Events { events }).await;
}

/// Send events to a session identified by `session_id`
pub async fn send_events_for_session(session_id: String, events: ServerEvents) {
    send(UpdateRequest::EventsForSession { session_id, events }).await;
}

async fn send(update_request: UpdateRequest) {
    debug!("Trying to get sender");
    if let Some(sender) = SENDER.lock().await.as_ref() {
        log::debug!("Sending update request: {update_request:?}");
        if let Err(err) = sender.send(update_request).await {
            error!("Could not send UpdateRequest: {:?}", err);
        } else {
            debug!("Done sending UpdateRequest");
        }
    } else {
        error!("Screen updater is not registered");
    }
}

pub async fn register<SESSION: Session, SESSIONSTORE: SessionStore<SESSION>>(
    storage: Storage<SESSION, SESSIONSTORE>,
) {
    let (tx, mut rx) = channel(10);
    *SENDER.lock().await = Some(tx);

    tokio::task::spawn(async move {
        while let Some(update_request) = rx.recv().await {
            trace!("Received screen update request \"{:?}\"", update_request);

            if let UpdateRequest::EventsForSession { session_id, events } = update_request {
                trace!("Sending events to session: {}", session_id);

                let session = StoredSession::load(storage.session_store(), &session_id).await;
                session.send_events(events).await;
                continue;
            }

            if let UpdateRequest::ScreenForSession { session_id } = update_request {
                trace!("Sending screen update to session: {}", session_id);

                let session = StoredSession::load(storage.session_store(), &session_id).await;
                session
                    .render_and_send_screen(storage.clone(), vec![], true)
                    .await;
                continue;
            }

            for stored_session in StoredSession::sessions(storage.session_store()).await {
                #[cfg(any(
                    feature = "transport_ffi",
                    feature = "transport_http",
                    feature = "transport_tauri"
                ))]
                if !crate::transport::session_connected(stored_session.session_id()).await {
                    continue;
                }

                let session = stored_session.session();

                match update_request {
                    UpdateRequest::AllScreensWithEvents { ref events } => {
                        if session.should_update_screen() {
                            trace!(
                                "Updating screen \"{}\" for session: {:?}",
                                session.screen_name(),
                                session
                            );
                            stored_session
                                .render_and_send_screen(storage.clone(), events.clone(), true)
                                .await;
                        }
                    }
                    UpdateRequest::ScreensWithEvents {
                        ref screen_names,
                        ref events,
                    } => {
                        if session.should_update_screen()
                            && screen_names
                                .iter()
                                .any(|screen_name| screen_name == session.screen_name())
                        {
                            trace!(
                                "Updating screen \"{}\" for session: {:?}",
                                session.screen_name(),
                                session
                            );
                            stored_session
                                .render_and_send_screen(storage.clone(), events.clone(), true)
                                .await;
                        }
                    }
                    UpdateRequest::EventsOnScreens {
                        ref screen_names,
                        ref events,
                    } => {
                        if screen_names
                            .iter()
                            .any(|screen_name| screen_name == session.screen_name())
                        {
                            trace!("Sending events to session: {:?}", session);
                            stored_session.send_events(events.clone()).await;
                        }
                    }
                    UpdateRequest::Events { ref events } => {
                        trace!("Sending events to session: {:?}", session);
                        stored_session.send_events(events.clone()).await;
                    }
                    UpdateRequest::EventsForSession { .. }
                    | UpdateRequest::ScreenForSession { .. } => {
                        panic!("Should not happen as it is handled before the sessions loop")
                    }
                }
            }
        }
    });

    info!("Registered screen updater");
}
