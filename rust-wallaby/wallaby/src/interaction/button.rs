#[cfg(feature = "backend")]
pub mod change_screen_button;
#[cfg(feature = "backend")]
pub mod static_button;

use crate::{Action, RenderedInteraction};
use serde::{Deserialize, Serialize};
use serde_json::Value;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RenderedButton(RenderedInteraction);

impl RenderedButton {
    #[allow(dead_code)]
    pub fn new<V: Into<Value>>(
        caption: Option<String>,
        _interaction_name: String,
        click: bool,
        extra_fields: V,
    ) -> RenderedButton {
        #[derive(Serialize)]
        struct ExtraFields {
            caption: Option<String>,

            #[serde(flatten)]
            extra_fields: Value,
        }

        let extra_fields = serde_json::to_value(ExtraFields {
            caption,
            extra_fields: extra_fields.into(),
        })
        .unwrap_or_default();

        let mut events = Vec::with_capacity(1);
        if click {
            events.push("click".to_string());
        }

        Self(RenderedInteraction::new(extra_fields, events))
    }
}

#[cfg(feature = "backend")]
/// Trait for defining a button interaction for a screen and session (can both be generic)
///
/// * `render`: Return the rendered instance of a button. See [`RenderedButton`].
/// * `action`: Run action when button is clicked. No need to run [`crate::update_screen`] as this always triggers a screen event.
/// * `actions`: List of screen actions. Can be overwritten to set custom fields. Defaults to a `"click"` action without custom fields.
#[async_trait::async_trait]
pub trait Button<SCREEN: crate::Screen<SESSION>, SESSION: crate::Session>: Sync + Send {
    /// Return the rendered instance of a button. See [`RenderedButton`].
    #[allow(unused_variables)]
    fn render(
        &self,
        interaction_name: String,
        screen: &SCREEN,
        session: &SESSION,
    ) -> RenderedButton {
        RenderedButton::new(None, interaction_name, true, ())
    }

    /// Run action when button is clicked. No need to run [`crate::update_screen`] as this always triggers a screen event.
    #[allow(unused_variables)]
    async fn action(
        &self,
        session: &mut SESSION,
        payload: &crate::ActionPayload,
    ) -> Vec<crate::Event> {
        vec![]
    }

    /// List of screen actions. Can be overwritten to set custom fields.
    ///
    /// Defaults to a `"click"` action without custom fields.
    fn actions(&self) -> Vec<crate::Action> {
        vec![crate::Action::new("click", None)]
    }
}

#[cfg(feature = "backend")]
pub struct ButtonInteraction<'a, SCREEN: crate::Screen<SESSION>, SESSION: crate::Session> {
    inner: &'a dyn Button<SCREEN, SESSION>,
}

#[cfg(feature = "backend")]
impl<'a, SCREEN: crate::Screen<SESSION>, SESSION: crate::Session>
    ButtonInteraction<'a, SCREEN, SESSION>
{
    pub fn new(inner: &'a dyn Button<SCREEN, SESSION>) -> Self {
        Self { inner }
    }
}

#[cfg(feature = "backend")]
#[async_trait::async_trait]
impl<SCREEN: crate::Screen<SESSION>, SESSION: crate::Session> crate::Interaction<SCREEN, SESSION>
    for ButtonInteraction<'_, SCREEN, SESSION>
{
    fn render(
        &self,
        interaction_name: String,
        screen: &SCREEN,
        session: &SESSION,
    ) -> crate::RenderedInteraction {
        self.inner.render(interaction_name, screen, session).0
    }

    async fn action(
        &self,
        session: &mut SESSION,
        payload: &crate::ActionPayload,
    ) -> Vec<crate::Event> {
        self.inner.action(session, payload).await
    }

    fn actions(&self) -> Vec<Action> {
        self.inner.actions()
    }
}
