use crate::{Button, ConfigureInteraction, Screen, Session};

/// Button interaction which changes the screen to the screen set by `configuration` attribute
///
/// ```
/// #[interaction(type = "button", configuration = "Home")]
/// home_button: ChangeScreenButton,
/// ```
pub struct ChangeScreenButton {
    screen: String,
}

impl ConfigureInteraction for ChangeScreenButton {
    fn configure(configuration: &str) -> Self {
        Self {
            screen: configuration.to_string(),
        }
    }
}

#[async_trait::async_trait]
impl<SCREEN: Screen<SESSION>, SESSION: Session> Button<SCREEN, SESSION> for ChangeScreenButton {
    async fn action(
        &self,
        session: &mut SESSION,
        _payload: &crate::ActionPayload,
    ) -> Vec<crate::Event> {
        session.set_screen_name(self.screen.clone());
        vec![]
    }
}
