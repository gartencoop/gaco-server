use crate::{Button, ConfigureInteraction, RenderedButton, Screen, Session};

/// Button interaction without action with caption set by `configuration` attribute
///
/// ```
/// #[interaction(type = "button", configuration = "Click ME")]
/// test_button: StaticButton,
/// ```
pub struct StaticButton {
    caption: Option<String>,
}

impl StaticButton {
    pub fn caption(&self) -> Option<&str> {
        self.caption.as_deref()
    }
}

impl<SCREEN: Screen<SESSION>, SESSION: Session> Button<SCREEN, SESSION> for StaticButton {
    fn render(
        &self,
        interaction_name: String,
        _screen: &SCREEN,
        _session: &SESSION,
    ) -> RenderedButton {
        RenderedButton::new(self.caption.clone(), interaction_name, true, ())
    }
}

impl ConfigureInteraction for StaticButton {
    fn configure(configuration: &str) -> Self {
        Self {
            caption: Some(configuration.to_string()),
        }
    }
}
