/// Configure an interaction by supplying the configuration in the `#[interaction]` macro
///
/// ```
/// #[interaction(type = "button", configuration = "bla")]
/// test_button: TestButton,
/// ```
///
/// -> calls the configure method on `TestButton` with `&str` "bla"
pub trait ConfigureInteraction {
    fn configure(configuration: &str) -> Self;
}
