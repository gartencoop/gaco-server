use crate::RenderedInteraction;
use serde::{Deserialize, Serialize};
use serde_json::Value;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct SelectionOption {
    #[serde(default)]
    value: Value,
    #[serde(default)]
    text: String,
}

impl SelectionOption {
    pub fn new<V: Into<Value>>(value: V, text: String) -> Self {
        Self {
            value: value.into(),
            text,
        }
    }

    pub fn text_only(text: String) -> Self {
        Self {
            value: Value::String(text.clone()),
            text,
        }
    }

    pub fn value(&self) -> &Value {
        &self.value
    }

    pub fn text(&self) -> &str {
        &self.text
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RenderedSelection(RenderedInteraction);

impl RenderedSelection {
    #[allow(dead_code)]
    pub fn new<V1: Into<Value>, V2: Into<Value>>(
        value: V1,
        _interaction_name: String,
        options: Vec<SelectionOption>,
        extra_fields: V2,
    ) -> Self {
        #[derive(Serialize)]
        struct ExtraFields {
            pub value: Value,

            options: Vec<SelectionOption>,

            #[serde(flatten)]
            extra_fields: Value,
        }

        let extra_fields = serde_json::to_value(ExtraFields {
            value: value.into(),
            options,
            extra_fields: extra_fields.into(),
        })
        .unwrap_or_default();

        Self(RenderedInteraction::new(
            extra_fields,
            vec!["enter".to_owned()],
        ))
    }
}

#[cfg(feature = "backend")]
/// Trait for defining a selection input interaction for a screen and session (can both be generic)
///
/// * `render`: Return the rendered instance of a selection input. See [`RenderedSelection`].
/// * `action`: Run action when input changed. No need to run [`crate::update_screen`] as this always triggers a screen event.
#[async_trait::async_trait]
pub trait Selection<SCREEN: crate::Screen<SESSION>, SESSION: crate::Session>: Sync + Send {
    /// Return the rendered instance of a selection input. See [`RenderedSelection`].
    #[allow(unused_variables)]
    fn render(
        &self,
        interaction_name: String,
        screen: &SCREEN,
        session: &SESSION,
    ) -> RenderedSelection {
        RenderedSelection::new((), interaction_name, vec![], ())
    }

    /// Run action when input changed. No need to run [`crate::update_screen`] as this always triggers a screen event.
    #[allow(unused_variables)]
    async fn action(
        &self,
        session: &mut SESSION,
        payload: &crate::ActionPayload,
    ) -> Vec<crate::Event> {
        vec![]
    }
}

#[cfg(feature = "backend")]
pub struct SelectionInteraction<'a, SCREEN: crate::Screen<SESSION>, SESSION: crate::Session> {
    inner: &'a dyn Selection<SCREEN, SESSION>,
}

#[cfg(feature = "backend")]
impl<'a, SCREEN: crate::Screen<SESSION>, SESSION: crate::Session>
    SelectionInteraction<'a, SCREEN, SESSION>
{
    pub fn new(inner: &'a dyn Selection<SCREEN, SESSION>) -> Self {
        Self { inner }
    }
}

#[cfg(feature = "backend")]
#[async_trait::async_trait]
impl<SCREEN: crate::Screen<SESSION>, SESSION: crate::Session>
    crate::Interaction<SCREEN, SESSION> for SelectionInteraction<'_, SCREEN, SESSION>
{
    fn render(
        &self,
        interaction_name: String,
        screen: &SCREEN,
        session: &SESSION,
    ) -> crate::RenderedInteraction {
        self.inner.render(interaction_name, screen, session).0
    }

    async fn action(
        &self,
        session: &mut SESSION,
        payload: &crate::ActionPayload,
    ) -> Vec<crate::Event> {
        self.inner.action(session, payload).await
    }

    fn actions(&self) -> Vec<crate::Action> {
        vec![crate::Action::new("enter", None)]
    }
}
