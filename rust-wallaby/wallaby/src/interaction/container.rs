use crate::RenderedInteraction;
use serde::{Deserialize, Serialize};
use serde_json::Value;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RenderedContainer(RenderedInteraction);

impl RenderedContainer {
    #[allow(dead_code)]
    pub fn new<V: Into<Value>>(disabled: bool, extra_fields: V) -> Self {
        #[derive(Serialize)]
        struct ExtraFields {
            #[serde(default)]
            pub disabled: bool,

            #[serde(flatten)]
            extra_fields: Value,
        }

        let extra_fields = serde_json::to_value(ExtraFields {
            disabled,
            extra_fields: extra_fields.into(),
        })
        .unwrap_or_default();

        Self(RenderedInteraction::new(extra_fields, vec![]))
    }
}

#[cfg(feature = "backend")]
/// Trait for defining a container interaction for a screen and session (can both be generic)
///
/// * `render`: Return the rendered instance of a container. See [`RenderedContainer`].
#[async_trait::async_trait]
pub trait Container<SCREEN: crate::Screen<SESSION>, SESSION: crate::Session>: Sync + Send {
    /// Return the rendered instance of a container. See [`RenderedContainer`].
    #[allow(unused_variables)]
    fn render(
        &self,
        interaction_name: String,
        screen: &SCREEN,
        session: &SESSION,
    ) -> RenderedContainer {
        RenderedContainer::new(false, ())
    }
}

#[cfg(feature = "backend")]
pub struct ContainerInteraction<'a, SCREEN: crate::Screen<SESSION>, SESSION: crate::Session> {
    inner: &'a dyn Container<SCREEN, SESSION>,
}

#[cfg(feature = "backend")]
impl<'a, SCREEN: crate::Screen<SESSION>, SESSION: crate::Session>
    ContainerInteraction<'a, SCREEN, SESSION>
{
    pub fn new(inner: &'a dyn Container<SCREEN, SESSION>) -> Self {
        Self { inner }
    }
}

#[cfg(feature = "backend")]
#[async_trait::async_trait]
impl<SCREEN: crate::Screen<SESSION>, SESSION: crate::Session>
    crate::Interaction<SCREEN, SESSION> for ContainerInteraction<'_, SCREEN, SESSION>
{
    fn render(
        &self,
        interaction_name: String,
        screen: &SCREEN,
        session: &SESSION,
    ) -> crate::RenderedInteraction {
        self.inner.render(interaction_name, screen, session).0
    }
}
