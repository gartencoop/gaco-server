use crate::{Action, Interaction};

#[cfg(feature = "backend")]
pub struct GenericInteraction<'a, SCREEN: crate::Screen<SESSION>, SESSION: crate::Session> {
    inner: &'a dyn Interaction<SCREEN, SESSION>,
}

#[cfg(feature = "backend")]
impl<'a, SCREEN: crate::Screen<SESSION>, SESSION: crate::Session>
    GenericInteraction<'a, SCREEN, SESSION>
{
    pub fn new(inner: &'a dyn Interaction<SCREEN, SESSION>) -> Self {
        Self { inner }
    }
}

#[cfg(feature = "backend")]
#[async_trait::async_trait]
impl<SCREEN: crate::Screen<SESSION>, SESSION: crate::Session>
    crate::Interaction<SCREEN, SESSION> for GenericInteraction<'_, SCREEN, SESSION>
{
    fn render(
        &self,
        interaction_name: String,
        screen: &SCREEN,
        session: &SESSION,
    ) -> crate::RenderedInteraction {
        self.inner.render(interaction_name, screen, session)
    }

    async fn action(
        &self,
        session: &mut SESSION,
        payload: &crate::ActionPayload,
    ) -> Vec<crate::Event> {
        self.inner.action(session, payload).await
    }

    fn actions(&self) -> Vec<Action> {
        self.inner.actions()
    }
}
