#[cfg(feature = "backend")]
pub mod static_output;

use crate::RenderedInteraction;
use serde::{Deserialize, Serialize};
use serde_json::Value;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RenderedOutput(RenderedInteraction);

impl RenderedOutput {
    #[allow(dead_code)]
    pub fn new<V1: Into<Value>, V2: Into<Value>>(value: V1, extra_fields: V2) -> Self {
        #[derive(Serialize)]
        struct ExtraFields {
            #[serde(skip_serializing_if = "Value::is_null", default)]
            pub value: Value,

            #[serde(flatten)]
            extra_fields: Value,
        }

        let extra_fields = serde_json::to_value(ExtraFields {
            value: value.into(),
            extra_fields: extra_fields.into(),
        })
        .unwrap_or_default();

        Self(RenderedInteraction::new(extra_fields, vec![]))
    }
}

#[cfg(feature = "backend")]
/// Trait for defining a output interaction for a screen and session (can both be generic)
///
/// * `render`: Return the rendered instance of an output. See [`RenderedOutput`].
#[async_trait::async_trait]
pub trait Output<SCREEN: crate::Screen<SESSION>, SESSION: crate::Session>: Sync + Send {
    /// Return the rendered instance of an output. See [`RenderedOutput`].
    #[allow(unused_variables)]
    fn render(
        &self,
        interaction_name: String,
        screen: &SCREEN,
        session: &SESSION,
    ) -> RenderedOutput {
        RenderedOutput::new((), ())
    }
}

#[cfg(feature = "backend")]
pub struct OutputInteraction<'a, SCREEN: crate::Screen<SESSION>, SESSION: crate::Session> {
    inner: &'a dyn Output<SCREEN, SESSION>,
}

#[cfg(feature = "backend")]
impl<'a, SCREEN: crate::Screen<SESSION>, SESSION: crate::Session>
    OutputInteraction<'a, SCREEN, SESSION>
{
    pub fn new(inner: &'a dyn Output<SCREEN, SESSION>) -> Self {
        Self { inner }
    }
}

#[cfg(feature = "backend")]
#[async_trait::async_trait]
impl<SCREEN: crate::Screen<SESSION>, SESSION: crate::Session>
    crate::Interaction<SCREEN, SESSION> for OutputInteraction<'_, SCREEN, SESSION>
{
    fn render(
        &self,
        interaction_name: String,
        screen: &SCREEN,
        session: &SESSION,
    ) -> crate::RenderedInteraction {
        self.inner.render(interaction_name, screen, session).0
    }
}
