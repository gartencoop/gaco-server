use crate::{ConfigureInteraction, Output, RenderedOutput, Screen, Session};
use serde_json::Value;

/// Output interaction with value set by `configuration` attribute
///
/// ```
/// #[interaction(type = "output", configuration = "Hello world!")]
/// hello_world: StaticOutput,
/// ```
pub struct StaticOutput {
    value: Value,
}

impl StaticOutput {
    pub fn value(&self) -> &Value {
        &self.value
    }
}

impl<SCREEN: Screen<SESSION>, SESSION: Session> Output<SCREEN, SESSION> for StaticOutput {
    fn render(
        &self,
        _interaction_name: String,
        _screen: &SCREEN,
        _session: &SESSION,
    ) -> RenderedOutput {
        RenderedOutput::new(self.value.clone(), ())
    }
}

impl ConfigureInteraction for StaticOutput {
    fn configure(configuration: &str) -> Self {
        Self {
            value: configuration.into(),
        }
    }
}
