use crate::{Action, RenderedInteraction};
use serde::{Deserialize, Serialize};
use serde_json::Value;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RenderedInput(RenderedInteraction);

impl RenderedInput {
    #[allow(dead_code)]
    pub fn new<V1: Into<Value>, V2: Into<Value>>(
        value: V1,
        _interaction_name: String,
        enteraction: bool,
        liveaction: bool,
        extra_fields: V2,
    ) -> Self {
        #[derive(Serialize)]
        struct ExtraFields {
            #[serde(skip_serializing_if = "Value::is_null", default)]
            pub value: Value,

            #[serde(flatten)]
            extra_fields: Value,
        }

        let extra_fields = serde_json::to_value(ExtraFields {
            value: value.into(),
            extra_fields: extra_fields.into(),
        })
        .unwrap_or_default();

        let mut events = Vec::with_capacity(2);
        if enteraction {
            events.push("enter".to_string());
        }
        if liveaction {
            events.push("live".to_string());
        }

        Self(RenderedInteraction::new(extra_fields, events))
    }
}

#[cfg(feature = "backend")]
/// Trait for defining a input interaction for a screen and session (can both be generic)
///
/// * `render`: Return the rendered instance of an input. See [`RenderedInput`].
/// * `action`: Run action when input changed. No need to run [`crate::update_screen`] as this always triggers a screen event.
/// * `actions`: List of screen actions. Can be overwritten to set custom fields. Defaults to an `"enter"` action without custom fields.
#[async_trait::async_trait]
pub trait Input<SCREEN: crate::Screen<SESSION>, SESSION: crate::Session>: Sync + Send {
    /// Return the rendered instance of an input. See [`RenderedInput`].
    #[allow(unused_variables)]
    fn render(
        &self,
        interaction_name: String,
        screen: &SCREEN,
        session: &SESSION,
    ) -> RenderedInput {
        RenderedInput::new((), interaction_name, true, false, ())
    }

    /// Run action when input changed. No need to run [`crate::update_screen`] as this always triggers a screen event.
    #[allow(unused_variables)]
    async fn action(
        &self,
        session: &mut SESSION,
        payload: &crate::ActionPayload,
    ) -> Vec<crate::Event> {
        vec![]
    }

    /// List of screen actions. Can be overwritten to set custom fields. Defaults to an `"enter"` action without custom fields.
    fn actions(&self) -> Vec<crate::Action> {
        vec![crate::Action::new("enter", None)]
    }
}

#[cfg(feature = "backend")]
pub struct InputInteraction<'a, SCREEN: crate::Screen<SESSION>, SESSION: crate::Session> {
    inner: &'a dyn Input<SCREEN, SESSION>,
}

#[cfg(feature = "backend")]
impl<'a, SCREEN: crate::Screen<SESSION>, SESSION: crate::Session>
    InputInteraction<'a, SCREEN, SESSION>
{
    pub fn new(inner: &'a dyn Input<SCREEN, SESSION>) -> Self {
        Self { inner }
    }
}

#[cfg(feature = "backend")]
#[async_trait::async_trait]
impl<SCREEN: crate::Screen<SESSION>, SESSION: crate::Session> crate::Interaction<SCREEN, SESSION>
    for InputInteraction<'_, SCREEN, SESSION>
{
    fn render(
        &self,
        interaction_name: String,
        screen: &SCREEN,
        session: &SESSION,
    ) -> crate::RenderedInteraction {
        self.inner.render(interaction_name, screen, session).0
    }

    async fn action(
        &self,
        session: &mut SESSION,
        payload: &crate::ActionPayload,
    ) -> Vec<crate::Event> {
        self.inner.action(session, payload).await
    }

    fn actions(&self) -> Vec<Action> {
        self.inner.actions()
    }
}
