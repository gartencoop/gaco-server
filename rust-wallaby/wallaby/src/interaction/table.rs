use crate::RenderedInteraction;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RenderedTable;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RenderedTableData {
    rows: Vec<RenderedTableDataRow>,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
#[serde(default)]
pub struct RenderedTableDataRow {
    #[serde(rename = "interaction")]
    interactions: HashMap<String, RenderedInteraction>,
}

impl RenderedTable {
    #[allow(dead_code)]
    pub fn create(rows: Vec<HashMap<String, RenderedInteraction>>) -> RenderedInteraction {
        #[derive(Serialize)]
        struct ExtraFields {
            data: RenderedTableData,
            #[serde(skip_serializing_if = "Option::is_none")]
            total_rows: Option<usize>,
        }

        let extra_fields = serde_json::to_value(ExtraFields {
            total_rows: Some(rows.len()),
            data: RenderedTableData {
                rows: rows
                    .into_iter()
                    .map(|row| RenderedTableDataRow { interactions: row })
                    .collect(),
            },
        })
        .unwrap_or_default();

        RenderedInteraction::new(extra_fields, vec![])
    }
}
