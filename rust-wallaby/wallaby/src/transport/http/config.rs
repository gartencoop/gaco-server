use std::path::PathBuf;

/// Config of http transport activated by `transport_http` feature
pub struct WallabyServerConfig {
    /// Port of webserver
    ///
    /// Defaults to `1111`
    pub port: u16,

    /// Assets folder
    ///
    /// Defaults to `public`
    pub assets: PathBuf,

    /// Default asset which is returned on all otherwise unknown paths
    ///
    /// Defaults to `index.html`
    pub default_asset: PathBuf,

    #[cfg(feature = "bridge")]
    /// URL of bridged typescript wallaby websocket
    ///
    /// Defaults to `ws://localhost:2000`
    pub bridge_url: String,
}

impl Default for WallabyServerConfig {
    fn default() -> Self {
        let assets = PathBuf::from("public");
        let mut default_asset = assets.clone();
        default_asset.push("index.html");

        Self {
            port: 1111,
            assets,
            default_asset,
            #[cfg(feature = "bridge")]
            bridge_url: "ws://localhost:2000".to_owned(),
        }
    }
}
