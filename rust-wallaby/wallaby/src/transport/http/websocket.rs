use crate::{
    handler::client_event::ClientEvent, ServerEvent, ServerEvents, Session, SessionStore, Storage,
};
use futures::{lock::Mutex, SinkExt, StreamExt};
use log::{debug, error, trace};
use once_cell::sync::Lazy;
use serde::Serialize;
use std::{collections::HashMap, sync::Arc};
use tokio::sync::mpsc::{channel, Sender};
use warp::ws::{Message, WebSocket};

type ConnectionStore = HashMap<String, Sender<Message>>;
static CONNECTIONS: Lazy<Arc<Mutex<ConnectionStore>>> =
    Lazy::new(|| Arc::new(Mutex::new(HashMap::new())));

pub async fn connection<SESSION: Session, SESSIONSTORE: SessionStore<SESSION>>(
    ws: WebSocket,
    storage: Storage<SESSION, SESSIONSTORE>,
) {
    debug!("Websocket client connected");

    let (tx, mut rx) = channel(10);
    let (mut ws_tx, mut ws_rx) = ws.split();
    tokio::task::spawn(async move {
        while let Some(payload) = rx.recv().await {
            if let Err(error) = ws_tx.send(payload).await {
                error!("WS_TX died: {:?}", error);
                break;
            }
        }
    });

    let mut session_id = None;

    while let Some(result) = ws_rx.next().await {
        let mut storage = storage.clone();
        let msg = match result {
            Ok(msg) => msg,
            Err(e) => {
                error!("websocket error: {}", e);
                break;
            }
        };

        if let Ok(json) = msg.to_str() {
            match serde_json::from_str::<ClientEvent>(json) {
                Err(error) => error!("Could not parse client event \"{}\": {:?}", json, error),
                Ok(event) => match event.name() {
                    "register" => {
                        session_id = Some(event.session_id().to_owned());
                        debug!("Websocket client registered: {:?}", event.session_id());

                        {
                            let mut connections = CONNECTIONS.lock().await;
                            connections.insert(event.session_id().to_owned(), tx.clone());
                        }

                        send_message(
                            event.session_id(),
                            serde_json::to_string(&WebsocketServerEvent::Connected)
                                .expect("Could not stringify connected event"),
                        )
                        .await;
                    }
                    _ => event.handle(&mut storage).await,
                },
            }
        }
    }

    debug!("Websocket client disconnected: {:?}", session_id);
    if let Some(session_id) = session_id {
        let mut connections = CONNECTIONS.lock().await;
        connections.remove(&session_id);
    }
}

/// Send client events
///
/// Uses cached websocket connections to clients (identified by session id)
pub async fn send_events(session_id: &str, events: ServerEvents) {
    for event in events {
        let json = match event {
            ServerEvent::Custom(custom) => serde_json::to_string(&custom),
            event => serde_json::to_string(&event),
        };
        match json {
            Err(err) => log::error!("Could not serialize event: {err:?}"),
            Ok(json) => send_message(session_id, json).await,
        }
    }
}

pub async fn send_message(session_id: &str, json: String) {
    let mut connections = CONNECTIONS.lock().await;
    if let Some(tx) = connections.get(session_id) {
        trace!("Sending message to {}: {}", session_id, json);
        if let Err(error) = tx.send(Message::text(json)).await {
            error!("Error sending message to {}: {:?}", session_id, error);
            connections.remove(session_id);
        }
    }
}

/// Check if a session is currently connected.
pub async fn session_connected(session_id: &str) -> bool {
    CONNECTIONS.lock().await.get(session_id).is_some()
}

#[derive(Serialize, Debug)]
#[serde(tag = "name", content = "payload", rename_all = "camelCase")]
pub enum WebsocketServerEvent {
    Connected,
}
