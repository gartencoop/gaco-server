use super::WallabyServerConfig;
use crate::{
    handler::Handler, storage::screen_store::ScreenGenerator, Session, SessionStore, Storage,
};
use warp::{filters::BoxedFilter, Filter, Reply};

/// Http transport, activated by `transport_http` feature
pub struct WallabyServer<SESSION: Session, SESSIONSTORE: SessionStore<SESSION>> {
    config: WallabyServerConfig,
    storage: Storage<SESSION, SESSIONSTORE>,
}

impl<SESSION: Session, SESSIONSTORE: SessionStore<SESSION>> WallabyServer<SESSION, SESSIONSTORE> {
    pub fn new(config: WallabyServerConfig, session_store: SESSIONSTORE) -> Self {
        Self {
            config,
            storage: Storage::new(session_store),
        }
    }

    /// Add screen to screen store.
    ///
    /// You must add the generator of the screen which is created by the [`crate::derive::Screen`] proc-macro:
    /// ```
    /// wallaby.add_screen(screen::Home::generator()).await;
    /// ```
    pub async fn add_screen(&mut self, generator: ScreenGenerator<SESSION>) {
        self.storage.screen_store_mut().add(generator).await;
    }

    /// Add handler to handler store.
    ///
    /// ```
    /// wallaby.add_handler(screen::Home::generator()).await;
    /// ```
    pub async fn add_handler<HANDLER: Handler<SESSION, SESSIONSTORE>>(&mut self) {
        self.storage.handler_store_mut().add::<HANDLER>().await;
    }

    /// start webserver and housekeeping tasks with extra warp filters
    #[cfg(feature = "extra_filter")]
    pub async fn start(&self, extra_filter: BoxedFilter<(impl Reply + 'static,)>) {
        crate::start(self.storage.clone()).await;

        #[cfg(feature = "bridge")]
        crate::bridge::start(&self.config.bridge_url).await;

        warp::serve(extra_filter.or(self.setup_filter()))
            .run(([0, 0, 0, 0], self.config.port))
            .await;
    }

    /// start webserver and housekeeping tasks
    #[cfg(not(feature = "extra_filter"))]
    pub async fn start(&self) {
        crate::start(self.storage.clone()).await;

        #[cfg(feature = "bridge")]
        crate::bridge::start(&self.config.bridge_url).await;

        warp::serve(self.setup_filter())
            .run(([0, 0, 0, 0], self.config.port))
            .await;
    }

    fn setup_filter(&self) -> BoxedFilter<(impl Reply,)> {
        warp::path("ws")
            .and(warp::ws())
            .and(self.storage_filter())
            .map(
                |ws: warp::ws::Ws, storage: Storage<SESSION, SESSIONSTORE>| {
                    ws.on_upgrade(|ws| super::websocket::connection(ws, storage))
                },
            )
            .or(warp::fs::dir(self.config.assets.clone()))
            .or(warp::fs::file(self.config.default_asset.clone()))
            .boxed()
    }

    pub fn storage_filter(
        &self,
    ) -> impl Filter<Extract = (Storage<SESSION, SESSIONSTORE>,), Error = std::convert::Infallible> + Clone
    {
        let storage = self.storage.clone();
        warp::any().map(move || storage.clone())
    }
}
