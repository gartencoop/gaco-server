use super::{IN_TX, OUT_RX};
use crate::handler::client_event::ClientEvent;
use jni::{objects::JClass, sys::jstring, JNIEnv};
use log::error;
use std::path::PathBuf;
use std::str::FromStr;

#[allow(non_snake_case)]
#[no_mangle]
pub extern "C" fn Java_de_freshx_wallaby_android_1lib_module_RustBackend_event(
    env: JNIEnv<'_>,
    _: JClass<'_>,
) -> jstring {
    let payload = OUT_RX
        .wait()
        .try_lock()
        .ok()
        .map(|mut out_rx| out_rx.blocking_recv().expect("OUT_RX is poisoned"))
        .unwrap_or_default();

    env.new_string(payload)
        .expect("Couldn't create java string!")
        .into_raw()
}

#[cfg(target_os = "android")]
#[allow(non_snake_case)]
#[no_mangle]
pub extern "C" fn Java_de_freshx_wallaby_android_1lib_module_RustBackend_send_1event(
    mut env: JNIEnv<'_>,
    _: JClass<'_>,
    payload: jni::objects::JString<'_>,
) {
    let payload = env.get_string(&payload).expect("invalid payload string");
    let payload = payload.to_str().expect("Could not convert JString to str");
    match serde_json::from_str::<ClientEvent>(payload)
        .map_err(|err| error!("Could not parse client event \"{}\": {:?}", payload, err))
    {
        Err(error) => error!("Could not parse client event \"{}\": {:?}", payload, error),
        Ok(event) => {
            IN_TX.wait().blocking_send(event).ok();
        }
    }
}
