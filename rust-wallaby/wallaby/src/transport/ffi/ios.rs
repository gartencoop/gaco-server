use super::{IN_TX, OUT_RX};
use crate::handler::client_event::ClientEvent;
use log::error;
use std::{ffi::CString, path::PathBuf};
use std::{os::raw::c_char, str::FromStr};

/// Event poll method
///
/// Should be called in a loop
///
/// Returns empty strings if called before `start()`
#[no_mangle]
pub extern "C" fn event() -> *mut c_char {
    let payload = OUT_RX
        .wait()
        .try_lock()
        .ok()
        .map(|mut out_rx| out_rx.blocking_recv().expect("OUT_RX is poisoned"))
        .unwrap_or_default();

    CString::new(payload)
        .expect("Could not create empty CString")
        .into_raw()
}

/// Send client event
/// # Safety
///
/// Destructs pointers
#[no_mangle]
pub unsafe extern "C" fn send_event(payload: *const u8, length: u32) {
    assert!(!payload.is_null(), "Null pointer in send_event()");

    let mut vec = vec![0u8; length as usize];
    std::ptr::copy_nonoverlapping(payload, vec.as_mut_ptr(), length as usize);
    let payload = match String::from_utf8(vec) {
        Ok(payload) => payload,
        Err(err) => {
            error!(
                "Could not create CStr from pointer for send_event payload: {:?}",
                err
            );
            return;
        }
    };

    match serde_json::from_str::<ClientEvent>(&payload) {
        Err(error) => error!("Could not parse client event \"{}\": {:?}", payload, error),
        Ok(event) => {
            IN_TX.wait().blocking_send(event).ok();
        }
    }
}

/// # Safety
///
/// Destroys a pointer without any checks.
#[no_mangle]
pub unsafe extern "C" fn free_payload(s: *mut c_char) {
    if s.is_null() {
        return;
    }
    let _ = CString::from_raw(s);
}
