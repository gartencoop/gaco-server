use crate::{
    handler::{client_event::ClientEvent, Handler},
    storage::screen_store::ScreenGenerator,
    ServerEvent, ServerEvents, Session, SessionStore, Storage,
};
use once_cell::sync::OnceCell;
use std::path::PathBuf;
use tokio::sync::{
    mpsc::{channel, Receiver, Sender},
    Mutex,
};

static IN_TX: OnceCell<Sender<ClientEvent>> = OnceCell::new();
static OUT_TX: OnceCell<Sender<ServerEvent>> = OnceCell::new();
static OUT_RX: OnceCell<Mutex<Receiver<ServerEvent>>> = OnceCell::new();

/// Send server events
///
/// Uses a bounded channel which should be polled with [`event()`]
#[allow(clippy::ptr_arg)]
pub async fn send_events(_session_id: &str, events: ServerEvents) {
    for event in events {
        OUT_TX
            .get()
            .expect("OUT_TX is not initialized")
            .send(event)
            .await
            .ok();
    }
}

/// Taury main entry point, activated by `transport_tauri` feature
pub struct WallabyTauri<SESSION: Session, SESSIONSTORE: SessionStore<SESSION>> {
    storage: Storage<SESSION, SESSIONSTORE>,
}

impl<SESSION: Session, SESSIONSTORE: SessionStore<SESSION>> WallabyTauri<SESSION, SESSIONSTORE> {
    pub fn new(session_store: SESSIONSTORE) -> Self {
        Self {
            storage: Storage::new(session_store),
        }
    }

    /// Add screen to screen store.
    ///
    /// You must add the generator of the screen which is created by the [`crate::derive::Screen`] proc-macro:
    /// ```
    /// wallaby.add_screen(screen::Home::generator()).await;
    /// ```
    pub async fn add_screen(&mut self, generator: ScreenGenerator<SESSION>) {
        self.storage.screen_store_mut().add(generator).await;
    }

    /// Add handler to handler store.
    ///
    /// ```
    /// wallaby.add_handler(screen::Home::generator()).await;
    /// ```
    pub async fn add_handler<HANDLER: Handler<SESSION, SESSIONSTORE>>(&mut self) {
        self.storage.handler_store_mut().add::<HANDLER>().await;
    }

    /// start event loop
    pub async fn start(self) {
        crate::start(self.storage.clone()).await;

        let (in_tx, mut in_rx) = channel(10);
        IN_TX.set(in_tx).expect("IN_TX was already set");
        let (out_tx, out_rx) = channel(10);
        OUT_TX.set(out_tx).expect("OUT_TX was already set");
        OUT_RX
            .set(Mutex::new(out_rx))
            .expect("OUT_RX was already set");

        tokio::task::spawn(async move {
            while let Some(event) = in_rx.recv().await {
                event.handle(&mut self.storage.clone()).await;
            }
        });
    }
}

/// Check if a session is currently connected.
/// Returns always true as tauri is always connected.
pub async fn session_connected(_session_id: &str) -> bool {
    true
}

/// Event poll method
///
/// Should be called in a loop
#[tauri::command]
pub async fn event() -> ServerEvent {
    OUT_RX
        .wait()
        .lock()
        .await
        .recv()
        .await
        .expect("Server Event channel was closed")
}

/// Send client event
#[tauri::command]
pub async fn send_event(event: ClientEvent) {
    IN_TX.wait().send(event).await.ok();
}

#[macro_export]
macro_rules! tauri_invoke_handler {
    ( ) => {{
        tauri::generate_handler![wallaby::event, wallaby::send_event]
    }};
}
