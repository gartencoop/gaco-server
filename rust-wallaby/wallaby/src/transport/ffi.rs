#[cfg(target_os = "android")]
mod android;

#[cfg(target_os = "android")]
pub use android::*;
#[cfg(target_os = "ios")]
mod ios;

#[cfg(target_os = "ios")]
pub use ios::*;

use crate::{
    handler::{client_event::ClientEvent, Handler},
    storage::screen_store::ScreenGenerator,
    ServerEvent, ServerEvents, Session, SessionStore, Storage,
};
use once_cell::sync::OnceCell;
use std::path::PathBuf;
use tokio::sync::{
    mpsc::{channel, Receiver, Sender},
    Mutex,
};

static IN_TX: OnceCell<Sender<ClientEvent>> = OnceCell::new();
static IN_RX: OnceCell<Mutex<Receiver<ClientEvent>>> = OnceCell::new();
static OUT_TX: OnceCell<Sender<String>> = OnceCell::new();
static OUT_RX: OnceCell<Mutex<Receiver<String>>> = OnceCell::new();

/// Send server events
///
/// Uses a bounded channel which should be polled with [`event()`]
#[allow(clippy::ptr_arg)]
pub async fn send_events(_session_id: &str, events: ServerEvents) {
    for event in events {
        let json = match event {
            ServerEvent::Custom(custom) => serde_json::to_string(&custom),
            event => serde_json::to_string(&event),
        };
        if let Ok(json) = json {
            OUT_TX
                .get()
                .expect("OUT_TX is not initialized")
                .send(json)
                .await
                .ok();
        }
    }
}

/// FFI main entry point, activated by `transport_ffi` feature
///
/// Mandatory call order of ffi methods:
/// * [`WallabyFfi.init()`] called from own ffi method exposed in product backend code (should be called `init`)
/// * [`WallabyFfi.start()`] called from own ffi method exposed in product backend code (should be called `start`)
/// * [`event()`] in a loop
/// * [`send_event()`] when needed, but after `start()`
///
/// ffi methods which must be implemented and exposed in product backend code:
/// * `start()` which calls `WallabyFfi.start()`]
/// * `custom_event()` which handles custom events (if any)
pub struct WallabyFfi<SESSION: Session, SESSIONSTORE: SessionStore<SESSION>> {
    storage: Storage<SESSION, SESSIONSTORE>,
}

impl<SESSION: Session, SESSIONSTORE: SessionStore<SESSION>> WallabyFfi<SESSION, SESSIONSTORE> {
    pub fn new(session_store: SESSIONSTORE) -> Self {
        Self {
            storage: Storage::new(session_store),
        }
    }

    /// Add screen to screen store.
    ///
    /// You must add the generator of the screen which is created by the [`crate::derive::Screen`] proc-macro:
    /// ```
    /// wallaby.add_screen(screen::Home::generator()).await;
    /// ```
    pub async fn add_screen(&mut self, generator: ScreenGenerator<SESSION>) {
        self.storage.screen_store_mut().add(generator).await;
    }

    /// Add handler to handler store.
    ///
    /// ```
    /// wallaby.add_handler(screen::Home::generator()).await;
    /// ```
    pub async fn add_handler<HANDLER: Handler<SESSION, SESSIONSTORE>>(&mut self) {
        self.storage.handler_store_mut().add::<HANDLER>().await;
    }

    /// initialize channels and start housekeeping tasks
    pub async fn init(&mut self) {
        crate::start(self.storage.clone()).await;

        let (in_tx, in_rx) = channel(10);
        IN_TX.set(in_tx).expect("IN_TX was already set");
        IN_RX.set(Mutex::new(in_rx)).expect("IN_RX was already set");
        let (out_tx, out_rx) = channel(10);
        OUT_TX.set(out_tx).expect("OUT_TX was already set");
        OUT_RX
            .set(Mutex::new(out_rx))
            .expect("OUT_RX was already set");
    }

    /// start event listener
    ///
    /// Blocks
    pub async fn start(self) {
        while let Some(event) = IN_RX
            .get()
            .expect("WallabyFfi.init must be called before WallabyFfi.start!")
            .lock()
            .await
            .recv()
            .await
        {
            event.handle(&mut self.storage.clone()).await;
        }
    }
}

/// Check if a session is currently connected.
/// Returns always true as ffi is always connected.
pub async fn session_connected(_session_id: &str) -> bool {
    true
}
