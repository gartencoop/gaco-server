pub mod button;
pub mod configure;
pub mod container;
pub mod generic;
pub mod input;
pub mod output;
pub mod selection;
pub mod table;

use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use std::{collections::HashMap, fmt::Debug};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RenderedInteraction {
    #[serde(flatten)]
    fields: Value,

    #[serde(rename = "_events")]
    events: HashMap<String, bool>,
}

impl RenderedInteraction {
    pub fn new<V: Into<Value>>(fields: V, events: Vec<String>) -> Self {
        Self {
            fields: fields.into(),
            events: events.into_iter().map(|event| (event, true)).collect(),
        }
    }
}

pub struct Action {
    pub action_type: String,
    pub extra_fields: Value,
}

impl Action {
    /// `action_type` is the type like "click". `extra_fields` will end up flattened in the action dictionary of the rendered screen.
    pub fn new(action_type: &str, extra_fields: Option<Value>) -> Self {
        Self {
            action_type: action_type.to_string(),
            extra_fields: extra_fields.unwrap_or_else(|| json!({})),
        }
    }
}

#[cfg(feature = "backend")]
/// Interaction methods
///
/// Can be overwritten with a custom trait for own interaction types
#[async_trait::async_trait]
pub trait Interaction<SCREEN: crate::Screen<SESSION>, SESSION: crate::Session>:
    Sync + Send
{
    #[allow(unused_variables)]
    fn render(
        &self,
        interaction_name: String,
        screen: &SCREEN,
        session: &SESSION,
    ) -> RenderedInteraction {
        RenderedInteraction::new(json!({}), self.action_types())
    }

    #[allow(unused_variables)]
    async fn action(
        &self,
        session: &mut SESSION,
        payload: &crate::ActionPayload,
    ) -> Vec<crate::Event> {
        vec![]
    }

    fn actions(&self) -> Vec<Action> {
        vec![]
    }

    /// Converts self.actions to a `Vec<String>`
    fn action_types(&self) -> Vec<String> {
        self.actions()
            .into_iter()
            .map(|action| action.action_type)
            .collect()
    }
}
