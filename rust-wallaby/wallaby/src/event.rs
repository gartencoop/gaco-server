pub mod alert;
pub mod custom;
pub mod message;
pub mod play_sound;
pub mod screen;

use crate::{Error, MessageLevel};
use alert::Alert;
use custom::CustomEvent;
use message::Message;
use play_sound::PlaySound;
use screen::Screen;
use serde::{Deserialize, Serialize};

/// A mix of internal([`Event::ExcludeInteraction`]) and client events
#[derive(Clone, Debug, Serialize)]
pub enum Event {
    /// Exclude interaction from screen.
    ///
    /// A panic is thrown if this event is used in a context without a screen like in [`crate::send_events()`]
    ExcludeInteraction(String),
    /// Exclude screen event.
    ///
    /// A panic is thrown if this event is used in a context without a screen like in [`crate::send_events()`]
    ExcludeScreen,
    /// Send a message which is displayed as a toast or similar on the client
    Message(Message),
    /// Send an alert prompt with a list of buttons which can trigger actions
    Alert(Alert),
    /// Play a sound on the client.
    ///
    /// Supported Sounds:
    /// * Ok
    /// * Error
    PlaySound(PlaySound),
    /// Send a custom value to the client, needs a custom receiver on the client side.
    Custom(CustomEvent),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase", tag = "name", content = "payload")]
pub enum ServerEvent {
    Screen(Screen),
    Message(Message),
    Alert(Alert),
    PlaySound(PlaySound),
    Custom(CustomEvent),
    End,
}

pub type ServerEvents = Vec<ServerEvent>;

impl From<Error> for ServerEvents {
    fn from(error: Error) -> Self {
        let message = Message::new(
            MessageLevel::Error,
            None,
            match error {
                Error::InteractionNotFound(interaction) => {
                    format!("Could not find interaction \"{}\"", interaction)
                }
                Error::ScreenNotFound(screen) => format!("Could not find screen \"{}\"", screen),
            },
        );

        vec![ServerEvent::Message(message)]
    }
}
