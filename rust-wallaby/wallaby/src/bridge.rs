use crate::{
    event::{alert::Alert, custom::CustomEvent, message, play_sound::PlaySound},
    Event, RenderedScreen,
};
use futures::{Future, SinkExt, StreamExt};
use log::{debug, error, trace};
use once_cell::sync::Lazy;
use rand::{distributions::Alphanumeric, thread_rng, Rng};
use serde::{Deserialize, Serialize};
use serde_json::{Map, Value};
use std::{
    collections::HashMap,
    pin::Pin,
    sync::{Arc, Mutex, RwLock},
    task::{Context, Poll, Waker},
    time::Duration,
};
use tokio::sync::mpsc::{channel, Sender};
use tokio_tungstenite::tungstenite::Message;

static MESSAGES: Lazy<Arc<Mutex<HashMap<String, Arc<Mutex<BridgeMessage>>>>>> =
    Lazy::new(|| Arc::new(Mutex::new(HashMap::new())));
static SENDER: Lazy<RwLock<Option<Sender<Message>>>> = Lazy::new(|| RwLock::new(None));

pub async fn start(url: &str) {
    let url = url::Url::parse(url).expect("Could not parse bridge url");

    tokio::task::spawn(async move {
        loop {
            {
                MESSAGES
                    .lock()
                    .expect("MESSAGES was poisoned")
                    .iter_mut()
                    .for_each(|(_, message)| {
                        message
                            .lock()
                            .expect("message was poisoned")
                            .set_response(Response {
                                events: vec![BridgeEvent::Message(message::Message::error(
                                    "Connection lost to typescript wallaby".to_owned(),
                                ))],
                                ..Default::default()
                            })
                    })
            }

            let (tx, mut rx) = channel::<Message>(100);
            *SENDER.write().expect("SENDER was poisoned") = Some(tx.clone());

            debug!("Connecting to typescript wallaby: {}", url);
            let connection = match tokio_tungstenite::connect_async(&url).await {
                Ok((ws_stream, _)) => ws_stream,
                Err(err) => {
                    error!("Failed to connect to bridge: {:?}", err);
                    tokio::time::sleep(Duration::from_secs(1)).await;
                    continue;
                }
            };
            let (mut write, read) = connection.split();
            debug!("Connected to typescript wallaby: {}", url);

            tokio::task::spawn(async move {
                while let Some(message) = rx.recv().await {
                    trace!("Sending message to typescript wallaby: {:?}", message);
                    write.send(message).await.expect("Could not send request");
                }
            });

            read.for_each(|message| async {
                trace!("Received message from typescript wallaby: {:?}", message);
                match message {
                    Ok(Message::Ping(_)) => {
                        tx.send(Message::Pong(vec![]))
                            .await
                            .map_err(|_| ())
                            .expect("Could not enqueue bridge request");
                    }
                    Ok(Message::Text(json)) => {
                        match serde_json::from_str::<BridgeResponse>(&json) {
                            Ok(response) => match MESSAGES
                                .lock()
                                .expect("MESSAGES was poisoned")
                                .get_mut(&response.id)
                            {
                                Some(message) => message
                                    .lock()
                                    .expect("message was poisoned")
                                    .set_response(response.response),
                                None => debug!(
                            "Received events for message which does not exist (anymore): {}",
                            response.id
                        ),
                            },
                            Err(err) => error!("Could not deserialize bridge message: {:?}", err),
                        }
                    }
                    Ok(msg) => debug!("Ignoring bridge message: {:?}", msg),
                    Err(err) => error!("Error in bridge communication: {:?}", err),
                };
            })
            .await;
            debug!("Connection lost to typescript wallaby: {}", url);
            tokio::time::sleep(Duration::from_secs(1)).await;
        }
    });
}

struct BridgeFuture {
    message: Arc<Mutex<BridgeMessage>>,
}

impl BridgeFuture {
    fn new(request: Request) -> Self {
        let message = BridgeMessage::new(request);

        Self {
            message: Arc::new(Mutex::new(message)),
        }
    }
}

impl Future for BridgeFuture {
    type Output = Response;
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut message = self.message.lock().expect("message was poisoned");
        message.waker = Some(cx.waker().clone());

        if let Some(response) = message.response.take() {
            MESSAGES
                .lock()
                .expect("MESSAGES was poisoned")
                .remove(&message.id);
            return Poll::Ready(response);
        }

        if let Some(request) = message.request.take() {
            MESSAGES
                .lock()
                .expect("MESSAGES was poisoned")
                .insert(message.id.clone(), self.message.clone());

            let bridge_request = BridgeRequest {
                id: message.id.clone(),
                request,
            };
            trace!(
                "Sending request to typescript wallaby: {:?}",
                bridge_request
            );
            let json =
                serde_json::to_string(&bridge_request).expect("Could not serialize bridge request");
            let message = Message::Text(json);

            if let Err(err) = SENDER
                .read()
                .expect("SENDER was poisoned")
                .as_ref()
                .expect("Bridge was not started!")
                .try_send(message)
            {
                error!("Error sending bridge request: {:?}", err);
            };
        }

        Poll::Pending
    }
}

#[derive(Debug)]
struct BridgeMessage {
    id: String,
    request: Option<Request>,
    response: Option<Response>,
    waker: Option<Waker>,
}

impl BridgeMessage {
    fn new(request: Request) -> Self {
        let id = thread_rng()
            .sample_iter(&Alphanumeric)
            .take(10)
            .map(char::from)
            .collect();

        Self {
            id,
            request: Some(request),
            response: None,
            waker: None,
        }
    }

    fn set_response(&mut self, response: Response) {
        trace!("Received response from typescript wallaby: {:#?}", response);
        self.response = Some(response);
        if let Some(waker) = self.waker.take() {
            waker.wake();
        }
    }
}

#[derive(Serialize, Debug)]
struct BridgeRequest {
    id: String,
    #[serde(flatten)]
    request: Request,
}

#[derive(Serialize, Debug)]
pub struct Request {
    pub locale: String,
    pub username: String,
    pub screen_name: String,
    #[serde(skip_serializing_if = "Value::is_null")]
    pub session: Value,
    pub name: &'static str,
    #[serde(skip_serializing_if = "Value::is_null")]
    pub payload: Value,
}

impl Request {
    pub fn new<V: Into<Value>>(
        locale: String,
        username: String,
        screen_name: String,
        session: Value,
        name: &'static str,
        payload: V,
    ) -> Self {
        Self {
            locale,
            username,
            screen_name,
            session,
            name,
            payload: payload.into(),
        }
    }

    pub async fn send(self) -> Response {
        BridgeFuture::new(self).await
    }
}

#[derive(Deserialize, Debug)]
struct BridgeResponse {
    id: String,
    #[serde(flatten)]
    response: Response,
}

#[derive(Deserialize, Debug, Default)]
pub struct Response {
    session: Value,
    events: Vec<BridgeEvent>,
    rendered_screen: Option<RenderedScreen>,
}

impl Response {
    pub fn session(&mut self) -> Value {
        self.session.take()
    }

    pub fn rendered_screen(&mut self) -> Option<RenderedScreen> {
        self.rendered_screen.take()
    }

    pub fn into_events(self) -> Vec<Event> {
        self.events.into_iter().map(Into::into).collect()
    }
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase", tag = "name", content = "payload")]
pub enum BridgeEvent {
    Alert(Alert),
    Message(message::Message),
    Modal(Map<String, Value>),
    PlaySound(PlaySound),
}

impl From<BridgeEvent> for Event {
    fn from(event: BridgeEvent) -> Self {
        match event {
            BridgeEvent::Alert(alert) => Event::Alert(alert),
            BridgeEvent::Message(message) => Event::Message(message),
            BridgeEvent::Modal(modal) => Event::Custom(CustomEvent::new("modal".to_owned(), modal)),
            BridgeEvent::PlaySound(play_sound) => Event::PlaySound(play_sound),
        }
    }
}
