use std::fmt::Debug;
use unic_langid::{langid, LanguageIdentifier};
static FALLBACK_LANGUAGE_IDENTIFIER: LanguageIdentifier = langid!("de-DE");

/// Trait for defining a session
///
/// * `screen_name`: return the current screen, can be used to return a default/login screen if not yet authorized
/// * `set_screen_name`: Is called by helo/action
/// * `should_update_screen`: Does the session in its current state permits asynchronous screen updates? Defaults to `true` if not overwritten
/// * `locale`: Return the locale, defaults to `"de-DE"` if not overwritten
pub trait Session: 'static + Sync + Send + Debug + Default + Clone {
    /// return the current screen, can be used to return a default/login screen if not yet authorized
    fn screen_name(&self) -> &str;

    /// Is called by helo/action
    fn set_screen_name(&mut self, screen_name: String);

    /// Does the session in its current state permits asynchronous screen updates?
    fn should_update_screen(&self) -> bool {
        true
    }

    /// Return the locale, defaults to "de-DE" if not overwritten
    fn locale(&self) -> &LanguageIdentifier {
        &FALLBACK_LANGUAGE_IDENTIFIER
    }

    #[cfg(feature = "bridge")]
    /// Get username of session for bridged screen
    fn bridge_username(&self, screen_name: &str) -> &str;
    #[cfg(feature = "bridge")]
    /// Get subset of session for bridged screen
    fn bridge_subset(&self, screen_name: &str) -> serde_json::Value;

    #[cfg(feature = "bridge")]
    /// Set subset of session for bridged screen
    fn set_bridge_subset(
        &mut self,
        screen_name: &str,
        subset: serde_json::Value,
    ) -> Result<crate::ServerEvents, crate::Error>;
}
