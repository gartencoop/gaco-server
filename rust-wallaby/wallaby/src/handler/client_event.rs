use serde::{Deserialize, Serialize};
use serde_json::Value;

#[derive(Deserialize, Serialize, Debug)]
pub struct ClientEvent {
    name: String,
    payload: ClientEventPayload,
}

impl ClientEvent {
    pub fn new<V: Into<Value>>(name: String, session_id: String, extra_fields: V) -> Self {
        Self {
            name,
            payload: ClientEventPayload {
                session_id,
                extra_fields: extra_fields.into(),
            },
        }
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn session_id(&self) -> &str {
        &self.payload.session_id
    }

    #[cfg(feature = "backend")]
    pub async fn handle<SESSION: crate::Session, SESSIONSTORE: crate::SessionStore<SESSION>>(
        self,
        storage: &mut crate::Storage<SESSION, SESSIONSTORE>,
    ) {
        log::trace!(
            "Handle client event: name={}, session_id={}",
            self.name(),
            self.session_id()
        );

        let handlers = storage.handler_store().get(self.name()).await;
        if handlers.is_empty() {
            log::debug!("No handlers registered for event \"{}\"", self.name());
            return;
        }

        let mut session =
            crate::StoredSession::load::<SESSIONSTORE>(storage.session_store(), self.session_id())
                .await;

        for handler in handlers {
            handler(
                storage,
                &mut session,
                &self.name,
                &self.payload.extra_fields,
            )
            .await;
        }
    }
}

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ClientEventPayload {
    #[serde(rename = "edid")]
    session_id: String,

    #[serde(flatten)]
    extra_fields: Value,
}
