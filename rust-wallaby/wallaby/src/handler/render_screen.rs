use crate::{
    event::screen::Screen, BoxedScreen, Event, ServerEvent, ServerEvents, Session, StoredSession,
};
use log::trace;

pub async fn render_screen<SESSION: Session>(
    mut screen: BoxedScreen<SESSION>,
    session: &StoredSession<SESSION>,
    events: Vec<Event>,
    update: bool,
) -> ServerEvents {
    let rendered_screen = match screen.execute(session.session()).await {
        Ok(rendered_screen) => rendered_screen,
        Err(err) => return err.into(),
    };

    let mut exclude_screen = false;
    let mut exclude_interactions = vec![];
    let mut events = events
        .into_iter()
        .filter_map(|event| match event {
            Event::Message(msg) => Some(ServerEvent::Message(msg)),
            Event::Alert(alert) => Some(ServerEvent::Alert(alert)),
            Event::PlaySound(play_sound) => Some(ServerEvent::PlaySound(play_sound)),
            Event::ExcludeScreen => {
                exclude_screen = true;
                None
            }
            Event::ExcludeInteraction(interaction) => {
                exclude_interactions.push(interaction);
                None
            }
            Event::Custom(custom) => Some(ServerEvent::Custom(custom)),
        })
        .collect::<Vec<_>>();

    if !exclude_screen {
        if let Some(proto_screen) = Screen::from_rendered_screen::<SESSION>(
            rendered_screen,
            session,
            update,
            exclude_interactions,
        )
        .await
        {
            events.push(ServerEvent::Screen(proto_screen));
        }
    }
    events.push(ServerEvent::End);

    trace!("events: {:?}", events);

    events
}
