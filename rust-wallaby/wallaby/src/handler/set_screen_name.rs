use crate::{Session, StoredSession};

/// Set screen in session
pub async fn set_screen_name<SESSION: Session>(
    session: &mut StoredSession<SESSION>,
    path: Option<&str>,
) {
    if let Some(screen_name) = path
        .and_then(|path| path.split(':').next().map(|name| name.to_string()))
        .and_then(|screen_name| match screen_name.len() {
            0 => None,
            _ => Some(screen_name),
        })
    {
        session.mut_session().set_screen_name(screen_name);
    }
}
