use super::{render_screen::render_screen, set_screen_name::set_screen_name};
use crate::{Error, EventHandler, ServerEvents, Session, SessionStore, Storage, StoredSession};
use log::error;
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use serde_json::Value;

/// handle event "helo": set current screen, run helo action on screen and save session
pub struct Helo;

impl Helo {
    async fn handle<SESSION: Session, SESSIONSTORE: SessionStore<SESSION>>(
        storage: &mut Storage<SESSION, SESSIONSTORE>,
        session: &mut StoredSession<SESSION>,
        payload: HeloPayload,
    ) -> Result<ServerEvents, Error> {
        set_screen_name(session, payload.path()).await;

        let mut screen = storage
            .screen_store()
            .get(session.session().screen_name())
            .await?;

        let old_screen_name = session.session().screen_name().to_owned();
        let events = screen.helo(session.mut_session(), &payload).await?;
        session.save(storage.session_store_mut()).await;

        if session.session().screen_name() != old_screen_name {
            screen = storage
                .screen_store()
                .get(session.session().screen_name())
                .await?;
        }

        Ok(render_screen(screen, session, events, false).await)
    }
}

#[async_trait::async_trait]
impl<SESSION: Session, SESSIONSTORE: SessionStore<SESSION>> EventHandler<SESSION, SESSIONSTORE>
    for Helo
{
    fn events() -> Vec<&'static str> {
        vec!["helo"]
    }

    async fn handle_event(
        storage: &mut Storage<SESSION, SESSIONSTORE>,
        session: &mut StoredSession<SESSION>,
        _name: &str,
        payload: &Value,
    ) {
        let payload: HeloPayload = match serde_json::from_value(payload.clone()) {
            Ok(payload) => payload,
            Err(err) => {
                error!("Could not parse helo payload: {:?}", err);
                return;
            }
        };

        let server_events = Helo::handle(storage, session, payload)
            .await
            .unwrap_or_else(|err| err.into());
        session.send_events(server_events).await;
    }
}

#[derive(Debug, Default, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct HeloPayload {
    #[serde(rename = "_path")]
    path: Option<String>,

    #[serde(flatten)]
    extra_fields: Value,
}

impl HeloPayload {
    pub fn path(&self) -> Option<&str> {
        self.path.as_deref()
    }

    /// Get extra fields as type, can be any type which implements [`DeserializeOwned`]
    pub fn extra_fields<T: DeserializeOwned>(&self) -> Option<T> {
        serde_json::from_value(self.extra_fields.clone()).ok()
    }
}
