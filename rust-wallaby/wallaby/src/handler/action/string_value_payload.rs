use serde::Deserialize;

#[derive(Deserialize)]
pub struct StringValuePayload {
    pub value: String,
}

impl StringValuePayload {
    pub fn into_value(self) -> String {
        self.value
    }
}
