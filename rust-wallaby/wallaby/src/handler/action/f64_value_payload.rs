use serde::Deserialize;

#[derive(Deserialize)]
pub struct F64ValuePayload {
    pub value: f64,
}

impl F64ValuePayload {
    pub fn into_value(self) -> f64 {
        self.value
    }
}
