use serde::Deserialize;

#[derive(Deserialize)]
pub struct U8ValuePayload {
    pub value: u8,
}

impl U8ValuePayload {
    pub fn into_value(self) -> u8 {
        self.value
    }
}
