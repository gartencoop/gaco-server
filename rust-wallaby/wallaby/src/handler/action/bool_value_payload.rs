use serde::Deserialize;

#[derive(Deserialize)]
pub struct BoolValuePayload {
    pub value: bool,
}

impl BoolValuePayload {
    pub fn into_value(self) -> bool {
        self.value
    }
}
