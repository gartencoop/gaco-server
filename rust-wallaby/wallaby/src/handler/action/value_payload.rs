use serde::Deserialize;
use serde_json::Value;

#[derive(Deserialize)]
pub struct ValuePayload {
    pub value: Value,
}

impl ValuePayload {
    pub fn into_value(self) -> Value {
        self.value
    }
}
