use serde::Deserialize;

#[derive(Deserialize)]
pub struct F32ValuePayload {
    pub value: f32,
}

impl F32ValuePayload {
    pub fn into_value(self) -> f32 {
        self.value
    }
}
