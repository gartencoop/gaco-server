#[cfg(feature = "backend")]
pub mod bool_value_payload;
#[cfg(feature = "backend")]
pub mod f32_value_payload;
#[cfg(feature = "backend")]
pub mod f64_value_payload;
#[cfg(feature = "backend")]
pub mod string_value_payload;
#[cfg(feature = "backend")]
pub mod u8_value_payload;
#[cfg(feature = "backend")]
pub mod value_payload;

use serde::{de::DeserializeOwned, Deserialize, Deserializer, Serialize};
use serde_json::value::Value;
use std::fmt::Debug;

#[cfg(feature = "backend")]
/// handle event "helo": Set current screen, run action on screen and save session
pub struct Action;

#[cfg(feature = "backend")]
impl Action {
    async fn handle<SESSION: crate::Session, SESSIONSTORE: crate::SessionStore<SESSION>>(
        storage: &mut crate::Storage<SESSION, SESSIONSTORE>,
        session: &mut crate::StoredSession<SESSION>,
        payload: ActionPayload,
    ) -> Result<crate::ServerEvents, crate::Error> {
        super::set_screen_name::set_screen_name(session, payload.path()).await;

        let mut screen = storage
            .screen_store()
            .get(session.session().screen_name())
            .await?;

        let (events, update) = if session.new {
            let mut events = screen
                .helo(session.mut_session(), &crate::HeloPayload::default())
                .await?;
            events.push(crate::Event::Message(crate::Message::error(
                "Session reset".to_owned(),
            )));
            (events, true)
        } else {
            let old_screen_name = session.session().screen_name().to_owned();
            let events = screen.action(session.mut_session(), &payload).await?;
            (events, session.session().screen_name() == old_screen_name)
        };
        session.save(storage.session_store_mut()).await;

        if !update {
            screen = storage
                .screen_store()
                .get(session.session().screen_name())
                .await?;
        }

        Ok(super::render_screen::render_screen(screen, session, events, update).await)
    }
}

#[cfg(feature = "backend")]
#[async_trait::async_trait]
impl<SESSION: crate::Session, SESSIONSTORE: crate::SessionStore<SESSION>>
    crate::EventHandler<SESSION, SESSIONSTORE> for Action
{
    fn events() -> Vec<&'static str> {
        vec!["action/genericAction"]
    }

    async fn handle_event(
        storage: &mut crate::Storage<SESSION, SESSIONSTORE>,
        session: &mut crate::StoredSession<SESSION>,
        _name: &str,
        payload: &Value,
    ) {
        let payload: ActionPayload = match serde_json::from_value(payload.clone()) {
            Ok(payload) => payload,
            Err(err) => {
                log::error!("Could not parse action payload: {:?}", err);
                return;
            }
        };

        let server_events = Action::handle(storage, session, payload)
            .await
            .unwrap_or_else(|err| err.into());
        session.send_events(server_events).await;
    }
}

#[derive(Debug, Default, Serialize, Deserialize, Clone)]
#[serde(default)]
pub struct ActionPayload {
    #[serde(rename = "_path")]
    pub path: Option<String>,

    pub table: Option<String>,

    #[serde(deserialize_with = "deserialize_row")]
    pub row: Option<usize>,

    #[serde(rename = "interaction")]
    pub interaction_name: Option<String>,

    #[serde(skip)]
    pub prefixes: Vec<String>,

    #[serde(rename = "type")]
    pub action_type: Option<String>,

    #[serde(flatten)]
    pub extra_fields: Value,
}

fn deserialize_row<'de, D>(deserializer: D) -> Result<Option<usize>, D::Error>
where
    D: Deserializer<'de>,
{
    Ok(usize::deserialize(deserializer).map(Some).unwrap_or(None))
}

impl ActionPayload {
    pub fn table(&self) -> Option<String> {
        self.table.clone()
    }

    pub fn row(&self) -> Option<usize> {
        self.row
    }

    pub fn interaction_name(&self) -> Option<String> {
        self.interaction_name.clone()
    }

    pub fn prefixes(&self) -> &[String] {
        self.prefixes.as_ref()
    }

    pub fn path(&self) -> Option<&str> {
        self.path.as_deref()
    }

    pub fn action_type(&self) -> Option<&str> {
        self.action_type.as_deref()
    }

    /// Get extra fields as type, can be used to get one of:
    /// * [`bool_value_payload::BoolValuePayload`]
    /// * [`f32_value_payload::F32ValuePayload`]
    /// * [`f64_value_payload::F64ValuePayload`]
    /// * [`string_value_payload::StringValuePayload`]
    /// * [`u8_value_payload::U8ValuePayload`]
    /// * [`value_payload::ValuePayload`]
    /// * a custom type which implements [`DeserializeOwned`]
    pub fn extra_fields<T: DeserializeOwned>(&self) -> Option<T> {
        serde_json::from_value(self.extra_fields.clone()).ok()
    }

    pub fn without_prefix(&self, prefix: &str) -> Option<Self> {
        self.interaction_name.as_ref().and_then(|interaction_name| {
            interaction_name
                .strip_prefix(prefix)
                .map(|interaction_name| Self {
                    interaction_name: Some(interaction_name.to_string()),
                    prefixes: {
                        let mut prefixes = self.prefixes.clone();
                        prefixes.push(prefix.to_owned());
                        prefixes
                    },
                    ..self.clone()
                })
        })
    }
}
