pub mod handler_store;
pub mod screen_store;
pub mod session_store;

use crate::Session;
use handler_store::HandlerStore;
use screen_store::ScreenStore;
use session_store::SessionStore;

#[derive(Clone)]
pub struct Storage<SESSION: Session, SESSIONSTORE: SessionStore<SESSION>> {
    screen_store: ScreenStore<SESSION>,
    handler_store: HandlerStore<SESSION, SESSIONSTORE>,
    session_store: SESSIONSTORE,
}

impl<SESSION: Session, SESSIONSTORE: SessionStore<SESSION>> Storage<SESSION, SESSIONSTORE> {
    pub fn new(session_store: SESSIONSTORE) -> Self {
        Self {
            screen_store: ScreenStore::new(),
            handler_store: HandlerStore::new(),
            session_store,
        }
    }

    pub fn screen_store(&self) -> &ScreenStore<SESSION> {
        &self.screen_store
    }

    pub fn screen_store_mut(&mut self) -> &mut ScreenStore<SESSION> {
        &mut self.screen_store
    }

    pub fn handler_store(&self) -> &HandlerStore<SESSION, SESSIONSTORE> {
        &self.handler_store
    }

    pub fn handler_store_mut(&mut self) -> &mut HandlerStore<SESSION, SESSIONSTORE> {
        &mut self.handler_store
    }

    pub fn session_store(&self) -> &SESSIONSTORE {
        &self.session_store
    }

    pub fn session_store_mut(&mut self) -> &mut SESSIONSTORE {
        &mut self.session_store
    }
}
