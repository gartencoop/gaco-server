#[cfg(feature = "bridge")]
pub mod bridge;
#[cfg(feature = "backend")]
pub mod configure;
#[cfg(feature = "backend")]
pub mod helo;
#[cfg(feature = "backend")]
pub mod load_data;
#[cfg(feature = "backend")]
pub mod query_params;
#[cfg(feature = "backend")]
pub mod set_data;

use crate::RenderedInteraction;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::{collections::HashMap, fmt::Debug};

#[cfg(feature = "backend")]
/// Screen methods
///
/// Please use [`crate::derive::Screen`] and do not implement on your own!
#[async_trait::async_trait]
pub trait Screen<SESSION: crate::Session>: 'static + Send + Sync {
    fn name(&self) -> &str;
    fn ui(&self) -> &str;

    async fn helo(
        &mut self,
        session: &mut SESSION,
        payload: &crate::HeloPayload,
    ) -> Result<Vec<crate::Event>, crate::Error>;
    async fn load_data(&mut self, session: &SESSION) -> Result<(), crate::Error>;

    fn render(&self, session: &SESSION) -> RenderedScreen;

    #[cfg(feature = "backend")]
    async fn action(
        &mut self, // needed for tables which run load_data before the getter
        session: &mut SESSION,
        payload: &crate::ActionPayload,
    ) -> Result<Vec<crate::Event>, crate::Error>;

    async fn execute(&mut self, session: &SESSION) -> Result<RenderedScreen, crate::Error> {
        self.load_data(session).await?;
        Ok(self.render(session))
    }
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct RenderedScreen {
    pub name: String,
    pub ui: Ui,
    #[serde(rename = "queryParams", skip_serializing_if = "Option::is_none")]
    pub query_params: Option<String>,

    #[serde(rename = "interaction")]
    pub interactions: HashMap<String, RenderedInteraction>,

    #[serde(rename = "action")]
    pub actions: HashMap<String, RenderedAction>,
}

#[derive(Debug, Clone, Default, Deserialize, Serialize)]
pub struct Ui {
    name: String,
}

impl Ui {
    pub fn new(name: String) -> Self {
        Self { name }
    }

    pub fn name(&self) -> &str {
        &self.name
    }
}

#[derive(Debug, Default, Deserialize, Serialize, Clone)]
#[serde(default)]
pub struct RenderedAction {
    #[serde(rename = "interaction")]
    pub interaction_name: String,
    #[serde(rename = "type")]
    pub action_type: String,
    pub table: Option<String>,
    #[serde(flatten)]
    pub extra_fields: Value,
}

impl RenderedAction {
    pub fn new(interaction_name: String, action_type: String, extra_fields: Value) -> Self {
        Self {
            interaction_name,
            action_type,
            table: None,
            extra_fields,
        }
    }

    pub fn add_prefix(&mut self, prefix: &str) {
        self.interaction_name = format!("{}{}", prefix, self.interaction_name);
    }
}

#[cfg(feature = "backend")]
/// Boxed type of structs which implement the [`crate::Screen`] trait for a generic [`crate::Session`]
pub type BoxedScreen<SESSION> = Box<dyn Screen<SESSION>>;
