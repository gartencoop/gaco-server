/// Configure a subscreen by supplying the configuration in the `#[screen]` macro
///
/// ```
/// #[screen(configuration = "bla")]
/// subscreen: TestScreen,
/// ```
///
/// -> calls the configure method on `TestScreen` with `&str` "bla"
///
/// If both configuration and data is set on a subscreen, first the configuration then the data is set
pub trait ConfigureScreen {
    fn configure(&mut self, configuration: &str);
}
