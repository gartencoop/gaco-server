use crate::{Error, Event, HeloPayload, Session};

/// Helo hook
///
/// Is called on helo actions.
///
/// Activate in screen macro by using hooks attribute:
/// ```
/// #[screen(session = "MySession", hooks(helo))]
/// pub struct MyScreen {}
/// ```
#[async_trait::async_trait]
pub trait Helo<SESSION: Session> {
    async fn helo(&self, session: &mut SESSION, payload: &HeloPayload)
        -> Result<Vec<Event>, Error>;
}
