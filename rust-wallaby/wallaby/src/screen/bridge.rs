use log::debug;

use crate::{bridge::Request, ConfigureScreen, RenderedScreen, Screen, Session};

pub struct BridgeScreen {
    name: String,
    rendered_screen: Option<RenderedScreen>,
    #[cfg(feature = "bridge_catchall")]
    catchall: bool,
}

impl BridgeScreen {
    pub fn new() -> Self {
        Self {
            name: String::new(),
            rendered_screen: None,
            #[cfg(feature = "bridge_catchall")]
            catchall: false,
        }
    }

    #[cfg(feature = "bridge_catchall")]
    pub fn catchall(name: &str) -> Self {
        let mut screen = Self::new();
        screen.configure(name);
        screen.catchall = true;
        screen
    }

    pub async fn request<SESSION: Session>(
        &mut self,
        request: Request,
        session: &mut SESSION,
    ) -> Result<Vec<crate::Event>, crate::Error> {
        let mut response = request.send().await;

        session.set_bridge_subset(&self.name, response.session())?;
        self.rendered_screen = response.rendered_screen();
        #[cfg(feature = "bridge_catchall")]
        {
            if let Some(rendered_screen) = self.rendered_screen.as_ref().filter(|_| self.catchall) {
                session.set_screen_name(rendered_screen.name.clone());
            }
        }

        Ok(response.into_events())
    }
}

impl ConfigureScreen for BridgeScreen {
    fn configure(&mut self, configuration: &str) {
        self.name = configuration.to_owned();
    }
}

#[async_trait::async_trait]
impl<SESSION: Session> Screen<SESSION> for BridgeScreen {
    fn name(&self) -> &str {
        &self.name
    }

    fn ui(&self) -> &str {
        &self.name
    }

    async fn helo(
        &mut self,
        session: &mut SESSION,
        payload: &crate::HeloPayload,
    ) -> Result<Vec<crate::Event>, crate::Error> {
        debug!("helo on bridge screen");
        self.request(
            Request::new(
                session.locale().to_string(),
                session.bridge_username(&self.name),
                self.name.clone(),
                session.bridge_subset(&self.name),
                "helo",
                serde_json::to_value(payload).unwrap_or_default(),
            ),
            session,
        )
        .await
    }

    async fn load_data(&mut self, session: &SESSION) -> Result<(), crate::Error> {
        debug!("load_data on bridge screen");

        if self.rendered_screen.is_none() {
            debug!("Loading bridge screen with helo request");
            let mut response = Request::new(
                session.locale().to_string(),
                session.bridge_username(&self.name),
                self.name.clone(),
                session.bridge_subset(&self.name),
                "helo",
                (),
            )
            .send()
            .await;
            self.rendered_screen = response.rendered_screen();
        }

        Ok(())
    }

    fn render(&self, _session: &SESSION) -> RenderedScreen {
        debug!("render on bridge screen");
        self.rendered_screen.clone().unwrap_or_default() // Only used if BridgeScreen is used as a subscreen
    }

    async fn action(
        &mut self,
        session: &mut SESSION,
        payload: &crate::ActionPayload,
    ) -> Result<Vec<crate::Event>, crate::Error> {
        debug!("action on bridge screen");
        self.request(
            Request::new(
                session.locale().to_string(),
                session.bridge_username(&self.name),
                self.name.clone(),
                session.bridge_subset(&self.name),
                "action",
                serde_json::to_value(payload).unwrap_or_default(),
            ),
            session,
        )
        .await
    }

    async fn execute(&mut self, session: &SESSION) -> Result<RenderedScreen, crate::Error> {
        debug!("execute on bridge screen");
        Ok(self.render(session))
    }
}
