use crate::Session;

/// [`QueryParams`] hook
///
/// Query params are added to the rendered screen and will end up as path in the browser url bar.
///
/// Is called for every screen, not for row screens/subscreens!
///
/// Activate in screen macro by using hooks attribute:
/// ```
/// #[screen(session = "MySession", hooks(query_params))]
/// pub struct MyScreen {}
/// ```
pub trait QueryParams<SESSION: Session> {
    fn query_params(&self, session: &SESSION) -> Option<String>;
}
