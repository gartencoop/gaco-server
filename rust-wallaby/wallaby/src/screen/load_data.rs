use crate::{Error, Session};

/// LoadData hook
///
/// Is called for every screen and subscreen, not for row screens!
///
/// Activate in screen macro by using hooks attribute:
/// ```
/// #[screen(session = "MySession", hooks(load_data))]
/// pub struct MyScreen {}
/// ```
#[async_trait::async_trait]
pub trait LoadData<SESSION: Session> {
    async fn load_data(&mut self, session: &SESSION) -> Result<(), Error>;
}
