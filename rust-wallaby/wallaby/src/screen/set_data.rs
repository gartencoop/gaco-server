/// [`SetData`] hook
///
/// Is called for every row screen if the data attribute is set:
/// ```
/// #[table(data = "bla")]
/// my_table: RowScreen,
///
/// bla: Option<D>,
/// ```
pub trait SetData<D> {
    fn set_data(&mut self, data: &D);
}
