#[derive(Debug)]
pub enum Error {
    InteractionNotFound(String),
    ScreenNotFound(String),
}
