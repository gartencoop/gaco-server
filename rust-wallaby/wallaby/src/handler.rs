pub mod action;
pub mod client_event;
#[cfg(feature = "backend")]
pub mod helo;
#[cfg(feature = "backend")]
pub mod render_screen;
#[cfg(feature = "backend")]
pub mod set_screen_name;

#[cfg(feature = "backend")]
/// Handle incoming events with access to storage and stored session
#[async_trait::async_trait]
pub trait EventHandler<SESSION: crate::Session, SESSIONSTORE: crate::SessionStore<SESSION>>:
    'static + Send + Sync
{
    fn events() -> Vec<&'static str>;

    async fn handle_event(
        storage: &mut crate::Storage<SESSION, SESSIONSTORE>,
        session: &mut crate::StoredSession<SESSION>,
        name: &str,
        payload: &serde_json::Value,
    );
}

#[cfg(feature = "backend")]
impl<T, SESSION, SESSIONSTORE> Handler<SESSION, SESSIONSTORE> for T
where
    T: EventHandler<SESSION, SESSIONSTORE>,
    SESSION: crate::Session,
    SESSIONSTORE: crate::SessionStore<SESSION>,
{
    fn events() -> Vec<&'static str> {
        T::events()
    }

    fn handle_event<'a>(
        storage: &'a mut crate::Storage<SESSION, SESSIONSTORE>,
        session: &'a mut crate::StoredSession<SESSION>,
        name: &'a str,
        payload: &'a serde_json::Value,
    ) -> std::pin::Pin<Box<dyn futures::Future<Output = ()> + Send + 'a>> {
        Box::pin(async move {
            T::handle_event(storage, session, name, payload).await;
        })
    }
}

#[cfg(feature = "backend")]
/// Handler trait which can be saved to the handler store
pub trait Handler<SESSION: crate::Session, SESSIONSTORE: crate::SessionStore<SESSION>>:
    'static + Send + Sync
{
    fn events() -> Vec<&'static str>;

    fn handle_event<'a>(
        storage: &'a mut crate::Storage<SESSION, SESSIONSTORE>,
        session: &'a mut crate::StoredSession<SESSION>,
        name: &'a str,
        payload: &'a serde_json::Value,
    ) -> std::pin::Pin<Box<dyn futures::Future<Output = ()> + Send + 'a>>;
}
