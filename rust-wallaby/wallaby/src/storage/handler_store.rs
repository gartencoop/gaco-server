use crate::{handler::Handler, Session, SessionStore, Storage, StoredSession};
use futures::lock::Mutex;
use serde_json::Value;
use std::{collections::HashMap, future::Future, sync::Arc};

pub type Handle<SESSION, SESSIONSTORE> =
    for<'a> fn(
        &'a mut Storage<SESSION, SESSIONSTORE>,
        &'a mut StoredSession<SESSION>,
        &'a str,
        &'a Value,
    ) -> std::pin::Pin<Box<dyn Future<Output = ()> + Send + 'a>>;

pub type Handlers<SESSION, SESSIONSTORE> = Vec<Handle<SESSION, SESSIONSTORE>>;

#[derive(Clone)]
pub struct HandlerStore<SESSION: Session, SESSIONSTORE: SessionStore<SESSION>> {
    handlers: Arc<Mutex<HashMap<String, Handlers<SESSION, SESSIONSTORE>>>>,
}

impl<SESSION: Session, SESSIONSTORE: SessionStore<SESSION>> HandlerStore<SESSION, SESSIONSTORE> {
    pub(crate) fn new() -> Self {
        Self {
            handlers: Arc::new(Mutex::new(HashMap::new())),
        }
    }

    pub async fn add<HANDLER: Handler<SESSION, SESSIONSTORE>>(&mut self) {
        let mut handlers = self.handlers.lock().await;
        for event in HANDLER::events() {
            handlers
                .entry(event.to_owned())
                .or_insert_with(Vec::new)
                .push(HANDLER::handle_event);
        }
    }

    pub async fn get(&self, name: &str) -> Handlers<SESSION, SESSIONSTORE> {
        self.handlers
            .lock()
            .await
            .get(name)
            .cloned()
            .unwrap_or_default()
    }
}
