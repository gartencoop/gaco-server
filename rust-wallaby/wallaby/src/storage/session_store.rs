pub mod in_memory;
pub mod single;

use crate::{
    handler::render_screen::render_screen, Event, ServerEvent, ServerEvents, Session, Storage,
};
use log::{debug, info};

/// Trait for implementing a session store
///
/// A session store should invalidate old sessions after a timeout.
#[async_trait::async_trait]
pub trait SessionStore<SESSION: Session>: Send + Sync + Clone + 'static {
    async fn session(&self, session_id: &str) -> Option<SESSION>;
    async fn save_session(&mut self, session_id: String, session: SESSION);
    async fn sessions(&self) -> Box<dyn Iterator<Item = (String, SESSION)>>;
    async fn session_ids(&self) -> Box<dyn Iterator<Item = String> + Send> {
        Box::new(
            self.sessions()
                .await
                .map(|(session_id, _)| session_id)
                .collect::<Vec<_>>()
                .into_iter(),
        )
    }
}

pub struct StoredSession<SESSION: Session> {
    session_id: String,
    pub session: SESSION,
    pub new: bool,
}

impl<SESSION: Session> StoredSession<SESSION> {
    pub async fn load<SESSIONSTORE: SessionStore<SESSION>>(
        store: &SESSIONSTORE,
        session_id: &str,
    ) -> Self {
        let session = store.session(session_id).await;
        debug!("Loaded session: {}", session_id);
        Self {
            session_id: session_id.to_string(),
            new: session.is_none(),
            session: session.unwrap_or_default(),
        }
    }

    pub async fn save<SESSIONSTORE: SessionStore<SESSION>>(&self, store: &mut SESSIONSTORE) {
        store
            .save_session(self.session_id.clone(), self.session.clone())
            .await;
        debug!("Saved session: {}", self.session_id);
    }

    pub fn session_id(&self) -> &str {
        &self.session_id
    }

    pub fn session(&self) -> &SESSION {
        &self.session
    }

    pub fn mut_session(&mut self) -> &mut SESSION {
        &mut self.session
    }

    pub fn reset(&mut self) {
        info!("Session reset: {}", self.session_id);
        self.session = SESSION::default();
    }

    /// Render and send screen.
    /// Does not send anything if the only event is a `ServerEvent::End`.
    pub async fn render_and_send_screen<SESSIONSTORE: SessionStore<SESSION>>(
        &self,
        storage: Storage<SESSION, SESSIONSTORE>,
        events: Vec<Event>,
        update: bool,
    ) {
        let events = match storage
            .screen_store()
            .get(self.session().screen_name())
            .await
        {
            Ok(screen) => render_screen(screen, self, events, update).await,
            Err(err) => err.into(),
        };
        if !(events.is_empty()
            || events.len() == 1
                && matches!(
                    events.first().expect("Could not only first event"),
                    ServerEvent::End
                ))
        {
            self.send_events(events).await;
        }
    }

    pub async fn send_events(&self, events: ServerEvents) {
        #[cfg(any(
            feature = "transport_ffi",
            feature = "transport_http",
            feature = "transport_tauri"
        ))]
        crate::transport::send_events(&self.session_id, events).await;
    }

    pub async fn sessions<SESSIONSTORE: SessionStore<SESSION>>(
        store: &SESSIONSTORE,
    ) -> Box<dyn Iterator<Item = Self> + Send> {
        Box::new(
            store
                .sessions()
                .await
                .map(|(session_id, session)| Self {
                    session_id,
                    session,
                    new: false,
                })
                .collect::<Vec<_>>()
                .into_iter(),
        )
    }
}
