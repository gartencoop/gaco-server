use crate::{BoxedScreen, Error, Session};
use futures::lock::Mutex;
use std::{collections::HashMap, sync::Arc};

pub type ScreenGenerator<SESSION> = Box<fn() -> BoxedScreen<SESSION>>;

#[derive(Clone)]
pub struct ScreenStore<SESSION: Session> {
    screens: Arc<Mutex<HashMap<String, ScreenGenerator<SESSION>>>>,
}

impl<SESSION: Session> ScreenStore<SESSION> {
    pub(crate) fn new() -> Self {
        Self {
            screens: Arc::new(Mutex::new(HashMap::new())),
        }
    }

    pub async fn add(&mut self, generator: ScreenGenerator<SESSION>) {
        self.screens
            .lock()
            .await
            .insert(generator().name().to_string(), generator);
    }

    /// # Errors
    ///
    /// Screen is not found in [`ScreenStore`]
    pub async fn get(&self, name: &str) -> Result<BoxedScreen<SESSION>, Error> {
        let screen = self
            .screens
            .lock()
            .await
            .get(name)
            .map(|generator| generator());

        #[cfg(feature = "bridge_catchall")]
        {
            return Ok(screen.unwrap_or_else(|| Box::new(crate::BridgeScreen::catchall(name))));
        }

        #[cfg(not(feature = "bridge_catchall"))]
        {
            screen.ok_or_else(|| Error::ScreenNotFound(name.to_string()))
        }
    }
}
