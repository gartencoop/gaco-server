use crate::{Session, SessionStore};
use futures::lock::Mutex;
use log::debug;
use std::{collections::HashMap, sync::Arc, time::Duration};
use tokio::time::Instant;

#[derive(Clone)]
struct InMemorySession<SESSION: Session> {
    session: SESSION,
    updated_at: Instant,
}

impl<SESSION: Session> InMemorySession<SESSION> {
    fn into_session(self) -> SESSION {
        self.session
    }
}

impl<SESSION: Session> From<SESSION> for InMemorySession<SESSION> {
    fn from(session: SESSION) -> Self {
        Self {
            session,
            updated_at: Instant::now(),
        }
    }
}

/// In memory session store which invalidates old sessions after 24 hours
#[derive(Clone)]
pub struct InMemorySessionStore<SESSION: Session> {
    sessions: Arc<Mutex<HashMap<String, InMemorySession<SESSION>>>>,
}

impl<SESSION: Session> InMemorySessionStore<SESSION> {
    pub fn new() -> Self {
        let sessions = Arc::new(Mutex::new(HashMap::new()));
        let store = Self {
            sessions: sessions.clone(),
        };
        tokio::spawn(async move {
            loop {
                tokio::time::sleep(Duration::from_secs(60)).await;
                let mut sessions = sessions.lock().await;
                if !sessions.is_empty() {
                    let count = sessions.len();
                    let max_age = Duration::from_secs(24 * 60 * 60); // expire sessions after 24 hours
                    sessions.retain(|_session_id, in_memory_session| {
                        Instant::now().duration_since(in_memory_session.updated_at) < max_age
                    });
                    let expired = count - sessions.len();
                    if expired > 0 {
                        debug!("{} sessions expired", expired);
                    }
                }
            }
        });
        store
    }
}

impl<SESSION: Session> Default for InMemorySessionStore<SESSION> {
    fn default() -> Self {
        Self::new()
    }
}

#[async_trait::async_trait]
impl<SESSION: Session> SessionStore<SESSION> for InMemorySessionStore<SESSION> {
    async fn session(&self, session_id: &str) -> Option<SESSION> {
        self.sessions
            .lock()
            .await
            .get(session_id)
            .cloned()
            .map(|in_memory_session| in_memory_session.into_session())
    }

    async fn save_session(&mut self, session_id: String, session: SESSION) {
        self.sessions
            .lock()
            .await
            .insert(session_id, session.into());
    }

    async fn sessions(&self) -> Box<dyn Iterator<Item = (String, SESSION)>> {
        Box::new(
            self.sessions.lock().await.clone().into_iter().map(
                |(session_id, in_memory_session)| (session_id, in_memory_session.into_session()),
            ),
        )
    }

    async fn session_ids(&self) -> Box<dyn Iterator<Item = String> + Send> {
        Box::new(
            self.sessions
                .lock()
                .await
                .keys()
                .cloned()
                .collect::<Vec<_>>()
                .into_iter(),
        )
    }
}
