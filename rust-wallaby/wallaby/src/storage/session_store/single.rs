use super::SessionStore;
use crate::Session;

use futures::lock::Mutex;
use std::sync::Arc;

/// Session store with a single session (always initialized) - only usefull with ffi
#[derive(Clone)]
pub struct SingleSessionStore<SESSION: Session> {
    session: Arc<Mutex<SESSION>>,
}

impl<SESSION: Session> SingleSessionStore<SESSION> {
    pub fn new() -> Self {
        let session = Arc::new(Mutex::new(SESSION::default()));
        Self { session }
    }
}

impl<SESSION: Session> Default for SingleSessionStore<SESSION> {
    fn default() -> Self {
        Self::new()
    }
}

#[async_trait::async_trait]
impl<SESSION: Session> SessionStore<SESSION> for SingleSessionStore<SESSION> {
    async fn session(&self, _session_id: &str) -> Option<SESSION> {
        Some(self.session.lock().await.clone())
    }

    async fn save_session(&mut self, _session_id: String, session: SESSION) {
        *self.session.lock().await = session;
    }

    async fn sessions(&self) -> Box<dyn Iterator<Item = (String, SESSION)>> {
        Box::new(
            std::iter::once(self.session.lock().await.clone())
                .map(|session| ("single".to_owned(), session)),
        )
    }

    async fn session_ids(&self) -> Box<dyn Iterator<Item = String> + Send> {
        Box::new(std::iter::once("single".to_owned()))
    }
}
