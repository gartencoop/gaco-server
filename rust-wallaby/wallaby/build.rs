fn main() {
    println!("cargo::rustc-check-cfg=cfg(docs, values(\"true\"))");

    if std::env::var("DOCS").is_ok() {
        println!("cargo:rustc-cfg=docs=\"true\"");
        return;
    }

    let mut transports = vec![];
    if cfg!(feature = "transport_http") {
        transports.push("transport_http");
    }
    if cfg!(feature = "transport_ffi") {
        transports.push("transport_ffi");
    }
    if cfg!(feature = "transport_tauri") {
        transports.push("transport_tauri");
    }

    if transports.is_empty() && !cfg!(feature = "frontend") {
        panic!("You must activate one of the transport features: transport_http, transport_ffi, transport_tauri or the frontend feature")
    }

    if transports.len() > 1 {
        panic!("You most only activate one of the transport features, currently active transport features: {}", transports.join(", "));
    }
}
