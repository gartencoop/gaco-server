use crate::types::{child::Child, screen::Screen};
use proc_macro2::{Span, TokenStream};
use quote::quote;
use syn::LitStr;

pub fn create(screen: &Screen) -> TokenStream {
    let ident = &screen.ident;
    let screen_name = screen.name.clone().unwrap_or_else(|| ident.to_string());
    let name = LitStr::new(&screen_name, Span::call_site());
    let session = &screen.session;
    let fields = screen.fields();
    let constructor = fields.iter().map(|child| match child {
        Child::Interaction(field) => {
            let ident = field
                .ident
                .as_ref()
                .expect("Missing field ident. Screen derive macro is not supported on tuples.");
            let ty = &field.ty;
            if let Some(configuration) = field.configuration.as_ref() {
                let configuration = LitStr::new(configuration, Span::call_site());
                quote!(#ident: wallaby::ConfigureInteraction::configure(#configuration),)
            } else if field.default {
                quote!(#ident: #ty::default(),)
            } else {
                quote!(#ident: #ty,)
            }
        }
        Child::Subscreen(screen) => {
            let ident = screen
                .ident
                .as_ref()
                .expect("Missing screen ident. Screen derive macro is not supported on tuples.");
            let ty = &screen.ty;

            if let Some(configuration) = screen.configuration.as_ref() {
                let configuration = LitStr::new(configuration, Span::call_site());
                quote!(#ident: {
                    let mut screen = #ty::new();
                    wallaby::ConfigureScreen::configure(&mut screen, #configuration);
                    screen
                },)
            } else {
                quote!(#ident: #ty::new(),)
            }
        }
        Child::Table(table) => {
            let ident = table
                .ident
                .as_ref()
                .expect("Missing table ident. Screen derive macro is not supported on tuples.");
            let ty = &table.ty;
            quote!(#ident: #ty::new(),)
        }
        Child::Data(ident) => quote!(#ident: Default::default(),),
    });
    let interaction_names = fields.iter().filter_map(|child| match child {
        Child::Interaction(field) => field
            .ident
            .as_ref()
            .map(|ident| proc_macro2::Literal::string(&format!("{ident}"))),
        _ => None,
    });

    quote! (
        impl #ident {
            pub fn new() -> Self {
                Self {
                    #(#constructor)*
                }
            }

            pub fn generator() -> Box<fn() -> wallaby::BoxedScreen<#session>> {
                Box::new(|| {
                    let boxed: wallaby::BoxedScreen<#session> = Box::new(Self::new());
                    boxed
                })
            }

            pub const fn screen_name() -> &'static str {
                #name
            }

            pub const fn interaction_names() -> &'static [&'static str] {
                &[
                    #(#interaction_names,)*
                ]
            }
        }
    )
}
