use crate::types::{child::Child, screen::Screen};
use proc_macro2::{Span, TokenStream};
use quote::quote;
use syn::{Ident, LitStr};

pub fn create(screen: &Screen) -> TokenStream {
    let ident = &screen.ident;
    let screen_name = screen.name.clone().unwrap_or_else(|| ident.to_string());
    let screen_ui = screen.ui.clone().unwrap_or_else(|| ident.to_string());
    let name = LitStr::new(&screen_name, Span::call_site());
    let ui = LitStr::new(&screen_ui, Span::call_site());
    let session = &screen.session;

    let helo = screen.hooks.helo();
    let load_data = screen.hooks.load_data();
    let query_params = screen.hooks.query_params();

    let fields = screen.fields();
    let interactions: Vec<_> = fields
        .iter()
        .filter_map(|child| match child {
            Child::Interaction(interaction) => Some(interaction),
            _ => None,
        })
        .collect();
    let interactions_action = interactions.iter().map(|interaction| {
        let ident = interaction.ident.as_ref().expect("Missing interaction ident. Screen derive macro is not supported on tuples.");
        let name = LitStr::new(&interaction.rename.clone().unwrap_or_else(||ident.to_string()), Span::call_site());
        let interaction = interaction.interaction_type.ident();
        quote!(#name => Ok(wallaby::Interaction::<Self, #session>::action(&wallaby::#interaction::new(&self.#ident), session, payload).await),)
    });
    let interactions_render = interactions.iter().map(|interaction| {
        let ident = interaction.ident.as_ref().expect("Missing interaction ident. Screen derive macro is not supported on tuples.");
        let name = LitStr::new(&interaction.rename.clone().unwrap_or_else(||ident.to_string()), Span::call_site());
        let interaction = interaction.interaction_type.ident();
        quote!((#name.to_string(), wallaby::Interaction::<Self, #session>::render(&wallaby::#interaction::new(&self.#ident), #name.to_string(), self, session)),)
    });
    let interactions_actions = interactions.iter().map(|interaction| {
        let ident = interaction.ident.as_ref().expect("Missing interaction ident. Screen derive macro is not supported on tuples.");
        let name = LitStr::new(&interaction.rename.clone().unwrap_or_else(||ident.to_string()), Span::call_site());
        let interaction = interaction.interaction_type.ident();
        quote!(
            wallaby::Interaction::<Self, #session>::actions(&wallaby::#interaction::new(&self.#ident))
                .into_iter()
                .map(|action| {
                    (format!("{}_{}", #name, action.action_type), wallaby::RenderedAction::new(#name.to_string(), action.action_type, action.extra_fields))
                })
                .collect::<Vec<_>>(),
        )
    });

    let subscreens: Vec<_> = fields
        .iter()
        .filter_map(|child| match child {
            Child::Subscreen(subscreen) => Some((
                subscreen.ident.as_ref().expect(
                    "Missing subscreen ident. Screen derive macro is not supported on tuples.",
                ),
                subscreen.prefix.as_ref().map_or_else(
                    || LitStr::new("", Span::call_site()),
                    |prefix| LitStr::new(prefix, Span::call_site()),
                ),
            )),
            _ => None,
        })
        .collect();
    let subscreens_render = subscreens.iter().map(|(ident, prefix)| {
        quote!(
            let subscreen = self.#ident.render(session);
            subscreen.interactions.into_iter()
                .for_each(|(name, mut interaction)| {
                    interactions.insert(format!("{}{}", #prefix, name), interaction);
                });
            subscreen.actions.into_iter()
                .for_each(|(name, mut action)| {
                    action.add_prefix(#prefix);
                    actions.insert(format!("{}{}", #prefix, name), action);
                });
        )
    });
    let subscreens_action = subscreens.iter().map(|(ident, prefix)| {
        quote!(
            if let Some(payload) = payload.without_prefix(#prefix).as_ref() {
                wallaby::log::trace!("Removed prefix of interaction: {}", #prefix);
                let res = wallaby::Screen::<#session>::action(&mut self.#ident, session, payload).await;
                if res.is_ok() {
                    return res;
                }
            }
        )
    });
    let subscreens_ident = subscreens.iter().map(|(ident, _)| ident);
    let subscreens_helo = subscreens_ident.clone();
    let subscreens_load_data = subscreens_ident;

    let tables: Vec<_> = fields
        .iter()
        .filter_map(|child| match child {
            Child::Table(table) => Some(table),
            _ => None,
        })
        .collect();
    let tables_render = tables.iter().map(|table| {
        let ident = table.ident.as_ref().expect("Missing table ident. Screen derive macro is not supported on tuples.");
        let name = LitStr::new(&ident.to_string(), Span::call_site());

        let rows = match (table.getter.as_ref(), table.data.as_ref()) {
            (Some(_), Some(_)) => panic!("Providing a getter and data at the same time is not possible!"),
            (None, None) => panic!("Please provide a getter or data attribute!"),
            (Some(getter), None) => {
                let getter = Ident::new(getter, Span::call_site());
                quote!(self.#getter(session))
            },
            (None, Some(data)) => {
                let data = Ident::new(data, Span::call_site());
                let ty = &table.ty;
                quote!(
                    self.#data.as_ref().map(|data|data.iter().map(|data| {
                        let mut row = #ty::new();
                        wallaby::SetData::set_data(&mut row, data);
                        row
                    })
                    .collect::<Vec<#ty>>())
                    .unwrap_or_default()
                )
            },
        };

        quote!(
            let rows = #rows;
            if !rows.is_empty() {
                interactions.insert(
                    #name.to_string(),
                    wallaby::RenderedTable::create(rows.iter().map(|row_screen| {
                        let mut interactions = wallaby::Screen::<#session>::render(row_screen, session).interactions;
                        interactions
                    }).collect())
                );
                if let Some(row_screen) = rows.get(0) {
                    wallaby::Screen::<#session>::render(row_screen, session).actions
                        .into_iter()
                        .for_each(|(name, mut action)| {
                            action.table = Some(#name.to_string());
                            actions.insert(format!("{}_{}", #name, name), action);
                        });
                }
            }
        )
    });
    let tables_action = tables.iter().map(|table| {
        let ident = table.ident.as_ref().expect("Missing table ident. Screen derive macro is not supported on tuples.");
        let name = LitStr::new(&ident.to_string(), Span::call_site());

        let rows = match (table.getter.as_ref(), table.data.as_ref()) {
            (Some(_), Some(_)) => panic!("Providing a getter and data at the same time is not possible!"),
            (None, None) => panic!("Please provide a getter or data attribute!"),
            (Some(getter), None) => {
                let getter = Ident::new(getter, Span::call_site());
                quote!(self.#getter(session))
            },
            (None, Some(data)) => {
                let data = Ident::new(data, Span::call_site());
                let ty = &table.ty;
                quote!(
                    self.#data.as_ref().map(|data|data.iter().map(|data| {
                        let mut row = #ty::new();
                        wallaby::SetData::set_data(&mut row, data);
                        row
                    })
                    .collect::<Vec<#ty>>())
                    .unwrap_or_default()
                )
            },
        };

        quote!(
            if let (Some(table), Some(row)) = (payload.table(), payload.row()) {
                if table.as_str() == #name {
                    wallaby::Screen::<#session>::load_data(self, session).await?;
                    let name = self.name();
                    if let Some(mut row_screen) = #rows.drain(..).nth(row) {
                        wallaby::log::trace!(
                            "Running action for interaction {:?} on table: {} row: {} screen: {} session: {:#?}",
                            payload.interaction_name(),
                            table,
                            row,
                            name,
                            session
                        );
                        let res = wallaby::Screen::<#session>::action(&mut row_screen, session, payload).await;
                        if res.is_ok() {
                            return res;
                        }
                    } else {
                        let _ = &self.#ident; // prevent warning that field is never used
                    }
                }
            }
        )
    });

    quote!(
        #[async_trait::async_trait]
        impl wallaby::Screen<#session> for #ident {
            fn name(&self) -> &str {
                #name
            }

            fn ui(&self) -> &str {
                #ui
            }

            async fn helo(&mut self, session: &mut #session, payload: &wallaby::HeloPayload) -> Result<Vec<wallaby::Event>, wallaby::Error> {
                wallaby::log::trace!("Calling helo on {} with session: {:#?}", #name, session);
                #[allow(clippy::eval_order_dependence)]
                let events = vec![
                        #(wallaby::Screen::<#session>::helo(&mut self.#subscreens_helo, session, payload).await?,)*
                        #helo?
                    ]
                    .into_iter()
                    .flatten()
                    .collect();
                wallaby::log::trace!("Called helo on {} with session: {:#?} - Events: {:#?}", #name, session, events);
                Ok(events)
            }

            async fn load_data(&mut self, session: &#session) -> Result<(), wallaby::Error> {
                wallaby::log::trace!("Calling load_data on {} with session: {:#?}", #name, session);
                #(wallaby::Screen::<#session>::load_data(&mut self.#subscreens_load_data, session).await?;)*
                #load_data?;
                wallaby::log::trace!("Called load_data on {} with session: {:#?}", #name, session);
                Ok(())
            }

            fn render(&self, session: &#session) -> wallaby::RenderedScreen {
                wallaby::log::trace!("Rendering {}", self.name());

                let mut interactions: std::collections::HashMap<_, _> = std::collections::HashMap::new();
                let mut actions: std::collections::HashMap<_, _> = std::collections::HashMap::new();
                #(#subscreens_render)* // fills interactions/actions
                #(#tables_render)* // fills interactions/actions
                vec![#(#interactions_render)*].into_iter().for_each(|(name, interaction)| {
                    interactions.insert(name, interaction);
                });
                let screen_actions: Vec<Vec<(String, wallaby::RenderedAction)>> = vec![#(#interactions_actions)*];
                screen_actions.into_iter().flatten().for_each(|(name, action)| {
                    actions.insert(name, action);
                });

                let rendered = wallaby::RenderedScreen {
                    name: self.name().to_string(),
                    ui: wallaby::Ui::new(self.ui().to_string()),
                    query_params: #query_params,
                    interactions,
                    actions
                };
                wallaby::log::trace!("Rendered {}: {:#?}", self.name(), rendered);
                rendered
            }

            async fn action(
                &mut self,
                session: &mut #session,
                payload: &wallaby::ActionPayload,
            ) -> Result<Vec<wallaby::Event>, wallaby::Error> {
                if let Some(interaction_name) = payload.interaction_name().as_ref() {
                    wallaby::log::trace!(
                        "Running action for interaction {} on {} with session: {:#?}",
                        interaction_name,
                        self.name(),
                        session
                    );
                    let events = match interaction_name.as_str() {
                        #(#interactions_action)*
                        _ => Err(wallaby::Error::InteractionNotFound(interaction_name.to_string())),
                    };
                    wallaby::log::trace!(
                        "Ran action for interaction {} on {} with session: {:#?} - Events: {:#?}",
                        interaction_name,
                        self.name(),
                        session,
                        events
                    );

                    if events.is_ok() {
                        return events;
                    }

                    #(#subscreens_action)* // returns if a subscreen can handle the action
                    #(#tables_action)* // returns if a table can handle the action

                    return events;
                }

                return Ok(vec![])
            }
        }
    )
}
