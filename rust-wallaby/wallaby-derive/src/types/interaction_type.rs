use darling::FromMeta;
use proc_macro2::Span;
use syn::Ident;

#[derive(Debug, FromMeta, Default)]
pub enum InteractionType {
    Output,
    Button,
    Input,
    Container,
    Selection,
    #[default]
    Generic,
}

impl InteractionType {
    pub fn ident(&self) -> Ident {
        Ident::new(&format!("{:?}Interaction", self), Span::call_site())
    }
}
