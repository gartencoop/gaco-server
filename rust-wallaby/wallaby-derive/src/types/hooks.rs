use darling::FromMeta;
use proc_macro2::TokenStream;
use quote::quote;

#[derive(Debug, Default, FromMeta)]
#[darling(default)]
pub struct Hooks {
    pub helo: bool,
    pub load_data: bool,
    pub query_params: bool,
}

impl Hooks {
    pub fn helo(&self) -> TokenStream {
        if self.helo {
            quote!(wallaby::Helo::helo(self, session, payload).await)
        } else {
            quote!(Ok(vec![]))
        }
    }

    pub fn load_data(&self) -> TokenStream {
        if self.load_data {
            quote!(wallaby::LoadData::load_data(self, session).await)
        } else {
            quote!(Ok(()))
        }
    }

    pub fn query_params(&self) -> TokenStream {
        if self.query_params {
            quote!(wallaby::QueryParams::query_params(self, session))
        } else {
            quote!(None)
        }
    }
}
