use darling::FromField;
use syn::Ident;

#[derive(FromField, Debug)]
#[darling(attributes(table))]
pub struct Table {
    pub ident: Option<Ident>,
    pub ty: syn::Type,

    #[darling(default)]
    pub getter: Option<String>,

    #[darling(default)]
    pub data: Option<String>,
}
