use darling::FromField;
use syn::Ident;

#[derive(FromField, Debug)]
#[darling(attributes(screen))]
pub struct Subscreen {
    pub ident: Option<Ident>,
    pub ty: syn::Type,

    #[darling(default)]
    pub prefix: Option<String>,

    #[darling(default)]
    pub configuration: Option<String>,
}
