use super::{child::Child, hooks::Hooks};
use darling::{ast, FromDeriveInput};
use syn::Ident;

#[derive(FromDeriveInput, Debug)]
#[darling(attributes(screen), supports(struct_any))]
pub struct Screen {
    pub ident: Ident,

    #[darling(default)]
    pub name: Option<String>,

    #[darling(default)]
    pub ui: Option<String>,
    pub session: Ident,
    pub data: ast::Data<(), Child>,

    #[darling(default)]
    pub hooks: Hooks,
}

impl Screen {
    pub fn fields(&self) -> Vec<&Child> {
        self.data
            .as_ref()
            .take_struct()
            .expect("Should never be enum")
            .fields
    }
}
