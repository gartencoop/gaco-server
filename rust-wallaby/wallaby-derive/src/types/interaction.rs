use super::interaction_type::InteractionType;
use darling::FromField;
use syn::Ident;

#[derive(FromField, Debug)]
#[darling(attributes(interaction))]
pub struct Interaction {
    pub ident: Option<Ident>,
    pub ty: syn::Type,

    #[darling(rename = "ty", default)]
    pub interaction_type: InteractionType,

    #[darling(default)]
    pub configuration: Option<String>,

    #[darling(default)]
    pub default: bool,

    #[darling(default)]
    pub rename: Option<String>,
}
