use super::{interaction::Interaction, subscreen::Subscreen, table::Table};
use darling::FromField;
use syn::Ident;

#[derive(Debug)]
pub enum Child {
    Interaction(Interaction),
    Subscreen(Subscreen),
    Table(Table),
    Data(Ident),
}

impl FromField for Child {
    fn from_field(field: &syn::Field) -> darling::Result<Self> {
        Ok(field
            .attrs
            .iter()
            .find_map(|attr| match attr.path().clone() {
                path if path.is_ident("interaction") => Interaction::from_field(field)
                    .map(Child::Interaction)
                    .map_err(|err| {
                        eprintln!("Error parsing interaction: {:?} - {:?}", attr.path(), err);
                    })
                    .ok(),
                path if path.is_ident("screen") => Subscreen::from_field(field)
                    .map(Child::Subscreen)
                    .map_err(|err| {
                        eprintln!("Error parsing subscreen: {:?} - {:?}", attr.path(), err);
                    })
                    .ok(),
                path if path.is_ident("table") => Table::from_field(field)
                    .map(Child::Table)
                    .map_err(|err| eprintln!("Error parsing table: {:?} - {:?}", attr.path(), err))
                    .ok(),
                _ => None,
            })
            .unwrap_or_else(|| {
                Child::Data(
                    field.ident.clone().expect(
                        "Missing field ident. Screen derive macro is not supported on tuples.",
                    ),
                )
            }))
    }
}
