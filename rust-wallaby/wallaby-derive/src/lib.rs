// Forbid warnings in release builds:
#![cfg_attr(not(debug_assertions), deny(warnings))]
#![forbid(unsafe_code)]
// needed for darling
#![allow(clippy::manual_unwrap_or_default)]

mod generator;
mod types;

use darling::FromDeriveInput;
use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, DeriveInput};
use types::screen::Screen;

#[proc_macro_derive(Screen, attributes(screen, interaction, table))]
pub fn derive_screen(input: TokenStream) -> TokenStream {
    let derive_input = parse_macro_input!(input as DeriveInput);
    let screen = match Screen::from_derive_input(&derive_input) {
        Ok(val) => val,
        Err(err) => {
            return err.write_errors().into();
        }
    };

    let implementation = generator::implementation::create(&screen);
    let constructor = generator::constructor::create(&screen);

    quote!(
        #implementation
        #constructor
    )
    .into()
}
