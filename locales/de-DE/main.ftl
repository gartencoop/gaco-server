login_failed = Anmeldung fehlgeschlagen!
login = Anmelden
logout = Abmelden
access_denied_title = Zugriff verweigert
access_denied_text = Leider bist du momentan nicht für den Zugriff auf die angeforderte Seite autorisiert.
access_denied_login = Bitte melde dich an um die Inhalte zu sehen.
contact_message_send = Deine Nachricht wurde eingereicht. Vielen Dank 😊
contact_message_error = Fehler beim einreichen der Nachricht. Bitte melde dich bei rene@freshx.de
culture = de
mailing_address = Postanschrift
mailing_address_line_0 = Gartencoop Freiburg e.V.
mailing_address_line_1 = Adlerstr. 12
mailing_address_line_2 = 79098 Freiburg im Breisgau
farmstead = Hofstelle
farmstead_line_0 = Grünzeug GmbH
farmstead_line_1 = Germanweg 8a
farmstead_line_2 = 79189 Tunsel
bank_details = Bankverbindung
bank_details_line_0 = Gartencoop Freiburg e.V.
bank_details_line_1 = IBAN: DE11 4306 0967 7911 9097 01
bank_details_line_2 = BIC: GENODEM1GLS
bank_details_line_3 = Bank: GLS Bank Bochum
contact = Kontaktdaten
contact_line_0 = Telefon: +49 761 2047 614
contact_line_1 = E-mail Allgemein: info@gartencoop.org
contact_line_2 = E-mail Neumitglieder: buero@gartencoop.org
contact_form_title = Fragen, Feedback, Vorschläge?
contact_form_name = Name
contact_form_email = E-Mail Adresse
contact_form_message = Nachricht
contact_form_send = Senden!
impress = Impressum
impress_title = Impressum und Haftungsausschluss
impress_text = <p>Impressum nach §5 Telemediengesetz</p><p>Gartencoop Freiburg e.V.</p><p>Adlerstr. 12</p><p>79098 Freiburg i.Br.</p><p>Vorstand: Maria Luisa Werne</p><p>email: <a href="mailto:info@gartencoop.org" rel="nofollow">info@gartencoop.org</a></p><p>eingetragen ins Vereinsregister: Registergericht Freiburg</p><p>&nbsp;</p><p>Haftungsausschluss / Disclaimer</p><p>1. Inhalte des Onlineangebotes</p><p>Die Gartencoop Freiburg e.V. übernimmt keinerlei Gewähr für die Aktualität, Richtigkeit und Vollständigkeit der bereitgestellten Informationen auf unserer Website. Haftungsansprüche gegen die Gartencoop Freiburg e.V., welche sich auf Schäden materieller oder ideeller Art beziehen, die durch die Nutzung oder Nichtnutzung der dargebotenen Informationen bzw. durch die Nutzung fehlerhafter und unvollständiger Informationen verursacht wurden, sind grundsätzlich ausgeschlossen, sofern seitens des Autors kein nachweislich vorsätzliches oder grob fahrlässiges Verschulden vorliegt. Alle Angebote sind freibleibend und unverbindlich. Die Gartencoop Freiburg e.V. behält es sich ausdrücklich vor, Teile der Seiten oder das gesamte Angebot ohne gesonderte Ankündigung zu verändern, zu ergänzen, zu löschen oder die Veröffentlichung zeitweise oder endgültig einzustellen.</p><p>2. Verweise und Links</p><p>Bei direkten oder indirekten Verweisen auf fremde Webseiten (“Hyperlinks”), die außerhalb des Verantwortungsbereiches der Gartencoop e.V. Freiburg, liegen, würde eine Haftungsverpflichtung ausschließlich in dem Fall in Kraft treten, in dem die Gartencoop Freiburg e.V. von den Inhalten Kenntnis hat und es technisch möglich und zumutbar wäre, die Nutzung im Falle rechtswidriger Inhalte zu verhindern. Die Gartencoop Freiburg e.V. erklärt hiermit ausdrücklich, dass zum Zeitpunkt der Linksetzung keine illegalen Inhalte auf den zu verlinkenden Seiten erkennbar waren. Auf die aktuelle und zukünftige Gestaltung, die Inhalte oder die Urheberschaft der verlinkten/verknüpften Seiten hat die Gartencoop Freiburg e.V. keinerlei Einfluss. Deshalb distanziert sie sich hiermit ausdrücklich von allen Inhalten aller verlinkten /verknüpften Seiten, die nach der Linksetzung verändert wurden. Diese Feststellung gilt für alle innerhalb des eigenen Internetangebotes gesetzten Links und Verweise sowie für Fremdeinträge in eingerichteten Gästebüchern, Diskussionsforen, Linkverzeichnissen, Mailinglisten und in allen anderen Formen von Datenbanken, auf deren Inhalt externe Schreibzugriffe möglich sind. Für illegale, fehlerhafte oder unvollständige Inhalte und insbesondere für Schäden, die aus der Nutzung oder Nichtnutzung solcherart dargebotener Informationen entstehen, haftet allein der Anbieter der Seite, auf welche verwiesen wurde, nicht derjenige, der über Links auf die jeweilige Veröffentlichung lediglich verweist.</p><p>3. Urheber- und Kennzeichenrecht</p><p>Die Gartencoop Freiburg e.V. ist bestrebt, in allen Publikationen die Urheberrechte der verwendeten Bilder, Grafiken, Tondokumente, Videosequenzen und Texte zu beachten, vorwiegend selbst erstellte Bilder, Grafiken, Tondokumente, Videosequenzen und Texte zu nutzen oder auf lizenzfreie Grafiken, Tondokumente, Videosequenzen und Texte zurückzugreifen. Alle innerhalb des Internetangebotes genannten und ggf. durch Dritte geschützten Marken- und Warenzeichen unterliegen uneingeschränkt den Bestimmungen des jeweils gültigen Kennzeichenrechts und den Besitzrechten der jeweiligen eingetragenen Eigentümer. Allein aufgrund der bloßen Nennung ist nicht der Schluss zu ziehen, dass Markenzeichen nicht durch Rechte Dritter geschützt sind! Das Copyright für veröffentlichte selbst erstellte Objekte bleibt, soweit nicht anders gekennzeichnet, allein bei der Gartencoop Freiburg e.V. . Eine Vervielfältigung oder Verwendung solcher Grafiken, Tondokumente, Videosequenzen und Texte in anderen elektronischen oder gedruckten Publikationen ist ohne ausdrückliche Zustimmung des bezeichneten Rechteinhabers/in nicht gestattet. Bei Bedarf, fragt uns gerne, wir vertreten für die meisten unserer Inhalte den Creative-Commons Gedanken!</p><p>4. Datenschutz</p><p>Wir verweisen auf unsere ausführliche <a href="/Datenschutz">Datenschutzrichtlinie</a>.</p><p>5.. Rechtswirksamkeit dieses Haftungsausschlusses</p><p>Dieser Haftungsausschluss/Disclaimer ist als Teil des Internetangebotes zu betrachten, von dem aus auf diese Seite verwiesen wurde. Sofern Teile oder einzelne Formulierungen dieses Textes der geltenden Rechtslage nicht, nicht mehr oder nicht vollständig entsprechen sollten, bleiben die übrigen Teile des Dokumentes in ihrem Inhalt und ihrer Gültigkeit davon unberührt.</p>
privacy = Datenschutz
privacy_title = Datenschutzerklärung
privacy_text = <p>Die Nutzung unserer Webseite ist in der Regel ohne Angabe personenbezogener Daten möglich. Soweit auf unseren Seiten personenbezogene Daten (beispielsweise Name, Anschrift oder E-Mail-Adressen) erhoben werden, erfolgt dies auf freiwilliger Basis. Diese Daten werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte weitergegeben.</p><p>Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich.</p><p>Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten durch Dritte zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit ausdrücklich widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-Mails, vor.</p><p>Information zum Datenschutz</p><p>Der Schutz Ihrer personenbezogenen Daten ist uns wichtig. Nach der EU-Datenschutz-Grundverordnung (DSGVO) sind wir verpflichtet, Sie darüber zu informieren, zu welchem Zweck Daten erhoben, gespeichert oder weiterleitet werden. Der Information können Sie auch entnehmen, welche Rechte Sie in puncto Datenschutz haben.</p><p>1. Verantwortlichkeit Für Die Datenverarbeitung</p><p>Verantwortlich für die Datenverarbeitung ist:</p><p>Gartencoop e.V. Freiburg</p><p>Adlerstr. 12</p><p>79098 Freiburg i.Br.</p><p>Vorstand: Maria Luisa Werne</p><p>email: <a href="mailto:info@gartencoop.org" rel="nofollow">info@gartencoop.org</a></p><p>&nbsp;</p><p>2. Zweck Der Datenverarbeitung</p><p>Die Datenverarbeitung erfolgt aufgrund gesetzlicher Vorgaben. Sie wird in dem Umfang durchgeführt, der zu Erbringung der Serviceleistungen dieser Webseiten notwendig ist.</p><p>Für Mitglieder der Gartencoop e.V. Freiburg werden diejenigen Daten verarbeitet, die zur Administration der Mitgliedschaft und der damit verbundenen Dienst-, Sach- und Serviceleistungen notwendig sind.</p><p>&nbsp;</p><p>3. Empfänger Ihrer Daten</p><p>Grundsätzlich übermitteln wir Ihre personenbezogenen Daten nicht an Dritte. Sollte in Sonderfällen eine Weitergabe notwendig werden, geschieht dies nur innerhalb des vorgesehenen gesetzlichen Rahmens und nach Ihrer Einwilligung.</p><p>&nbsp;</p><p>4. Speicherung Ihrer Daten</p><p>Wir bewahren Ihre personenbezogenen Daten nur solange auf, wie dies für die Erbringung der Serviceleistungen dieser Webseiten notwendig ist.</p><p>Für Mitglieder der Gartencoop e.V. Freiburg werden diejenigen Daten gespeichert, die zur Administration der Mitgliedschaft und der damit Verbunden Dienst-, Sach- und Serviceleistungen notwendig sind. Diese Daten werden mit Ende der Mitgliedschaft gelöscht.</p><p>&nbsp;</p><p>5. Ihre Rechte</p><p>Sie haben das Recht, über die Sie betreffenden personenbezogenen Daten Auskunft zu erhalten. Auch können Sie die Berichtigung unrichtiger Daten verlangen.</p><p>Darüber hinaus steht Ihnen unter gegebenen Voraussetzungen das Recht auf Löschung von Daten, das Recht auf Einschränkung der Datenverarbeitung sowie das Recht auf Datenübertragbarkeit zu.</p><p>Die Verarbeitung Ihrer Daten erfolgt auf Basis von gesetzlichen Regelungen. Nur in Ausnahmefällen wird Ihr ausdrückliches Einverständnis benötigt. In diesen Fällen haben Sie das Recht, die Einwilligung für die zukünftige Verarbeitung zu widerrufen.</p><p>Sie haben ferner das Recht, sich bei der zuständigen Aufsichtsbehörde für den Datenschutz zu beschweren, wenn Sie der Ansicht sind, dass die Verarbeitung Ihrer personenbezogenen Daten nicht rechtmäßig erfolgt.</p><p>Die Anschrift der für uns zuständigen Aufsichtsbehörde lautet:</p><p>Name: Der Landesbeauftragte für den Datenschutz und die Informationsfreiheit Baden-Württemberg</p><p>Anschrift: Königstraße 10a, 70173 Stuttgart</p><p>&nbsp;</p><p>6. Rechtliche Grundlagen</p><p>Rechtsgrundlage für die Verarbeitung Ihrer Daten ist Artikel 9 Absatz 2 lit. h) DSGVO in Verbindung mit Paragraf 22 Absatz 1 Nr. 1 lit. b) Bundesdatenschutzgesetz.</p><p>&nbsp;</p>
calendar_import_ics = <p>Der Gartencoop Kalender kann recht einfach importiert werden.</p><p><a href="{ $ics_url }">Kalender als .ics Datei</a></p><h2>Android</h2><p>Die App ICSx⁵ aus <a href="https://f-droid.org/en/packages/at.bitfire.icsdroid/">F-Droid</a> oder <a href="https://play.google.com/store/apps/details?id=at.bitfire.icsdroid">PlayStore</a> installieren und die URL zur ics Datei einfügen (Lange auf den Link -> Link kopieren)</p><h2>iOS</h2><p>Den Link klicken, das Betriebsystem sollte anbieten den Kalender zu abbonieren</p>
calendar_import_ics_title = Gartencoop Kalender abbonieren
vp_status_unknown = Unbekannt
vp_status_possible = Möglich
vp_status_impossible = Unmöglich
vp_status_operational = Fix
user_without_membership = Deinem Benutzer ist aktuell keine Mitgliedschaft hinterlegt. Bitte melde dich beim Büroteam: buero@gartencoop.org
bidding_success = Dein Gebot wurde gespeichert. Vielen Dank 😊
bidding_error_ended = Die Gebotabgabe ist beendet. Dein Gebot konnte nicht gespeichert werden.
bidding_error_correction = Du kannst dein Gebot nur nach oben korrigieren.
admin_bidding_remove = Gebot entfernen
admin_bidding_success = Das Gebot wurde gespeichert.
admin_bidding_remove_success = Das Gebot wurde gespeichert.
admin_bidding_error = Das Gebot konnte nicht gespeichert werden.
bidding_bid = Monatsgebot
bidding_current_bid = Dein aktuelles Gebot: { $euros }€/Monat { $euros_year }€/Jahr.
bidding_current_bid_n_shares = Dein aktuelles Gebot: { $euros }€/Monat { $euros_year }€/Jahr ( { $shares_euros }€/Monat/Anteil ).
bidding_no_bid = Du hast noch kein Gebot abgegeben.
bidding_one_share = Du beziehst einen Anteil.
bidding_half_share = Du beziehst einen halbe Anteil.
bidding_oneandhalf_shares = Du beziehst eineinhalb Anteile.
bidding_two_shares = Du beziehst zwei Anteile.
bidding_average_euros = Durchschnitt: { $euros }€/Monat/Anteil
bidding_extrapolated = Extrapoliertes Budget: { $euros }€/Jahr
bidding_shares = Gebotene Anteile: { $bidded_shares } von { $total_shares }
bidding_members = Gebotene Mitgliedschaften: { $bidded_members } von { $total_members }
bidding_title = Bieteverfahren für Budget { $year }
bidding_page_title = Solidarisch Bieten
bidding_page_subtitle = Für ein gutes Essen für alle
bidding_page_text = Auf unserer Mitgliederversammlung wollen wir jedes Jahr gemeinsam die Finanzierung für das kommende Anbaujahr sichern. Mit der Summe aller Beitrags-Gebote tragen wir gemeinsam die Kosten unserer solidarischen Landwirtschaft. <br>Wichtig ist uns hierbei, dass jede*r ihre/seine finanzielle Situation wohl überlegt einschätzt. In diesem Jahr erstmalig anhand eines neuen Verfahrens zur Richtwertermittlung, unter Einbeziehung des jeweiligen mntl. Durchschnitt-Netto-Einkommen. Hierbei kommt es darauf an, dass niemand von dem Beitrag finanziell überfordert wird und gleichzeitig in der Summe das Budget für das kommende Jahr gestemmt werden kann. Gemeinsam ermöglichen wir es der ganzen Gemeinschaft, Zugang zu solidarisch und ökologisch produziertem Gemüse zu haben.<br><br>Die Summe der Durschnitts-Netto-Einkommen beträgt hochgerechnet auf 254 Mitgliedschaften 5.440.384 € jährlich. Dem gegenüber steht ein Gartencoop-Budget für 2025 von 428.700€, das wir gemeinsam aufbringen müssen. Daraus errechnet sich der diesjährige Richtwert von 8% eures monatlichen Durchschnitt-Netto-Einkommens.<br><br>Wer sich nicht an der Einkommensabfrage beteiligt hat, findet die Infos dazu, wie das Durchschnitt-Netto-Einkommen berechnet wird, hier: <a href="https://www.gartencoop.org/tunsel/node/7567">https://www.gartencoop.org/tunsel/node/7567</a> (in der PDF "Flyer").<br><br>Wenn ihr nur einen halben Anteil an Gemüse bekommt, könnt ihr den errechneten Richtwert halbieren. Wenn ihr zwei Anteile erhaltet, entsprechend verdoppeln bzw. wenn ihr 1,5 Anteile erhaltet, multipliziert ihr ihn mit 1,5 usw.<br><br>Wichtig ist: Ihr entscheidet grundsätzlich nach eigener Einschätzung und anhand eurer individuellen Möglichkeiten. Der Richtwert bietet euch hierzu Orientierungshilfe im solidarischen Bietverfahren.
menu_start = News
menu_calendar = Kalender
menu_my_events = Meine Termine
menu_calendar_import = Kalender abonieren (Anleitung für Android und iOS)
menu_my_membership = Meine Mitgliedschaft
menu_my_vp = Mein VP
menu_manage_profile = Profil verwalten
menu_my_account = Mein Benutzer
menu_vps = Verteilpunkte
menu_way_to_tunsel = Anfahrt Tunsel
menu_internal_contact = Interne Kontakte
menu_handbook = Handbuch
menu_new_content = Neuen Inhalt erstellen
menu_documents = Dokumentenliste
menu_protocols = Protokolle
menu_teams = Arbeitsgruppen
menu_decisions = Beschlusssammlung
menu_trailer_rent = Anhängerausleihe
menu_ads = Kleinanzeigen
menu_recipes = Rezepte
menu_newsletters = Newsletterarchiv
menu_harvest_stats = Erntestatistik
menu_financial_status = Finanzstatus
thousand_separator = .
comma = ,
stats_waiting_list = Warteliste
stats_no_vegetables = kein Gemüse
stats_half_shares = Halbe Anteile
stats_shares = Anteile
stats_one_and_half_shares = Eineinhalb Anteile
stats_two_shares = Doppelte Anteile
stats_free_half_shares = Halbe Umsonstanteile
stats_free_shares = Umsonstanteile
stats_free_one_and_half_shares = Eineinhalb Umsonstanteile
stats_free_two_shares = Doppelte Umsonstanteile
stats_test_half_shares = Halbe Testanteile
stats_test_shares = Testanteile
stats_test_one_and_half_shares = Eineinhalb Testanteile
stats_test_two_shares = Doppelte Testanteile
access_level_guest = Gast
access_level_user = Benutzer
access_level_member = Mitglied
access_level_manager = Manager_in
access_level_admin = Administrator_in
invitation_validity = Der Link ist gültig bis zum { $date }.
invitation_already = Du bist bereits teil der Mitgliedschaft { $name }.
invitation_new = Du wurdest eingeladen der Mitgliedschaft { $name } beizutreten.
 Möchtest du die Einladung annehmen?
invitation_switch = Du wurdest eingeladen der Mitgliedschaft { $name } beizutreten.
 Allerdings wirst du dann von deiner bisherigen Mitglieschaft { $old_name } ausgetragen!
 Möchtest du die Einladung annehmen?
invitation_yes = Ja
invitation_no = Nein
invitation_invalid = Dein Einladungslink ist nicht gültig!
invitation_error = Beim hinzufügen zur Mitgliedschaft { $name } ist was schiefgegangen:
 { $error }
invitation_success = Du wurdest erfolgreich der Mitgliedschaft { $name } hinzugefügt!
mail_income_subject = GaCo-Bietverfahren 2024 - Schritt 1: Einkommens- und Ackereinsätzeabgabe
mail_income_body = //English version below//
 
 Liebes Gartencoop-Mitglied,
 
 du hast es vermutlich schon mitbekommen: ein neues Bietverfahren wird in der GaCo eingeführt.
 
 Der erste Schritt des Bietverfahrens startet mit dieser Mail: Ihr findet hier einen personalisierten Link, unter dem ihr anonym euer ermitteltes „Durchschnitts-Haushalts-Netto-Einkommen“ abgeben sollt.
 Der nächste Schritt folgt dann Anfang November - wie jedes Jahr erfolgt der Aufruf, euer Gebot für das kommende Anbaujahr abzugeben. Die Gebotsabgabe erfolgt anhand des neuen Richtwerts, der ein prozentualer Anteil des eigenen Netto-Einkommens ist.
 
 Zusätzlich bekommt ihr als Orientierungshilfe den Überblick über die Einkommens-Situation/-Verteilung der Mitgliedschaften - natürlich anonym - und eine Reflektionsskala, mit der ihr noch weitere Faktoren bei der Festlegung eures Gebots mitberücksichtigen könnt.
 
 Und wie errechnet ihr eigentlich diesen Einkommens-Wert?
 Ausführliche Infos dazu und ein Erklär-Video findet ihr auch unter dem Link:
 
 { $link }
 
 Also, schnappt euch eure Mitgliedschafts-MitMenschen, den Taschenrechner und legt los!
 
 Neben dem Netto-Einkommen eurer Mitgliedschaft fragen wir euch unter dem genannten Link auch nach der Anzahl der Ackereinsätze, zu denen ihr im Jahr 2025 gehen wollt. Mit Hilfe dieser Angabe können die Gärtner:innen besser planen, mit wie viel Unterstützung sie auf dem Acker rechnen können.
 Überlegt euch also in eurer Mitgliedschaft, wie oft ihr nächstes Jahr realistischerweise nach Tunsel kommen könnt - und notiert euch auch diese Zahl, als kleine Erinnerung, um euch dann auch tatsächlich zu den Einsätzen anzumelden.
 Bitte gebt nur die Anzahl der Ackereinsätze an und nicht andere Unterstützungen, die ihr plant, wie z.B. Fahrradverteilung, Mitarbeit in einer AG etc. (da die Angabe nur dann aussagekräftig für das A-Team ist).
 
 
 Liebe Grüße,
 eure Bietverfahren-AG
 bei Fragen meldet euch gern:
 bietverfahren@gartencoop.org
 
 --
 
 Dear member of the Gartencoop,
 
 The Gartencoop follows a pay-what-you-can principle, where each year, members make an offer for their monthly payments.
 As you may have heard or read there will be a new procedure on how these contributions are offered.
 In short: as before an orientation value will be communicated to all members to decide how much they want to give monthly - however it will no longer be the same reference value for all, instead it will be based on the income of each membership.
 
 The first step of the new procedure consists in collecting anynmous data about the income of the members. Therefor you are asked to enter the average net income of your household, following this link:
 
 { $link }
 
 What exactly is this average net income of a household? When you click on the link, you will find all information and a video-tutorial.
 
 And what comes next?
 Begin of november: As every year, you will be asked to decide about your monthly contribution for the next season. Only the reference value will be a % of your net income and not a fixed € prize.
 
 
 Query on farm work shifts for 2025
 
 In the query we'll also ask you: In how many farm work shifts are you planning to take part in 2025?
 
 This will help the gardeners plan how much assistance they are likely to get. Please therefor only count farm work shifts, not other shifts such as bicycle shifts, group work, etc.
 
 
 If you have any questions, contact us: bietverfahren@gartencoop.org
 
 
 Best regards,
 the Bietverfahren-AG
password_forgot_email_subject = Passwort zurücksetzen
password_forgot_email_body = Hallo,
 
 Du hast eine Anfrage zum Zurücksetzen deines Passworts gestellt. Klicke auf den folgenden Link, um dein Passwort zurückzusetzen:
 
 https://www.gartencoop.org/activate_user_{ $user }_{ $token }
 
 Wenn du diese Anfrage nicht gestellt hast, kannst du diese E-Mail einfach ignorieren.

 Viele Grüße,
 Dein GaCo-Team
new_user_email_subject = Neue Benutzer aktivieren
new_user_email_body = Hallo,
 
 Du hast einen neuen Benutzer auf der Gartencoop Webseite angelegt. Um den Benutzer zu aktivieren, besuche bitte diesen Link:

 https://www.gartencoop.org/activate_user_{ $user }_{ $token }
 
 Wenn du keinen Benutzer erstellt, dann kannst du diese E-Mail einfach ignorieren.

 Viele Grüße,
 Dein GaCo-Team
password_changed = Dein Passwort wurde geändert
language_changed = Deine Sprache wurde geändert
change_profile_header = Änderungen am Profil vornehmen
change_profile_user = Angemeldet als
change_profile_change_language = Sprache ändern
change_profile_language = Sprache
change_profile_change_password = Passwort ändern
change_profile_password = Neues Passwort
change_profile_password2 = Wiederholung neues Passwort
change_profile_passwords_not_matching = Die Passwörter stimmen nicht überein
change_profile_link_not_valid = Dein Link ist nicht gültig, bitte fordere einen neuen Link an.
change_profile_create_new_link = Neuen Link anfordern
change_profile_password_too_short = Das Passwort ist zu kurz, bitte nutze Mindestens 8 Zeichen
login_username = Benutzername oder email
login_password = Passwort
login_register = Neuen Benutzer anlegen
login_forgot_password = Passwort vergessen
membership_id = Mitgliedsnummer
membership_main_person = Hauptperson
membership_invoice_help = Bei Überweisungen bitte immer "GaCo{ $id }" als Betreff nutzen.
membership_contact = Kontakt
membership_change_my_date = Meine Daten ändern
membership_membership = Mitgliedschaft
membership_created_at = erstellt am
membership_started_at = gestartet am
membership_end_at = endet am
membership_type = Mitgliedstyp
membership_shares = Anteile
membership_current_bid = Aktuelles Gebot: { $bid }€ im Monat
membership_distribution_point = VP
membership_members = Mitessende
membership_add_member = Mitessende einladen
membership_copy_invitation_link = Einladungslink kopieren
membership_link_copied_to_clipboard = Der Link wurde in die Zwischenablage kopiert
membership_name = Name
membership_surname = Nachname
membership_street = Straße
membership_zip = PLZ
membership_city = Stadt
membership_discard = verwerfen
membership_save = speichern
membership_type_waiting_list = Warteliste
membership_type_no_vegetables = Passiv
membership_type_active = Aktiv
membership_type_test_membership = Probe
membership_type_team = Team
membership_type_free_vegetables = Umsonstanteil
membership_type_resigned = Ausgetreten
forgot_password_header = Passwort zurücksetzen
forgot_password_text = Du bekommst eine Email mit Anweisungen an die Adresse, die du bei deiner Anmeldung verwendest.
forgot_password_button = Absenden
vp_title = Verteilpunkt: { $title }
vp_opening_hours = Öffnungszeiten
vp_code = Code
vp_members = Mitglieder
vp_waiting_members = Mitglieder auf der Warteliste
vp_contact = Kontakt
vp_case_storage = Ladefläche
vp_created_at = Erstellt am 
vp_status = Status
vp_space = Fläche
vp_infos = Informationen
vp_name = Name
vps_title = Übersicht unserer Verteilpunkte
register_user_title = Neues Konto anlegen
register_user_description = Du bekommst nach dem Registieren eine E-Mail mit Anweisungen, wie du ein Passwort festlegen kannst.
register_user_username = Benutzername
register_user_name = Name
register_user_phone = Telefonnummer
register_user_email = Email Adresse
register_user_submit = Absenden
register_user_username_exists = Es gibt schon ein Konto mit deinem Benutzernamen
register_user_email_exists = Es gibt schon ein Konto mit deiner Email Adresse
search_type = Was suchst du?
search_type_all = Egal
search_type_documents = Dokumente
search_type_events = Termine
search_input = Suche
search_event_category = Kategorie
search_document_team = Team
search_document_topic = Thema