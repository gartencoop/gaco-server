login_failed = Login failed!
login = login
logout = logout
access_denied_title = Access denied
access_denied_text = You are currently not authorized to access the requested page
access_denied_login = Please log in to view the content.
contact_message_send = Your message was submitted. Thank you 😊
contact_message_error = Error submitting message. Please contact rene@freshx.de
culture = en
mailing_address = Mailing Address
mailing_address_line_0 = Gartencoop Freiburg e.V.
mailing_address_line_1 = Adlerstr. 12
mailing_address_line_2 = 79098 Freiburg im Breisgau
farmstead = Farmstead
farmstead_line_0 = Grünzeug GmbH
farmstead_line_1 = Germanweg 8a
farmstead_line_2 = 79189 Tunsel
bank_details = Bank Details
bank_details_line_0 = Gartencoop Freiburg e.V.
bank_details_line_1 = IBAN: DE11 4306 0967 7911 9097 01
bank_details_line_2 = BIC: GENODEM1GLS
bank_details_line_3 = Bank: GLS Bank Bochum
contact = Contact
contact_line_0 = Phone: +49 761 2047 614
contact_line_1 = E-mail general: info@gartencoop.org
contact_line_2 = E-mail new members: buero@gartencoop.org
contact_form_title = Questions, Feedback, Suggestions?
contact_form_name = name
contact_form_email = email address
contact_form_message = message
contact_form_send = Send!
impress = Impress
impress_title = Impress/Disclaimer
impress_text = <p>Impressum nach §5 Telemediengesetz</p><p>Gartencoop Freiburg e.V.</p><p>Adlerstr. 12</p><p>79098 Freiburg i.Br.</p><p>Vorstand: Maria Luisa Werne</p><p>email: <a href="mailto:info@gartencoop.org" rel="nofollow">info@gartencoop.org</a></p><p>eingetragen ins Vereinsregister: Registergericht Freiburg</p><p>&nbsp;</p><p>Haftungsausschluss / Disclaimer</p><p>1. Inhalte des Onlineangebotes</p><p>Die Gartencoop Freiburg e.V. übernimmt keinerlei Gewähr für die Aktualität, Richtigkeit und Vollständigkeit der bereitgestellten Informationen auf unserer Website. Haftungsansprüche gegen die Gartencoop Freiburg e.V., welche sich auf Schäden materieller oder ideeller Art beziehen, die durch die Nutzung oder Nichtnutzung der dargebotenen Informationen bzw. durch die Nutzung fehlerhafter und unvollständiger Informationen verursacht wurden, sind grundsätzlich ausgeschlossen, sofern seitens des Autors kein nachweislich vorsätzliches oder grob fahrlässiges Verschulden vorliegt. Alle Angebote sind freibleibend und unverbindlich. Die Gartencoop Freiburg e.V. behält es sich ausdrücklich vor, Teile der Seiten oder das gesamte Angebot ohne gesonderte Ankündigung zu verändern, zu ergänzen, zu löschen oder die Veröffentlichung zeitweise oder endgültig einzustellen.</p><p>2. Verweise und Links</p><p>Bei direkten oder indirekten Verweisen auf fremde Webseiten (“Hyperlinks”), die außerhalb des Verantwortungsbereiches der Gartencoop e.V. Freiburg, liegen, würde eine Haftungsverpflichtung ausschließlich in dem Fall in Kraft treten, in dem die Gartencoop Freiburg e.V. von den Inhalten Kenntnis hat und es technisch möglich und zumutbar wäre, die Nutzung im Falle rechtswidriger Inhalte zu verhindern. Die Gartencoop Freiburg e.V. erklärt hiermit ausdrücklich, dass zum Zeitpunkt der Linksetzung keine illegalen Inhalte auf den zu verlinkenden Seiten erkennbar waren. Auf die aktuelle und zukünftige Gestaltung, die Inhalte oder die Urheberschaft der verlinkten/verknüpften Seiten hat die Gartencoop Freiburg e.V. keinerlei Einfluss. Deshalb distanziert sie sich hiermit ausdrücklich von allen Inhalten aller verlinkten /verknüpften Seiten, die nach der Linksetzung verändert wurden. Diese Feststellung gilt für alle innerhalb des eigenen Internetangebotes gesetzten Links und Verweise sowie für Fremdeinträge in eingerichteten Gästebüchern, Diskussionsforen, Linkverzeichnissen, Mailinglisten und in allen anderen Formen von Datenbanken, auf deren Inhalt externe Schreibzugriffe möglich sind. Für illegale, fehlerhafte oder unvollständige Inhalte und insbesondere für Schäden, die aus der Nutzung oder Nichtnutzung solcherart dargebotener Informationen entstehen, haftet allein der Anbieter der Seite, auf welche verwiesen wurde, nicht derjenige, der über Links auf die jeweilige Veröffentlichung lediglich verweist.</p><p>3. Urheber- und Kennzeichenrecht</p><p>Die Gartencoop Freiburg e.V. ist bestrebt, in allen Publikationen die Urheberrechte der verwendeten Bilder, Grafiken, Tondokumente, Videosequenzen und Texte zu beachten, vorwiegend selbst erstellte Bilder, Grafiken, Tondokumente, Videosequenzen und Texte zu nutzen oder auf lizenzfreie Grafiken, Tondokumente, Videosequenzen und Texte zurückzugreifen. Alle innerhalb des Internetangebotes genannten und ggf. durch Dritte geschützten Marken- und Warenzeichen unterliegen uneingeschränkt den Bestimmungen des jeweils gültigen Kennzeichenrechts und den Besitzrechten der jeweiligen eingetragenen Eigentümer. Allein aufgrund der bloßen Nennung ist nicht der Schluss zu ziehen, dass Markenzeichen nicht durch Rechte Dritter geschützt sind! Das Copyright für veröffentlichte selbst erstellte Objekte bleibt, soweit nicht anders gekennzeichnet, allein bei der Gartencoop Freiburg e.V. . Eine Vervielfältigung oder Verwendung solcher Grafiken, Tondokumente, Videosequenzen und Texte in anderen elektronischen oder gedruckten Publikationen ist ohne ausdrückliche Zustimmung des bezeichneten Rechteinhabers/in nicht gestattet. Bei Bedarf, fragt uns gerne, wir vertreten für die meisten unserer Inhalte den Creative-Commons Gedanken!</p><p>4. Datenschutz</p><p>Wir verweisen auf unsere ausführliche <a href="/Datenschutz">Datenschutzrichtlinie</a>.</p><p>5.. Rechtswirksamkeit dieses Haftungsausschlusses</p><p>Dieser Haftungsausschluss/Disclaimer ist als Teil des Internetangebotes zu betrachten, von dem aus auf diese Seite verwiesen wurde. Sofern Teile oder einzelne Formulierungen dieses Textes der geltenden Rechtslage nicht, nicht mehr oder nicht vollständig entsprechen sollten, bleiben die übrigen Teile des Dokumentes in ihrem Inhalt und ihrer Gültigkeit davon unberührt.</p>
privacy = Privacy
privacy_title = Privacy
privacy_text = <p>Die Nutzung unserer Webseite ist in der Regel ohne Angabe personenbezogener Daten möglich. Soweit auf unseren Seiten personenbezogene Daten (beispielsweise Name, Anschrift oder E-Mail-Adressen) erhoben werden, erfolgt dies auf freiwilliger Basis. Diese Daten werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte weitergegeben.</p><p>Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich.</p><p>Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten durch Dritte zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit ausdrücklich widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-Mails, vor.</p><p>Information zum Datenschutz</p><p>Der Schutz Ihrer personenbezogenen Daten ist uns wichtig. Nach der EU-Datenschutz-Grundverordnung (DSGVO) sind wir verpflichtet, Sie darüber zu informieren, zu welchem Zweck Daten erhoben, gespeichert oder weiterleitet werden. Der Information können Sie auch entnehmen, welche Rechte Sie in puncto Datenschutz haben.</p><p>1. Verantwortlichkeit Für Die Datenverarbeitung</p><p>Verantwortlich für die Datenverarbeitung ist:</p><p>Gartencoop e.V. Freiburg</p><p>Adlerstr. 12</p><p>79098 Freiburg i.Br.</p><p>Vorstand: Maria Luisa Werne</p><p>email: <a href="mailto:info@gartencoop.org" rel="nofollow">info@gartencoop.org</a></p><p>&nbsp;</p><p>2. Zweck Der Datenverarbeitung</p><p>Die Datenverarbeitung erfolgt aufgrund gesetzlicher Vorgaben. Sie wird in dem Umfang durchgeführt, der zu Erbringung der Serviceleistungen dieser Webseiten notwendig ist.</p><p>Für Mitglieder der Gartencoop e.V. Freiburg werden diejenigen Daten verarbeitet, die zur Administration der Mitgliedschaft und der damit verbundenen Dienst-, Sach- und Serviceleistungen notwendig sind.</p><p>&nbsp;</p><p>3. Empfänger Ihrer Daten</p><p>Grundsätzlich übermitteln wir Ihre personenbezogenen Daten nicht an Dritte. Sollte in Sonderfällen eine Weitergabe notwendig werden, geschieht dies nur innerhalb des vorgesehenen gesetzlichen Rahmens und nach Ihrer Einwilligung.</p><p>&nbsp;</p><p>4. Speicherung Ihrer Daten</p><p>Wir bewahren Ihre personenbezogenen Daten nur solange auf, wie dies für die Erbringung der Serviceleistungen dieser Webseiten notwendig ist.</p><p>Für Mitglieder der Gartencoop e.V. Freiburg werden diejenigen Daten gespeichert, die zur Administration der Mitgliedschaft und der damit Verbunden Dienst-, Sach- und Serviceleistungen notwendig sind. Diese Daten werden mit Ende der Mitgliedschaft gelöscht.</p><p>&nbsp;</p><p>5. Ihre Rechte</p><p>Sie haben das Recht, über die Sie betreffenden personenbezogenen Daten Auskunft zu erhalten. Auch können Sie die Berichtigung unrichtiger Daten verlangen.</p><p>Darüber hinaus steht Ihnen unter gegebenen Voraussetzungen das Recht auf Löschung von Daten, das Recht auf Einschränkung der Datenverarbeitung sowie das Recht auf Datenübertragbarkeit zu.</p><p>Die Verarbeitung Ihrer Daten erfolgt auf Basis von gesetzlichen Regelungen. Nur in Ausnahmefällen wird Ihr ausdrückliches Einverständnis benötigt. In diesen Fällen haben Sie das Recht, die Einwilligung für die zukünftige Verarbeitung zu widerrufen.</p><p>Sie haben ferner das Recht, sich bei der zuständigen Aufsichtsbehörde für den Datenschutz zu beschweren, wenn Sie der Ansicht sind, dass die Verarbeitung Ihrer personenbezogenen Daten nicht rechtmäßig erfolgt.</p><p>Die Anschrift der für uns zuständigen Aufsichtsbehörde lautet:</p><p>Name: Der Landesbeauftragte für den Datenschutz und die Informationsfreiheit Baden-Württemberg</p><p>Anschrift: Königstraße 10a, 70173 Stuttgart</p><p>&nbsp;</p><p>6. Rechtliche Grundlagen</p><p>Rechtsgrundlage für die Verarbeitung Ihrer Daten ist Artikel 9 Absatz 2 lit. h) DSGVO in Verbindung mit Paragraf 22 Absatz 1 Nr. 1 lit. b) Bundesdatenschutzgesetz.</p><p>&nbsp;</p>
calendar_import_ics = <p>The Gartencoop calendar can be imported easily.</p><p><a href="{ $ics_url }">Calendar as .ics file</a></p><h2>Android</h2><p>Install the app ICSx⁵ from <a href="https://f-droid.org/en/packages/at.bitfire.icsdroid/">F-Droid</a> or <a href="https://play.google.com/store/apps/details?id=at.bitfire.icsdroid">PlayStore</a> and import this URL (long press -> copy link)</p><h2>iOS</h2><p>Click the link, iOS should give you an option to subscribe to the calendar.</p>
calendar_import_ics_title = Subscribe to Gartencoop calendar
vp_status_unknown = Unknown
vp_status_possible = Possible
vp_status_impossible = Impossible
vp_status_operational = Operational
user_without_membership = Your user is currently not linked to a membership. Please contact the office at: buero@gartencoop.org
bidding_success = Your bid was saved. Thank you 😊
bidding_error_ended = The bidding is over, your bid could not be saved.
bidding_error_correction = You can only raise you bid.
admin_bidding_remove = Remove bid
admin_bidding_success = The bid was saved.
admin_bidding_remove_success = The bid was removed.
admin_bidding_error = The bid could not be saved.
bidding_bid = monthly bid
bidding_current_bid = Your current bid: { $euros }€/month { $euros_year }€/year.
bidding_current_bid_n_shares = Your current bid: { $euros }€/month { $euros_year }€/year ( { $shares_euros }€/month/share ).
bidding_no_bid = You did not yet bid.
bidding_one_share = You obtain one share.
bidding_half_share = You obtain a half share.
bidding_oneandhalf_shares = You obtain one and a half shares.
bidding_two_shares = You obtain two shares.
bidding_average_euros = Average: { $euros }€/month/share
bidding_extrapolated = Extrapolated budget: { $euros }€/year
bidding_shares = Bidded shares: { $bidded_shares }/{ $total_shares }
bidding_members = Bidded memberships: { $bidded_members }/{ $total_members }
bidding_title = Bidding process for budget { $year }
bidding_page_title = Bidding in solidarity
bidding_page_subtitle = Good food for everybody
bidding_page_text = At our annual member meeting, we aim to collectively secure funding for the upcoming growing season. Through the total of all contribution pledges, we share the costs of our community-supported agriculture.<br><br>It’s important to us that each person carefully assesses their own financial situation. This year, we’re introducing a new method to calculate a guideline rate, based on each member’s average monthly net income. This approach is intended to ensure that no one is financially overburdened by their contribution, while also ensuring that the total budget needed for the upcoming year can be met. Together, we make it possible for everyone in our community to have access to sustainably and ecologically produced vegetables.<br><br>The total average net income of our 254 memberships is estimated at €5,440,384 annually. In contrast, the 2025 budget for the garden cooperative is €428,700, which we need to raise together. This results in this year’s guideline rate of 8% of your average monthly net income.<br><br>For those who did not participate in the income survey, information on how the average net income is calculated can be found here: <a href="https://www.gartencoop.org/tunsel/node/7567">https://www.gartencoop.org/tunsel/node/7567</a> (see the PDF “Flyer”).<br><br>If you receive only a half share of vegetables, you can halve the calculated guideline rate. If you receive two shares, you can double it; if you receive 1.5 shares, multiply it by 1.5, and so on.<br><br>The key point is this: you decide based on your own judgment and individual financial circumstances. The guideline rate is simply intended as a reference in the cooperative’s bidding process.
menu_start = News
menu_calendar = Calendar
menu_my_events = My events
menu_calendar_import = Subscribe (Howto for Android and iOS)
menu_my_membership = My membership
menu_my_vp = My VP
menu_manage_profile = Manage profile
menu_my_account = My user
menu_vps = VPs
menu_way_to_tunsel = Way to Tunsel
menu_internal_contact = Internal contacts
menu_handbook = Handbook
menu_new_content = Create new content
menu_documents = Documents
menu_protocols = Protocols
menu_teams = Teams
menu_decisions = Decisions
menu_trailer_rent = Trailer rent
menu_ads = Ads
menu_recipes = Recipes
menu_newsletters = Newsletterarchive
menu_harvest_stats = Harvest stats
menu_financial_status = Financial status
thousand_separator = ,
comma = .
stats_waiting_list = waiting list
stats_no_vegetables = no vegetables
stats_half_shares = half shares
stats_shares = shares
stats_one_and_half_shares = 1.5 shares
stats_two_shares = two shares
stats_free_half_shares = half free shares
stats_free_shares = free shares
stats_free_one_and_half_shares = 1.5 free shares
stats_free_two_shares = two free shares
stats_test_half_shares = half test shares
stats_test_shares = test shares
stats_test_one_and_half_shares = 1.5 test shares
stats_test_two_shares = two test shares
access_level_guest = Guest
access_level_user = User
access_level_member = Member
access_level_manager = Manager
access_level_admin = Admin
invitation_validity = The link is valid until { $date }.
invitation_already = Your are already part of membership { $name }.
invitation_new = Do you want to accept the invitation to the membership{ $name }?
invitation_switch = You are invited to join the membership { $name } but you are already in the membership { $old_name }. Do you want to switch to membership { $name }?
invitation_yes = Yes
invitation_no = No
invitation_invalid = Your invitation link is invalid!
invitation_error = Error when adding you to the membership { $name }: { $error }
invitation_success = You successfully joined the membership { $name }!
mail_income_subject = GaCo-Bietverfahren 2024 - Schritt 1: Einkommens- und Ackereinsätzeabgabe
mail_income_body = //English version below//
 
 Liebes Gartencoop-Mitglied,
 
 du hast es vermutlich schon mitbekommen: ein neues Bietverfahren wird in der GaCo eingeführt.
 
 Der erste Schritt des Bietverfahrens startet mit dieser Mail: Ihr findet hier einen personalisierten Link, unter dem ihr anonym euer ermitteltes „Durchschnitts-Haushalts-Netto-Einkommen“ abgeben sollt.
 Der nächste Schritt folgt dann Anfang November - wie jedes Jahr erfolgt der Aufruf, euer Gebot für das kommende Anbaujahr abzugeben. Die Gebotsabgabe erfolgt anhand des neuen Richtwerts, der ein prozentualer Anteil des eigenen Netto-Einkommens ist.
 
 Zusätzlich bekommt ihr als Orientierungshilfe den Überblick über die Einkommens-Situation/-Verteilung der Mitgliedschaften - natürlich anonym - und eine Reflektionsskala, mit der ihr noch weitere Faktoren bei der Festlegung eures Gebots mitberücksichtigen könnt.
 
 Und wie errechnet ihr eigentlich diesen Einkommens-Wert?
 Ausführliche Infos dazu und ein Erklär-Video findet ihr auch unter dem Link:
 
 { $link }
 
 Also, schnappt euch eure Mitgliedschafts-MitMenschen, den Taschenrechner und legt los!
 
 Neben dem Netto-Einkommen eurer Mitgliedschaft fragen wir euch unter dem genannten Link auch nach der Anzahl der Ackereinsätze, zu denen ihr im Jahr 2025 gehen wollt. Mit Hilfe dieser Angabe können die Gärtner:innen besser planen, mit wie viel Unterstützung sie auf dem Acker rechnen können.
 Überlegt euch also in eurer Mitgliedschaft, wie oft ihr nächstes Jahr realistischerweise nach Tunsel kommen könnt - und notiert euch auch diese Zahl, als kleine Erinnerung, um euch dann auch tatsächlich zu den Einsätzen anzumelden.
 Bitte gebt nur die Anzahl der Ackereinsätze an und nicht andere Unterstützungen, die ihr plant, wie z.B. Fahrradverteilung, Mitarbeit in einer AG etc. (da die Angabe nur dann aussagekräftig für das A-Team ist).
 
 
 Liebe Grüße,
 eure Bietverfahren-AG
 bei Fragen meldet euch gern:
 bietverfahren@gartencoop.org
 
 --
 
 Dear member of the Gartencoop,
 
 The Gartencoop follows a pay-what-you-can principle, where each year, members make an offer for their monthly payments.
 As you may have heard or read there will be a new procedure on how these contributions are offered.
 In short: as before an orientation value will be communicated to all members to decide how much they want to give monthly - however it will no longer be the same reference value for all, instead it will be based on the income of each membership.
 
 The first step of the new procedure consists in collecting anynmous data about the income of the members. Therefor you are asked to enter the average net income of your household, following this link:
 
 { $link }
 
 What exactly is this average net income of a household? When you click on the link, you will find all information and a video-tutorial.
 
 And what comes next?
 Begin of november: As every year, you will be asked to decide about your monthly contribution for the next season. Only the reference value will be a % of your net income and not a fixed € prize.
 
 
 Query on farm work shifts for 2025
 
 In the query we'll also ask you: In how many farm work shifts are you planning to take part in 2025?
 
 This will help the gardeners plan how much assistance they are likely to get. Please therefor only count farm work shifts, not other shifts such as bicycle shifts, group work, etc.
 
 
 If you have any questions, contact us: bietverfahren@gartencoop.org
 
 
 Best regards,
 the Bietverfahren-AG
password_forgot_email_subject = Reset Password
password_forgot_email_body = Hello,
 
 You have requested to reset your password. Click on the following link to reset your password:
 
 https://www.gartencoop.org/activate_user_{ $user }_{ $token }
 
 If you did not make this request, you can simply ignore this email.

 Best regards,
 Your GaCo Team
new_user_email_subject = Activate New User
new_user_email_body = Hello,
 
 You have created a new user on the Gartencoop website. To activate the user, please visit this link:

 https://www.gartencoop.org/activate_user_{ $user }_{ $token }
 
 If you did not create a user, you can simply ignore this email.

 Best regards,
 Your GaCo Team
password_changed = Your password was changed
language_changed = Your language setting was changed
change_profile_header = Make changes to profile
change_profile_user = Logged in as
change_profile_change_language = Change language
change_profile_language = Language
change_profile_change_password = Change password
change_profile_password = New password
change_profile_password2 = Repeat new password
change_profile_passwords_not_matching = The passwords do not match
change_profile_link_not_valid = Your link is not valid, please request a new link.
change_profile_create_new_link = Request a new link
change_profile_password_too_short = The password is too short, please use minimum 8 characters
login_username = Username or email address
login_password = Password
login_register = Create new user
login_forgot_password = I forgot my password
membership_id = Membership id
membership_main_person = Main person
membership_invoice_help = Please always use "GaCo{ $id }" as the subject for transfers.
membership_contact = Contact
membership_change_my_date = Change my data
membership_membership = Membership
membership_created_at = created at
membership_started_at = started at
membership_end_at = ends at
membership_type = Type of membership
membership_shares = Shares
membership_current_bid = Current bid: { $bid }€ per month
membership_distribution_point = Distribution point
membership_members = Members
membership_add_member = Add member
membership_copy_invitation_link = Copy invitation link
membership_link_copied_to_clipboard = Link was copied to clipboard
membership_name = Name
membership_surname = Surname
membership_street = Street
membership_zip = Zip
membership_city = City
membership_discard = discard
membership_save = save
membership_type_waiting_list = Waiting List
membership_type_no_vegetables = Passiv
membership_type_active = Active
membership_type_test_membership = Test Tembership
membership_type_team = Team
membership_type_free_vegetables = Free Vegetables
membership_type_resigned = Resigned
forgot_password_header = Reset password
forgot_password_text = You will receive an email with instructions to the address you used to register.
forgot_password_button = Send
vp_title = Distribution point: { $title }
vp_opening_hours = Opening hours
vp_code = Code
vp_members = Members
vp_waiting_members = Members on waiting list
vp_contact = Contact
vp_case_storage = Loading area
vp_created_at = Created at 
vp_status = Status
vp_space = Space
vp_infos = Information
vps_title = Overview of our distribution points
register_user_title = Create new user
register_user_description = After registering, you will receive an email with instructions on how to set a password.
register_user_username = Username
register_user_name = Name
register_user_phone = Phone number
register_user_email = Email address
register_user_submit = Submit
register_user_username_exists = There is already a user with your username
register_user_email_exists = There is already a user with your email address
search_type = What are you searching for?
search_type_all = Everything
search_type_documents = Documents
search_type_events = Events
search_input = Search
search_event_category = Category
search_document_team = Team
search_document_topic = Topic